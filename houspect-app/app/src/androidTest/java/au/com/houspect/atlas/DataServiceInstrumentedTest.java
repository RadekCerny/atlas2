package au.com.houspect.atlas;

import android.content.Context;
import android.os.Looper;
import android.support.test.InstrumentationRegistry;
import android.support.test.annotation.UiThreadTest;
import android.support.test.rule.UiThreadTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.j256.ormlite.stmt.QueryBuilder;
import com.saasplications.genie.domain.model.ActivityInstance;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;

import au.com.houspect.atlas.models.CalendarItem;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.models.RecentInspection;
import au.com.houspect.atlas.services.DataService;
import au.com.houspect.atlas.services.DataServiceImpl;
import au.com.houspect.atlas.services.GenieService;
import au.com.houspect.atlas.services.GenieServiceCallback;
import au.com.houspect.atlas.services.GenieServiceImpl;
import au.com.houspect.atlas.services.GenieServiceResponse;
import au.com.houspect.atlas.services.SharedPreferencesService;
import au.com.houspect.atlas.services.SharedPreferencesServiceImpl;
import io.reactivex.Single;

import static android.R.attr.data;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class DataServiceInstrumentedTest {

    private String sessionToken;
    private ActivityInstance activityInstance;
    private DataService sut;

    @Before
    public void login() throws Exception {

        Context appContext = InstrumentationRegistry.getTargetContext();

        final CountDownLatch loginSignal = new CountDownLatch(1);
        GenieService genieService = new GenieServiceImpl();
        SharedPreferencesService sharedPreferencesService = new SharedPreferencesServiceImpl(appContext);
        genieService.login("alfredw", "T2AndM3", new GenieServiceCallback<GenieServiceResponse<String>>() {
            @Override
            public void completed(GenieServiceResponse<String> response) {

                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                assertNotEquals(0, response.getData());
                sessionToken = response.getData();
                loginSignal.countDown();
            }
        });
        loginSignal.await(7, TimeUnit.SECONDS);

        sut = new DataServiceImpl(appContext, genieService, sharedPreferencesService);
        sharedPreferencesService.saveSessionToken(sessionToken);

    }

    private void mockDrafts() {
        // mock ORMLite data
        Draft mockDraft1 = new Draft();
        mockDraft1.setCreatedByUsername("mockUsername");
        mockDraft1.setXml("<PropertyInspection code=\"mockCode1\" id=\"mockId1\"></PropertyInspection>");
        mockDraft1.setId("mockId1");

        Draft mockDraft2 = new Draft();
        mockDraft2.setXml("<PropertyInspection code=\"mockCode2\" id=\"mockId2\"></PropertyInspection>");
        mockDraft2.setCreatedByUsername("mockUsername2");
        mockDraft2.setId("mockId2");

        Draft mockDraft3 = new Draft();
        mockDraft3.setXml("<PropertyInspection code=\"mockCode2\" id=\"mockId3\"></PropertyInspection>");
        mockDraft3.setCreatedByUsername("mockUsername");
        mockDraft3.setId("mockId3");

        sut.save(mockDraft1, Draft.class);
        sut.save(mockDraft2, Draft.class);
        sut.save(mockDraft3, Draft.class);
    }

    private void cleanDrafts() {

        sut.removeAll(Draft.class);
    }

    @Test
    public void getJobs() throws Exception {

        Context appContext = InstrumentationRegistry.getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final HashMap<String,Boolean> results = new HashMap<>();
        results.put("success", false);
        sut.getJobs(appContext, new DataService.GetDataCallback<Job>() {
            @Override
            public void onCompletion(List<Job> data) {
                assertNotNull(data);
                assertTrue(data.size()>0);
                results.put("success", true);
                signal.countDown();
            }

            @Override
            public void onError(String message) {

                results.put("success", false);
                signal.countDown();
            }
        });

        signal.await(7, TimeUnit.SECONDS);
        assertTrue(results.get("success"));
    }

    @Test
    public void getInspections() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        final CountDownLatch jobsSignal = new CountDownLatch(1);
        final HashMap<String,Boolean> jobsResults = new HashMap<>();
        jobsResults.put("success", false);
        sut.getJobs(appContext, new DataService.GetDataCallback<Job>() {
            @Override
            public void onCompletion(List<Job> data) {
                assertNotNull(data);
                assertTrue(data.size()>0);
                jobsResults.put("success", true);
                jobsSignal.countDown();
            }

            @Override
            public void onError(String message) {

                jobsResults.put("success", false);
                jobsSignal.countDown();
            }
        });

        jobsSignal.await(7, TimeUnit.SECONDS);
        assertTrue(jobsResults.get("success"));

        final CountDownLatch signal = new CountDownLatch(1);
        final HashMap<String,Boolean> results = new HashMap<>();
        results.put("success", false);
        sut.getInspections(appContext, new DataService.GetDataCallback<Inspection>() {
            @Override
            public void onCompletion(List<Inspection> data) {
                assertNotNull(data);
                assertTrue(data.size()>0);
                results.put("success", true);
                signal.countDown();
            }

            @Override
            public void onError(String message) {

                results.put("success", false);
                signal.countDown();
            }
        });

        signal.await(7, TimeUnit.SECONDS);
        assertTrue(results.get("success"));
    }

    @Test
    public void getRecentInspections() throws Exception {

        Context appContext = InstrumentationRegistry.getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final HashMap<String,Boolean> results = new HashMap<>();
        results.put("success", false);
        sut.getRecentInspections(appContext, new DataService.GetDataCallback<RecentInspection>() {
            @Override
            public void onCompletion(List<RecentInspection> data) {
                assertNotNull(data);
                assertTrue(data.size()>0);
                results.put("success", true);
                signal.countDown();
            }

            @Override
            public void onError(String message) {

                results.put("success", false);
                signal.countDown();
            }
        });

        signal.await(7, TimeUnit.SECONDS);
        assertTrue(results.get("success"));
    }

    @Test
    public void getRecentInspectionsForDraftDeletion() throws Exception {

        Context appContext = InstrumentationRegistry.getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final HashMap<String,Boolean> results = new HashMap<>();
        results.put("success", false);
        sut.getRecentInspectionsForDraftDeletion(appContext, new DataService.GetDataCallback<RecentInspection>() {
            @Override
            public void onCompletion(List<RecentInspection> data) {
                assertNotNull(data);
                assertTrue(data.size()>0);
                results.put("success", true);
                signal.countDown();
            }

            @Override
            public void onError(String message) {

                results.put("success", false);
                signal.countDown();
            }
        });

        signal.await(7, TimeUnit.SECONDS);
        assertTrue(results.get("success"));
    }

    @Rule
    public UiThreadTestRule uiThreadTestRule = new UiThreadTestRule();

    /**
     * Make 2x refreshJobsAndInspections call.
     * Local data Jobs and Inspections should remain the same.
     * Drafts should be remain the same before and after refresh.
     * Test in 2nd refreshJobsAndInspections call, number of Jobs and Investigations should be one less than the Jobs and Investigations retrieved from 1st call.
     * (Due to Draft creation with matching Job/Inspection id).
     * Ensure the onSuccess is also invoked in the UI main thread.
     */
    @Test
    public void refreshJobsAndInspections() throws Throwable {
        Context appContext = InstrumentationRegistry.getTargetContext();


        final HashMap<String,Object> results = new HashMap<>();

        final CountDownLatch signal0 = new CountDownLatch(1);
        uiThreadTestRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                results.put("mainThreadId", Thread.currentThread().getId());
            }
        });

        signal0.await(7, TimeUnit.SECONDS);

        sut.removeAll(RecentInspection.class);
        sut.removeAll(Draft.class);
        final List<Draft> expectedDrafts = sut.getList(null, Draft.class);

        final CountDownLatch signal = new CountDownLatch(1);
        results.put("success", false);



        sut.refreshJobsAndInspections(appContext, new DataService.RefreshCallback() {
            @Override
            public void onSuccess() {
                List<Job> jobs = sut.getList(new DataService.GetCallback<Job>() {
                    @Override
                    public QueryBuilder<Job, ?> preQuery(QueryBuilder<Job, ?> queryBuilder) {
                        return queryBuilder;
                    }
                }, Job.class);
                List<Inspection> inspections = sut.getList(new DataService.GetCallback<Inspection>() {
                    @Override
                    public QueryBuilder<Inspection, ?> preQuery(QueryBuilder<Inspection, ?> queryBuilder) {
                        return queryBuilder;
                    }
                }, Inspection.class);
                List<Draft> drafts = sut.getList(new DataService.GetCallback<Draft>() {
                    @Override
                    public QueryBuilder<Draft, ?> preQuery(QueryBuilder<Draft, ?> queryBuilder) {
                        return queryBuilder;
                    }
                }, Draft.class);
                List<RecentInspection> recentInspections = sut.getList(null, RecentInspection.class);

                results.put("jobs", jobs);
                results.put("inspections", inspections);
                results.put("drafts", drafts);
                results.put("recentInspections", recentInspections);

                assertNotNull(jobs);
                assertNotNull(inspections);
                assertNotNull(drafts);
                assertTrue(jobs.size() > 0);
                assertTrue(inspections.size() > 0);
                assertEquals(expectedDrafts.size(), drafts.size());
                assertTrue(recentInspections.size() > 0);
                results.put("success", true);

                assertEquals((long)results.get("mainThreadId"), Thread.currentThread().getId());

                signal.countDown();
            }


            @Override
            public void onError(Throwable e) {
                results.put("success", false);
                signal.countDown();
            }
        });

        signal.await(3000, TimeUnit.SECONDS);
        assertTrue((boolean)results.get("success"));

        // Note - simulate a Draft with a matching Job id - test that subsequent refresh will have the Job and Inspection filtered out
        Draft draft = sut.inspectionToDraft(((List<Inspection>)results.get("inspections")).get(0));
        sut.save(draft, Draft.class);
        final List<Draft> expectedDrafts2 = sut.getList(null, Draft.class);
        final int expectedNumberOfDrafts = expectedDrafts2.size();
        final int expectedNumberOfJobs = ((List<Job>)results.get("jobs")).size()-1; // minus the draft with matching id
        final int expectedNumberOfInspections = ((List<Inspection>)results.get("inspections")).size()-1; // minus the draft with matching id
        final int expectedNumberOfRecentInspections = ((List<RecentInspection>)results.get("recentInspections")).size();

        results.put("success", false);
        final CountDownLatch signal2 = new CountDownLatch(1);

        sut.refreshJobsAndInspections(appContext, new DataService.RefreshCallback() {
            @Override
            public void onSuccess() {
                List<Job> jobs = sut.getList(new DataService.GetCallback<Job>() {
                    @Override
                    public QueryBuilder<Job, ?> preQuery(QueryBuilder<Job, ?> queryBuilder) {
                        return queryBuilder;
                    }
                }, Job.class);
                List<Inspection> inspections = sut.getList(new DataService.GetCallback<Inspection>() {
                    @Override
                    public QueryBuilder<Inspection, ?> preQuery(QueryBuilder<Inspection, ?> queryBuilder) {
                        return queryBuilder;
                    }
                }, Inspection.class);
                List<Draft> drafts = sut.getList(new DataService.GetCallback<Draft>() {
                    @Override
                    public QueryBuilder<Draft, ?> preQuery(QueryBuilder<Draft, ?> queryBuilder) {
                        return queryBuilder;
                    }
                }, Draft.class);
                List<RecentInspection> recentInspections = sut.getList(null, RecentInspection.class);

                assertNotNull(jobs);
                assertNotNull(inspections);
                assertNotNull(drafts);
                assertEquals(expectedNumberOfJobs, jobs.size());
                assertEquals(expectedNumberOfInspections, inspections.size());
                assertEquals(expectedNumberOfDrafts, drafts.size());
                assertEquals(expectedNumberOfRecentInspections, recentInspections.size());

                results.put("success", true);

                assertEquals((long)results.get("mainThreadId"), Thread.currentThread().getId());


                signal2.countDown();
            }


            @Override
            public void onError(Throwable e) {
                results.put("success", false);
                signal2.countDown();
            }
        });

        signal2.await(300, TimeUnit.SECONDS);
        assertTrue((boolean)results.get("success"));


    }


    @Test
    public void testRx_getRecentInspectionsForDraftDeletion() throws Exception {

        Context appContext = InstrumentationRegistry.getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final HashMap<String,Boolean> results = new HashMap<>();
        results.put("success", false);

        Single<List<RecentInspection>> single = sut.getRecentInspectionsForDraftDeletion();
        single.subscribe(recentInspectionsForDraftDeletion -> {
            assertNotNull(recentInspectionsForDraftDeletion);
            assertNotNull(recentInspectionsForDraftDeletion.size() > 0);
            results.put("success", true);
            signal.countDown();
        }, error -> {
            results.put("success", false);
            signal.countDown();
        });

        signal.await(7, TimeUnit.SECONDS);
        assertTrue(results.get("success"));
    }

    @Test
    public void testRx_getDraftsCreatedBy() throws Exception {

        cleanDrafts();
        mockDrafts();
        final CountDownLatch signal = new CountDownLatch(1);
        final HashMap<String,Boolean> results = new HashMap<>();
        results.put("success", false);

        Single<List<Draft>> single = sut.getDraftsCreatedBy("mockUsername");
        single.subscribe(drafts -> {
            assertNotNull(drafts);
            assertEquals(2,drafts.size());
            assertEquals("mockId1", drafts.get(0).getId());
            assertEquals("mockId3", drafts.get(1).getId());
            results.put("success", true);
            signal.countDown();
        }, error -> {
            results.put("success", false);
            signal.countDown();
        });

        signal.await(7, TimeUnit.SECONDS);
        assertTrue(results.get("success"));
        cleanDrafts();
    }

    @Test
    public void testRx_getDraftsCreatedBy_case2() throws Exception {

        cleanDrafts();
        mockDrafts();
        final CountDownLatch signal = new CountDownLatch(1);
        final HashMap<String,Boolean> results = new HashMap<>();
        results.put("success", false);

        Single<List<Draft>> single = sut.getDraftsCreatedBy("mockUsername2");
        single.subscribe(drafts -> {
            assertNotNull(drafts);
            assertEquals(1,drafts.size());
            assertEquals("mockId2", drafts.get(0).getId());
            results.put("success", true);
            signal.countDown();
        }, error -> {
            results.put("success", false);
            signal.countDown();
        });

        signal.await(7, TimeUnit.SECONDS);
        assertTrue(results.get("success"));
        cleanDrafts();
    }

    @Test
    public void testRx_getRemainingDraftsAfterProcessingObsoleteDrafts() throws Exception {

        cleanDrafts();
        mockDrafts();
        final CountDownLatch signal = new CountDownLatch(1);
        final HashMap<String,Boolean> results = new HashMap<>();
        results.put("success", false);


        RecentInspection mockObsoleteDraft = new RecentInspection();
        mockObsoleteDraft.setId("mockId1");
        List<RecentInspection> mockObsoleteDrafts = new ArrayList<>();
        mockObsoleteDrafts.add(mockObsoleteDraft);

        Single<List<Draft>> single = sut.getRemainingDraftsAfterProcessingObsoleteDrafts(mockObsoleteDrafts, "mockUsername");
        single.subscribe(drafts -> {
            assertNotNull(drafts);
            assertEquals(1,drafts.size());
            assertEquals("mockId3", drafts.get(0).getId());
            results.put("success", true);
            signal.countDown();
        }, error -> {
            results.put("success", false);
            signal.countDown();
        });

        signal.await(7, TimeUnit.SECONDS);
        assertTrue(results.get("success"));
        cleanDrafts();
    }

    @Test
    public void testRx_scanAndCleanObsoleteDrafts_threads() throws Exception {

        final CountDownLatch signal = new CountDownLatch(1);
        final HashMap<String,Object> results = new HashMap<>();
        results.put("success", false);

        sut.scanAndCleanObsoleteDrafts("mockUsername")
                .subscribe(drafts -> {
                    assertTrue(Looper.myLooper() == Looper.getMainLooper());
                    results.put("success", true);
                    results.put("after", new Date());
                    signal.countDown();
                }, error -> {
                    assertTrue(Looper.myLooper() == Looper.getMainLooper());
                    results.put("success", false);
                    results.put("after", new Date());
                    signal.countDown();
                });

        Date callingThreadDate = new Date();

        signal.await(7, TimeUnit.SECONDS);
        assertTrue((Boolean) results.get("success"));
        assertTrue(callingThreadDate.getTime() < ((Date)results.get("after")).getTime()); // proves that it is observed on background thread i.e. the current thread is not blocked
    }

    @Test
    public void testRx_getUpcomingCalendarItems() throws Exception {

        final CountDownLatch signal = new CountDownLatch(1);
        final HashMap<String, Boolean> results = new HashMap<>();
        results.put("success", false);

        sut.getUpcomingCalendarItems()
                .subscribe(calendarItems -> {
                    assertNotNull(calendarItems);
                    assertTrue(calendarItems.size() > 0);
                    results.put("success", true);
                    signal.countDown();
                }, error -> {
                    results.put("success", false);
                    signal.countDown();
                });

        signal.await(7, TimeUnit.SECONDS);
        assertTrue(results.get("success"));
    }

}
