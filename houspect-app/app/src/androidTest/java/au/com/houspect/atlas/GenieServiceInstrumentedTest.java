package au.com.houspect.atlas;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import com.saasplications.genie.domain.model.ActivityInstance;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionClient;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.models.RecentInspection;
import au.com.houspect.atlas.services.GenieService;
import au.com.houspect.atlas.services.GenieServiceCallback;
import au.com.houspect.atlas.services.GenieServiceImpl;
import au.com.houspect.atlas.services.GenieServiceResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class GenieServiceInstrumentedTest {

    private String sessionToken;
    private ActivityInstance activityInstance;

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("au.com.houspect.atlas.test", appContext.getPackageName());
    }

    @Test
    public void login() throws Exception {

        final CountDownLatch loginSignal = new CountDownLatch(1);
        GenieService sut = new GenieServiceImpl();
        sut.login("alfredw", "T2AndM3", new GenieServiceCallback<GenieServiceResponse<String>>() {
            @Override
            public void completed(GenieServiceResponse<String> response) {

                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                assertNotEquals(0, response.getData());
                loginSignal.countDown();
            }
        });
        loginSignal.await(7, TimeUnit.SECONDS);
    }

    @Test
    public void getJobs() throws Exception {
        final CountDownLatch loginSignal = new CountDownLatch(1);
        GenieService sut = new GenieServiceImpl();
        final GenieServiceInstrumentedTest self = this;
        sut.login("alfredw", "T2AndM3", new GenieServiceCallback<GenieServiceResponse<String>>() {
            @Override
            public void completed(GenieServiceResponse<String> response) {

                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                assertNotEquals(0, response.getData());
                self.sessionToken = response.getData();
                //GenieServiceInstrumentedTest.this.sessionToken = response.getData();
                loginSignal.countDown();
            }
        });
        loginSignal.await(7, TimeUnit.SECONDS);

        final CountDownLatch activitySignal = new CountDownLatch(1);

        sut.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
            @Override
            public void completed(GenieServiceResponse<ActivityInstance> response) {
                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                activityInstance = response.getData();
                activitySignal.countDown();
            }
        });
        activitySignal.await(7, TimeUnit.SECONDS);

        final CountDownLatch jobsSignal = new CountDownLatch(1);
        sut.getJobs(sessionToken, activityInstance, new GenieServiceCallback<GenieServiceResponse<List<Job>>>() {

            @Override
            public void completed(GenieServiceResponse<List<Job>> response) {
                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                assertTrue(response.getData().size() > 0);
                jobsSignal.countDown();
            }
        });

        jobsSignal.await(7, TimeUnit.SECONDS);
    }

    @Test
    public void getInspections() throws Exception {
        final CountDownLatch loginSignal = new CountDownLatch(1);
        GenieService sut = new GenieServiceImpl();
        final GenieServiceInstrumentedTest self = this;
        sut.login("alfredw", "T2AndM3", new GenieServiceCallback<GenieServiceResponse<String>>() {
            @Override
            public void completed(GenieServiceResponse<String> response) {

                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                assertNotEquals(0, response.getData());
                self.sessionToken = response.getData();
                loginSignal.countDown();
            }
        });
        loginSignal.await(7, TimeUnit.SECONDS);

        final CountDownLatch activitySignal = new CountDownLatch(1);

        sut.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
            @Override
            public void completed(GenieServiceResponse<ActivityInstance> response) {
                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                activityInstance = response.getData();
                activitySignal.countDown();
            }
        });
        activitySignal.await(7, TimeUnit.SECONDS);

        final CountDownLatch jobsSignal = new CountDownLatch(1);
        sut.getJobs(sessionToken, activityInstance, new GenieServiceCallback<GenieServiceResponse<List<Job>>>() {

            @Override
            public void completed(GenieServiceResponse<List<Job>> response) {
                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                assertTrue(response.getData().size() > 0);
                jobsSignal.countDown();
            }
        });

        jobsSignal.await(37, TimeUnit.SECONDS);

        final CountDownLatch inspectionsSignal = new CountDownLatch(1);
        sut.getInspections(sessionToken, activityInstance, new GenieServiceCallback<GenieServiceResponse<List<Inspection>>>() {

            @Override
            public void completed(GenieServiceResponse<List<Inspection>> response) {
                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                assertTrue(response.getData().size() > 0);
                inspectionsSignal.countDown();
            }
        });

        inspectionsSignal.await(37, TimeUnit.SECONDS);
    }

    @Test
    public void getRecentInspections() throws Exception {
        final CountDownLatch loginSignal = new CountDownLatch(1);
        GenieService sut = new GenieServiceImpl();
        final GenieServiceInstrumentedTest self = this;
        sut.login("alfredw", "T2AndM3", new GenieServiceCallback<GenieServiceResponse<String>>() {
            @Override
            public void completed(GenieServiceResponse<String> response) {

                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                assertNotEquals(0, response.getData());
                self.sessionToken = response.getData();
                //GenieServiceInstrumentedTest.this.sessionToken = response.getData();
                loginSignal.countDown();
            }
        });
        loginSignal.await(7, TimeUnit.SECONDS);

        final CountDownLatch activitySignal = new CountDownLatch(1);

        sut.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
            @Override
            public void completed(GenieServiceResponse<ActivityInstance> response) {
                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                activityInstance = response.getData();
                activitySignal.countDown();
            }
        });
        activitySignal.await(7, TimeUnit.SECONDS);

        final CountDownLatch signal = new CountDownLatch(1);
        sut.getRecentInspections(sessionToken, activityInstance, new GenieServiceCallback<GenieServiceResponse<List<RecentInspection>>>() {

            @Override
            public void completed(GenieServiceResponse<List<RecentInspection>> response) {
                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                assertTrue(response.getData().size() > 0);
                signal.countDown();
            }
        });

        signal.await(1007, TimeUnit.SECONDS);
    }

    @Test
    public void testRx_getCalendarItems() throws Exception {
        final CountDownLatch loginSignal = new CountDownLatch(1);
        GenieService sut = new GenieServiceImpl();
        final GenieServiceInstrumentedTest self = this;
        sut.login("alfredw", "T2AndM3", new GenieServiceCallback<GenieServiceResponse<String>>() {
            @Override
            public void completed(GenieServiceResponse<String> response) {

                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                assertNotEquals(0, response.getData());
                self.sessionToken = response.getData();
                //GenieServiceInstrumentedTest.this.sessionToken = response.getData();
                loginSignal.countDown();
            }
        });
        loginSignal.await(7, TimeUnit.SECONDS);

        final CountDownLatch activitySignal = new CountDownLatch(1);

        sut.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
            @Override
            public void completed(GenieServiceResponse<ActivityInstance> response) {
                assertNotNull(response);
                assertEquals(true, response.isSuccess());
                assertNotNull(response.getData());
                activityInstance = response.getData();
                activitySignal.countDown();
            }
        });
        activitySignal.await(7, TimeUnit.SECONDS);

        final CountDownLatch signal = new CountDownLatch(1);

        sut.getUpcomingCalendarItems(sessionToken, activityInstance)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {

                    assertNotNull(response);
                    assertEquals(true, response.isSuccess());
                    assertNotNull(response.getData());
                    assertTrue(response.getData().size() > 0);
                    signal.countDown();

                }, error -> {
                    assertTrue("should not fail", false);
                    signal.countDown();
                });

        signal.await(1007, TimeUnit.SECONDS);
    }
}
