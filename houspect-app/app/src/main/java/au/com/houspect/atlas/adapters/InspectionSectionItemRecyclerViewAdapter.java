package au.com.houspect.atlas.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import au.com.houspect.atlas.BR;
import au.com.houspect.atlas.R;
import au.com.houspect.atlas.fragments.JobListFragment;
import au.com.houspect.atlas.fragments.dummy.DummyContent.DummyItem;
import au.com.houspect.atlas.models.InspectionSectionItem;
import au.com.houspect.atlas.models.InspectionSectionItemTwoNumbers;
import au.com.houspect.atlas.viewmodels.InspectionSectionItemTwoNumbersViewModel;
import au.com.houspect.atlas.viewmodels.InspectionSectionItemViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link JobListFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class InspectionSectionItemRecyclerViewAdapter extends RecyclerView.Adapter<InspectionSectionItemRecyclerViewAdapter.BindingHolder> {

    public interface Listener {
        void onRequestForOtherNaOptions(InspectionSectionItemViewModel viewModel);
    }

    private Context mContext;

    private final List<InspectionSectionItem> mValues;
    //private final InspectionSectionFragment.OnListFragmentInteractionListener mListener;
    private int mEnumItemResourceId;
    private int mTextItemResourceId;
    private int mTwoNumbersResourceId;

    private Listener mListener;

    // uuid of the BindingHolder to Rx Disposable - this disposable needs to be disposed when the view of the bindingHolder is recycled
    private Map<String, Disposable> uuidToDisposable = new HashMap<>();

    public InspectionSectionItemRecyclerViewAdapter(List<InspectionSectionItem> items,
                                                    //InspectionSectionFragment.OnListFragmentInteractionListener listener,
                                                    int enumItemResourceId,
                                                    int textItemResourceId,
                                                    int twoNumbersResourceId,
                                                    Listener listener) {

        mValues = items;
        //mListener = listener;
        mEnumItemResourceId = enumItemResourceId;
        mTextItemResourceId = textItemResourceId;
        mTwoNumbersResourceId = twoNumbersResourceId;
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        InspectionSectionItem item = mValues.get(position);
        return item.getType().getInt();
    }


    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();

        int resourceId = mTextItemResourceId;
        boolean isEnumItem = InspectionSectionItem.Type.Enum.getInt() == viewType;
        if (isEnumItem) resourceId = mEnumItemResourceId;
        boolean isTwoNumbersItem = InspectionSectionItem.Type.TwoNumbers.getInt() == viewType;
        if (isTwoNumbersItem) resourceId = mTwoNumbersResourceId;

        View view = LayoutInflater.from(mContext)
                .inflate(resourceId, parent, false);
        return (isEnumItem) ? new EnumItemViewHolder(view) : new BindingHolder(view); // both Text and TwoNumbers use the same BindingHolder
    }

    @Override
    public void onBindViewHolder(final BindingHolder holder, int position) {

        InspectionSectionItem item = mValues.get(position);
        int viewType = getItemViewType(position);

        boolean isEnumItem = InspectionSectionItem.Type.Enum.getInt() == viewType;

        if (isEnumItem) {
            final EnumItemViewHolder itemViewHolder = (EnumItemViewHolder) holder;
            refreshDisplay(itemViewHolder, item, LayoutInflater.from(mContext));
        }
        else {
            refreshDisplayBase(holder, item);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public static class BindingHolder extends RecyclerView.ViewHolder {
        public ViewDataBinding mBinding;
        public final TextView mTextViewTitle;

        public BindingHolder(View view) {
            super(view);
            mBinding = DataBindingUtil.bind(view);

            mTextViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
        }
    }

    @Override
    public void onViewRecycled(BindingHolder holder) {
        super.onViewRecycled(holder);

        if (holder instanceof EnumItemViewHolder) {
            Disposable disposable = uuidToDisposable.remove(((EnumItemViewHolder)holder).uuid);
            if (disposable != null) disposable.dispose();
        }
    }

    class EnumItemViewHolder extends BindingHolder {

        public final RadioGroup mViewGroupOptions;
        public String uuid = UUID.randomUUID().toString();

        public EnumItemViewHolder(View view) {
            super(view);
            mViewGroupOptions = (RadioGroup) view.findViewById(R.id.viewGroupOptions);
        }
    }

    private void refreshDisplay(EnumItemViewHolder viewHolder, final InspectionSectionItem inspectionSectionItem, LayoutInflater inflater) {

        refreshDisplayBase(viewHolder, inspectionSectionItem);
        viewHolder.mBinding.setVariable(BR.handler, new InspectionSectionItemOptionHandler());
        viewHolder.mViewGroupOptions.setOnCheckedChangeListener(null);
        viewHolder.mViewGroupOptions.removeAllViews();
        viewHolder.mViewGroupOptions.clearCheck();

        //Log.d("Atlas Radio", "RadioGroup ( " + inspectionSectionItem.getId() + ", " + inspectionSectionItem.getText() + ") Result = (" + inspectionSectionItem.getResult() + ")");
        for (int i = 0; i < inspectionSectionItem.getOptions().size(); i++) {
            InspectionSectionItem.Option option = inspectionSectionItem.getOptions().get(i);
            View viewOption = inflater.inflate(R.layout.fragment_inspection_section_enum_item_option, null);
            RadioButton radioButton = (RadioButton) viewOption.findViewById(R.id.radioButton);
            radioButton.setText(option.getText());
            // TODO:: change id
            radioButton.setId(i);

            viewHolder.mViewGroupOptions.addView(viewOption);

            if (option.getId().equals(inspectionSectionItem.getResult())) radioButton.setChecked(true);
            //Log.d("Atlas Radio", "Option i=" + i + " ( " + option.getId() + ", " + option.getText() + "), RadioButton Id = (" + radioButton.getId() + ") checked=" + radioButton.isChecked());
        }

        viewHolder.mViewGroupOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                InspectionSectionItemOptionHandler.onCheckedChanged(group, checkedId, inspectionSectionItem);
            }
        });
    }

    private void refreshDisplayBase(BindingHolder viewHolder, InspectionSectionItem inspectionSectionItem) {
        boolean isTwoNumbers = inspectionSectionItem instanceof InspectionSectionItemTwoNumbers;
        if (!isTwoNumbers) {
            EnumItemViewHolder enumItemViewHolder = (EnumItemViewHolder)viewHolder;
            InspectionSectionItemViewModel viewModel = new InspectionSectionItemViewModel(inspectionSectionItem, mListener);
            viewHolder.mBinding.setVariable(BR.viewModel, viewModel);
            RadioGroup radioGroupForOptions = enumItemViewHolder.mViewGroupOptions;
            if (radioGroupForOptions != null) {
                Disposable disposable =
                        viewModel.otherCheckedEvent.subscribe(onOtherChecked -> {
                    radioGroupForOptions.clearCheck();
                });

                uuidToDisposable.put(enumItemViewHolder.uuid, disposable);
            }
        }
        else {
            InspectionSectionItemTwoNumbersViewModel viewModel = new InspectionSectionItemTwoNumbersViewModel((InspectionSectionItemTwoNumbers)inspectionSectionItem);
            viewHolder.mBinding.setVariable(BR.viewModel, viewModel);
        }
    }

    public static class InspectionSectionItemOptionHandler {
        public static void onCheckedChanged(RadioGroup radioGroup, int checkedId, InspectionSectionItem inspectionSectionItem) {
            boolean isCleared = checkedId == -1;
            if (isCleared) {
                inspectionSectionItem.setResult("");
                return;
            }
            InspectionSectionItem.Option selectedOption = inspectionSectionItem.getOptions().get(checkedId);
            inspectionSectionItem.setResult(selectedOption.getId());
        }
    }

}
