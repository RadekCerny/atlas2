package au.com.houspect.atlas.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.List;

import au.com.houspect.atlas.fragments.InspectionFragment;

import static au.com.houspect.atlas.services.DataIntentServiceReceiver.Event.ACTIVITY_END;
import static au.com.houspect.atlas.services.DataIntentServiceReceiver.Event.ACTIVITY_START;

public class DataIntentServiceReceiver extends BroadcastReceiver {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static DataIntentServiceReceiver.Event ACTIVITY_START = new DataIntentServiceReceiver.Event(2001);
        public static DataIntentServiceReceiver.Event ACTIVITY_END = new DataIntentServiceReceiver.Event(2002);
    }

    public interface Listener {
        void onReceive(Event event, String message);
    }

    private Listener listener;


    public DataIntentServiceReceiver() {

    }

    public DataIntentServiceReceiver(Listener listener) {
        this.listener = listener;
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        String message = intent.getStringExtra(DataSyncJob.EXTENDED_DATA_MESSAGE);
        boolean isEmptyMessage = message == null || message.length() == 0;
        //if (context instanceof Listener) ((Listener)context).onReceive(isEmptyMessage ? ACTIVITY_END : ACTIVITY_START, message);
        if (listener != null) listener.onReceive(isEmptyMessage ? ACTIVITY_END : ACTIVITY_START, message);
    }
}
