package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.databinding.FragmentLoginBinding;
import au.com.houspect.atlas.services.GenieService;
import au.com.houspect.atlas.services.GenieServiceCallback;
import au.com.houspect.atlas.services.GenieServiceImpl;
import au.com.houspect.atlas.services.GenieServiceResponse;
import au.com.houspect.atlas.services.SharedPreferencesService;
import au.com.houspect.atlas.services.SharedPreferencesServiceImpl;
import au.com.houspect.atlas.viewmodels.LoginViewModel;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.R.attr.value;
import static au.com.houspect.atlas.fragments.LoginFragment.Event.FORGOT_PASSWORD;
import static au.com.houspect.atlas.fragments.LoginFragment.Event.LOGIN_FAILURE;
import static au.com.houspect.atlas.fragments.LoginFragment.Event.LOGIN_IN_PROGRESS;
import static au.com.houspect.atlas.fragments.LoginFragment.Event.LOGIN_SUCCESS;
import static au.com.houspect.atlas.fragments.LoginFragment.Event.NOT_ME;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static LoginFragment.Event LOGIN_IN_PROGRESS = new LoginFragment.Event(100);
        public static LoginFragment.Event LOGIN_SUCCESS = new LoginFragment.Event(101);
        public static LoginFragment.Event LOGIN_FAILURE = new LoginFragment.Event(102);
        public static LoginFragment.Event FORGOT_PASSWORD = new LoginFragment.Event(103);
        public static LoginFragment.Event NOT_ME = new LoginFragment.Event(104);
    }

    private LoginViewModel viewModel;
    private SharedPreferencesService sharedPreferencesService;
    private List<Disposable> mDisposables = Collections.synchronizedList(new ArrayList<Disposable>());

    private OnFragmentInteractionListener mListener;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final FragmentLoginBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);

        View view = binding.getRoot();

        final Runnable onLogin = new Runnable() {
            @Override
            public void run() {
               login();
            }
        };
        final Runnable onForgotPassword = new Runnable() {
            @Override
            public void run() {
                mListener.onFragmentInteraction(FORGOT_PASSWORD, viewModel.username.get());
            }
        };
        sharedPreferencesService = new SharedPreferencesServiceImpl(getContext());
        final Runnable onNotMe = new Runnable() {
            @Override
            public void run() {

                viewModel.isProcessing.set(true);
                Single.fromCallable(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {

                        // clear cached credentials
                        sharedPreferencesService.saveUsername(null);
                        sharedPreferencesService.savePassword(null);

                        return true;
                    }
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<Object>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(Object value) {
                                viewModel.isProcessing.set(false);
                                viewModel.shouldUseCachedCredentials.set(false);
                                mListener.onFragmentInteraction(NOT_ME, null);
                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT);
                            }
                        });
            }
        };

        viewModel = new LoginViewModel(onLogin, onForgotPassword, onNotMe);

        Single.fromCallable(new Callable<LoginViewModel>() {
            @Override
            public LoginViewModel call() throws Exception {
                String username = sharedPreferencesService.getUsername();
                boolean hasCachedCredentials = username != null && username.length() > 0 && sharedPreferencesService.getPassword() != null;
                LoginViewModel dummyViewModel = new LoginViewModel(null, null, null);
                dummyViewModel.shouldUseCachedCredentials.set(hasCachedCredentials);
                dummyViewModel.username.set(username);
                return dummyViewModel;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<LoginViewModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposables.add(d);
                    }

                    @Override
                    public void onSuccess(LoginViewModel dummyViewModel) {
                        String username = dummyViewModel.username.get();
                        boolean hasCachedCredentials = dummyViewModel.shouldUseCachedCredentials.get();
                        if (hasCachedCredentials) {
                            viewModel.shouldUseCachedCredentials.set(true);
                            viewModel.username.set(username);
                        }
                        else {
                            viewModel.shouldUseCachedCredentials.set(false);
                        }
                        viewModel.username.set(username);

                        viewModel.isProcessing.set(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            viewModel.setVersion(version);
        } catch (Exception e) {
            viewModel.setVersion("");
        }

        binding.setViewModel(viewModel);

        return view;
    }

    private void login() {

        Single single = Single.fromCallable(new Callable() {
            @Override
            public Object call() throws Exception {
                final SharedPreferencesService sharedPreferencesService = new SharedPreferencesServiceImpl(getContext());
                String username = viewModel.username.get();
                String password = viewModel.shouldUseCachedCredentials.get() ? sharedPreferencesService.getPassword() : viewModel.getPassword();
                if (username != null) username = username.trim();
                if (password != null) password = password.trim();

                return new Pair(username, password);
            }
        });

        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Pair<String,String>>() {
            @Override
            public void onSubscribe(Disposable d) {
                mDisposables.add(d);
            }

            @Override
            public void onSuccess(Pair<String, String> value) {

                // Note: stage 3 behavior - postpone login to actual server activity if it is a re-login
                boolean isRelogin = viewModel.shouldUseCachedCredentials.get();

                if (isRelogin) {
                    mListener.onFragmentInteraction(LOGIN_SUCCESS, null);
                    return;
                }
                // --


                String username = value.first;
                String password = value.second;
                GenieService genieService = new GenieServiceImpl();

                boolean isValid = validateCredentials(username, password);
                if (!isValid) {
                    mListener.onFragmentInteraction(LOGIN_FAILURE, "Please supply valid username & password");
                    return;
                }

                final String usernameFinal = username;
                final String passwordFinal = password;

                mListener.onFragmentInteraction(LOGIN_IN_PROGRESS, null);

                genieService.login(username, password, new GenieServiceCallback<GenieServiceResponse<String>>() {

                    @Override
                    public void completed(GenieServiceResponse<String> response) {
                        if (response.isSuccess()) {

                            final String sessionToken = response.getData();

                            Single single1 = Single.fromCallable(new Callable() {
                                @Override
                                public Object call() throws Exception {

                                    sharedPreferencesService.saveUsername(usernameFinal);
                                    sharedPreferencesService.savePassword(passwordFinal);
                                    sharedPreferencesService.saveSessionToken(sessionToken);

                                    return true;
                                }
                            });

                            single1.subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new SingleObserver() {
                                        @Override
                                        public void onSubscribe(Disposable d) {
                                            mDisposables.add(d);
                                        }

                                        @Override
                                        public void onSuccess(Object value) {
                                            mListener.onFragmentInteraction(LOGIN_SUCCESS, null);
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            mListener.onFragmentInteraction(LOGIN_FAILURE, e.getMessage());
                                        }
                                    });

                        }
                        else {
                            mListener.onFragmentInteraction(LOGIN_FAILURE, response.getErrorMessage());
                        }
                    }
                });

            }

            @Override
            public void onError(Throwable e) {
                mListener.onFragmentInteraction(LOGIN_FAILURE, e.getMessage());
            }
        });

    }

    private boolean validateCredentials(String username, String password) {
        boolean isValid = false;
        isValid = username != null && username.length() > 0 && password != null && password.length() > 0;
        return isValid;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        for (Disposable disposable : mDisposables) {
            if (!disposable.isDisposed()) disposable.dispose();
        }
        mDisposables = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Event event, String message);
    }
}
