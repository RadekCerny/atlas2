package au.com.houspect.atlas.services;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by alfredwong on 13/4/17.
 */

public class SynchronisedService {

    public static AtomicBoolean isServerActivityInProgress = new AtomicBoolean(false);
}
