package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.utilities.SessionUtil;
import au.com.houspect.atlas.utilities.Util;

//import com.badoualy.stepperindicator.StepperIndicator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InspectionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InspectionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InspectionFragment extends Fragment {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static InspectionFragment.Event CANCEL = new InspectionFragment.Event(701);
    }
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_INSPECTION = "inspection";
    private static final String ARG_CURRENT_PAGE_INDEX = "currentPageIndex";
    private static final String ARG_IS_DRAFT_EDITING = "isDraftEditing";

    private Inspection mInspection;

    private int mCurrentPageIndex;
    private boolean mIsDraftEditing; // indicates if current display is for Inspection preview (i.e. before Inspection is converted to Draft) or actual Draft editing


    private FragmentActivity mContext;

    private OnFragmentInteractionListener mListener;

    //private ViewPager mViewPagerInspectionSections;
    //private InspectionSectionFragmentStatePagerAdapter mAdapeterInspectionSections;
    //private StepperIndicator mStepperIndicator;
    //private CircleIndicator mViewPagerIndicator;

    private ViewGroup mViewGroupSection;
    private TextView mTextViewPageIndicator;

    private InspectionStartFragment mInspectionStartFragment;
    private InspectionEndFragment mInspectionEndFragment;

    private Fragment mFragmentCurrentPage;

    public InspectionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param inspection
     * @return A new instance of fragment InspectionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InspectionFragment newInstance(Inspection inspection, int currentPageIndex, boolean isDraftEditing) {
        InspectionFragment fragment = new InspectionFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_INSPECTION, inspection);
        args.putInt(ARG_CURRENT_PAGE_INDEX, currentPageIndex);
        args.putBoolean(ARG_IS_DRAFT_EDITING, isDraftEditing);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mInspection = getArguments().getParcelable(ARG_INSPECTION);
            mCurrentPageIndex = getArguments().getInt(ARG_CURRENT_PAGE_INDEX);
            mIsDraftEditing = getArguments().getBoolean(ARG_IS_DRAFT_EDITING);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inspection2, container, false);

        mInspectionStartFragment = InspectionStartFragment.newInstance(mInspection.getId(), mIsDraftEditing);
        mInspectionEndFragment = InspectionEndFragment.newInstance(mInspection.getId());
        //TODO:: dependency inject
        if (mIsDraftEditing) SessionUtil.singleton.setDraftInEdit((Draft)mInspection);

        mViewGroupSection = (ViewGroup) view.findViewById(R.id.viewGroupSection);
        mTextViewPageIndicator = (TextView) view.findViewById(R.id.textViewPageIndicator);
        refreshCurrentPage();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            mContext = (FragmentActivity) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        SessionUtil.singleton.setDraftInEdit(null);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Event event);
    }

    private void refreshCurrentPage() {
        Fragment currentPage;
        boolean isFirstPage = isFirstPage();
        boolean isLastPage = isLastPage();
        boolean isSecondLastPage = isSecondLastPage();
        String pageIndicator = (mInspection).getSections().size() + " sections";
        if (isFirstPage) {
            currentPage = InspectionStartFragment.newInstance(mInspection.getId(), mIsDraftEditing);
        }
        else if (isLastPage) {
            currentPage = InspectionEndFragment.newInstance(mInspection.getId());
        }
        else if (isSecondLastPage) {
            currentPage = InspectionRecommendationSummaryFragment.newInstance(mInspection.getId());
        }
        else {
            int sectionIndex = mCurrentPageIndex - 1;
            currentPage = InspectionSectionFragment.newInstance(mInspection.getSections().get(sectionIndex), mInspection);
            pageIndicator = (sectionIndex+1)+"/"+(mInspection.getSections().size());
        }
        showFragment(currentPage, R.id.viewGroupSection);

        mTextViewPageIndicator.setText(pageIndicator);
    }


    void showFragment(Fragment fragment, int containerId) {

        // Note: do not perform mSessionUtil.setFragmentActive as this is also used by showFragmentPrompt
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(containerId, fragment);
        transaction.commit();

        mFragmentCurrentPage = fragment;

    }

    void hideFragment(Fragment fragment) {

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.remove(fragment);
        transaction.commit();

        if (mFragmentCurrentPage == fragment) mFragmentCurrentPage = null;
    }

    public boolean isFirstPage() {
        boolean isFirstPage = mCurrentPageIndex == 0;
        return isFirstPage;
    }

    public boolean isLastPage() {
        return mCurrentPageIndex == mInspection.getSections().size()+1+1;
    }

    public boolean isSecondLastPage() {
        return mCurrentPageIndex == mInspection.getSections().size()+1;
    }

    public void next() {
        boolean isLastPage = isLastPage();
        if (isLastPage) return;
        mCurrentPageIndex++;
        refreshCurrentPage();
    }

    public void back() {
        boolean isFirstPage = isFirstPage();
        if (isFirstPage) {
            // TODO:: validate any dirty changes - show prompt to confirm discard or not
            mListener.onFragmentInteraction(Event.CANCEL);
            return;
        }
        mCurrentPageIndex--;
        refreshCurrentPage();
    }

    public void goTo(int index) {
        mCurrentPageIndex = index;
        refreshCurrentPage();
    }

    public void goToSection(int sectionIndex) {
        mCurrentPageIndex = sectionIndex+1;
        refreshCurrentPage();
    }

    public void refreshDisplay() {
        if (mFragmentCurrentPage == null) return;
        if (mFragmentCurrentPage instanceof InspectionSectionFragment) ((InspectionSectionFragment)mFragmentCurrentPage).refreshDisplay();
    }

    // Returns the index of current InspectionSection on display. If current page is start or end, this function returns -1.
    public int getCurrentInspectionSectionIndex() {
        if (isLastPage() || isFirstPage() || isSecondLastPage()) return -1;
        int sectionIndex = mCurrentPageIndex - 1;;
        return sectionIndex;
    }

    // Insert section after the currentPageIndex
    private void insertInspectionSection(InspectionSection section) {
        int currentSectionIndex = getCurrentInspectionSectionIndex();
        if (currentSectionIndex == -1) return;
        int insertionIndex = currentSectionIndex+1;
        mInspection.getSections().add(insertionIndex, section);
        next();
    }

    // Clone current InspectionSection. Insert it after current section. Navigate to the new clone.
    public void cloneInspectionSection(InspectionSection inspectionSectionToClone) {
        //int currentSectionIndex = getCurrentInspectionSectionIndex();
        //if (currentSectionIndex == -1) return;
        InspectionSection section = inspectionSectionToClone; //mInspection.getSections().get(currentSectionIndex);
        try {
            // update Inspection.Meta data information - this is needed to have correct suffix numbering during clone operation
            int maxRepeatingSeq = mInspection.getMeta().getMaxRepeatingSeqByRepeatingKey(section.getRepeatingKey());
            String name = Util.removeTrailingNumber(section.getName());
            int repeatingSeq = maxRepeatingSeq+1;

            InspectionSection clone = (InspectionSection) section.clone(repeatingSeq+"");
            int sectionIndexOfTheClone = insertClonedInspectionSectionAtTheEndOfTheSequence(clone, inspectionSectionToClone, mInspection);
            //mInspection.getSections().add(inspectionSectionToClone.getSectionIndex()+1, clone);

            //insertInspectionSection(clone);

            mInspection.getMeta().addSection(clone);
            goToSection(sectionIndexOfTheClone);

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    public void removeInspectionSection(InspectionSection inspectionSection) {
        boolean isCurrentPageToBeRemoved = inspectionSection.getSectionIndex() == getCurrentInspectionSectionIndex();
        mInspection.getSections().remove(inspectionSection.getSectionIndex());
        if (isCurrentPageToBeRemoved) back();
    }

    /**
     * Given a newly cloned InspectionSection. Insert the clone to the end of the repeatingSeq in inspection.getSections().
     * @param clone
     * @return Section index of the newly inserted clone.
     */
    protected int insertClonedInspectionSectionAtTheEndOfTheSequence(InspectionSection clone, InspectionSection inspectionSectionCloned, Inspection inspection) {

        int startIndex = -1;
        //inspection.getSections().indexOf(inspectionSectionCloned);
        // Note - indexOf cannot be used as the inspectionSectionCloned is a different instance to the section contained in inspection.getSections()
        for (int i = 0; i < inspection.getSections().size(); i++) {
            if (inspection.getSections().get(i).getId().equalsIgnoreCase(inspectionSectionCloned.getId())) {
                startIndex = i;
                break;
            }
        }
        int insertionIndex = startIndex+1;
        for (int i = insertionIndex; i < inspection.getSections().size(); i++) {
            // stop the loop if current position is no longer part of the same repeatingKey seq
            InspectionSection section = inspection.getSections().get(i);
            if (section.getRepeatingKey() == null) break;
            if (!section.getRepeatingKey().equalsIgnoreCase(inspectionSectionCloned.getRepeatingKey())) break;
            // increment insertionIndex in search for the end of the repeatingKey seq
            insertionIndex++;
        }

        inspection.getSections().add(insertionIndex, clone);

        return insertionIndex;
    }

    public Inspection getInspection() { return mInspection; }

    /**
     * Indicates if current display is for Inspection preview (i.e. before its conversion to Draft) or actual Draft browsing/editing.
     * @return
     */
    public boolean isDraftEditing() {
        return mIsDraftEditing;
    }

    public void invokeCamera() {
        if (mFragmentCurrentPage instanceof InspectionSectionFragment) {
            ((InspectionSectionFragment)mFragmentCurrentPage).invokeCamera();
        }
        else if (mFragmentCurrentPage instanceof InspectionStartFragment) {
            ((InspectionStartFragment)mFragmentCurrentPage).invokeCamera();
        }
    }

    public void invokePhotoLibrary() {
        if (mFragmentCurrentPage instanceof InspectionSectionFragment) {
            ((InspectionSectionFragment)mFragmentCurrentPage).invokePhotoLibrary();
        }
        else if (mFragmentCurrentPage instanceof InspectionStartFragment) {
            ((InspectionStartFragment)mFragmentCurrentPage).invokePhotoLibrary();
        }
    }

    public void copyChangesTo(Draft draft) {
        if (mFragmentCurrentPage instanceof InspectionRecommendationSummaryFragment) {
            ((InspectionRecommendationSummaryFragment)mFragmentCurrentPage).copyChangesTo(draft);
        }
        else if (mFragmentCurrentPage instanceof InspectionStartFragment) {
            ((InspectionStartFragment)mFragmentCurrentPage).copyChangesTo(draft);
        }
    }

}
