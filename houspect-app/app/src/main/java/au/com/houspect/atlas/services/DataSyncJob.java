package au.com.houspect.atlas.services;


import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.j256.ormlite.stmt.QueryBuilder;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.InspectionImageUploadLog;
import au.com.houspect.atlas.models.InspectionSectionImageUploadLog;
import au.com.houspect.atlas.models.RecentInspection;
import au.com.houspect.atlas.utilities.SessionUtil;
import au.com.houspect.atlas.utilities.Util;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by alfredwong on 12/3/17.
 */

public class DataSyncJob extends Job {

    public static final String TAG = "au.com.houspect.atlas.services.DataSyncJob";
    // Defines a custom Intent action
    public static final String BROADCAST_ACTION =
            "au.com.houspect.atlas.services.action.BROADCAST";
    // Defines the key for the status "extra" in an Intent
    public static final String EXTENDED_DATA_MESSAGE =
            "au.com.houspect.atlas.services.action.MESSAGE";

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        synchroniseData(true, getContext());
        return Result.SUCCESS;
    }

    private static Map<String,Boolean> synchronizedHashMap = Collections.synchronizedMap(new HashMap<String, Boolean>());

    public static void cancelAll() {
        Single.fromCallable(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                JobManager.instance().cancelAllForTag(TAG);
                return true;
            }
        }).subscribeOn(Schedulers.io()).subscribe();
        synchronizedHashMap.clear();
    }

    public static void schedulePeriodicJob() {

        Log.i("Atlas", "schedulePeriodicJob");
        final String randomUUID = UUID.randomUUID().toString();
        boolean isScheduled = true;
        synchronizedHashMap.put(randomUUID, isScheduled);

        Single.fromCallable(new Callable<Object>() {
            @Override
            public Object call() throws Exception {

                CountDownLatch signal = new CountDownLatch(1);
                signal.await(30, TimeUnit.SECONDS);

                boolean isScheduled = synchronizedHashMap.containsKey(randomUUID) && synchronizedHashMap.get(randomUUID);
                synchronizedHashMap.remove(randomUUID);
                if (!isScheduled) return false;

                // don't schedule a new job, if one is already scheduled
                //Set<JobRequest> jobRequests = JobManager.instance().getAllJobRequestsForTag(TAG);
                //if (!jobRequests.isEmpty()) {
                //    JobManager.instance().cancelAllForTag(TAG);
                //}
                cancelAll();

                int jobId = new JobRequest.Builder(TAG)
                        .setPeriodic(900*1000) // 15min
                        .setPersisted(true)
                        .build()
                        .schedule();


                return true;
            }
        }).subscribeOn(Schedulers.io()).subscribe();


    }

    public void synchroniseData(boolean shouldAttemptRetry, final Context context) {



        if (!SynchronisedService.isServerActivityInProgress.compareAndSet(false,true)) {

            Log.i("DataSyncJob", "DataSyncJob delayed by 60s as another data synchronisation activity is in progress");
            CountDownLatch retrySignal = new CountDownLatch(1);
            try {
                retrySignal.await(60, TimeUnit.SECONDS);
            } catch (Exception e) {
                Log.e("DataSyncJob", "Error encountered (" + e.getMessage() + ")");
                return;
            }
            if (shouldAttemptRetry) synchroniseData(true, context);
            return;
        }


        final WeakReference<Context> weakContext = new WeakReference<Context>(context);


        GenieService genieService = new GenieServiceImpl();
        final SharedPreferencesService sharedPreferencesService = new SharedPreferencesServiceImpl(context);
        DataService dataService = new DataServiceImpl(context, genieService, sharedPreferencesService);
        try {

            broadcast("Synchronising data", context);


            uploadDrafts(dataService, context);
            uploadRecentInspections(dataService, context);
            uploadPhotos(dataService, sharedPreferencesService, context);

            broadcast("Downloading data", context);

            final CountDownLatch signal = new CountDownLatch(1);
            dataService.refreshJobsAndInspections(weakContext.get(), new DataService.RefreshCallback() {
                @Override
                public void onSuccess() {
                    showToastOnUIThread("Jobs & inspections downloaded successfully.", weakContext.get());
                    signal.countDown();
                }

                @Override
                public void onError(Throwable e) {
                    showToastOnUIThread("Failed to download jobs & inspections (" + e.getMessage() + ").", weakContext.get());
                    signal.countDown();
                }
            });

            signal.await(90, TimeUnit.SECONDS);

            // loop through RecentInspection, for those that are isFinal(), look for Zeus Draft (that is not pending to upload) and delete
            List<RecentInspection> recentInspections = dataService.getList(null, RecentInspection.class);
            for (RecentInspection recentInspection : recentInspections) {
                boolean shouldLookForDraftToDelete = recentInspection.isFinal();
                if (!shouldLookForDraftToDelete) continue;

                Draft draftToDelete = dataService.get(recentInspection.getId(), Draft.class);
                if (draftToDelete == null) continue;

                // only delete draft if the id is matching + Atlas Draft or Zeus Draft
                boolean shouldDeleteDraft = draftToDelete.isAtlasDraft() || draftToDelete.isZeusDraft();
                if (!shouldDeleteDraft) continue;

                dataService.remove(draftToDelete.getId(), Draft.class);
            }

            broadcast("", context);

            SynchronisedService.isServerActivityInProgress.set(false);

        } catch (Exception e) {

            final String message = e.getMessage();
            showToastOnUIThread("Failed to synchronise data (" + message + ").", weakContext.get());
            broadcast("", context);
            Log.d("DataSyncJob", message);
            SynchronisedService.isServerActivityInProgress.set(false);
        }
    }

    /**
     * Internal function for uploading functions.
     * @param dataService
     * @param sharedPreferencesService
     * @param context
     * @throws Exception
     */
    private void uploadPhotos(DataService dataService, SharedPreferencesService sharedPreferencesService, Context context) throws Exception {
        boolean shouldSkipUploadPhoto = !sharedPreferencesService.getShouldUploadPhotosWhenOnline();

        if (shouldSkipUploadPhoto) {
            broadcast("Skipping photo upload as per confguration in settings.", context);
        }
        else {
            broadcast("Uploading photos", context);
            synchronisePhotos(dataService, context);
        }
    }

    /**
     * Internal function for uploading RecentInspections that are pendingForUploadToServer.
     * @param dataService
     * @param context
     * @throws Exception
     */
    private void uploadRecentInspections(DataService dataService, Context context) throws Exception {

        final WeakReference<Context> weakContext = new WeakReference<Context>(context);

        List<RecentInspection> recentInspections = dataService.getList(new DataService.GetCallback<RecentInspection>() {
            @Override
            public QueryBuilder<RecentInspection, ?> preQuery(QueryBuilder<RecentInspection, ?> queryBuilder) {
                try {
                    queryBuilder.where().eq("isPendingUploadToServer", true);
                } catch (Exception e) {
                    Log.e("Atlas - DataSyncJob", e.getLocalizedMessage());
                }
                return queryBuilder;
            }
        }, RecentInspection.class);
        for (final RecentInspection recentInspection : recentInspections) {

            boolean shouldSkip = !recentInspection.getPendingUploadToServer();
            if (shouldSkip) continue;

            broadcast("Retry submitting Zeus Final Report (" + recentInspection.getCode() + ")", context);

            final CountDownLatch uploadSignal = new CountDownLatch(1);

            dataService.retrySubmitFinalReport(recentInspection, new DataService.UploadCallback() {
                @Override
                public void onSuccess() {
                    showToastOnUIThread("Zeus Final Report (" + recentInspection.getCode() + ") submitted successfully.", weakContext.get());
                    uploadSignal.countDown();
                }

                @Override
                public void onEror(Throwable e) {
                    showToastOnUIThread("Cannot submit Zeus Final Report (" + recentInspection.getCode() + "). " + e.getLocalizedMessage(), weakContext.get());
                    uploadSignal.countDown();
                }
            });

            uploadSignal.await(30, TimeUnit.SECONDS);
        }
    }

    /**
     * Internal functions for uploading Atlas Drafts (repeated upload regardless of previous uploadedTime) and Zeus Drafts not already uploaded to the server
     * @param dataService
     * @param context
     * @throws Exception
     */
    private void uploadDrafts(DataService dataService, Context context) throws Exception {

        final WeakReference<Context> weakContext = new WeakReference<Context>(context);

        List<Draft> drafts = dataService.getList(new DataService.GetCallback<Draft>() {
            @Override
            public QueryBuilder<Draft, ?> preQuery(QueryBuilder<Draft, ?> queryBuilder) {
                try {
                    queryBuilder.where().ne("mode", Draft.MODE_ZEUS_FINAL_REPORT);
                } catch (Exception e) {
                    Log.e("Atlas - DataSyncJob", e.getLocalizedMessage());
                }
                return queryBuilder;
            }
        }, Draft.class);
        Log.d("DataSyncJob", "Drafts " + drafts.size());
        for (final Draft draft : drafts) {

            // Populate property code
            Util.parseInspectionXml(draft.getXml(), draft);

            boolean shouldSkip = SessionUtil.singleton.getDraftInEdit() != null && draft.getId().equals(SessionUtil.singleton.getDraftInEdit().getId());
            if (shouldSkip) {
                showToastOnUIThread("Upload skipped. Draft (" + draft.getCode() + ") is being edited.", weakContext.get());
                continue;
            }


            Log.d("DataSyncJob", "Draft " + draft.getCode());

            // TODO:: review logic in the future, at the moment, business logic is evolving, relaxing constraint
            //boolean hasImageUploadError = imageUploadResults.containsKey("hasError");
            //if (hasImageUploadError) {
            //    broadcast("Upload skipped. Draft (" + draft.getCode() + ") has pending images stored offline that are not yet successfully uploaded.");
            //    continue;
            //}

            // skip Zeus Draft upload if uploadedTime > 0 - reason - already uploaded - it is stored in app for display only
            shouldSkip = draft.isZeusDraft() && !draft.isPendingForUploadToServer();
            if (shouldSkip) continue;

            // skip Zeus Final Report upload - postpone - check RecentInspection where pendingUploadToServer=true, get corresponding Draft and upload
            // note - this such draft shouldnt exist in the drafts list unless - the ORMLite QueryBuilder.where.ne throws exception or has error
            shouldSkip = draft.isZeusFinalReport();
            if (shouldSkip) continue;

            Log.d("DataSyncJob", "Uploading Draft " + draft.getCode());

            broadcast("Uploading draft (" + draft.getCode() + ")", context);
            final CountDownLatch uploadSignal = new CountDownLatch(1);
            dataService.uploadDraft(draft, new DataService.UploadCallback() {
                @Override
                public void onSuccess() {
                    showToastOnUIThread("Draft (" + draft.getCode() + ") uploaded successfully.", weakContext.get());
                    uploadSignal.countDown();
                }

                @Override
                public void onEror(Throwable e) {
                    String reason = e.getMessage();
                    if (reason != null && reason.length() > 0) reason = " (" + reason + ")";
                    else reason = "";
                    showToastOnUIThread("Failed to upload draft (" + draft.getCode() + "). Reason: " + reason + ".", weakContext.get());
                    uploadSignal.countDown();
                }
            });
            uploadSignal.await(30, TimeUnit.SECONDS);
        }
    }

    public void synchronisePhotos(DataService dataService, final Context context) throws Exception {

        final WeakReference<Context> weakContext = new WeakReference<Context>(context);

        // check if there are pending photos
        List<InspectionSectionImageUploadLog> inspectionSectionImageUploadLogs =
                dataService.getList(null, InspectionSectionImageUploadLog.class);

        List<InspectionImageUploadLog> inspectionImageUploadLogs =
                dataService.getList(null, InspectionImageUploadLog.class);

        final HashMap<String, Boolean> imageUploadResults = new HashMap<>();
        int count = 1;
        final int totalNumberOfPhotos = inspectionSectionImageUploadLogs.size() + inspectionImageUploadLogs.size();
        Log.i("Atlas", "Uploading " + totalNumberOfPhotos + " photo instructions");
        for (final InspectionSectionImageUploadLog inspectionSectionImageUploadLog : inspectionSectionImageUploadLogs) {

            String message = !inspectionSectionImageUploadLog.isForDeletion()
                                ? "Uploading photo " + count + "/" + totalNumberOfPhotos
                                : "Request photo deletion " + count + "/" + totalNumberOfPhotos;

            broadcast(message, context);

            final int currentPhoto = count;
            final CountDownLatch uploadImageSignal = new CountDownLatch(1);
            dataService.uploadImage(inspectionSectionImageUploadLog, new DataService.UploadCallback() {
                @Override
                public void onSuccess() {
                    String successMessage = !inspectionSectionImageUploadLog.isForDeletion()
                                            ? "Uploaded photo " + currentPhoto + "/" + totalNumberOfPhotos + " successfully"
                                            : "Requested photo deletion " + currentPhoto + "/" + totalNumberOfPhotos + " successfully";
                    showToastOnUIThread(successMessage, weakContext.get());
                    uploadImageSignal.countDown();
                }

                @Override
                public void onEror(Throwable e) {
                    String reason = e.getMessage();
                    if (reason != null && reason.length() > 0)
                        reason = " (" + reason + ")";
                    else reason = "";

                    String errorMessage = inspectionSectionImageUploadLog.isForDeletion()
                                            ? "Failed to upload photo " + currentPhoto + "/" + totalNumberOfPhotos + ". " + reason + "."
                                            : "Failed to request photo deletion" + currentPhoto + "/" + totalNumberOfPhotos + ". " + reason + ".";
                    showToastOnUIThread(errorMessage, weakContext.get());
                    imageUploadResults.put("hasError", true);
                    uploadImageSignal.countDown();
                }
            });

            uploadImageSignal.await(30, TimeUnit.SECONDS);
            count++;
        }

        for (final InspectionImageUploadLog inspectionImageUploadLog : inspectionImageUploadLogs) {

            String message = !inspectionImageUploadLog.isForDeletion()
                    ? "Uploading photo " + count + "/" + totalNumberOfPhotos
                    : "Request photo deletion " + count + "/" + totalNumberOfPhotos;

            broadcast(message, context);

            final int currentPhoto = count;
            final CountDownLatch uploadImageSignal = new CountDownLatch(1);
            dataService.uploadImage(inspectionImageUploadLog, new DataService.UploadCallback() {
                @Override
                public void onSuccess() {
                    String successMessage = !inspectionImageUploadLog.isForDeletion()
                            ? "Uploaded photo " + currentPhoto + "/" + totalNumberOfPhotos + " successfully"
                            : "Requested photo deletion " + currentPhoto + "/" + totalNumberOfPhotos + " successfully";
                    showToastOnUIThread(successMessage, weakContext.get());
                    uploadImageSignal.countDown();
                }

                @Override
                public void onEror(Throwable e) {
                    String reason = e.getMessage();
                    if (reason != null && reason.length() > 0)
                        reason = " (" + reason + ")";
                    else reason = "";

                    String errorMessage = inspectionImageUploadLog.isForDeletion()
                            ? "Failed to upload photo " + currentPhoto + "/" + totalNumberOfPhotos + ". " + reason + "."
                            : "Failed to request photo deletion" + currentPhoto + "/" + totalNumberOfPhotos + ". " + reason + ".";
                    showToastOnUIThread(errorMessage, weakContext.get());
                    imageUploadResults.put("hasError", true);
                    uploadImageSignal.countDown();
                }
            });

            uploadImageSignal.await(30, TimeUnit.SECONDS);
            count++;
        }
    }

    public static void broadcast(String message, Context context) {
        Intent localIntent =
                new Intent(BROADCAST_ACTION)
                        // Puts the status into the Intent
                        .putExtra(EXTENDED_DATA_MESSAGE, message);
        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);
    }

    public static void showToastOnUIThread(final String message, Context context) {
        final WeakReference<Context> weakReference = new WeakReference<Context>(context);
        (new Handler(Looper.getMainLooper())).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(weakReference.get(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
