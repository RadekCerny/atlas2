package au.com.houspect.atlas.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import java.util.List;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.fragments.JobListFragment;
import au.com.houspect.atlas.fragments.dummy.DummyContent.DummyItem;
import au.com.houspect.atlas.models.InspectionSection;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link JobListFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class InspectionEndSectionItemRecyclerViewAdapter extends RecyclerView.Adapter<InspectionEndSectionItemRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private OnInteractionListener mListener;

    private final List<InspectionSection> mValues;
    private int mItemResourceId;

    public InspectionEndSectionItemRecyclerViewAdapter(List<InspectionSection> items,
                                                       int itemResourceId,
                                                       OnInteractionListener listener) {

        mValues = items;
        mItemResourceId = itemResourceId;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext)
                .inflate(mItemResourceId, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final InspectionSection item = mValues.get(position);
        final ItemViewHolder itemViewHolder = (ItemViewHolder)holder;
        itemViewHolder.mItem = item;


        int checkedResourceId = item.isComplete() ? R.mipmap.ic_checked_green : R.mipmap.ic_checked_red;
        itemViewHolder.mCheckedTextView.setCheckMarkDrawable(checkedResourceId);
        itemViewHolder.mCheckedTextView.setText(item.getName() + " " + item.getInstanceName());
        itemViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSelection(item, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
        }
    }


    class ItemViewHolder extends ViewHolder {

        public final CheckedTextView mCheckedTextView;
        public InspectionSection mItem;

        public ItemViewHolder(View view) {
            super(view);

            mCheckedTextView = (CheckedTextView) view.findViewById(R.id.checkedTextView);
        }
    }

    public interface OnInteractionListener {
        void onSelection(InspectionSection inspectionSection, int sectionIndex);
    }

}
