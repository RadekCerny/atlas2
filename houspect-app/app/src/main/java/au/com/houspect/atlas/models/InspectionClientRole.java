package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alfredwong on 29/06/17.
 */

public class InspectionClientRole implements Parcelable {

    private String id;
    private String group;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public InspectionClientRole() {}

    //region Parcelable
    protected InspectionClientRole(Parcel in) {
        id = in.readString();
        group = in.readString();
        description = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(group);
        dest.writeString(description);
    }

    public static final Creator CREATOR = new Creator() {

        @Override
        public InspectionClientRole createFromParcel(Parcel source) {
            return new InspectionClientRole(source);
        }

        @Override
        public InspectionClientRole[] newArray(int size) {
            return new InspectionClientRole[size];
        }
    };
    //endregion

}
