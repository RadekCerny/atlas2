package au.com.houspect.atlas.services;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;

import com.google.common.base.Function;
import com.saasplications.genie.configuraiton.UserType;
import com.saasplications.genie.domain.client.Response;
import com.saasplications.genie.domain.client.ResponseStatus;
import com.saasplications.genie.domain.client.activity.ActivityClient;
import com.saasplications.genie.domain.client.activity.ActivityClientDefaultImpl;
import com.saasplications.genie.domain.client.activity.payload.CreateActivityRequest;
import com.saasplications.genie.domain.client.activity.payload.DataPublicationRequest;
import com.saasplications.genie.domain.client.activity.payload.ForgotPasswordRequest;
import com.saasplications.genie.domain.client.activity.payload.MethodInvocationRequest;
import com.saasplications.genie.domain.client.activity.sidepayload.UploadFileRequest;
import com.saasplications.genie.domain.client.login.LoginClient;
import com.saasplications.genie.domain.client.login.LoginClientDefaultImpl;
import com.saasplications.genie.domain.client.login.payload.SessionRequest;
import com.saasplications.genie.domain.client.session.SessionDataClient;
import com.saasplications.genie.domain.client.session.SessionDataClientDefaultImpl;
import com.saasplications.genie.domain.client.session.payload.Ping;
import com.saasplications.genie.domain.model.ActivityInstance;
import com.saasplications.genie.domain.model.GridData;
import com.saasplications.genie.domain.model.SessionContext;

import org.w3c.dom.Element;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.houspect.atlas.BuildConfig;
import au.com.houspect.atlas.models.CalendarItem;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionClient;
import au.com.houspect.atlas.models.InspectionImageUploadLog;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.models.InspectionSectionImageUploadLog;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.models.RecentInspection;
import au.com.houspect.atlas.utilities.MediaUtil;
import au.com.houspect.atlas.utilities.Util;
import io.reactivex.Single;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

/**
 * Created by alfredwong on 12/02/2017.
 */

public class GenieServiceImpl implements GenieService {


    private final String execXUrl = "http://crp.saasplications.com.au/GenieService/genieservice.svc/restish/ExecX";
    private final String execAnonymousXUrl = "http://crp.saasplications.com.au/GenieService/genieservice.svc/restish/ExecAnonymousX";
    private final String createSessionXUrl = "http://crp.saasplications.com.au/GenieService/genieservice.svc/restish/CreateSessionX";
    private final String pingUrl = "http://crp.saasplications.com/GenieService/genieservice.svc/restish/Ping";
    private final String site = BuildConfig.SITE; // "HOUSPECTTEST";

    public void login(String username, String password, final GenieServiceCallback<GenieServiceResponse<String>> callback) {

        String loginUrl = createSessionXUrl;
        LoginClient loginClient = new LoginClientDefaultImpl(loginUrl);
        SessionRequest sessionRequest = new SessionRequest(username, password, site, UserType.Primary);

        loginClient.createSession(sessionRequest, new Function1<Response<SessionContext>, Unit>() {
            @Override
            public Unit invoke(Response<SessionContext> sessionContextResponse) {
                boolean isSuccess = "SUCCESS".equalsIgnoreCase(sessionContextResponse.getStatus().name());
                GenieServiceResponse response = null;
                if (isSuccess) {
                    //getSessionData(sessionContextResponse.getResult().getSessionToken(), callback);
                    response = new GenieServiceResponse(true, sessionContextResponse.getResult().getSessionToken(), null);
                }
                else {
                    //callback.completed(null);
                    response = new GenieServiceResponse(false, null, sessionContextResponse.getMessage());
                }
                callback.completed(response);
                return Unit.INSTANCE;
            }
        });
    }

    @Override
    public void getJobs(String sessionToken,
                        ActivityInstance activityInstance,
                        final GenieServiceCallback<GenieServiceResponse<List<Job>>> callback) {

        getActivityData(sessionToken, activityInstance,
                "listMyInspections", new GenieServiceCallback<GenieServiceResponse<List<Job>>>() {
                    @Override
                    public void completed(final GenieServiceResponse<List<Job>> jobsResponse) {

                        GenieServiceResponse response = null;

                        if (!jobsResponse.isSuccess()) {
                            response = new GenieServiceResponse(false, null, jobsResponse.getErrorMessage());
                            callback.completed(response);
                            return;
                        }

                        response = new GenieServiceResponse(true, jobsResponse.getData(), null);
                        callback.completed(response);

                    }
                }, getJobsResponseConverter());

    }

    @Override
    public void getInspections(String sessionToken,
                               ActivityInstance activityInstance,
                               final GenieServiceCallback<GenieServiceResponse<List<Inspection>>> callback) {

            getActivityData(sessionToken, activityInstance,
                    "exportSelected", new GenieServiceCallback<GenieServiceResponse<List<Inspection>>>() {
                        @Override
                        public void completed(GenieServiceResponse<List<Inspection>> inspectionsResponse) {

                            GenieServiceResponse response = null;

                            if (!inspectionsResponse.isSuccess()) {
                                response = new GenieServiceResponse(false, null, inspectionsResponse.getErrorMessage());
                                callback.completed(response);
                                return;
                            }

                            response = new GenieServiceResponse(true, inspectionsResponse.getData(), null);
                            callback.completed(response);
                        }
                    }, getInspectionsResponseConverter());
    }

    @Override
    public void getRecentInspections(String sessionToken,
                                     ActivityInstance activityInstance,
                                     final GenieServiceCallback<GenieServiceResponse<List<RecentInspection>>> callback) {
        getActivityData(sessionToken, activityInstance,
                "listMyRecentInspections", new GenieServiceCallback<GenieServiceResponse<List<RecentInspection>>>() {
                    @Override
                    public void completed(GenieServiceResponse<List<RecentInspection>> inspectionsResponse) {

                        GenieServiceResponse response = null;

                        if (!inspectionsResponse.isSuccess()) {
                            response = new GenieServiceResponse(false, null, inspectionsResponse.getErrorMessage());
                            callback.completed(response);
                            return;
                        }

                        response = new GenieServiceResponse(true, inspectionsResponse.getData(), null);
                        callback.completed(response);
                    }
                }, getRecentInspectionsResponseConverter());
    }

    @Override
    public void getRecentInspectionsForDraftDeletion(String sessionToken,
                                                     ActivityInstance activityInstance,
                                                     final GenieServiceCallback<GenieServiceResponse<List<RecentInspection>>> callback) {
        getActivityData(sessionToken, activityInstance,
                "listMyRecentInspectionsAll", new GenieServiceCallback<GenieServiceResponse<List<RecentInspection>>>() {
                    @Override
                    public void completed(GenieServiceResponse<List<RecentInspection>> inspectionsResponse) {

                        GenieServiceResponse response = null;

                        if (!inspectionsResponse.isSuccess()) {
                            response = new GenieServiceResponse(false, null, inspectionsResponse.getErrorMessage());
                            callback.completed(response);
                            return;
                        }

                        response = new GenieServiceResponse(true, inspectionsResponse.getData(), null);
                        callback.completed(response);
                    }
                }, getRecentInspectionsResponseConverter());
    }

    @Override
    public Single<GenieServiceResponse<List<CalendarItem>>> getUpcomingCalendarItems(String sessionToken, ActivityInstance activityInstance) {
        return Single.create(subscriber -> {

            getActivityData(sessionToken, activityInstance,
                    "listUpcomingCalendarItems", new GenieServiceCallback<GenieServiceResponse<List<CalendarItem>>>() {
                        @Override
                        public void completed(GenieServiceResponse<List<CalendarItem>> calendarItemsResponse) {

                            GenieServiceResponse response = null;

                            if (!calendarItemsResponse.isSuccess()) {
                                response = new GenieServiceResponse(false, null, calendarItemsResponse.getErrorMessage());
                                subscriber.onSuccess(response);
                                return;
                            }

                            response = new GenieServiceResponse(true, calendarItemsResponse.getData(), null);
                            subscriber.onSuccess(response);

                        }
                    }, getCalendarItemsResponseConverter());
        });
    }

    @Override
    public void createActivity(String sessionToken, GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback) {
        createActivity(sessionToken, callback, jobDataPublication(), recentInspectionDataPublication(), upcomingCalendarItemDataPublication());
    }

    @Override
    public void createActivity(String sessionToken,
                               final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback,
                               DataPublicationRequest ... dataPublicationRequests) {
        final ActivityClient activityClient = new ActivityClientDefaultImpl(execXUrl);
        //List<DataPublicationRequest> dataPublicationRequests = new ArrayList<>();
        //dataPublicationRequests.add(new DataPublicationRequest("lvTransactions","listMyInspections","",false,false,""));
        //dataPublicationRequests.add(new DataPublicationRequest("recentInspections","listMyRecentInspections","",false,false,""));
        //dataPublicationRequests.add(new DataPublicationRequest("recentInspections","listMyRecentInspections","",false,false,""));
        CreateActivityRequest request = new CreateActivityRequest("ERP.PropertyInspection",
                "ImportExport",
                null,
                sessionToken,
                dataPublicationRequests,
                //dataPublicationRequests.toArray(new DataPublicationRequest[1]),
                true);

        activityClient.create(request, new Function1<Response<ActivityInstance>, Unit>() {
            @Override
            public Unit invoke(final Response<ActivityInstance> activityInstanceResponse) {
                GenieServiceResponse response = null;
                if (activityInstanceResponse.getStatus() == ResponseStatus.SUCCESS) {
                    response = new GenieServiceResponse(true, activityInstanceResponse.getResult(), null);
                }
                else {
                    response = new GenieServiceResponse(false, null, activityInstanceResponse.getMessage());
                }
                callback.completed(response);
                return Unit.INSTANCE;
            }
        });
    }

    @Override
    public DataPublicationRequest jobDataPublication() {
        return new DataPublicationRequest("lvTransactions","listMyInspections","",false,false,"");
    }

    @Override
    public DataPublicationRequest recentInspectionDataPublication() {
        return new DataPublicationRequest("recentInspections","listMyRecentInspections","",false,false,"");
    }

    @Override
    public DataPublicationRequest recentInspectionForDraftDeletionDataPublication() {
        return new DataPublicationRequest("recentInspections","listMyRecentInspectionsAll","",false,false,"");
    }

    @Override
    public DataPublicationRequest upcomingCalendarItemDataPublication() {
        return new DataPublicationRequest("myCalendar", "listUpcomingCalendarItems", "", false, false, "");
    }

    @Override
    public void uploadDraft(Draft draft,
                            String sessionToken,
                            ActivityInstance activityInstance,
                            final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback) {
        final ActivityClient activityClient = new ActivityClientDefaultImpl(execXUrl);
        MethodInvocationRequest methodInvocationRequest = new MethodInvocationRequest(activityInstance, "importFromOffline", sessionToken, draft.getXml(), null, null);
        activityClient.invoke(methodInvocationRequest, new Function1<Response<ActivityInstance>, Unit>() {
            @Override
            public Unit invoke(Response<ActivityInstance> activityInstanceResponse) {
                GenieServiceResponse response;
                if (activityInstanceResponse.getStatus() == ResponseStatus.SUCCESS) {
                    response = new GenieServiceResponse(true, activityInstanceResponse.getResult(), null);
                }
                else {
                    response = new GenieServiceResponse(false, null, activityInstanceResponse.getMessage());
                }
                callback.completed(response);
                return Unit.INSTANCE;
            }
        });
    }

    @Override
    public void notifyDraftIsActive(Draft draft,
                                    String sessionToken,
                                    ActivityInstance activityInstance,
                                    final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback) {
        final ActivityClient activityClient = new ActivityClientDefaultImpl(execXUrl);
        MethodInvocationRequest methodInvocationRequest = new MethodInvocationRequest(activityInstance, "importFromOffline", sessionToken, draft.getPropertyInspectionElementOnly(true), null, null);
        activityClient.invoke(methodInvocationRequest, new Function1<Response<ActivityInstance>, Unit>() {
            @Override
            public Unit invoke(Response<ActivityInstance> activityInstanceResponse) {
                GenieServiceResponse response;
                if (activityInstanceResponse.getStatus() == ResponseStatus.SUCCESS) {
                    response = new GenieServiceResponse(true, activityInstanceResponse.getResult(), null);
                }
                else {
                    response = new GenieServiceResponse(false, null, activityInstanceResponse.getMessage());
                }
                callback.completed(response);
                return Unit.INSTANCE;
            }
        });
    }

    @Override
    public void uploadImage(InspectionSectionImageUploadLog inspectionSectionImageUploadLog,
                            int width,
                            int height,
                            String imageAsBase64String,
                            boolean shouldGzip,
                            String sessionToken,
                            ActivityInstance activityInstance,
                            final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback) {

        final ActivityClient activityClient = new ActivityClientDefaultImpl(execXUrl);
        UploadFileRequest uploadFileRequest = new UploadFileRequest(inspectionSectionImageUploadLog.getFileName(), "BASE64", imageAsBase64String, width+"", height+"", inspectionSectionImageUploadLog.isForDeletion());
        Map<String,String> attributesKeyValue = new HashMap<>();
        attributesKeyValue.put("contextObject", "Sections.Images");
        attributesKeyValue.put("inspectionId", inspectionSectionImageUploadLog.getInspectionId());
        attributesKeyValue.put("sectionKey", inspectionSectionImageUploadLog.getSectionKey());
        attributesKeyValue.put("sectionId", inspectionSectionImageUploadLog.getSectiondId());
        if (inspectionSectionImageUploadLog.getRepeatingKey() != null) attributesKeyValue.put("repeatingKey", inspectionSectionImageUploadLog.getRepeatingKey());
        if (inspectionSectionImageUploadLog.getRepeatingSeq() != null) attributesKeyValue.put("repeatingSeq", inspectionSectionImageUploadLog.getRepeatingSeq());
        MethodInvocationRequest methodInvocationRequest =
                new MethodInvocationRequest
                        (activityInstance,
                        "acquireFromSession",
                                sessionToken,
                                "",
                                uploadFileRequest,
                                attributesKeyValue);
        activityClient.invoke(methodInvocationRequest, shouldGzip, new Function1<Response<ActivityInstance>, Unit>() {
            @Override
            public Unit invoke(Response<ActivityInstance> activityInstanceResponse) {
                GenieServiceResponse response;
                if (activityInstanceResponse.getStatus() == ResponseStatus.SUCCESS) {
                    response = new GenieServiceResponse(true, activityInstanceResponse.getResult(), null);
                }
                else {
                    response = new GenieServiceResponse(false, null, activityInstanceResponse.getMessage());
                }
                callback.completed(response);
                return Unit.INSTANCE;
            }
        });
    }

    @Override
    public void uploadImage(InspectionImageUploadLog inspectionImageUploadLog,
                            int width,
                            int height,
                            String imageAsBase64String,
                            boolean shouldGzip,
                            String sessionToken,
                            ActivityInstance activityInstance,
                            final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback) {

        final ActivityClient activityClient = new ActivityClientDefaultImpl(execXUrl);
        UploadFileRequest uploadFileRequest = new UploadFileRequest(inspectionImageUploadLog.getFileName(), "BASE64", imageAsBase64String, width+"", height+"", inspectionImageUploadLog.isForDeletion());
        Map<String,String> attributesKeyValue = new HashMap<>();
        attributesKeyValue.put("attachmentKey", "CoverPhoto");
        attributesKeyValue.put("inspectionId", inspectionImageUploadLog.getInspectionId());
        MethodInvocationRequest methodInvocationRequest =
                new MethodInvocationRequest
                        (activityInstance,
                                "acquireFromSession",
                                sessionToken,
                                "",
                                uploadFileRequest,
                                attributesKeyValue);
        activityClient.invoke(methodInvocationRequest, shouldGzip, new Function1<Response<ActivityInstance>, Unit>() {
            @Override
            public Unit invoke(Response<ActivityInstance> activityInstanceResponse) {
                GenieServiceResponse response;
                if (activityInstanceResponse.getStatus() == ResponseStatus.SUCCESS) {
                    response = new GenieServiceResponse(true, activityInstanceResponse.getResult(), null);
                }
                else {
                    response = new GenieServiceResponse(false, null, activityInstanceResponse.getMessage());
                }
                callback.completed(response);
                return Unit.INSTANCE;
            }
        });
    }

    @Override
    public void forgotPassword(String username,
                               String email,
                               final GenieServiceCallback<GenieServiceResponse<String>> callback) {
        final ActivityClient activityClient = new ActivityClientDefaultImpl(execAnonymousXUrl);
        ForgotPasswordRequest request = new ForgotPasswordRequest(site, username, email);
        activityClient.forgotPassword(request, new Function1<Response<Unit>, Unit>() {
            @Override
            public Unit invoke(Response<Unit> unitResponse) {
                GenieServiceResponse response;
                if (unitResponse.getStatus() == ResponseStatus.SUCCESS) {
                    response = new GenieServiceResponse(true, null, null);
                } else {
                    response = new GenieServiceResponse(false, null, unitResponse.getMessage());
                }
                callback.completed(response);
                return Unit.INSTANCE;
            }
        });
    }

    @Override
    public void ping(String sessionToken, final GenieServiceCallback<GenieServiceResponse<String>> callback) {
        final SessionDataClient sessionDataClient = new SessionDataClientDefaultImpl(pingUrl);
        Ping ping = new Ping(sessionToken);
        sessionDataClient.ping(ping, new Function1<Response<Unit>, Unit>() {
            @Override
            public Unit invoke(Response<Unit> unitResponse) {
                GenieServiceResponse response;
                if (unitResponse.getStatus() == ResponseStatus.SUCCESS) {
                    response = new GenieServiceResponse(true, null, null);
                } else {
                    response = new GenieServiceResponse(false, null, unitResponse.getMessage());
                }
                callback.completed(response);
                return Unit.INSTANCE;
            }
        });
    }

    //region Private methods
    private <T> void getActivityData(String sessionToken,
                                    ActivityInstance activityInstance,
                                    String methodName,
                                    final GenieServiceCallback<GenieServiceResponse<List<T>>> callback,
                                    final Function<Response<ActivityInstance>,GenieServiceResponse<List<T>>> responseConverter) {
        final ActivityClient activityClient = new ActivityClientDefaultImpl(execXUrl);
        MethodInvocationRequest methodInvocationRequest = new MethodInvocationRequest(activityInstance, methodName, sessionToken, null, null, null);
        activityClient.invoke(methodInvocationRequest, new Function1<Response<ActivityInstance>, Unit>() {
            @Override
            public Unit invoke(Response<ActivityInstance> activityInstanceResponse) {
                GenieServiceResponse response = responseConverter.apply(activityInstanceResponse);
                callback.completed(response);
                return Unit.INSTANCE;
            }
        });

    }

    private Function<Response<ActivityInstance>,GenieServiceResponse<List<Job>>> getJobsResponseConverter() {

        Function<Response<ActivityInstance>,GenieServiceResponse<List<Job>>> function = new Function<Response<ActivityInstance>, GenieServiceResponse<List<Job>>>() {
            @Nullable
            @Override
            public GenieServiceResponse<List<Job>> apply(Response<ActivityInstance> activityInstanceResponse) {
                GenieServiceResponse response = null;
                if (activityInstanceResponse.getStatus().equals(ResponseStatus.SUCCESS)) {
                    boolean hasDataError = false;
                    hasDataError = activityInstanceResponse.getResult().getDataSets().size() == 0;
                    if (!hasDataError) hasDataError = !(activityInstanceResponse.getResult().getDataSets().get(0) instanceof GridData);

                    if (!hasDataError) {

                        GridData gridData = (GridData) activityInstanceResponse.getResult().getDataSets().get(0);
                        List<Job> jobs = Util.parseJobsGenieGridData(gridData);


                        response = new GenieServiceResponse(true, jobs, null);
                    }
                    else {
                        response = new GenieServiceResponse(false, null, "Error encountered parsing data received from server (Missing GridData).");
                    }

                } else {
                    response = new GenieServiceResponse(false, null, activityInstanceResponse.getMessage());
                }
                return response;
            }
        };

        return function;
    }

    private Function<Response<ActivityInstance>,GenieServiceResponse<List<Inspection>>> getInspectionsResponseConverter() {

        Function<Response<ActivityInstance>,GenieServiceResponse<List<Inspection>>> function = new Function<Response<ActivityInstance>, GenieServiceResponse<List<Inspection>>>() {
            @Nullable
            @Override
            public GenieServiceResponse<List<Inspection>> apply(Response<ActivityInstance> activityInstanceResponse) {
                GenieServiceResponse response = null;
                if (activityInstanceResponse.getStatus().equals(ResponseStatus.SUCCESS)) {

                    List<Inspection> inspections = new ArrayList<>();
                    for (Element element : activityInstanceResponse.getResult().getOtherElements()) {
                        if (!element.getTagName().equals("PropertyInspection")) continue;
                        Inspection inspection = new Inspection();
                        Util.parseInspectionXml(element, inspection);
                        String nodeXml = Util.xmlNodeToString(element);
                        inspection.setXml(nodeXml);
                        inspections.add(inspection);
                    }
                    response = new GenieServiceResponse(true, inspections, null);

                } else {
                    response = new GenieServiceResponse(false, null, activityInstanceResponse.getMessage());
                }
                return response;
            }
        };

        return function;
    }

    private Function<Response<ActivityInstance>,GenieServiceResponse<List<RecentInspection>>> getRecentInspectionsResponseConverter() {

        Function<Response<ActivityInstance>,GenieServiceResponse<List<RecentInspection>>> function = new Function<Response<ActivityInstance>, GenieServiceResponse<List<RecentInspection>>>() {
            @Nullable
            @Override
            public GenieServiceResponse<List<RecentInspection>> apply(Response<ActivityInstance> activityInstanceResponse) {
                GenieServiceResponse response = null;
                if (activityInstanceResponse.getStatus().equals(ResponseStatus.SUCCESS)) {
                    boolean hasDataError = false;
                    hasDataError = activityInstanceResponse.getResult().getDataSets().size() == 0;
                    if (!hasDataError) hasDataError = !(activityInstanceResponse.getResult().getDataSets().get(0) instanceof GridData);

                    if (!hasDataError) {

                        GridData gridData = (GridData) activityInstanceResponse.getResult().getDataSets().get(0);
                        List<RecentInspection> recentInspections = Util.parseRecentInspectionsGenieGridData(gridData);


                        response = new GenieServiceResponse(true, recentInspections, null);
                    }
                    else {
                        response = new GenieServiceResponse(false, null, "Error encountered parsing data received from server (Missing GridData).");
                    }

                } else {
                    response = new GenieServiceResponse(false, null, activityInstanceResponse.getMessage());
                }
                return response;
            }
        };

        return function;
    }

    private Function<Response<ActivityInstance>,GenieServiceResponse<List<CalendarItem>>> getCalendarItemsResponseConverter() {

        Function<Response<ActivityInstance>,GenieServiceResponse<List<CalendarItem>>> function = new Function<Response<ActivityInstance>, GenieServiceResponse<List<CalendarItem>>>() {
            @Nullable
            @Override
            public GenieServiceResponse<List<CalendarItem>> apply(Response<ActivityInstance> activityInstanceResponse) {
                GenieServiceResponse response = null;
                if (activityInstanceResponse.getStatus().equals(ResponseStatus.SUCCESS)) {
                    boolean hasDataError = false;
                    hasDataError = activityInstanceResponse.getResult().getDataSets().size() == 0;
                    if (!hasDataError) hasDataError = !(activityInstanceResponse.getResult().getDataSets().get(0) instanceof GridData);

                    if (!hasDataError) {

                        GridData gridData = (GridData) activityInstanceResponse.getResult().getDataSets().get(0);
                        List<CalendarItem> calendarItems = Util.parseCalendarItemsGenieGridData(gridData);


                        response = new GenieServiceResponse(true, calendarItems, null);
                    }
                    else {
                        response = new GenieServiceResponse(false, null, "Error encountered parsing data received from server (Missing GridData).");
                    }

                } else {
                    response = new GenieServiceResponse(false, null, activityInstanceResponse.getMessage());
                }
                return response;
            }
        };

        return function;
    }
    //endregion

}
