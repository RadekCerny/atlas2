package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alfredwong on 1/4/17.
 */

public class InspectionSectionImage extends IdBase implements Parcelable {
    private String fileName;
    private String label;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public InspectionSectionImage() {}

    //region Parcelable
    private InspectionSectionImage(Parcel in) {
        setId(in.readString());
        fileName = in.readString();
        label = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(fileName);
        dest.writeString(label);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public InspectionSectionImage createFromParcel(Parcel source) {
            return new InspectionSectionImage(source);
        }

        @Override
        public InspectionSectionImage[] newArray(int size) {
            return new InspectionSectionImage[size];
        }
    };
    //endregion Parcelable

    //region Business logic
    public void updateWith(InspectionSectionImage updatedInspectionSectionImage) {
        label = updatedInspectionSectionImage.getLabel();
    }
    //endregion
}
