package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.saasplications.genie.domain.model.File;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionImage;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.services.DataService;
import au.com.houspect.atlas.services.DataServiceImpl;
import au.com.houspect.atlas.services.GenieService;
import au.com.houspect.atlas.services.GenieServiceImpl;
import au.com.houspect.atlas.services.SharedPreferencesService;
import au.com.houspect.atlas.services.SharedPreferencesServiceImpl;
import au.com.houspect.atlas.utilities.AttachPhotoUtil;
import au.com.houspect.atlas.utilities.MediaUtil;
import au.com.houspect.atlas.utilities.Util;
import au.com.houspect.atlas.viewmodels.InspectionStartViewModel;

//import com.badoualy.stepperindicator.StepperIndicator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InspectionStartFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InspectionStartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InspectionStartFragment extends ViewModelFragment<InspectionStartViewModel> implements OnMapReadyCallback, AttachPhotoUtil.Listener {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static InspectionStartFragment.Event NEXT = new InspectionStartFragment.Event(801);
        public static InspectionStartFragment.Event BACK = new InspectionStartFragment.Event(802);
        public static InspectionStartFragment.Event START_INSPECTION = new InspectionStartFragment.Event(803);

        public static InspectionStartFragment.Event PHOTO_TAKEN = new InspectionStartFragment.Event(804);
        public static InspectionStartFragment.Event PHOTO_GALLERY = new InspectionStartFragment.Event(805);
        public static InspectionStartFragment.Event PHOTO_ERROR = new InspectionStartFragment.Event(806);
        public static InspectionStartFragment.Event PHOTO_STORAGE_PERMISSION_REQUEST_TAKE_A_PHOTO = new InspectionStartFragment.Event(807);
        public static InspectionStartFragment.Event PHOTO_STORAGE_PERMISSION_REQUEST_PICK_A_PHOTO = new InspectionStartFragment.Event(808);
    }

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_INSPECTION_ID = "inspectionId";
    private static final String ARG_IS_DRAFT_EDITING = "isDraftEditing";


    private Button mButtonCamera;
    private ImageView mImageViewCoverPhoto;

    private Inspection mInspection;
    private boolean mIsDraftEditing; // indicates if current display is for Inspection preview (i.e. before Inspection is converted to Draft) or actual Draft editing
    private OnFragmentInteractionListener mListener;

    private AttachPhotoUtil mAttachPhotoUtil;

    public InspectionStartFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param inspectionId
     * @return A new instance of fragment InspectionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InspectionStartFragment newInstance(String inspectionId, boolean isDraftEditing) {
        InspectionStartFragment fragment = new InspectionStartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_INSPECTION_ID, inspectionId);
        args.putBoolean(ARG_IS_DRAFT_EDITING, isDraftEditing);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mIsDraftEditing = getArguments().getBoolean(ARG_IS_DRAFT_EDITING);

            // Note:: certain device cannot handle storage of Inspection in bundle, hence store inspectionId instead. Revise this design in the future.

            String inspectionId = getArguments().getString(ARG_INSPECTION_ID);

            // TODO:: dependency inject
            GenieService genieService = new GenieServiceImpl();
            SharedPreferencesService sharedPreferencesService = new SharedPreferencesServiceImpl(getContext());
            DataService dataService = new DataServiceImpl(getContext(), genieService, sharedPreferencesService);
            mInspection = dataService.get(inspectionId, Draft.class);
            if (mInspection == null) {
                Log.i("InspectionStart", "There is no Draft record for id (" + inspectionId + ")");
                mInspection = dataService.get(inspectionId, Inspection.class);
                if (mInspection == null) {
                    Log.e("InspectionStart", "There is also no Inspection record for id (" + inspectionId + ")");
                }
            }
            try {
                Util.parseInspectionXml(mInspection.getXml(), mInspection);
            } catch (Exception e) {
                Log.e("SideMenu", e.getLocalizedMessage());
            }

            // End Note
        }
    }

    @Override
    protected void onViewModelCreation() {

        final WeakReference<InspectionStartFragment> weakSelf = new WeakReference(this);

        mViewModel = new InspectionStartViewModel
                (
                        mInspection,
                        new Runnable() {
                            @Override
                            public void run() {
                                mListener.onFragmentInteraction(weakSelf.get(), InspectionStartFragment.Event.BACK, mInspection);
                            }
                        },
                        new Runnable() {
                            @Override
                            public void run() {
                                mListener.onFragmentInteraction(weakSelf.get(), mIsDraftEditing ? InspectionStartFragment.Event.NEXT : Event.START_INSPECTION, mInspection);
                            }
                        }
                );
    }

    @Override
    protected int getLayoutResourceId() {
        boolean shouldShowMap = checkPlayServices();
        return shouldShowMap ? R.layout.fragment_inspection_start : R.layout.fragment_inspection_start_without_map;
    }

    @Override
    protected void onConfigureViewDataBinding(ViewDataBinding viewDataBinding) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        boolean shouldShowMap = checkPlayServices();
        if (shouldShowMap) {
            // Get the SupportMapFragment and request notification
            // when the map is ready to be used.
            SupportMapFragment supportMapFragment
                    = (SupportMapFragment) getChildFragmentManager()// getActivity().getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            supportMapFragment.getMapAsync(this);
        }

        final WeakReference<InspectionStartFragment> weakSelf = new WeakReference(this);

        mAttachPhotoUtil = new AttachPhotoUtil(this,
                // onPhotoError
                new Runnable() {
                    @Override
                    public void run() {
                        mListener.onFragmentInteraction(weakSelf.get(), InspectionStartFragment.Event.PHOTO_ERROR, mInspection);
                    }
                },
                // onShouldSelectFromLibrary
                new Runnable() {
                    @Override
                    public void run() {
                        mListener.onFragmentInteraction(weakSelf.get(), InspectionStartFragment.Event.PHOTO_STORAGE_PERMISSION_REQUEST_PICK_A_PHOTO, mInspection);

                    }
                },
                // onShouldTakeAPhoto
                new Runnable() {
                    @Override
                    public void run() {
                        mListener.onFragmentInteraction(weakSelf.get(), InspectionStartFragment.Event.PHOTO_STORAGE_PERMISSION_REQUEST_TAKE_A_PHOTO, mInspection);
                    }
                },
                this
        );

        mButtonCamera = (Button) view.findViewById(R.id.buttonCamera);
        mButtonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsDraftEditing) mAttachPhotoUtil.showPhotoActionOptions();
                else mListener.onFragmentInteraction(weakSelf.get(), Event.START_INSPECTION, mInspection);
            }
        });

        mImageViewCoverPhoto = (ImageView) view.findViewById(R.id.imageViewCoverPhoto);

        if (mInspection.getImage() != null) {
            java.io.File photoFolder = MediaUtil.getOutputMedaiaFolder(getActivity());
            java.io.File photoFile = new java.io.File(photoFolder.getPath() + java.io.File.separator + mInspection.getImage().getFileName());
            Picasso.with(getActivity()).load(photoFile).resize(300, 300).centerInside().into(mImageViewCoverPhoto);
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(InspectionStartFragment inspectionStartFragment, Event event, Inspection inspection);
    }

    //region OnMapReadyCallback
    @Override
    public void onMapReady(GoogleMap googleMap) {

        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        LatLng sydney = new LatLng(mInspection.getLatitude(), mInspection.getLongitude());
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.addMarker(new MarkerOptions().position(sydney)
                .title(mInspection.getAddress()));
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
    //endregion


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    // region AttachPhotoUtil related
    public void invokeCamera() {
        mAttachPhotoUtil.invokeCamera();
    }

    public void invokePhotoLibrary() {
        mAttachPhotoUtil.invokePhotoLibrary();
    }

    @Override
    public void onPhotoReadyToBeAttached(java.io.File file) {
        InspectionImage inspectionImage = new InspectionImage();
        inspectionImage.setId("");
        inspectionImage.setFileName(file.getName());
        mInspection.setImage(inspectionImage);
        mListener.onFragmentInteraction(this, Event.PHOTO_TAKEN, mInspection);
        Picasso.with(getActivity()).load(new java.io.File(file.getAbsolutePath())).resize(300,300).centerInside().into(mImageViewCoverPhoto);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mAttachPhotoUtil.onActivityResult(requestCode, resultCode, data);
    }

    // endregion

    /**
     * This fragment does not hold the same Draft object instance that holds the InspectionSection referenced in (input-centric) InspectionSectionFragment.
     * However, there is possible input from end user - cover photo.
     * Copy the input changes to the target draft i.e. usually the Draft in the InspectionFragment - the original object instance that leads to this fragment.
     * @param draft
     */
    public void copyChangesTo(Draft draft) {
        draft.setImage(mInspection.getImage());
    }
}
