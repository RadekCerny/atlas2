package au.com.houspect.atlas.models;

import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableChar;
import android.databinding.ObservableField;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alfredwong on 30/1/17.
 */

public class InspectionSectionItem extends IdBase implements Parcelable, Cloneable {

    // Questions related
    public final ObservableBoolean observableMandatory = new ObservableBoolean(true);
    private String na;
    private boolean isMandatory = true;
    private String resultType;
    private String text;
    private String naText;
    private List<Option> options;

    // Answers related
    private String result;
    private String resultComments;

    // Clone related
    private String seedId;

    public final ObservableField<String> observableResultComments = new ObservableField<>("");

    public InspectionSectionItem() {

        observableMandatory.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (observable == observableMandatory) {
                    if (observableMandatory.get() != isMandatory) isMandatory = observableMandatory.get();
                }
            }
        });

        observableResultComments.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (observable == observableResultComments) {
                    if (observableResultComments.get() != resultComments) resultComments = observableResultComments.get();
                }
            }
        });

    }

    public static class Option extends IdBase implements Parcelable, Cloneable {
        private String text;

        public Option() {}

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        //region Parcelable
        private Option(Parcel in) {
            setId(in.readString());
            text = in.readString();
        }

        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(getId());
            dest.writeString(text);
        }

        public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

            @Override
            public Option createFromParcel(Parcel source) {
                return new Option(source);
            }

            @Override
            public Option[] newArray(int size) {
                return new Option[size];
            }
        };
        //endregion

        //region Cloneable
        protected Object clone() throws CloneNotSupportedException {
            Option option = new Option();
            option.setText(getText());
            option.setId(getId());
            return option;
        }
        //endregion
    }

    public String getNa() {
        return na;
    }

    public void setNa(String na) {
        this.na = na;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
        observableMandatory.set(isMandatory);
    }

    public String getText() { return text; }

    public void setText(String text) { this.text = text; }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public String getNaText() {
        return naText;
    }

    public void setNaText(String naText) {
        this.naText = naText;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResultComments() {
        return resultComments;
    }

    public void setResultComments(String resultComments) {
        this.resultComments = resultComments;
        observableResultComments.set(resultComments);
    }

    public String getSeedId() {
        return seedId;
    }

    public void setSeedId(String seedId) {
        this.seedId = seedId;
    }

    //region Parcelable
    protected InspectionSectionItem(Parcel in) {
        setId(in.readString());
        na = in.readString();
        isMandatory = in.readByte() != 0;
        text = in.readString();
        resultType = in.readString();
        naText = in.readString();
        options = in.createTypedArrayList(Option.CREATOR); //Arrays.asList((Option[]) in.readParcelableArray(Option.class.getClassLoader()));
        result = in.readString();
        resultComments = in.readString();
        observableMandatory.set(isMandatory);
        observableResultComments.set(resultComments);
        seedId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(na);
        dest.writeByte((byte) (isMandatory ? 1 : 0));
        dest.writeString(text);
        dest.writeString(resultType);
        dest.writeString(naText);
        dest.writeTypedList(options);
        dest.writeString(result);
        dest.writeString(resultComments);
        dest.writeString(seedId);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public InspectionSectionItem createFromParcel(Parcel source) {
            return new InspectionSectionItem(source);
        }

        @Override
        public InspectionSectionItem[] newArray(int size) {
            return new InspectionSectionItem[size];
        }
    };
    //endregion Parcelable

    //region Cloneable
    protected Object clone() throws CloneNotSupportedException {
        InspectionSectionItem inspectionSectionItem = new InspectionSectionItem();
        inspectionSectionItem.setId("0");
        inspectionSectionItem.setSeedId(getId());
        inspectionSectionItem.setNa("0"); // do not clone result / answer
        inspectionSectionItem.setMandatory(isMandatory());
        inspectionSectionItem.setText(getText());
        inspectionSectionItem.setResultType(getResultType());
        inspectionSectionItem.setNaText(getNaText());

        List<InspectionSectionItem.Option> inspectionSectionItemOptions = new ArrayList<>();
        for (InspectionSectionItem.Option option : getOptions()) {
            inspectionSectionItemOptions.add((InspectionSectionItem.Option)option.clone());
        }
        inspectionSectionItem.setOptions(inspectionSectionItemOptions);

        // do not clone the results / answers
        inspectionSectionItem.setResult("");
        inspectionSectionItem.setResultComments("");

        return inspectionSectionItem;
    }
    //endregion

    public boolean isComplete() {
        boolean isComplete = result != null && result.length() > 0;
        if (!isComplete) isComplete = isNa();
        return isComplete;
    }

    public enum Type {
        Text(0), Enum(1), TwoNumbers(2);

        private int mType;

        private Type(int type) {
            mType = type;
        }

        public boolean equals(Type type) {
            return mType == type.mType;
        }

        public int getInt() {
            return mType;
        }
    }

    public Type getType() {

        boolean isEnum = "Enum".equalsIgnoreCase(resultType);
        if (!isEnum && resultType != null) isEnum = resultType.contains("Enum");
        if (isEnum) return Type.Enum;

        boolean isTwoNumbers = "TwoNumbers".equalsIgnoreCase(resultType);
        if (!isTwoNumbers && resultType != null) isTwoNumbers = resultType.contains("TwoNumbers");
        if (isTwoNumbers) return Type.TwoNumbers;

        return Type.Text;

    }


    public boolean isNa() {
        return na != null && (na.equalsIgnoreCase("1") || na.equalsIgnoreCase("other"));
    }

    public void resetOther() {
        na="0";
        setResultComments("");
    }

    public void setAsOther() {
        na = "1";
    }

    public void resetOtherNa() {
        na="0";
        setResultComments("");
    }

    public void setAsOtherNaWithNaText() {
        na = "1";
        setResultComments(getNaText());
    }

    public void setAsOtherNa() {
        na = "other";
    }

    public boolean isOtherNaWithNaText() {
        return "1".equalsIgnoreCase(na) && (naText != null && naText.equalsIgnoreCase(resultComments));
    }
}
