package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import au.com.houspect.atlas.PhotoActivity;
import au.com.houspect.atlas.R;
import au.com.houspect.atlas.adapters.PhotoRecyclerViewAdapter;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.utilities.MediaUtil;
import au.com.houspect.atlas.utilities.Util;

import static android.app.Activity.RESULT_OK;


/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * {@link PhotoGalleryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PhotoGalleryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PhotoGalleryFragment extends Fragment implements PhotoRecyclerViewAdapter.OnPhotoInteractionListener {


    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static Event PHOTO_SELECTED = new Event(601);
        public static Event CANCEL = new Event(602);
        public static Event PHOTO_DELETED = new Event(603);
        public static Event PHOTO_UPDATED = new Event(604);
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_INSPECTION_SECTION = "inspectionSection";

    // TODO: Rename and change types of parameters
    private RecyclerView mRecyclerViewPhotoGallery;
    private Button mButtonCancel;
    private ImageView mImageViewSelected;
    private TextView mTextViewSelectedPhotoInfo;
    private PhotoRecyclerViewAdapter.Value<InspectionSectionImage> selectedImage = null;

    private OnFragmentInteractionListener mListener;

    private InspectionSection mInspectionSection;

    public PhotoGalleryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param inspectionSection Parameter 1.
     * @return A new instance of fragment CameraPreviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PhotoGalleryFragment newInstance(InspectionSection inspectionSection) {
        PhotoGalleryFragment fragment = new PhotoGalleryFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_INSPECTION_SECTION, inspectionSection);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mInspectionSection = getArguments().getParcelable(ARG_INSPECTION_SECTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photo_gallery, container, false);
        mButtonCancel = (Button) view.findViewById(R.id.buttonCancel);
        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(Event.CANCEL, null);
            }
        });
        mRecyclerViewPhotoGallery = (RecyclerView) view.findViewById(R.id.recyclerViewPhotoGallery);
        int numberOfColumns = Util.calculateNoOfColumns(getContext(), 140, 20);
        mRecyclerViewPhotoGallery.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));

        List<PhotoRecyclerViewAdapter.Value> values = new ArrayList<>();
        File photoFolder = MediaUtil.getOutputMedaiaFolder(getActivity());
        for (InspectionSectionImage inspectionSectionImage : mInspectionSection.getImages()) {
            File photoFile = new File(photoFolder.getPath() + File.separator + inspectionSectionImage.getFileName());
            String photoPath = photoFile.getAbsolutePath();
            // TODO - if local file does not exist, construct server url
            // if (!photoFile.exists()) photoPath = "http://....";

            PhotoRecyclerViewAdapter.Value value = new PhotoRecyclerViewAdapter.Value<>();
            value.dataObject = inspectionSectionImage;
            value.photoPath = photoPath;
            values.add(value);
        }

        PhotoRecyclerViewAdapter adapter = new PhotoRecyclerViewAdapter(values, this, R.layout.fragment_photo, getContext());
        mRecyclerViewPhotoGallery.setAdapter(adapter);

        mImageViewSelected = (ImageView) view.findViewById(R.id.imageViewSelected);
        mTextViewSelectedPhotoInfo = (TextView) view.findViewById(R.id.textViewSelectedPhotoInfo);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Event event, HashMap<String,Object> dataObject);
    }

    //@Override
    //public void onPhotoInteraction(PhotoRecyclerViewAdapter.Value<InspectionSectionImage> value) {
    //    mListener.onFragmentInteraction(Event.PHOTO_SELECTED, photoPath);
    //}

    @Override
    public void onPhotoDelete(PhotoRecyclerViewAdapter.Value value, int position) {
        InspectionSectionImage inspectionSectionImage = (InspectionSectionImage) value.dataObject;
        String localPhotoFilePath = Util.getLocalPhotoFilePath(getActivity(), inspectionSectionImage);
        boolean isLocalPhoto = localPhotoFilePath != null;
        if (isLocalPhoto) {
            File file = new File(localPhotoFilePath);
            file.delete();
        }

        mInspectionSection.getImages().remove(inspectionSectionImage);

        ((PhotoRecyclerViewAdapter)mRecyclerViewPhotoGallery.getAdapter()).getValues().remove(position);
        mRecyclerViewPhotoGallery.getAdapter().notifyItemRemoved(position);

        HashMap<String, Object> dataObject = new HashMap<>();
        dataObject.put("inspectionSection", mInspectionSection);
        dataObject.put("inspectionSectionImage", inspectionSectionImage);

        mListener.onFragmentInteraction(Event.PHOTO_DELETED, dataObject);
    }

    @Override
    public void onPhotoSelected(PhotoRecyclerViewAdapter.Value value, int position) {

        /*
        Uri uri = MediaUtil.getImageUri(getContext(), new File(value.photoPath));
        int rotationOld = MediaUtil.getImageRotationOld(getContext(), uri);
        int rotationNew = MediaUtil.getImageRotation(getContext(), uri);
        int exifOrientation = MediaUtil.getExifOrientation(getContext(), uri);
        int exifOrientationByPath = MediaUtil.getExifOrientation(getContext(), value.photoPath);
        int mediaStoreRotation = -1;
        try {
            MediaUtil.getRotationFromMediaStore(getContext(), uri);
        } catch (Exception e) {

        }

        Bitmap bitmap = BitmapFactory.decodeFile(value.photoPath);
        bitmap = MediaUtil.rotateBitmap(bitmap, rotationNew);
        mImageViewSelected.setImageBitmap(bitmap);

        String info = "New Rotation: " + rotationNew + ", exifOrientation: " + exifOrientation + ", exifOrientationByPath: " + exifOrientationByPath + ", mediaStoreRotation: " + mediaStoreRotation;
        info += "\r\n";
        info += "photoPath: " + value.photoPath;
        info += "\r\n";
        info += "uri: " + uri.getPath();

        mTextViewSelectedPhotoInfo.setText(info);*/


        /*Uri uri = Uri.fromFile(new File(value.photoPath));
        int rotation = MediaUtil.getImageRotation(getContext(), uri);
        Bitmap bitmap = BitmapFactory.decodeFile(value.photoPath);
        bitmap = MediaUtil.rotateBitmap(bitmap, rotation);
        mImageViewSelected.setImageBitmap(bitmap);

        mImageViewSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageViewSelected.setImageBitmap(null);
                mImageViewSelected.setVisibility(View.GONE);
                mTextViewSelectedPhotoInfo.setVisibility(View.GONE);
            }
        });

        mImageViewSelected.setVisibility(View.VISIBLE);
        mTextViewSelectedPhotoInfo.setVisibility(View.VISIBLE);*/

        selectedImage = value;

        Intent intent = new Intent(getActivity(), PhotoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(PhotoActivity.ARG_INSPECTION_ID, "-");
        bundle.putString(PhotoActivity.ARG_INSPECTION_SECTION_ID, "--");
        bundle.putString(PhotoActivity.ARG_PHOTO_PATH, value.photoPath);
        bundle.putParcelable(PhotoActivity.ARG_INSPECTION_SECTION_IMAGE, (InspectionSectionImage)value.dataObject);

        intent.putExtra("bundle", bundle);

        startActivityForResult(intent, 1000);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        Log.d("PhotoGalleryFragment", "onActivityResult (" + requestCode + ") resultCode (" + resultCode + ")");

        if (requestCode != 1000) return;
        if (resultCode != RESULT_OK) return;

        String inspectionId = data.getStringExtra(PhotoEditFragment.ARG_INSPECTION_ID);
        String inspectionSectionId = data.getStringExtra(PhotoEditFragment.ARG_INSPECTION_SECTION_ID);
        String photoPath = data.getStringExtra(PhotoEditFragment.ARG_PHOTO_PATH);
        InspectionSectionImage inspectionSectionImage = data.getParcelableExtra(PhotoEditFragment.ARG_INSPECTION_SECTION_IMAGE);
        boolean isPhotoMarkUped = data.getBooleanExtra(PhotoEditFragment.ARG_INSPECTION_SECTION_IMAGE_IS_MARKUPED, false);
        Log.d("PhotoGalleryFragment", "inspectionId (" + inspectionId + ") inspectionSectionId (" + inspectionSectionId + ") photoPath (" + photoPath + ")");
        Log.d("PhotoGalleryFragment", "inspectionSectionImage.label (" + inspectionSectionImage.getLabel() + ")");


        if (selectedImage == null) return;

        selectedImage.dataObject.updateWith(inspectionSectionImage);

        HashMap<String, Object> dataObject = new HashMap<>();
        dataObject.put("inspectionSection", mInspectionSection);
        dataObject.put("inspectionSectionImage", selectedImage.dataObject);
        dataObject.put("isPhotoMarkUped", isPhotoMarkUped);
        mListener.onFragmentInteraction(Event.PHOTO_UPDATED, dataObject);

    }
}
