package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.viewmodels.CheckboxConfirmationViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CheckboxConfirmationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CheckboxConfirmationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CheckboxConfirmationFragment extends ViewModelFragment<CheckboxConfirmationViewModel> {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static Event CANCEL = new Event(1101);
        public static Event CONFIRMED = new Event(1102);
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TITLE_STRING_RESOURCE_ID = "titleStringResourceId";
    private static final String ARG_CONTENT_STRING_RESOURCE_ID = "contentStringResourceId";
    private static final String ARG_CHECKBOX_TEXT_STRING_RESOURCE_ID = "checkboxStringResourceId";
    private static final String ARG_TAG = "tag"; // pass through object e.g. Draft

    private OnFragmentInteractionListener mListener;

    private int mTitleStringResourceId;
    private int mContentStringResourceId;
    private int mCheckboxStringResourceId;
    private Parcelable mTag;

    public CheckboxConfirmationFragment() {
        // Required empty public constructor
    }

    @Override
    protected void onViewModelCreation() {

        mViewModel = new CheckboxConfirmationViewModel
                (getResources().getString(mTitleStringResourceId),
                        getResources().getString(mContentStringResourceId),
                        getResources().getString(mCheckboxStringResourceId),
                        new Runnable() {
                            @Override
                            public void run() {
                                mListener.onFragmentInteraction(Event.CANCEL, mTag);
                            }
                        },
                        new Runnable() {
                            @Override
                            public void run() {
                                mListener.onFragmentInteraction(Event.CONFIRMED, mTag);
                            }
                        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_checkbox_confirmation;
    }

    @Override
    protected void onConfigureViewDataBinding(ViewDataBinding viewDataBinding) {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CommentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CheckboxConfirmationFragment newInstance(int titleStringResourceId, int contentStringResourceId, int checkboxStringResourceId, Parcelable tag) {
        CheckboxConfirmationFragment fragment = new CheckboxConfirmationFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TITLE_STRING_RESOURCE_ID, titleStringResourceId);
        args.putInt(ARG_CONTENT_STRING_RESOURCE_ID, contentStringResourceId);
        args.putInt(ARG_CHECKBOX_TEXT_STRING_RESOURCE_ID, checkboxStringResourceId);
        args.putParcelable(ARG_TAG, tag);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitleStringResourceId = getArguments().getInt(ARG_TITLE_STRING_RESOURCE_ID);
            mContentStringResourceId = getArguments().getInt(ARG_CONTENT_STRING_RESOURCE_ID);
            mCheckboxStringResourceId = getArguments().getInt(ARG_CHECKBOX_TEXT_STRING_RESOURCE_ID);
            mTag = getArguments().getParcelable(ARG_TAG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Event event, Parcelable tag);
    }


}
