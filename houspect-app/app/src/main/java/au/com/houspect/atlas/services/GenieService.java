package au.com.houspect.atlas.services;

import com.saasplications.genie.domain.client.activity.payload.DataPublicationRequest;
import com.saasplications.genie.domain.model.ActivityInstance;

import java.util.List;

import au.com.houspect.atlas.models.CalendarItem;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionClient;
import au.com.houspect.atlas.models.InspectionImageUploadLog;
import au.com.houspect.atlas.models.InspectionSectionImageUploadLog;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.models.RecentInspection;
import io.reactivex.Single;

/**
 * Created by alfredwong on 12/02/2017.
 */

public interface GenieService {

    void login(String username, String password, final GenieServiceCallback<GenieServiceResponse<String>> callback);

    /**
     * Download jobs from server.
     * @param sessionToken Session Token obtained from login.
     * @param activityInstance Jobs download needs to be performed within an ActivityInstance. If it is null, CreateActivity will be invoked.
     */
    void getJobs(final String sessionToken,
                 final ActivityInstance activityInstance,
                 final GenieServiceCallback<GenieServiceResponse<List<Job>>> callback);

    /**
     * Download inspections from server.
     * @param sessionToken Session Token obtained from login.
     * @param activityInstance Jobs download needs to be performed within an ActivityInstance. If it is null, CreateActivity will be invoked.
     */
    void getInspections(final String sessionToken,
                        final ActivityInstance activityInstance,
                        final GenieServiceCallback<GenieServiceResponse<List<Inspection>>> callback);

    /**
     * Download recent inspections from server.
     * @param sessionToken Session token obtained from logib.
     * @param activityInstance Recent clients download needs to be performed within an ActivityInstance. If it is null, CreateActivity will be invoked.
     * @param callback
     */
    void getRecentInspections(final String sessionToken,
                              final ActivityInstance activityInstance,
                              final GenieServiceCallback<GenieServiceResponse<List<RecentInspection>>> callback);

    /**
     * Download recent inspections from server to be compared with drafts for deletion.
     * @param sessionToken
     * @param activityInstance
     * @param callback
     */
    void getRecentInspectionsForDraftDeletion(final String sessionToken,
                                              final ActivityInstance activityInstance,
                                              final GenieServiceCallback<GenieServiceResponse<List<RecentInspection>>> callback);

    /**
     * Download upcoming calendar items from server. Returns an observable Single.
     * @param sessionToken
     * @param activityInstance
     * @return
     */
    Single<GenieServiceResponse<List<CalendarItem>>> getUpcomingCalendarItems(final String sessionToken,
                                                                              final ActivityInstance activityInstance);

    void createActivity(String sessionToken,
                        final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback);
    void createActivity(String sessionToken,
                        final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback,
                        DataPublicationRequest ... dataPublicationRequests);

    DataPublicationRequest jobDataPublication();
    DataPublicationRequest recentInspectionDataPublication();
    DataPublicationRequest recentInspectionForDraftDeletionDataPublication();
    DataPublicationRequest upcomingCalendarItemDataPublication();

    /**
     * Upload draft to server.
     * @param draft
     * @param sessionToken
     * @param activityInstance
     * @param callback
     */
    void uploadDraft(final Draft draft,
                     final String sessionToken,
                     final ActivityInstance activityInstance,
                     final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback);

    /**
     * Notify server that the draft is active.
     * @param draft
     * @param sessionToken
     * @param activityInstance
     * @param callback
     */
    void notifyDraftIsActive(final Draft draft,
                             final String sessionToken,
                             final ActivityInstance activityInstance,
                             final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback);

    /**
     * Upload image to server.
     * @param  inspectionSectionImageUploadLog
     * @param imageAsBase64String
     * @param shouldGzip
     * @param sessionToken
     * @param activityInstance
     * @param callback
     */
    public void uploadImage(InspectionSectionImageUploadLog inspectionSectionImageUploadLog,
                            int width,
                            int height,
                            String imageAsBase64String,
                            boolean shouldGzip,
                            String sessionToken,
                            ActivityInstance activityInstance,
                            final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback);

    void uploadImage(InspectionImageUploadLog inspectionImageUploadLog,
                            int width,
                            int height,
                            String imageAsBase64String,
                            boolean shouldGzip,
                            String sessionToken,
                            ActivityInstance activityInstance,
                            final GenieServiceCallback<GenieServiceResponse<ActivityInstance>> callback);

    void forgotPassword(String username, String email, final GenieServiceCallback<GenieServiceResponse<String>> callback);

    void ping(String sessionToken, final GenieServiceCallback<GenieServiceResponse<String>> callback);
}
