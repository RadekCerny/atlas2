package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by alfredwong on 09/06/2017.
 */

public class RecentInspection extends IdBase implements Parcelable {

    //region fields from server
    @DatabaseField
    private String code;
    @DatabaseField
    private String clientName;
    @DatabaseField
    private String address;
    @DatabaseField
    private String suburb;
    @DatabaseField
    private String date;
    @DatabaseField
    private String reportType;
    @DatabaseField
    private String mobile;
    @DatabaseField
    private String status;
    //endregion

    @DatabaseField
    private boolean isPendingUploadToServer;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean getPendingUploadToServer() {
        return isPendingUploadToServer;
    }

    public void setPendingUploadToServer(Boolean pendingUploadToServer) {
        isPendingUploadToServer = pendingUploadToServer;
    }

    public RecentInspection() {}

    //region Parcelable
    private RecentInspection(Parcel in) {
        setId(in.readString());
        code = in.readString();
        clientName = in.readString();
        address = in.readString();
        suburb = in.readString();
        date = in.readString();
        reportType = in.readString();
        mobile = in.readString();
        status = in.readString();
        isPendingUploadToServer = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(code);
        dest.writeString(clientName);
        dest.writeString(address);
        dest.writeString(suburb);
        dest.writeString(date);
        dest.writeString(reportType);
        dest.writeString(mobile);
        dest.writeString(status);
        dest.writeByte((byte)(isPendingUploadToServer ? 1 : 0));
    }

    public static final Creator CREATOR = new Creator() {

        @Override
        public RecentInspection createFromParcel(Parcel source) {
            return new RecentInspection(source);
        }

        @Override
        public RecentInspection[] newArray(int size) {
            return new RecentInspection[size];
        }
    };
    //endregion

    public boolean isFinal() {
        if ("submitted".equalsIgnoreCase(status)) return true;
        else if ("waiting".equalsIgnoreCase(status)) return true;
        else if ("review2".equalsIgnoreCase(status)) return true;
        else if ("reviewok".equalsIgnoreCase(status)) return true;
        return false;
    }
}
