package au.com.houspect.atlas.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.util.SortedList;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import au.com.houspect.atlas.BR;
import au.com.houspect.atlas.R;
import au.com.houspect.atlas.fragments.JobListFragment;
import au.com.houspect.atlas.fragments.dummy.DummyContent.DummyItem;
import au.com.houspect.atlas.models.CalendarItem;
import au.com.houspect.atlas.models.ITemporal;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.utilities.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Calendar;
import java.util.function.Function;

import static android.os.Build.VERSION_CODES.M;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link JobListFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class JobsRecyclerViewAdapter extends RecyclerView.Adapter<JobsRecyclerViewAdapter.ViewHolder> {


    private static final int TYPE_JOB = 0;
    private static final int TYPE_SECTION_HEADER = 1;
    private static final int TYPE_CALENDAR_ITEM = 2;

    private final List<ITemporal> mValues;
    private final Map<Integer,String> mSectionHeaders = new HashMap<>(); // map position
    private final Map<String, List<ITemporal>> mDateToTemporals = new HashMap<>();
    private final JobListFragment.OnListFragmentInteractionListener mListener;
    private int mItemResourceId;
    private int mCalendarResourceId;
    private int mSectionResourceId;

    public JobsRecyclerViewAdapter(List<Job> items,
                                   List<CalendarItem> calendarItems,
                                   JobListFragment.OnListFragmentInteractionListener listener,
                                   int itemResourceId,
                                   int calendarItemResourceId,
                                   int sectionResourceId) {

        mValues = computeSectionedList(items, calendarItems, mSectionHeaders, mDateToTemporals);
        mListener = listener;
        mItemResourceId = itemResourceId;
        mCalendarResourceId = calendarItemResourceId;
        mSectionResourceId = sectionResourceId;
    }

    /**
     * Partition temporals into different list with hash key = ITemporal.getDate().
     * The partitions are stored in hashCache.
     * @param temporals
     * @param hashCache
     */
    private void hashCacheITemporal(List<ITemporal> temporals, Map<String, List<ITemporal>> hashCache) {
        for (ITemporal x : temporals) {
            String key = x.getDate();
            List<ITemporal> list;
            if (!hashCache.containsKey(key)) {
                list = new ArrayList<>();
                hashCache.put(key, list);
            }
            list = hashCache.get(key);
            list.add(x);
        }
    }

    private List<ITemporal> computeSectionedList(List<Job> items,
                                                 List<CalendarItem> calendarItems,
                                                 // output
                                                 Map<Integer,String> mSectionHeaders,
                                                 // output
                                                 Map<String,List<ITemporal>> dateToTemporals) {

        //HashMap<String,List<ITemporal>> dateToTemporals = new HashMap<>();
        hashCacheITemporal(Arrays.asList(items.toArray(new ITemporal[0])), dateToTemporals);
        hashCacheITemporal(Arrays.asList(calendarItems.toArray(new ITemporal[0])), dateToTemporals);

        List<String> sortedDates = Arrays.asList(dateToTemporals.keySet().toArray(new String[0]));
        Collections.sort(sortedDates, (d1, d2) -> {
            long time1 = Util.computeTimeSince1stJan1970(d1, "00:00");
            long time2 = Util.computeTimeSince1stJan1970(d2, "00:00");
            if (time1 == time2) return 0;
            if (time1 < time2) return -1;
            return 1; // i.e. time1 > time2
        }); // ascending

        List<ITemporal> sectionedList = new ArrayList<>();

        int position = 0;
        for (int sectionIndex = 0; sectionIndex < sortedDates.size(); sectionIndex++) {

            String date = sortedDates.get(sectionIndex);

            // indicate that a section header should be displayed
            sectionedList.add(null);
            mSectionHeaders.put(position, date);
            position++;

            // indicate temporal items should be displayed
            List<ITemporal> temporals = dateToTemporals.get(date);
            Util.sortTemporals(temporals, true);

            sectionedList.addAll(temporals);
            position += temporals.size();
        }

        return sectionedList;
    }

    @Override
    public int getItemViewType(int position) {
        Object value = mValues.get(position);
        if (value == null) return TYPE_SECTION_HEADER;
        if (value instanceof CalendarItem) return TYPE_CALENDAR_ITEM;
        return TYPE_JOB;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int resourceId = viewType == TYPE_SECTION_HEADER
                ? mSectionResourceId
                : (viewType == TYPE_JOB ? mItemResourceId : mCalendarResourceId);
        View view = LayoutInflater.from(parent.getContext())
                .inflate(resourceId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        int type = getItemViewType(position);
        boolean isSectionHeader = type == TYPE_SECTION_HEADER;

        if (!isSectionHeader) {

            boolean isJob = type == TYPE_JOB;
            ITemporal temporal = mValues.get(position);
            holder.viewDataBinding.setVariable(BR.model, temporal);

            boolean isLastItem = (position == mValues.size()-1);
            holder.viewDataBinding.setVariable(BR.isLastItem, isLastItem);

            if (isJob) {
                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.onListFragmentInteraction((Job)temporal);//(itemViewHolder.mItem);
                        }
                    }
                });
            }
        }
        else {
            String dateAsString = mSectionHeaders.get(position);
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(dateAsString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (date != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                holder.viewDataBinding.setVariable(BR.month, calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.ENGLISH));
                holder.viewDataBinding.setVariable(BR.day, calendar.get(Calendar.DATE)+"");
            }
            else {
                holder.viewDataBinding.setVariable(BR.month, "");
                holder.viewDataBinding.setVariable(BR.day, "");
            }

            List<ITemporal> sectionTemporals = mDateToTemporals.get(dateAsString);
            boolean hasJobInSection = sectionTemporals != null;
            //sectionTemporals.stream().anyMatch(x -> x instanceof Job) // Note - can't use this as it is only available to API 24+
            // revert to raw loop
            for (ITemporal x : sectionTemporals) {
                hasJobInSection = x instanceof Job;
                if (hasJobInSection) break;
            }
            holder.viewDataBinding.setVariable(BR.hasJobs, hasJobInSection);

        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        ViewDataBinding viewDataBinding;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            viewDataBinding = DataBindingUtil.bind(view);
        }
    }
}
