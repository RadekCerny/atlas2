package au.com.houspect.atlas.utilities;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionItem;
import au.com.houspect.atlas.models.Job;

/**
 * Created by alfredwong on 28/01/2017.
 */

public class DemoUtil {

    public List<Job> generateJobs(Context context) { //int numberOfJobs) {

        /*
        List<Job> list = new ArrayList<>();


        for (int i = 0; i < numberOfJobs; i++) {

            int random = (new Random()).nextInt() % 3;
            random = Math.max(random, 1);

            for (int j = 0; j < random; j++) {
                Job j1 = new Job();
                j1.setClientName("Client " + i + " (QLD-" + i + ")");
                j1.setStreet("83 Federal Street");
                j1.setSuburb("RED HILL QLD 4059");
                j1.setDate("0" + Math.max(1,i) + "/02/2017");
                j1.setTime(j+8 + ":45 AM");
                j1.setNotes("Client will be at the property to meet you");
                j1.setReportType("QLD Short Building (hourly agreement)");
                list.add(j1);
            }
        }

        return list;*/

        AssetManager assetManager = context.getAssets();
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream is = assetManager.open("jobs.xml");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;

            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Job> jobs = null;

        try {
            jobs = Util.parseJobsXml(stringBuilder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jobs;
    }

    private static File getFileFromPath(Object obj, String fileName) {
        ClassLoader classLoader = obj.getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        return new File(resource.getPath());
    }

    public List<Inspection> generateInspections(Context context) {
        AssetManager assetManager = context.getAssets();
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream is = assetManager.open("inspections.xml");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;

            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Inspection> inspections = null;

        try {
            inspections = Util.parseInspectionsXml(stringBuilder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inspections;
    }

    public Inspection generateInspection(Context context, String filename) {

        //File file = getFileFromPath(context, "test.xml");
        AssetManager assetManager = context.getAssets();
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream is = assetManager.open(filename);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;

            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Inspection inspection = new Inspection();

        try {
            Util.parseInspectionXml(stringBuilder.toString(), inspection);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inspection;
    }

    public Inspection generateInspection1BAK() {
        Inspection x = new Inspection();
        x.setId("inspection1");
        x.setAddress("203 alfred st Cromer NSW 2099");
        x.setOccupancy("Tenant");
        x.setNoOfBedrooms(6);
        x.setNoOfBathrooms(2);
        x.setPropertyLevels("2");
        x.setPropertyFeatures("Freestanding Garage, Granny flat");

        // Section 1
        InspectionSection section = new InspectionSection();
        section.setId("section1");
        section.setName("Overview");
        section.setOptional(false);


        InspectionSectionItem item = new InspectionSectionItem();
        item.setText("The buidling is a [VAR].");

        InspectionSectionItem.Option o1 = new InspectionSectionItem.Option();
        o1.setText("single story");
        InspectionSectionItem.Option o2 = new InspectionSectionItem.Option();
        o2.setText("two story house");
        InspectionSectionItem.Option o3 = new InspectionSectionItem.Option();
        o3.setText("three story house");

        List<InspectionSectionItem.Option> options = new ArrayList<>();
        options.add(o1);
        options.add(o2);
        options.add(o3);

        item.setOptions(options);

        List<InspectionSectionItem> items = new ArrayList<>();
        items.add(item);
        items.add(item);
        items.add(item);
        items.add(item);
        items.add(item);

        section.setItems(items);

        // Section 2
        InspectionSection section2 = new InspectionSection();
        section2.setId("section2");
        section2.setName("Roof Covering");
        section2.setOptional(false);


        InspectionSectionItem item2_1 = new InspectionSectionItem();
        item2_1.setText("Covering is constructed of [VAR] material");

        InspectionSectionItem.Option o2_1 = new InspectionSectionItem.Option();
        o2_1.setText("clay title");
        InspectionSectionItem.Option o2_2 = new InspectionSectionItem.Option();
        o2_2.setText("concrete tile");
        InspectionSectionItem.Option o2_3 = new InspectionSectionItem.Option();
        o2_3.setText("fibre cement");

        List<InspectionSectionItem.Option> options2 = new ArrayList<>();
        options2.add(o2_1);
        options2.add(o2_2);
        options2.add(o2_3);

        item2_1.setOptions(options2);

        List<InspectionSectionItem> items2 = new ArrayList<>();
        items2.add(item2_1);
        items2.add(item2_1);
        items2.add(item2_1);
        items2.add(item2_1);
        items2.add(item2_1);

        section2.setItems(items2);


        // Section 3
        InspectionSection section3 = new InspectionSection();
        section3.setId("section3");
        section3.setName("Eaves");
        section3.setOptional(false);


        InspectionSectionItem item3_1 = new InspectionSectionItem();
        item3_1.setText("Eaves are [VAR]");

        InspectionSectionItem.Option o3_1 = new InspectionSectionItem.Option();
        o3_1.setText("boxed");
        InspectionSectionItem.Option o3_2 = new InspectionSectionItem.Option();
        o3_2.setText("exposed");
        InspectionSectionItem.Option o3_3 = new InspectionSectionItem.Option();
        o3_3.setText("exposed and lined on the rake");

        List<InspectionSectionItem.Option> options3 = new ArrayList<>();
        options3.add(o3_1);
        options3.add(o3_2);
        options3.add(o3_3);

        item3_1.setOptions(options3);

        List<InspectionSectionItem> items3 = new ArrayList<>();
        items3.add(item3_1);
        items3.add(item3_1);
        items3.add(item3_1);
        items3.add(item3_1);
        items3.add(item3_1);

        section3.setItems(items3);



        List<InspectionSection> sections = new ArrayList<>();
        sections.add(section);
        section.setParentInspectionId(x.getId());
        sections.add(section2);
        section2.setParentInspectionId(x.getId());
        sections.add(section3);
        section3.setParentInspectionId(x.getId());
        //sections.add(section);
        //sections.add(section2);
        //sections.add(section);
        //sections.add(section2);
        //sections.add(section);

        x.setSections(sections);

        return x;

    }
}
