package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import au.com.houspect.atlas.BR;
import au.com.houspect.atlas.R;
import au.com.houspect.atlas.viewmodels.YesNoConfirmationViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link YesNoConfirmationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link YesNoConfirmationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class YesNoConfirmationFragment extends Fragment {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static Event NO = new Event(1201);
        public static Event YES = new Event(1202);
    }

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TITLE = "title";
    private static final String ARG_CONTENT = "content";
    private static final String ARG_CONTEXT = "context";
    private static final String ARG_REQUEST_CODE = "requestCode";

    private OnFragmentInteractionListener mListener;

    private YesNoConfirmationViewModel mViewModel;

    private String mTitle;
    private String mContent;
    private Parcelable mContext;
    private int mRequestCode;

    public YesNoConfirmationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CommentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static YesNoConfirmationFragment newInstance(String title, String content, int requestCode, Parcelable context) {
        YesNoConfirmationFragment fragment = new YesNoConfirmationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_CONTENT, content);
        args.putParcelable(ARG_CONTEXT, context);
        args.putInt(ARG_REQUEST_CODE, requestCode);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitle = getArguments().getString(ARG_TITLE);
            mContent = getArguments().getString(ARG_CONTENT);
            mContext = getArguments().getParcelable(ARG_CONTEXT);
            mRequestCode = getArguments().getInt(ARG_REQUEST_CODE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_yesno_confirmation, container, false);
        View view = binding.getRoot();

        mViewModel = new YesNoConfirmationViewModel
                (mTitle,
                        mContent,
                        new Runnable() {
                            @Override
                            public void run() {
                                mListener.onFragmentInteraction(Event.YES, mRequestCode, mContext);
                            }
                        },
                        new Runnable() {
                            @Override
                            public void run() {
                                mListener.onFragmentInteraction(Event.NO, mRequestCode, mContext);
                            }
                        });
        binding.setVariable(BR.viewModel, mViewModel);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Event event, int requestCode, Parcelable context);
    }


}
