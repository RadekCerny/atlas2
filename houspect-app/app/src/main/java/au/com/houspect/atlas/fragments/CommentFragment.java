package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.adapters.SnippetRecyclerViewAdapter;
import au.com.houspect.atlas.databinding.FragmentCommentBinding;
import au.com.houspect.atlas.databinding.FragmentRecommendationBinding;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionSnippet;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CommentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CommentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CommentFragment extends Fragment implements SnippetRecyclerViewAdapter.OnSnippetInteractionListener {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static Event CANCEL = new Event(401);
        public static Event DONE = new Event(402);
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_INSPECTION_SECTION = "inspectionSection";
    private static final String ARG_TYPE = "type";

    private OnFragmentInteractionListener mListener;

    private Button mButtonCancel;
    private Button mButtonDone;
    private EditText mEditTextComment;
    private RecyclerView mRecyclerViewSnippet;

    private TextView mTextViewTitle;

    private InspectionSection mInspectionSection;
    private Type mType;

    public static enum Type {
        COMMENT("comment", 0),
        RECOMMENDATION("recommendation", 1);

        private String mStringValue;
        private int mIntValue;
        private Type(String stringValue, int intValue) {
            mStringValue = stringValue;
            mIntValue = intValue;
        }

        private boolean equals(Type type) {
            return type.mIntValue == mIntValue;
        }
    }

    public CommentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param InspectionSection Parameter 1.
     * @param Type Parameter 2.
     * @return A new instance of fragment CommentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CommentFragment newInstance(InspectionSection inspectionSection, Type type) {
        CommentFragment fragment = new CommentFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_INSPECTION_SECTION, inspectionSection);
        args.putInt(ARG_TYPE, type.mIntValue);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mInspectionSection = getArguments().getParcelable(ARG_INSPECTION_SECTION);
            int typeAsInt = getArguments().getInt(ARG_TYPE);
            mType = Type.COMMENT;
            if (typeAsInt == Type.RECOMMENDATION.mIntValue) mType = Type.RECOMMENDATION;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //View view = inflater.inflate(R.layout.fragment_comment, container, false);

        boolean isComments = mType.equals(Type.COMMENT);

        int resourceId = (mType.equals(Type.COMMENT)) ? R.layout.fragment_comment : R.layout.fragment_recommendation;

        ViewDataBinding binding = DataBindingUtil.inflate(inflater, resourceId, container, false);
        View view = binding.getRoot();

        if (isComments) {
            ((FragmentCommentBinding)binding).setInspectionSection(mInspectionSection);
        }
        else {
            ((FragmentRecommendationBinding)binding).setInspectionSection(mInspectionSection);
        }
        mTextViewTitle = (TextView)view.findViewById(R.id.textViewTitle);

        //if (mType.equals(Type.COMMENT)) mTextViewTitle.setText("Comments");
        //else if (mType.equals(Type.RECOMMENDATION)) mTextViewTitle.setText("Recommendations");

        mButtonCancel = (Button)view.findViewById(R.id.buttonCancel);
        mButtonDone = (Button)view.findViewById(R.id.buttonDone);
        mEditTextComment = (EditText) view.findViewById(R.id.editTextComment);
        mEditTextComment.requestFocus();
        showSoftKeyboard(mEditTextComment);

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(mEditTextComment);
                mListener.onFragmentInteraction(Event.CANCEL, null);
            }
        });

        mButtonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(mEditTextComment);
                mListener.onFragmentInteraction(Event.DONE, mEditTextComment.getText().toString());
            }
        });

        mRecyclerViewSnippet = (RecyclerView) view.findViewById(R.id.recyclerViewSnippet);
        List<InspectionSectionSnippet> filteredSnippets = new ArrayList<>();
        for (InspectionSectionSnippet snippet : mInspectionSection.getSnippets()) {
            boolean isCorrectSnippet = (snippet.isComment() && mType.equals(Type.COMMENT)) || (snippet.isRecommendation() && mType.equals(Type.RECOMMENDATION));
            if (!isCorrectSnippet) continue;
            filteredSnippets.add(snippet);
        }
        mRecyclerViewSnippet.setAdapter(new SnippetRecyclerViewAdapter(filteredSnippets, this, R.layout.fragment_snippet));

        return view;
    }

    private void hideSoftKeyboard(View v){
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void showSoftKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Event event, String comment);
    }

    @Override
    public void onSnippetSelected(InspectionSectionSnippet selectedSnippt) {
        mEditTextComment.append("\r\n");
        mEditTextComment.append(selectedSnippt.getValue());
    }

}
