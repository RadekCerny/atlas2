package au.com.houspect.atlas.viewmodels;

import android.view.View;
import android.widget.CompoundButton;

/**
 * Created by alfredwong on 17/2/17.
 */

public class CheckboxConfirmationViewModel {

    private String title;
    private String content;
    private String checkboxText;
    private Runnable onCancelled;
    private Runnable onConfirmed;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCheckboxText() {
        return checkboxText;
    }

    public void setCheckboxText(String checkboxText) {
        this.checkboxText = checkboxText;
    }

    public CheckboxConfirmationViewModel(String title, String content, String checkboxText, Runnable onCancelled, Runnable onConfirmed) {
        this.title = title;
        this.content = content;
        this.checkboxText = checkboxText;
        this.onCancelled = onCancelled;
        this.onConfirmed = onConfirmed;
    }

    public void onCheckedChanged(CompoundButton checkbox, boolean isChecked) {
        if (isChecked) onConfirmed.run();
    }

    public void onClick(View cancelButton) {
        onCancelled.run();
    }

}
