package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by alfredwong on 3/4/17.
 * ORM model for tracking image upload status.
 */

public class InspectionSectionImageUploadLog extends IdBase {

    @DatabaseField
    private String inspectionId;
    @DatabaseField
    private String sectiondId;
    @DatabaseField
    private String sectionKey;
    @DatabaseField
    private String repeatingKey;
    @DatabaseField
    private String repeatingSeq;
    @DatabaseField
    private long lastSuccessfulUploadTime;
    @DatabaseField
    private boolean isForDeletion;

    public String getFileName() {
        return getId();
    }

    public void setFileName(String fileName) {
        setId(fileName);
    }

    public String getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(String inspectionId) {
        this.inspectionId = inspectionId;
    }

    public String getSectiondId() {
        return sectiondId;
    }

    public void setSectiondId(String sectiondId) {
        this.sectiondId = sectiondId;
    }

    public String getSectionKey() {
        return sectionKey;
    }

    public void setSectionKey(String sectionKey) {
        this.sectionKey = sectionKey;
    }

    public String getRepeatingKey() {
        return repeatingKey;
    }

    public void setRepeatingKey(String repeatingKey) {
        this.repeatingKey = repeatingKey;
    }

    public String getRepeatingSeq() {
        return repeatingSeq;
    }

    public void setRepeatingSeq(String repeatingSeq) {
        this.repeatingSeq = repeatingSeq;
    }

    public long getLastSuccessfulUploadTime() {
        return lastSuccessfulUploadTime;
    }

    public void setLastSuccessfulUploadTime(long lastSuccessfulUploadTime) {
        this.lastSuccessfulUploadTime = lastSuccessfulUploadTime;
    }

    public boolean isForDeletion() {
        return isForDeletion;
    }

    public void setForDeletion(boolean forDeletion) {
        isForDeletion = forDeletion;
    }

    public InspectionSectionImageUploadLog() {}

}
