package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import au.com.houspect.atlas.BR;

//import com.badoualy.stepperindicator.StepperIndicator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public abstract class ViewModelFragment<T> extends Fragment {

    protected T mViewModel;

    public ViewModelFragment() {
        // Required empty public constructor
    }

    protected abstract void onViewModelCreation();

    protected abstract int getLayoutResourceId();

    protected abstract void onConfigureViewDataBinding(ViewDataBinding viewDataBinding);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        onViewModelCreation();
        int layoutResourceId = getLayoutResourceId();
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(inflater, layoutResourceId, container, false);
        viewDataBinding.setVariable(BR.viewModel, mViewModel);
        onConfigureViewDataBinding(viewDataBinding);
        View view = viewDataBinding.getRoot();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
