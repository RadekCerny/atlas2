package au.com.houspect.atlas.services;

/**
 * Created by alfredwong on 12/02/2017.
 */
public interface GenieServiceCallback<T> {

    /**
     * This will be done on the UI thread so there is no need for
     * implementing classes to enforce this.
     *
     * @param response
     */
    public void completed(T response);

}