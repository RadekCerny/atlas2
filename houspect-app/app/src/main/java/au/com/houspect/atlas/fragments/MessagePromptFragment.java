package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import au.com.houspect.atlas.BR;
import au.com.houspect.atlas.R;
import au.com.houspect.atlas.viewmodels.MessagePromptViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MessagePromptFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MessagePromptFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MessagePromptFragment extends Fragment {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static Event OK = new Event(1301);
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TITLE = "title";
    private static final String ARG_MESSAGE = "message";

    private OnFragmentInteractionListener mListener;

    private MessagePromptViewModel mViewModel;

    public MessagePromptFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CommentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MessagePromptFragment newInstance(String title, String message) {
        MessagePromptFragment fragment = new MessagePromptFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mViewModel = new MessagePromptViewModel(
                    getArguments().getString(ARG_TITLE),
                    getArguments().getString(ARG_MESSAGE),
                    new Runnable() {
                        @Override
                        public void run() {
                            mListener.onFragmentInteraction(Event.OK);
                        }
                    }
            );
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_message_prompt, container, false);
        View view = binding.getRoot();
        binding.setVariable(BR.viewModel, mViewModel);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Event event);
    }


}
