package au.com.houspect.atlas.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.fragments.SideMenuFragment;
import au.com.houspect.atlas.models.SideMenuItem;
import au.com.houspect.atlas.utilities.SessionUtil;

/**
 * {@link RecyclerView.Adapter} that can display a {@link SideMenuItem} and makes a call to the
 * specified {@link SideMenuFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class SideMenuAdapter extends RecyclerView.Adapter<SideMenuAdapter.ViewHolder> {

    private final List<SideMenuItem> mValues;
    private final SideMenuFragment.OnListFragmentInteractionListener mListener;
    private int mItemResourceId;
    // TODO:: dependency inject
    private SessionUtil mSessionUtil;

    public SideMenuAdapter(List<SideMenuItem> items, SideMenuFragment.OnListFragmentInteractionListener listener, int itemResourceId, SessionUtil sessionUtil) {
        mValues = items;
        mListener = listener;
        mItemResourceId = itemResourceId;
        mSessionUtil = sessionUtil;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(mItemResourceId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTextViewTitle.setText(holder.mItem.getTitle());
        holder.mImageViewIcon.setImageResource(holder.mItem.getIcon());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });

        // check for highlight information
        if (mSessionUtil != null) {
            boolean shouldHighlight = mSessionUtil.getSideMenuActive() != null && mSessionUtil.getSideMenuActive().getTitle().equals(holder.mItem.getTitle());
            if (shouldHighlight) holder.mView.setBackgroundResource(R.color.colorSideMenuHighlight);
            else holder.mView.setBackgroundResource(android.R.color.transparent);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTextViewTitle;
        public final ImageView mImageViewIcon;
        public SideMenuItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTextViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            mImageViewIcon = (ImageView) view.findViewById(R.id.imageViewIcon);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTextViewTitle.getText() + "'";
        }
    }

}
