package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.utilities.MediaUtil;
import au.com.houspect.atlas.views.CameraPreview;

import static android.content.ContentValues.TAG;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;


/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * {@link CameraPreviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CameraPreviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CameraPreviewFragment extends Fragment {


    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static Event PHOTO_TAKEN = new Event(301);
        public static Event CANCEL = new Event(302);
        public static Event PHOTO_GALLERY = new Event(303);
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Button mButtonCancel;
    private Button mButtonCamera;
    private Button mButtonPhotoGallery;
    private ViewGroup mViewGroupCameraPreview;
    private CameraPreview mCameraPreview;
    private OnFragmentInteractionListener mListener;

    // TODO:: fix weakreference - remove mInspection
    private Inspection mInspection;
    private InspectionSection mInspectionSection;

    private Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            String pathPrefix = mInspection.getId() + "_" + mInspectionSection.getId(); //mInspectionSection.getParent().get().getId() + "_" + mInspectionSection.getId();
            File pictureFile = null; // MediaUtil.getOutputMediaFile(getContext(), MEDIA_TYPE_IMAGE, pathPrefix);

            if (pictureFile == null) Log.d(TAG, "Error creating media file, check storage permissions");

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "Error accessing file: " + e.getMessage());
            }
            mListener.onFragmentInteraction(Event.PHOTO_TAKEN, pictureFile.getAbsolutePath());

        }
    };

    public CameraPreviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CameraPreviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CameraPreviewFragment newInstance(String param1, String param2) {
        CameraPreviewFragment fragment = new CameraPreviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        // TODO:: dependency inject
        //mInspectionSection = (new DemoUtil()).generateInspection1().getSections().get(0);
        //mInspection = (new DemoUtil()).generateInspection1();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_camera_preview, container, false);
        mButtonPhotoGallery = (Button) view.findViewById(R.id.buttonPhotoGallery);
        mButtonPhotoGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(Event.PHOTO_GALLERY, null);
            }
        });

        mButtonCancel = (Button) view.findViewById(R.id.buttonCancel);
        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(Event.CANCEL, null);
            }
        });

        mButtonCamera = (Button) view.findViewById(R.id.buttonCamera);
        mButtonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCameraPreview.getCamera().takePicture(null, null, mPictureCallback);
            }
        });
        mViewGroupCameraPreview = (ViewGroup) view.findViewById(R.id.viewGroupCameraPreview);
        showCameraPreview();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Event event, Object data);
    }

    private void showCameraPreview() {

        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            e.printStackTrace();
            // Camera is not available (in use or does not exist)
        }

        if (c == null) return;

        CameraPreview p = new CameraPreview(this.getContext(), c);
        mCameraPreview = p;
        mViewGroupCameraPreview.addView(p);

    }

}
