package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.adapters.SideMenuInspectionTOCAdapter;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.services.DataService;
import au.com.houspect.atlas.services.DataServiceImpl;
import au.com.houspect.atlas.services.GenieService;
import au.com.houspect.atlas.services.GenieServiceImpl;
import au.com.houspect.atlas.services.SharedPreferencesService;
import au.com.houspect.atlas.services.SharedPreferencesServiceImpl;
import au.com.houspect.atlas.utilities.SessionUtil;
import au.com.houspect.atlas.utilities.Util;


/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SideMenuInspectionTOCFragment extends Fragment implements SideMenuInspectionTOCAdapter.Listener {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static SideMenuInspectionTOCFragment.Event ITEM_SELECTED = new SideMenuInspectionTOCFragment.Event(3001);
        public static SideMenuInspectionTOCFragment.Event CLONE_SECTION_REQUESTED = new SideMenuInspectionTOCFragment.Event(3002);
        public static SideMenuInspectionTOCFragment.Event REMOVE_CLONED_SECTION_REQUESTED = new SideMenuInspectionTOCFragment.Event(3003);

    }

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "columnCount";
    private static final String ARG_INSPECTION_ID = "inspectionId";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private SideMenuInspectionTOCAdapter mAdapter;
    private Inspection mInspection;

    private SessionUtil mSessionUtil;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SideMenuInspectionTOCFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static SideMenuInspectionTOCFragment newInstance(int columnCount, Inspection inspection) {
        SideMenuInspectionTOCFragment fragment = new SideMenuInspectionTOCFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putString(ARG_INSPECTION_ID, inspection.getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);

            // Note:: certain device cannot handle storage of Inspection in bundle, hence store inspectionId instead. Revise this design in the future.

            String inspectionId = getArguments().getString(ARG_INSPECTION_ID);

            // TODO:: dependency inject
            GenieService genieService = new GenieServiceImpl();
            SharedPreferencesService sharedPreferencesService = new SharedPreferencesServiceImpl(getContext());
            DataService dataService = new DataServiceImpl(getContext(), genieService, sharedPreferencesService);
            mInspection = dataService.get(inspectionId, Draft.class);
            if (mInspection == null) {
                Log.i("SideMenu", "There is no Draft record for id (" + inspectionId + ")");
                mInspection = dataService.get(inspectionId, Inspection.class);
                if (mInspection == null) {
                    Log.e("SideMenu", "There is also no Inspection record for id (" + inspectionId + ")");
                }
            }
            try {
                Util.parseInspectionXml(mInspection.getXml(), mInspection);
            } catch (Exception e) {
                Log.e("SideMenu", e.getLocalizedMessage());
            }

            // End Note

        }

        //TODO:: dependency inject
        mSessionUtil = SessionUtil.singleton;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sidemenu_inpsection_toc, container, false);
        boolean isRecyclerView = view instanceof RecyclerView;
        RecyclerView recyclerView = isRecyclerView ? (RecyclerView)view :  (RecyclerView)view.findViewById(R.id.list);

        // Set the adapter
        if (recyclerView != null) {
            Context context = view.getContext();
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            SideMenuInspectionTOCAdapter adapter = new SideMenuInspectionTOCAdapter(mInspection.getSections(), this, R.layout.fragment_sidemenu_inspection_toc_item, mSessionUtil);
            mAdapter = adapter;

            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // @param int selectedTOCIndex - 0-based index inclusive of start, sections, end
        void onListFragmentInteraction(Event event, InspectionSection selectedSection, int selectedTOCIndex);
    }

    public void refresh() {
        mAdapter.notifyDataSetChanged();
    }


    //region SideMenuInspectionTOCAdapter.Listener
    @Override
    public void onCloneRequested(InspectionSection selectedSection, int selectedTOCIndex) {
        mListener.onListFragmentInteraction(Event.CLONE_SECTION_REQUESTED, selectedSection, selectedTOCIndex);
    }

    @Override
    public void onRemoveCloneRequested(InspectionSection selectedSection, int selectedTOCIndex) {
        mListener.onListFragmentInteraction(Event.REMOVE_CLONED_SECTION_REQUESTED, selectedSection, selectedTOCIndex);
    }

    @Override
    public void onItemSelected(InspectionSection selectedSection, int selectedTOCIndex) {
        mListener.onListFragmentInteraction(Event.ITEM_SELECTED, selectedSection, selectedTOCIndex);
    }
    //endregion
}
