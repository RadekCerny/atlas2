package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import au.com.houspect.atlas.utilities.Util;

import static android.R.attr.format;

/**
 * Created by alfredwong on 28/01/2017.
 */

public class Job extends IdBase implements Parcelable, ITemporal {

    @DatabaseField
    private String code;
    @DatabaseField
    private String clientName;
    @DatabaseField
    private String street;
    @DatabaseField
    private String suburb;
    @DatabaseField
    private String date;
    @DatabaseField
    private String time;
    @DatabaseField
    private long timeSince1stJan1970;
    private String notes;
    @DatabaseField
    private String reportType;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
        timeSince1stJan1970 = computeTimeSince1stJan1970();
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
        timeSince1stJan1970 = computeTimeSince1stJan1970();
    }

    public long getTimeSince1stJan1970() {
        return timeSince1stJan1970;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    /**
     * Compute time in milliseconds since January 1, 1970 base on getDate() and getTime()
     * @return
     */
    public long computeTimeSince1stJan1970() {
        return Util.computeTimeSince1stJan1970(date, time);
    }

    public Job() {}

    //region Parcelable
    private Job(Parcel in) {
        setId(in.readString());
        code = in.readString();
        clientName = in.readString();
        street = in.readString();
        suburb = in.readString();
        date = in.readString();
        time = in.readString();
        notes = in.readString();
        reportType = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(code);
        dest.writeString(clientName);
        dest.writeString(street);
        dest.writeString(suburb);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(notes);
        dest.writeString(reportType);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Job createFromParcel(Parcel source) {
            return new Job(source);
        }

        @Override
        public Job[] newArray(int size) {
            return new Job[size];
        }
    };
    //endregion
}
