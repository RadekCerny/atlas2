package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.HashMap;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.services.GenieService;
import au.com.houspect.atlas.services.GenieServiceCallback;
import au.com.houspect.atlas.services.GenieServiceImpl;
import au.com.houspect.atlas.services.GenieServiceResponse;
import au.com.houspect.atlas.viewmodels.ForgotPasswordViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ForgotPasswordFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ForgotPasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ForgotPasswordFragment extends ViewModelFragment<ForgotPasswordViewModel> {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static ForgotPasswordFragment.Event CANCEL = new ForgotPasswordFragment.Event(1601);
        public static ForgotPasswordFragment.Event SUBMIT = new ForgotPasswordFragment.Event(1602);

    }

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_USERNAME = "username";

    private String mUsername;
    private OnFragmentInteractionListener mListener;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param username
     * @return A new instance of fragment InspectionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ForgotPasswordFragment newInstance(String username) {
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USERNAME, username);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUsername = getArguments().getString(ARG_USERNAME);
        }
    }

    @Override
    protected void onViewModelCreation() {
        mViewModel = new ForgotPasswordViewModel(
                getResources().getString(R.string.message_forgot_password_title),
                mUsername,
                new Runnable() {

                    @Override
                    public void run() {
                        mListener.onFragmentInteraction(Event.CANCEL, null);
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {

                        if (!mViewModel.validateData()) {
                            Toast.makeText(getContext(), getResources().getString(R.string.message_forgot_password_invalid_data), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        HashMap<String,String> data = new HashMap<>();
                        data.put("username", mViewModel.getUsername());
                        data.put("email", mViewModel.getEmail());
                        mListener.onFragmentInteraction(Event.SUBMIT, data);
                    }
                });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    protected void onConfigureViewDataBinding(ViewDataBinding viewDataBinding) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Event event, Object data);
    }

}
