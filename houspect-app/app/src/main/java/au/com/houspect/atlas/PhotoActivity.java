package au.com.houspect.atlas;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import au.com.houspect.atlas.fragments.InspectionFragment;
import au.com.houspect.atlas.fragments.LoginFragment;
import au.com.houspect.atlas.fragments.PhotoEditFragment;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.utilities.MediaUtil;

public class PhotoActivity extends AppCompatActivity {

    public static final String ARG_INSPECTION_ID = "inspectionId";
    public static final String ARG_INSPECTION_SECTION_ID = "inspectionSectionId";
    public static final String ARG_PHOTO_PATH = "photoPath"; // existing photo path
    public static final String ARG_INSPECTION_SECTION_IMAGE = "inspectionSectionImage";

    private String inspectionId;
    private String inspectionSectionId;
    private String photoPath;
    private InspectionSectionImage inspectionSectionImage;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        Bundle bundle = getIntent().getBundleExtra("bundle");
        inspectionId = bundle.getString(ARG_INSPECTION_ID);
        inspectionSectionId = bundle.getString(ARG_INSPECTION_SECTION_ID);
        photoPath = bundle.getString(ARG_PHOTO_PATH);
        inspectionSectionImage = bundle.getParcelable(ARG_INSPECTION_SECTION_IMAGE);

        boolean needsToWaitForUserPermissionAction =
                MediaUtil.checkAndRequestPhotoStoragePermission(this,
                MediaUtil.PermissionRequestCode.REQUEST_WRITE_STORAGE_EDIT_PHOTO);

        if (!needsToWaitForUserPermissionAction) {
            Log.d("PhotoActivity", "onCreate (" + inspectionId + ") (" + inspectionSectionId + ") photoPath (" + photoPath + ")");
            showPhotoEdit(inspectionId, inspectionSectionId, photoPath, inspectionSectionImage);
        }
    }

    void showPhotoEdit(String inspectionId, String inspectionSectionId, String photoPath, InspectionSectionImage inspectionSectionImage) {
        PhotoEditFragment fragment = PhotoEditFragment.newInstance(inspectionId, inspectionSectionId, photoPath, inspectionSectionImage);
        showFragment(fragment);
    }

    void showFragment(Fragment fragment) {

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        showFragment(fragment, R.id.frameLayoutContent, true);
    }

    void showFragment(Fragment fragment, int containerId, boolean shouldAnimate) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (shouldAnimate) transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(containerId, fragment);
        transaction.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if (grantResults.length == 0
                || grantResults[0] !=
                PackageManager.PERMISSION_GRANTED) {

            Log.i("Atlas", "Photo storage permission has been denied by user");

            Toast.makeText(this, "Fail to save photo. Photo storage permission has been denied by user.", Toast.LENGTH_SHORT).show();

            setResult(RESULT_CANCELED);
            return;

        } else {
            Log.i("Atlas", "Photo storage permission has been granted by user");
        }


        boolean isEditPhoto = requestCode == MediaUtil.PermissionRequestCode.REQUEST_WRITE_STORAGE_EDIT_PHOTO.getCode();
        if (isEditPhoto) {
            Log.d("PhotoActivity", "onCreate (" + inspectionId + ") (" + inspectionSectionId + ") photoPath (" + photoPath + ")");
            showPhotoEdit(inspectionId, inspectionSectionId, photoPath, inspectionSectionImage);
        }
    }
}
