package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alfredwong on 30/1/17.
 */

public class InspectionSection extends IdBase implements Parcelable, Cloneable {
    private String parentInspectionCode;
    private String parentInspectionId;

    private String key;
    private String heading;
    private String repeatingKey;
    private String repeatingSeq;
    private String instanceName;

    // Questions related
    private String name;
    private boolean isOptional;
    private String status;
    private List<InspectionSectionItem>  items;
    private List<InspectionSectionSnippet> snippets;

    // Answers related
    private String comments;
    private String recommendations;
    private boolean isNa;

    private List<InspectionSectionImage> images;

    //region app attributes i.e. not server related
    private boolean isAClone = false;
    private String cloneSourceName; // Note: deprecated - consider removing it
    //endregion

    //region in-memory attributes - wont be serialised to xml or stored in persistent storage
    // TODO - phase out sectionIndex - old code (prior to 2018 Scope 3 - Item 11) it is used by the flow:
    // TOC (side menu) -- press clone "+" or remove clone "-" --> handle is passed to InspectionFragment's cloneInspectionSection
    // cloneInspectionSection uses getSectionIndex to compute next position to insert the clone + navigation - goToSection.
    // It is no longer needed as cloneInspectionSection uses indexOf the section to be cloned to get the info.
    // The current dependency is by InspectionFragment's removeInspectionSection. It can also use indexOf to compute the index for removal.
    private int sectionIndex;

    public int getSectionIndex() {
        return sectionIndex;
    }

    public void setSectionIndex(int sectionIndex) {
        this.sectionIndex = sectionIndex;
    }
    //endregion

    public InspectionSection() {}

    public String getParentInspectionId() {
        return parentInspectionId;
    }

    public void setParentInspectionId(String parentInspectionId) {
        this.parentInspectionId = parentInspectionId;
    }

    public String getParentInspectionCode() {
        return parentInspectionCode;
    }

    public void setParentInspectionCode(String parentInspectionCode) {
        this.parentInspectionCode = parentInspectionCode;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getRepeatingKey() {
        return repeatingKey;
    }

    public void setRepeatingKey(String repeatingKey) {
        this.repeatingKey = repeatingKey;
    }

    public String getRepeatingSeq() {
        return repeatingSeq;
    }

    public void setRepeatingSeq(String repeatingSeq) {
        this.repeatingSeq = repeatingSeq;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOptional() {
        return isOptional;
    }

    public void setOptional(boolean optional) {
        isOptional = optional;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<InspectionSectionItem> getItems() {
        return items;
    }

    public void setItems(List<InspectionSectionItem> items) {
        this.items = items;
    }

    public List<InspectionSectionSnippet> getSnippets() {
        return snippets;
    }

    public void setSnippets(List<InspectionSectionSnippet> snippets) {
        this.snippets = snippets;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    public boolean isNa() {
        return isNa;
    }

    public void setNa(boolean na) {
        isNa = na;
    }

    public List<InspectionSectionImage> getImages() {
        return images;
    }

    public void setImages(List<InspectionSectionImage> images) {
        this.images = images;
    }

    public boolean isAClone() {
        return isAClone;
    }

    public void setAClone(boolean AClone) {
        isAClone = AClone;
    }

    public String getCloneSourceName() {
        return cloneSourceName;
    }

    public void setCloneSourceName(String cloneSourceName) {
        this.cloneSourceName = cloneSourceName;
    }

    //region Parcelable
    private InspectionSection(Parcel in) {
        setId(in.readString());
        name = in.readString();
        isOptional = in.readByte() != 0;
        status = in.readString();
        items = in.createTypedArrayList(InspectionSectionItem.CREATOR);
        snippets = in.createTypedArrayList(InspectionSectionSnippet.CREATOR);
        comments = in.readString();
        recommendations = in.readString();
        isNa = in.readByte() != 0;
        images = in.createTypedArrayList(InspectionSectionImage.CREATOR);

        parentInspectionId = in.readString();
        key = in.readString();
        heading = in.readString();
        repeatingKey = in.readString();
        repeatingSeq = in.readString();
        instanceName = in.readString();

        isAClone = in.readByte() != 0;
        cloneSourceName = in.readString();


        sectionIndex = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(name);
        dest.writeByte((byte)(isOptional ? 1 : 0));
        dest.writeString(status);
        dest.writeTypedList(items);
        dest.writeTypedList(snippets);
        dest.writeString(comments);
        dest.writeString(recommendations);
        dest.writeByte((byte)(isNa ? 1 : 0));
        dest.writeTypedList(images);
        dest.writeString(parentInspectionId);
        dest.writeString(key);
        dest.writeString(heading);
        dest.writeString(repeatingKey);
        dest.writeString(repeatingSeq);
        dest.writeString(instanceName);
        dest.writeByte((byte)(isAClone ? 1 : 0));
        dest.writeString(cloneSourceName);

        dest.writeInt(sectionIndex);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public InspectionSection createFromParcel(Parcel source) {
            return new InspectionSection(source);
        }

        @Override
        public InspectionSection[] newArray(int size) {
            return new InspectionSection[size];
        }
    };
    //endregion

    //region Clone related & Cloneable
    public boolean canClone() {
        boolean canClone = !TextUtils.isEmpty(getRepeatingKey());
        if (!canClone) return false;
        canClone = "1".equalsIgnoreCase(getRepeatingSeq());
        return canClone;
    }

    public Object clone(String repeatingSeqToBeSetOnClone) throws CloneNotSupportedException {
        InspectionSection inspectionSection = new InspectionSection();
        inspectionSection.setAClone(true);
        inspectionSection.setId(getId());
        inspectionSection.setKey("0");
        inspectionSection.setParentInspectionId(getParentInspectionId());
        inspectionSection.setName(getName());
        inspectionSection.setCloneSourceName(getName());
        inspectionSection.setOptional(isOptional());
        inspectionSection.setStatus(getStatus());
        inspectionSection.setInstanceName(repeatingSeqToBeSetOnClone);

        List<InspectionSectionItem> inspectionSectionItems = new ArrayList<>();
        for (InspectionSectionItem item : getItems()) {
            inspectionSectionItems.add((InspectionSectionItem)item.clone());
        }
        inspectionSection.setItems(inspectionSectionItems);

        List<InspectionSectionSnippet> inspectionSectionSnippets = new ArrayList<>();
        for (InspectionSectionSnippet snippet : getSnippets()) {
            inspectionSectionSnippets.add((InspectionSectionSnippet)snippet.clone());
        }
        inspectionSection.setSnippets(inspectionSectionSnippets);

        inspectionSection.setRecommendations(""); // do not clone recommendations
        inspectionSection.setNa(isNa());
        inspectionSection.setRepeatingKey(getRepeatingKey());
        inspectionSection.setRepeatingSeq(repeatingSeqToBeSetOnClone);
        inspectionSection.setHeading(getHeading());
        inspectionSection.setImages(new ArrayList<InspectionSectionImage>());

        return inspectionSection;
    }
    //endregion

    public boolean isComplete() {
        boolean isComplete = true;
        if (isNa()) return isComplete;
        for (InspectionSectionItem item : items) {
            isComplete = item.isComplete();
            if (!isComplete) break;
        }
        return isComplete;
    }
}
