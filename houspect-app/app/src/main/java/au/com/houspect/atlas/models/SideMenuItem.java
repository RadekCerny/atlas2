package au.com.houspect.atlas.models;

/**
 * Created by alfredwong on 26/01/2017.
 */

public class SideMenuItem {
    private int mIcon;
    private String mTitle;

    public int getIcon() {
        return mIcon;
    }

    public void setIcon(int icon) {
        mIcon = icon;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public SideMenuItem(int icon, String title) {
        mIcon = icon;
        mTitle = title;
    }
}