package au.com.houspect.atlas.viewmodels;

import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;

/**
 * Created by alfredwong on 2/3/17.
 */

public class LoginViewModel {

    public final ObservableField<String> username = new ObservableField<>("");
    private String password;
    public final ObservableBoolean shouldUseCachedCredentials = new ObservableBoolean(false);
    public final ObservableBoolean isProcessing = new ObservableBoolean(true); // default to true
    private String version;
    private Runnable onLogin;
    private Runnable onForgotPassword;
    private Runnable onNotMe;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public LoginViewModel(Runnable onLogin, Runnable onForgotPassword, Runnable onNotMe) {
        this.onLogin = onLogin;
        this.onForgotPassword = onForgotPassword;
        this.onNotMe = onNotMe;
    }

    public void onLoginClick(View v) {
        onLogin.run();
    }

    public void onForgotPasswordClick(View v) {
        onForgotPassword.run();
    }

    public void onNotMeClick(View v) { onNotMe.run(); }
}
