package au.com.houspect.atlas.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import au.com.houspect.atlas.BR;
import au.com.houspect.atlas.fragments.JobListFragment;
import au.com.houspect.atlas.fragments.dummy.DummyContent.DummyItem;


/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link JobListFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class BindingRecyclerViewAdapter<T> extends RecyclerView.Adapter<BindingRecyclerViewAdapter.BindingHolder> {


    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SECTION_HEADER = 1;

    private final List<T> mValues;
    private final OnListInteractionListener mListener;
    private int mItemResourceId;

    public interface OnListInteractionListener<T> {
        public void onListInteraction(T selectedItem);
    }

    public BindingRecyclerViewAdapter(List<T> items, OnListInteractionListener listener,
                                      int itemResourceId) {

        mValues = items;
        mListener = listener;
        mItemResourceId = itemResourceId;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(mItemResourceId, parent, false);
        return new BindingHolder(view);
    }

    @Override
    public void onBindViewHolder(BindingRecyclerViewAdapter.BindingHolder holder, int position) {

        final T item = mValues.get(position);
        final BindingHolder itemViewHolder = holder;
        holder.viewDataBinding.setVariable(BR.model, item);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListInteraction(item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class BindingHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ViewDataBinding viewDataBinding;

        public BindingHolder(View view) {
            super(view);
            mView = view;
            viewDataBinding = DataBindingUtil.bind(view);
        }
    }

}
