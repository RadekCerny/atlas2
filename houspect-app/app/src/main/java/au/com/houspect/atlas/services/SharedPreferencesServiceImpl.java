package au.com.houspect.atlas.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.securepreferences.SecurePreferences;

/**
 * Created by alfredwong on 2/3/17.
 */

public class SharedPreferencesServiceImpl implements SharedPreferencesService {

    private static String KEY_SESSION_TOKEN = "sessionToken";
    private static String KEY_USERNAME = "username";
    private static String KEY_PASSWORD = "password";
    private static String KEY_SHOULD_UPLOAD_PHOTOS_WHEN_ONLINE = "shouldUploadPhotosWhenOnline";
    private static String KEY_SHOULD_RESIZE_PHOTOS_WHEN_UPLOAD = "shouldResizePhotosWhenUpload";
    private static String KEY_RESIZE_PHOTOS_MAX_WIDTH_OR_HEIGHT = "resizePhotosMaxWidthOrHeight";

    private Context context;

    public SharedPreferencesServiceImpl(Context context) {
        this.context = context;
    }

    private SharedPreferences getSharedPreferences() {
        SharedPreferences sharedPreferences = new SecurePreferences(context); //context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    @Override
    public void saveSessionToken(String sessionToken) {
        getSharedPreferences().edit().putString(KEY_SESSION_TOKEN, sessionToken).commit();
    }

    @Override
    public String getSessionToken() {
        return getSharedPreferences().getString(KEY_SESSION_TOKEN, null);
    }

    @Override
    public void saveUsername(String username) {
        getSharedPreferences().edit().putString(KEY_USERNAME, username).commit();
    }

    @Override
    public String getUsername() {
        return getSharedPreferences().getString(KEY_USERNAME, null);
    }

    @Override
    public void savePassword(String password) {
        getSharedPreferences().edit().putString(KEY_PASSWORD, password).commit();
    }

    @Override
    public String getPassword() {
        return getSharedPreferences().getString(KEY_PASSWORD, null);
    }

    @Override
    public void saveShouldUploadPhotosWhenOnline(boolean yesOrNo) {
        getSharedPreferences().edit().putBoolean(KEY_SHOULD_UPLOAD_PHOTOS_WHEN_ONLINE, yesOrNo).commit();
    }

    @Override
    public boolean getShouldUploadPhotosWhenOnline() {
        return getSharedPreferences().getBoolean(KEY_SHOULD_UPLOAD_PHOTOS_WHEN_ONLINE, true);
    }

    @Override
    public void saveShouldResizePhotosWhenUpload(boolean yesOrNo) {
        getSharedPreferences().edit().putBoolean(KEY_SHOULD_RESIZE_PHOTOS_WHEN_UPLOAD, yesOrNo).commit();
    }

    @Override
    public boolean getShouldResizePhotosWhenUpload() {
        return getSharedPreferences().getBoolean(KEY_SHOULD_RESIZE_PHOTOS_WHEN_UPLOAD, true);
    }

    @Override
    public void saveResizePhotosToMaxWidthOrHeight(int maxWidthOrHeight) {
        getSharedPreferences().edit().putInt(KEY_RESIZE_PHOTOS_MAX_WIDTH_OR_HEIGHT, maxWidthOrHeight).commit();
    }

    @Override
    public int getResizePhotosToMaxWidthOrHeight() {
        return getSharedPreferences().getInt(KEY_RESIZE_PHOTOS_MAX_WIDTH_OR_HEIGHT, 1024);
    }
}
