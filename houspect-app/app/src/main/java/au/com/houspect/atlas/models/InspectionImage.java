package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alfredwong on 2/8/17.
 */

public class InspectionImage extends IdBase implements Parcelable {
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public InspectionImage() {}

    //region Parcelable
    private InspectionImage(Parcel in) {
        setId(in.readString());
        fileName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(fileName);
    }

    public static final Creator CREATOR = new Creator() {

        @Override
        public InspectionImage createFromParcel(Parcel source) {
            return new InspectionImage(source);
        }

        @Override
        public InspectionImage[] newArray(int size) {
            return new InspectionImage[size];
        }
    };
    //endregion Parcelable
}
