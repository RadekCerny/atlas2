package au.com.houspect.atlas.constants;

/**
 * Created by alfredwong on 11/6/17.
 */

public class YesNoConfirmationFragmentRequestCode {
    private int value;
    public YesNoConfirmationFragmentRequestCode(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static YesNoConfirmationFragmentRequestCode StartInspection = new YesNoConfirmationFragmentRequestCode(300);
    public static YesNoConfirmationFragmentRequestCode SaveAsZeusDraft = new YesNoConfirmationFragmentRequestCode(310);
    public static YesNoConfirmationFragmentRequestCode SubmitFinalReport = new YesNoConfirmationFragmentRequestCode(320);
    public static YesNoConfirmationFragmentRequestCode SaveAsAtlasDraft = new YesNoConfirmationFragmentRequestCode(330);
    public static YesNoConfirmationFragmentRequestCode CloneInspectionSection = new YesNoConfirmationFragmentRequestCode(340);
    public static YesNoConfirmationFragmentRequestCode RemoveCloneInspectionSection = new YesNoConfirmationFragmentRequestCode(350);

    public static YesNoConfirmationFragmentRequestCode get(int requestCode) {
        if (requestCode == StartInspection.getValue()) return StartInspection;
        else if (requestCode == SaveAsZeusDraft.getValue()) return SaveAsZeusDraft;
        else if (requestCode == SubmitFinalReport.getValue()) return SubmitFinalReport;
        else if (requestCode == SaveAsAtlasDraft.getValue()) return SaveAsAtlasDraft;
        else if (requestCode == CloneInspectionSection.getValue()) return CloneInspectionSection;
        else if (requestCode == RemoveCloneInspectionSection.getValue()) return RemoveCloneInspectionSection;
        return null;
    }
}
