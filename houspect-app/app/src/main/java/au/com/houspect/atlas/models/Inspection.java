package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.j256.ormlite.field.DatabaseField;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.HashMap;
import java.util.List;

import au.com.houspect.atlas.utilities.Util;

/**
 * Created by alfredwong on 30/1/17.
 */

public class Inspection extends IdBase implements Parcelable {
    private String code;
    private String submit;
    @DatabaseField
    private String address;
    private double latitude;
    private double longitude;
    private String inspectionReportType;
    private String date;
    private String time;
    private String duration;
    private String jobNotes;
    private String occupancy;
    private int noOfBedrooms;
    private int noOfBathrooms;
    private String propertyLevels;
    private String propertyFeatures;
    private String numberOfUnits;
    private InspectionClient client;
    private List<InspectionClient> contacts;
    private List<InspectionSection> sections;
    private String recommendations;
    private InspectionImage image;
    private String theirJobNo;

    // App attribute only
    // Indicates if the underlying xml has been parsed into the other fields
    private boolean isXmlParsed;

    public boolean isXmlParsed() {
        return isXmlParsed;
    }

    public void setXmlParsed(boolean xmlParsed) {
        isXmlParsed = xmlParsed;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    @DatabaseField
    private String xml;

    //region Meta
    private Meta mMeta = new Meta();
    public Meta getMeta() { return mMeta; }
    public static class Meta {
        private HashMap<String, Integer> mSectionRepeatingKeyToMaxRepeatingSeq;
        public HashMap<String, Integer> getSectionRepeatingKeyToSectionCount() { return mSectionRepeatingKeyToMaxRepeatingSeq; }
        public Meta() {
            mSectionRepeatingKeyToMaxRepeatingSeq = new HashMap<>();
        }
        public void addSection(InspectionSection section) {
            String repeatingKey = section.getRepeatingKey();
            if (repeatingKey == null) return;
            String repeatingSeqAsString = section.getRepeatingSeq();
            int maxRepeatingSeq = getMaxRepeatingSeqByRepeatingKey(repeatingKey);
            int repeatingSeq = 0;
            if (repeatingSeqAsString != null && NumberUtils.isDigits(repeatingSeqAsString)) repeatingSeq = Integer.parseInt(repeatingSeqAsString);
            maxRepeatingSeq = Math.max(maxRepeatingSeq, repeatingSeq);
            mSectionRepeatingKeyToMaxRepeatingSeq.put(repeatingKey, maxRepeatingSeq);
        }
        public int getMaxRepeatingSeqByRepeatingKey(String repeatingKey) {
            int maxRepeatingSeq = 0;
            if (mSectionRepeatingKeyToMaxRepeatingSeq.containsKey(repeatingKey)) {
                maxRepeatingSeq = mSectionRepeatingKeyToMaxRepeatingSeq.get(repeatingKey);
            }
            return maxRepeatingSeq;
        }
    }
    //endregion

    public Inspection() {}

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getSubmit() {
        return submit;
    }

    public void setSubmit(String submit) {
        this.submit = submit;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getInspectionReportType() {
        return inspectionReportType;
    }

    public void setInspectionReportType(String inspectionReportType) {
        this.inspectionReportType = inspectionReportType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getJobNotes() {
        return jobNotes;
    }

    public void setJobNotes(String jobNotes) {
        this.jobNotes = jobNotes;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public int getNoOfBedrooms() {
        return noOfBedrooms;
    }

    public void setNoOfBedrooms(int noOfBedrooms) {
        this.noOfBedrooms = noOfBedrooms;
    }

    public int getNoOfBathrooms() {
        return noOfBathrooms;
    }

    public void setNoOfBathrooms(int noOfBathrooms) {
        this.noOfBathrooms = noOfBathrooms;
    }

    public String getPropertyLevels() {
        return propertyLevels;
    }

    public void setPropertyLevels(String propertyLevels) {
        this.propertyLevels = propertyLevels;
    }

    public String getPropertyFeatures() {
        return propertyFeatures;
    }

    public void setPropertyFeatures(String propertyFeatures) {
        this.propertyFeatures = propertyFeatures;
    }

    public String getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(String numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public InspectionClient getClient() {
        return client;
    }

    public void setClient(InspectionClient client) {
        this.client = client;
    }

    public List<InspectionClient> getContacts() {
        return contacts;
    }

    public void setContacts(List<InspectionClient> contacts) {
        this.contacts = contacts;
    }

    public List<InspectionSection> getSections() {
        return sections;
    }

    public void setSections(List<InspectionSection> inspectionSections) {
        this.sections = inspectionSections;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    public InspectionImage getImage() {
        return image;
    }

    public void setImage(InspectionImage inspectionImage) {
        this.image = inspectionImage;
    }

    public String getTheirJobNo() {
        return theirJobNo;
    }

    public void setTheirJobNo(String theirJobNo) {
        this.theirJobNo = theirJobNo;
    }

    //region Parcelable
    protected Inspection(Parcel in) {
        setId(in.readString());
        code = in.readString();
        submit = in.readString();
        address = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        inspectionReportType = in.readString();
        date = in.readString();
        time = in.readString();
        duration = in.readString();
        jobNotes = in.readString();
        occupancy = in.readString();
        noOfBedrooms = in.readInt();
        noOfBathrooms = in.readInt();
        propertyLevels = in.readString();
        propertyFeatures = in.readString();
        numberOfUnits = in.readString();
        client = in.readParcelable(InspectionClient.class.getClassLoader());
        contacts = in.createTypedArrayList(InspectionClient.CREATOR);
        sections = in.createTypedArrayList(InspectionSection.CREATOR);
        xml = in.readString();
        recommendations = in.readString();
        image = in.readParcelable(InspectionImage.class.getClassLoader());
        theirJobNo = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(code);
        dest.writeString(submit);
        dest.writeString(address);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(inspectionReportType);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(duration);
        dest.writeString(jobNotes);
        dest.writeString(occupancy);
        dest.writeInt(noOfBedrooms);
        dest.writeInt(noOfBathrooms);
        dest.writeString(propertyLevels);
        dest.writeString(propertyFeatures);
        dest.writeString(numberOfUnits);
        dest.writeParcelable(client, flags);
        dest.writeTypedList(contacts);
        dest.writeTypedList(sections);
        dest.writeString(xml);
        dest.writeString(recommendations);
        dest.writeParcelable(image, flags);
        dest.writeString(theirJobNo);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Inspection createFromParcel(Parcel source) {
            return new Inspection(source);
        }

        @Override
        public Inspection[] newArray(int size) {
            return new Inspection[size];
        }
    };
    //endregion

}
