package au.com.houspect.atlas.viewmodels;

import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.widget.CompoundButton;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import au.com.houspect.atlas.models.InspectionSectionItemTwoNumbers;

/**
 * Created by alfredwong on 12/7/18.
 */

public class InspectionSectionItemTwoNumbersViewModel {

    private InspectionSectionItemTwoNumbers model;
    public ObservableBoolean isCalculateTotalChecked = new ObservableBoolean(false);
    public ObservableField<String> observableNumber1 = new ObservableField("");
    public ObservableField<String> observableNumber2 = new ObservableField("");
    public ObservableField<String> observableTotal = new ObservableField("");

    public InspectionSectionItemTwoNumbersViewModel(InspectionSectionItemTwoNumbers model) {
        this.model = model;

        boolean isModelNumber1Empty = this.model.isNumber1Empty();
        boolean isModelNumber2Empty = this.model.isNumber2Empty();

        // ref - radek's email - If number1 is blank and number2 is blank then assume the calculate total is checked
        boolean shouldCheckCalculateTotal = (isModelNumber1Empty && isModelNumber2Empty) || (!isModelNumber1Empty && !isModelNumber2Empty) || !isModelNumber1Empty || !isModelNumber2Empty;
        // ref - email with radek rule 1a
        if ("na".equalsIgnoreCase(this.model.getNumber1())) shouldCheckCalculateTotal = false;
        isCalculateTotalChecked.set(shouldCheckCalculateTotal);

        // model to display - avoid na from getting displayed
        String number1 = isModelNumber1Empty ? "" : this.model.getNumber1();
        String number2 = isModelNumber2Empty || "na".equalsIgnoreCase(this.model.getNumber1()) ? "" : this.model.getNumber2();

        observableNumber1.set(number1);
        observableNumber2.set(number2);
        observableTotal.set(this.model.getResult());

        observableNumber1.addOnPropertyChangedCallback(onObservablePropertyChangedCallback);
        observableNumber2.addOnPropertyChangedCallback(onObservablePropertyChangedCallback);
        observableTotal.addOnPropertyChangedCallback(onObservablePropertyChangedCallback);
    }

    private Observable.OnPropertyChangedCallback onObservablePropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            // only handles certain properties
            if (sender != observableNumber1 && sender != observableNumber2 && sender != observableTotal) return;
            // sync model values base on view model obserable values
            updateModelValues();
        }
    };

    /**
     * Base on the ViewModel observable values, update model property values i.e. number1, number2 and result
     */
    public void updateModelValues() {
        // default to get manual input from total field i.e. EditText binding to observableTotal
        model.setResult(observableTotal.get());

        // if calculate total is checked - utilise number1 and number2
        // ref - radek's email -  if calculate total is ticked, send number1=”4” and number2=”33”.
        if (isCalculateTotalChecked.get()) {
            model.setNumber1(observableNumber1.get());
            model.setNumber2(observableNumber2.get());
            NumberFormat numberFormat = new DecimalFormat("##.##");
            String total = numberFormat.format(model.calculateTotal());
            model.setResult(total);

            // temporarily disable observation of changes to observable in order to update display w/o recursively triggering updateModelValues again
            observableTotal.removeOnPropertyChangedCallback(onObservablePropertyChangedCallback);
            observableTotal.set(model.getResult());
            observableTotal.addOnPropertyChangedCallback(onObservablePropertyChangedCallback);
        }
        // ref - radek's email - If unticked send number1=”na” and number2=”33” or whatever number is type into the total
        else {
            model.setNumber1("na");
            model.setNumber2(observableTotal.get());
            model.setResult(observableTotal.get());
        }
    }


    public InspectionSectionItemTwoNumbers getModel() {
        return model;
    }

    public void onCalculateTotalCheckedChanged(CompoundButton checkbox, boolean isChecked) {

        // ref - radek's email - If user unticks then blank out number1 and number2 and let the total be editable
        if (!isChecked) {
            observableNumber1.set("");
            observableNumber2.set("");
        }
        // missing rule - confirmation with radek/enza via email
        else {
            observableTotal.set("");
        }


    }
}
