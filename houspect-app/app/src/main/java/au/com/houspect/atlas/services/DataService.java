package au.com.houspect.atlas.services;

import android.content.Context;

import com.j256.ormlite.stmt.QueryBuilder;

import java.util.List;

import au.com.houspect.atlas.models.CalendarItem;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.IdBase;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionImage;
import au.com.houspect.atlas.models.InspectionImageUploadLog;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.models.InspectionSectionImageUploadLog;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.models.RecentInspection;
import io.reactivex.Single;

/**
 * Created by alfredwong on 18/02/2017.
 */

public interface DataService {


    //region Refresh jobs & inspections
    interface RefreshCallback {
        void onSuccess();
        void onError(Throwable e);
    }

    // TODO:: remove context when offline demo is no longer needed
    /**
     * Downloads Jobs and Inspections from server.
     * Process the Jobs and Inspections and save them into locate DB - ORMLite.
     * Processing and download activities are peformed in background / io thread.
     * Control is returned via callback at the UI thread.
     * @param callback
     */
    void refreshJobsAndInspections(Context context, RefreshCallback callback);
    //endregion

    //region Upload data
    interface  UploadCallback {
        void onSuccess();
        void onEror(Throwable e);
    }

    /**
     * Save as Atlas draft in the app as a persistent Draft record without submit attribute or with submit="0".
     * The draft record has mode set to 0.
     * This function also uploads the draft xml to server for backup.
     * @param draft
     * @param callback
     * @param shouldSkipUpload
     */
    void saveAtlasDraft(Draft draft, UploadCallback callback, boolean shouldSkipUpload);

    /**
     * Save as Zeus draft. It leaves the draft record in the app with mode=1.
     * It also uploads the draft xml to the server with submit="draft".
     * @param draft
     * @param callback
     * @param shouldSkipUpload
     */
    void saveZeusDraft(Draft draft, UploadCallback callback, boolean shouldSkipUpload);

    /**
     * Submit to server as final report.
     * The draft xml has submit="1".
     * On successful upload, it deletes the draft record from the app.
     * On upload failure, it creates a corresponding RecentInspection record with isPendingUploadToServe=true. The residual draft record has mode set to 2.
     * @param draft
     * @param callback
     * @param shouldSkipUpload
     */
    void submitFinalReport(Draft draft, UploadCallback callback, boolean shouldSkipUpload);

    /**
     * Retry submit to server a final report.
     * @param recentInspection
     * @param uploadCallback
     */
    void retrySubmitFinalReport(RecentInspection recentInspection, UploadCallback uploadCallback);
    /**
     * Raw function that upload draft (xml) to the server at background / io thread
     * control is returned via callback at the ui thread.
     * This function does not consider business logic such as Atlas Draft, Zeus Draft vs Final Report.
     * Use saveZeusDraft, saveAtlasDraft, submitFinalReport instead.
     */
    void uploadDraft(Draft draft, UploadCallback callback);

    /**
     * Notify server that the Draft is active. This should be called when Job is converted to Draft.
     * @param draft
     * @param callback
     */
    void notifyDraftIsActive(Draft draft, UploadCallback callback);

    /**
     * Upload image to the server at background / io thread.
     * Control is returned via callback at the ui thread.
     * @param inspectionSectionImageUploadLog
     * @param callback
     */
    void uploadImage(final InspectionSectionImageUploadLog inspectionSectionImageUploadLog,
                            final UploadCallback callback);

    /**
     * Upload image to the server at background / io thread.
     * Control is returned via callback at the ui thread.
     * @param inspectionImageUploadLog
     * @param callback
     */
    void uploadImage(final InspectionImageUploadLog inspectionImageUploadLog,
                     final UploadCallback callback);
    //endregion

    //region Scan and clean obsolete drafts
    // Returns remaining drafts after cleanuo (download from server on what is considered as obsolete and clean offline drafts)

    /**
     * Download from server on the list of obsolete drafts (cancelled).
     * Compare that with offline drafts and delete identified drafts.
     * Return the remaining drafts created by username after cleanup.
     * The returned Single is configured to be subscribed on background thread and observed on UI thread.
     * @param username
     * @return
     */
    Single<List<Draft>> scanAndCleanObsoleteDrafts(String username);
    //endregion

    //region Download data from server
    interface GetDataCallback<T> {
        void onCompletion(List<T> data);
        void onError(String message);
    }

    // TODO:: remove context param once DemoUtil is phased out
    // download jobs from server - note: this is a thread blocking call
    void getJobs(Context context, GetDataCallback<Job> callback) throws Exception;
    // download inspections from server - note: this is a thread blocking call
    void getInspections(Context context, GetDataCallback<Inspection> callback) throws Exception;
    void getRecentInspections(Context context, GetDataCallback<RecentInspection> callback) throws Exception;
    void getRecentInspectionsForDraftDeletion(Context context, GetDataCallback<RecentInspection> callback) throws Exception;

    Single<List<RecentInspection>> getRecentInspectionsForDraftDeletion();
    Single<List<CalendarItem>> getUpcomingCalendarItems();
    //endregion

    //region Get data from offline storage

    Single<List<Draft>> getDraftsCreatedBy(final String username);
    Single<List<Draft>> getRemainingDraftsAfterProcessingObsoleteDrafts(final List<RecentInspection> recentInspectionsForDraftDeletion, String username);

    //endregion

    /**
     * Prior to saving the draft, this method updates fields xml, modifiedTime, noOfSections, noOfCompletedSections.
     * @param draft
     */
    void saveDraft(Draft draft);

    interface GetSessionResult {
        String getSessionToken();
        String getErrorMessage();
    }
    GetSessionResult getSessionToken() throws Exception;


    //region Get Data from app offline data storage ORMLite
    interface GetCallback<T> {
        QueryBuilder<T,?> preQuery(QueryBuilder<T,?> queryBuilder);
    }

    <T extends IdBase> List<T> getList(GetCallback<T> callback, Class<T> classType);
    <T extends IdBase> T get(String id, Class<T> classType);
    <T extends IdBase> void removeAll(Class<T> classType);
    <T extends IdBase> void remove(String id, Class<T> classType);
    <T extends IdBase> void remove(List<T> data, Class<T> classType);
    <T extends IdBase> void save(List<T> data, Class<T> classType);
    <T extends IdBase> void save(T data, Class<T> classType);
    //endregion


    Draft inspectionToDraft(Inspection inspection) throws Exception;
    InspectionSectionImageUploadLog getInspectionSectionImageUploadLog(InspectionSectionImage inspectionSectionImage, InspectionSection inspectionSection);
    InspectionSectionImageUploadLog getInspectionSectionImageUploadLog(InspectionSectionImage inspectionSectionImage, InspectionSection inspectionSection, boolean isForDeletion);
    InspectionImageUploadLog getInspectionImageUploadLog(InspectionImage inspectionImage, Inspection inspection);
    InspectionImageUploadLog getInspectionImageUploadLog(InspectionImage inspectionImage, Inspection inspection, boolean isForDeletion);
}
