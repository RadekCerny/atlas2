package au.com.houspect.atlas.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.media.ExifInterface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static android.media.ExifInterface.ORIENTATION_FLIP_VERTICAL;
import static android.media.ExifInterface.ORIENTATION_ROTATE_180;
import static android.media.ExifInterface.ORIENTATION_ROTATE_270;
import static android.media.ExifInterface.ORIENTATION_ROTATE_90;
import static android.media.ExifInterface.ORIENTATION_TRANSPOSE;
import static android.media.ExifInterface.ORIENTATION_TRANSVERSE;

/**
 * Created by alfredwong on 3/2/17.
 */

public class MediaUtil {

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;


    private static String TAG = "Atlas.MediaUtil";

    public static class PermissionRequestCode {

        private int code;

        public int getCode() {
            return code;
        }

        public PermissionRequestCode(int code) {
            this.code = code;
        }

        public static final PermissionRequestCode REQUEST_WRITE_STORAGE_TAKE_A_PHOTO = new PermissionRequestCode(112);
        public static final PermissionRequestCode REQUEST_WRITE_STORAGE_PICK_A_PHOTO = new PermissionRequestCode(113);
        public static final PermissionRequestCode REQUEST_WRITE_STORAGE_EDIT_PHOTO = new PermissionRequestCode(115);
    }

    /**
     * Create a file Uri for saving an image or video
     */

    private static Uri getOutputMediaFileUri(Context context, int type, String pathPrefix) {
        return Uri.fromFile(getOutputMediaFile(context, type, pathPrefix));
    }

    public static File getOutputMediaFolder(Context context, String pathPrefix) {
        File dir = getExternalStorageDirectory(context);
        if (dir == null) {
            dir = getInternalStorageDirectory();
        } else {
            dir = getExternalStorageDirectory(context);
        }

        //File dir = context.getFilesDir();
        File subDir = new File(dir, pathPrefix);
        return subDir;
    }

    public static File getExternalStorageDirectory(Context context) {
        File returnFile = null;
        if (Environment.isExternalStorageEmulated()) {
            File CardPathFolder = new File(Environment.getExternalStorageDirectory().toString());//(System.getenv("SECONDARY_STORAGE"));
            if (CardPathFolder.getFreeSpace() > 0) {
                returnFile = CardPathFolder;
            }
        } else {
            returnFile = new File(Environment.getExternalStorageDirectory().toString());
        }
        return returnFile;
    }

    public static File getInternalStorageDirectory() {
        File returnFile = null;
        if (!Environment.isExternalStorageEmulated()) {
            File internalFolder = new File(Environment.getDataDirectory().toString());
            returnFile = internalFolder;
        } else {
            returnFile = new File(Environment.getExternalStorageDirectory().toString());
        }
        return returnFile;
    }

    /*public static File getOutputMediaFile(String filename){


        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        //return new File(mediaStorageDir.getPath() + File.separator + filename + ".jpg");

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");

    }*/

    // Returns the Uri for a photo stored on disk given the fileName
    public static File getFile(Context context, String fileName) {
        // Only continue if the SD Card is mounted
        if (isExternalStorageAvailable()) {
            // Get safe storage directory for photos
            // Use `getExternalFilesDir` on Context to access package-specific directories.
            // This way, we don't need to request external read/write runtime permissions.
            File mediaStorageDir = new File(
                    context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "Atlas");

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
                Log.d("Atlas", "failed to create directory");
            }

            // Return the file target for the photo based on filename
            //File file = Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));
            File file = new File(mediaStorageDir.getPath() + File.separator + fileName);
            return file;
            // wrap File object into a content provider
            // required for API >= 24
            // See https://guides.codepath.com/Sharing-Content-with-Intents#sharing-files-with-api-24-or-higher
            //return FileProvider.getUriForFile(MyActivity.this, "com.codepath.fileprovider", file);
        }
        return null;
    }

    // Returns true if external storage for photos is available
    private static boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    public static File getOutputMedaiaFolder(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        String appName = stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);

        File subDir = getOutputMediaFolder(context, appName);
        return subDir;
    }

    public static File getOutputMediaFile(Context context, int type, String filename) {
        File subDir = getOutputMedaiaFolder(context);

        if (!subDir.exists()) {
            if (!subDir.mkdirs()) {
                Log.d("Atlas", "failed to create directory " + subDir.getAbsolutePath());
                return null;
            }
        }

        // Create a media file name
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(subDir.getPath() + File.separator +
                    filename + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(subDir.getPath() + File.separator +
                    filename + ".mp4");
        } else {
            return null;
        }

        return mediaFile;

    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int rotation) {
        Matrix matrix = new Matrix();
        matrix.postRotate(rotation);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
    }

    public static Base64ImageInfo base64EncodeImageFile(String filePath, int rotation, boolean shouldGzip) {
        //encode image to base64 string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);

        if (rotation > 0) {
            bitmap = rotateBitmap(bitmap, rotation);
        }

        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();
        String base64String = null;

        try {
            System.gc();
            base64String = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        } catch (Exception e) {
            Log.e("Atlas", "Base64EncodeImageFile exception: " + e.getLocalizedMessage());
        } catch (OutOfMemoryError e) {
            Log.e("Atlas", "Base64EncodeImageFile OutOfMemoryError (re-attempt by reducing compression quality): " + e.getLocalizedMessage());
            baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            imageBytes = baos.toByteArray();
            base64String = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        }

        Log.i("Atlas", "base64String before Gzip length (" + base64String.length() + ")");

        if (!shouldGzip)
            return new Base64ImageInfo(originalWidth, originalHeight, filePath, base64String);

        byte[] gzippedData = null;
        try {
            gzippedData = GzipUtil.compress(base64String);
        } catch (Exception e) {
            Log.e("Atlas", e.getLocalizedMessage());
            return null;
        }
        String gzippedBase64String = Base64.encodeToString(gzippedData, Base64.DEFAULT);

        Log.i("Atlas", "gzippedBase64String length (" + gzippedBase64String.length() + ")");

        return new Base64ImageInfo(originalWidth, originalHeight, filePath, gzippedBase64String);
    }


    public static Bitmap getBitmap(String photoPath, boolean isThumbnail) {

        //final BitmapFactory.Options options = new BitmapFactory.Options();
        //if (isThumbnail) options.inSampleSize = 4;
        //options.inJustDecodeBounds = false;

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        // Set inSampleSize
        options.inSampleSize = 4;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;


        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);
        return bitmap;
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static void saveBitmap(Bitmap bitmap, File destinationFile) {

        OutputStream outputStream;
        try {
            outputStream = new FileOutputStream(destinationFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            Log.e("Atlas", "saveBitmap Error: ", e);
        }
    }

    /**
     * @param activity
     * @return Boolean to indicates if the app needs to wait for user permission action i.e. Deny, Allow Dialog prompt
     */
    public static boolean checkAndRequestPhotoStoragePermission(final Activity activity, final PermissionRequestCode permissionRequestCode) {
        //ask for the permission in android M
        int permission = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        boolean needsToWaitForUserPermissionAction = permission != PackageManager.PERMISSION_GRANTED;

        if (!needsToWaitForUserPermissionAction) {
            permission = ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.CAMERA);
            needsToWaitForUserPermissionAction = permission != PackageManager.PERMISSION_GRANTED;
        }

        if (needsToWaitForUserPermissionAction) {
            Log.i(TAG, "Permission to record denied");

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Permission to access the SD-CARD is required for this app to attach a photo.")
                        .setTitle("Permission required");

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        Log.i(TAG, "Clicked");
                        makeRequest(activity, permissionRequestCode);
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            } else {
                makeRequest(activity, permissionRequestCode);
            }
        }
        return needsToWaitForUserPermissionAction;
    }


    protected static void makeRequest(Activity activity, PermissionRequestCode permissionRequestCode) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                permissionRequestCode.getCode());
    }


    public static ResizedImageInfo resizeImage(String filePath, int maxHeightOrWidth) {
        Bitmap b = BitmapFactory.decodeFile(filePath);

        int originalWidth = b.getWidth();
        int originalHeight = b.getHeight();

        boolean isTallImage = originalHeight > originalWidth;

        boolean shouldResize = false;
        if (isTallImage) shouldResize = maxHeightOrWidth < originalHeight;
        else shouldResize = maxHeightOrWidth < originalWidth;

        if (!shouldResize)
            return new ResizedImageInfo(originalWidth, originalHeight, filePath, false);

        int destinationHeight = isTallImage ? maxHeightOrWidth : (int) (originalHeight * ((float) maxHeightOrWidth / (float) originalWidth));
        int destinationWidth = isTallImage ? (int) (originalWidth * ((float) maxHeightOrWidth / (float) originalHeight)) : maxHeightOrWidth;


        Bitmap b2 = Bitmap.createScaledBitmap(b, destinationWidth, destinationHeight, false);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        // compress to the format you want, JPEG, PNG...
        // 70 is the 0-100 quality percentage
        b2.compress(Bitmap.CompressFormat.JPEG, 70, outStream);

        File compressedFile = null;
        try {
            compressedFile = new File(filePath + ".compressed.jpg");
            compressedFile.createNewFile();
            FileOutputStream fo = new FileOutputStream(compressedFile);
            fo.write(outStream.toByteArray());
            fo.close();
        } catch (Exception e) {
            return null;
        }
        return new ResizedImageInfo(destinationWidth, destinationHeight, compressedFile.getAbsolutePath(), true);

    }

    public static class ResizedImageInfo extends ImageInfo {
        private boolean isResized;

        public ResizedImageInfo(int width, int height, String absolutePath, boolean isResized) {
            super(width, height, absolutePath);
            this.isResized = isResized;
        }

        public boolean isResized() {
            return isResized;
        }
    }


    public static class Base64ImageInfo extends ImageInfo {
        private String base64String;

        public Base64ImageInfo(int width, int height, String absolutePath, String base64String) {
            super(width, height, absolutePath);
            this.base64String = base64String;
        }

        public String getBase64String() {
            return base64String;
        }
    }

    public static class ImageInfo {
        private int width;
        private int height;
        private String absolutePath;

        public ImageInfo(int width, int height, String absolutePath) {
            this.width = width;
            this.height = height;
            this.absolutePath = absolutePath;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public String getAbsolutePath() {
            return absolutePath;
        }

    }

    public static Uri getImageUri(Context context, File file) {
        Uri uri = null;
        if(Build.VERSION.SDK_INT>=24) {
            uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
        } else {
            uri = Uri.fromFile(file);
        }
        return uri;
    }

    /**
     * @return 0, 90, 180 or 270. 0 could be returned if there is no data about rotation
     */
    public static int getImageRotation(Context context, Uri imageUri) {
        try {
            ExifInterface exif = new ExifInterface(imageUri.getPath());
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            if (rotation == ExifInterface.ORIENTATION_UNDEFINED)
                return getRotationFromMediaStore(context, imageUri);
            else return exifToDegrees(rotation);
        } catch (IOException e) {
            return 0;
        }
    }

    public static int getImageRotationOld(Context context, Uri imageUri) {
        try {
            ExifInterface exif = new ExifInterface(imageUri.getPath());
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            if (rotation == ExifInterface.ORIENTATION_UNDEFINED)
                return getRotationFromMediaStore(context, imageUri);
            else return exifToDegreesOld(rotation);
        } catch (IOException e) {
            return 0;
        }
    }

    public static int getExifOrientation(Context context, Uri imageUri) {
        try {
            ExifInterface exif = new ExifInterface(imageUri.getPath());
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            return rotation;
        } catch (IOException e) {
            return -1;
        }
    }

    public static int getExifOrientation(Context context, String filePath) {
        try {
            ExifInterface exif = new ExifInterface(filePath);
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            return rotation;
        } catch (IOException e) {
            return -1;
        }
    }

    public static int getRotationFromMediaStore(Context context, Uri imageUri) {
        String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION};
        Cursor cursor = context.getContentResolver().query(imageUri, columns, null, null, null);
        if (cursor == null) return 0;

        cursor.moveToFirst();

        int orientationColumnIndex = cursor.getColumnIndex(columns[1]);
        return cursor.getInt(orientationColumnIndex);
    }

    public static int exifToDegreesOld(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        } else {
            return 0;
        }
    }

    public static int exifToDegrees(int orientation) {
        int rotation;
        switch (orientation) {
            case ORIENTATION_ROTATE_90:
            case ORIENTATION_TRANSPOSE:
                rotation = 90;
                break;
            case ORIENTATION_ROTATE_180:
            case ORIENTATION_FLIP_VERTICAL:
                rotation = 180;
                break;
            case ORIENTATION_ROTATE_270:
            case ORIENTATION_TRANSVERSE:
                rotation = 270;
                break;
            default:
                rotation = 0;
        }
        return rotation;
    }
}
