package au.com.houspect.atlas.models;

public interface ITemporal {
    String getDate();
    String getTime();
}
