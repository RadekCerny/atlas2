package au.com.houspect.atlas.models;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by alfredwong on 3/8/17.
 * ORM model for tracking image upload status.
 */

public class InspectionImageUploadLog extends IdBase {

    @DatabaseField
    private String inspectionId;
    @DatabaseField
    private long lastSuccessfulUploadTime;
    @DatabaseField
    private boolean isForDeletion;

    public String getFileName() {
        return getId();
    }

    public void setFileName(String fileName) {
        setId(fileName);
    }

    public String getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(String inspectionId) {
        this.inspectionId = inspectionId;
    }

    public long getLastSuccessfulUploadTime() {
        return lastSuccessfulUploadTime;
    }

    public void setLastSuccessfulUploadTime(long lastSuccessfulUploadTime) {
        this.lastSuccessfulUploadTime = lastSuccessfulUploadTime;
    }

    public boolean isForDeletion() {
        return isForDeletion;
    }

    public void setForDeletion(boolean forDeletion) {
        isForDeletion = forDeletion;
    }

    public InspectionImageUploadLog() {}

}
