package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by alfredwong on 24/07/2018.
 */

public class CalendarItem extends IdBase implements Parcelable, ITemporal {

    @DatabaseField
    private String note;
    @DatabaseField
    private String date;
    @DatabaseField
    private String time;
    @DatabaseField
    private String street;
    @DatabaseField
    private String suburb;
    @DatabaseField
    private String createdByUsername = ""; // default to empty

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getCreatedByUsername() {
        return createdByUsername;
    }

    public void setCreatedByUsername(String createdByUsername) {
        this.createdByUsername = createdByUsername;
    }

    public CalendarItem() {}

    //region Parcelable
    private CalendarItem(Parcel in) {
        setId(in.readString());
        note = in.readString();
        date = in.readString();
        time = in.readString();
        street = in.readString();
        suburb = in.readString();
        createdByUsername = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(note);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(street);
        dest.writeString(suburb);
        dest.writeString(createdByUsername);
    }

    public static final Creator CREATOR = new Creator() {

        @Override
        public CalendarItem createFromParcel(Parcel source) {
            return new CalendarItem(source);
        }

        @Override
        public CalendarItem[] newArray(int size) {
            return new CalendarItem[size];
        }
    };
    //endregion
}
