package au.com.houspect.atlas.viewmodels;

import android.view.View;

/**
 * Created by alfredwong on 17/2/17.
 */

public class YesNoConfirmationViewModel {

    private String title;
    private String content;
    private Runnable onYes;
    private Runnable onNo;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public YesNoConfirmationViewModel(String title, String content, Runnable onYes, Runnable onNo) {
        this.title = title;
        this.content = content;
        this.onYes = onYes;
        this.onNo = onNo;
    }

    public void onYesClick(View button) {
        onYes.run();
    }

    public void onNoClick(View button) {
        onNo.run();
    }

}
