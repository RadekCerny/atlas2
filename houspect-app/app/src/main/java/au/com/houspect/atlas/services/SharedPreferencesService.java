package au.com.houspect.atlas.services;

import android.content.Context;

/**
 * Created by alfredwong on 2/3/17.
 */

public interface SharedPreferencesService {
    void saveSessionToken(String sessionToken);
    String getSessionToken();
    void saveUsername(String username);
    String getUsername();
    void savePassword(String password);
    String getPassword();
    void saveShouldUploadPhotosWhenOnline(boolean yesOrNo);
    boolean getShouldUploadPhotosWhenOnline();
    void saveShouldResizePhotosWhenUpload(boolean yesOrNo);
    boolean getShouldResizePhotosWhenUpload();
    void saveResizePhotosToMaxWidthOrHeight(int maxWidthOrHeight);
    int getResizePhotosToMaxWidthOrHeight();
}
