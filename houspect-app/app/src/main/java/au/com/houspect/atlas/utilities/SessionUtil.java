package au.com.houspect.atlas.utilities;

import android.support.v4.app.Fragment;

import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.SideMenuItem;

/**
 * Created by alfredwong on 29/01/2017.
 */

public class SessionUtil {

    // TODO:: phase this out once dependency injection is sorted
    public static SessionUtil singleton = new SessionUtil();

    private Fragment mFragmentActive;
    private Fragment mFragmentPromptActive;
    private SideMenuItem mSideMenuActive;
    private Draft mDraftInEdit;

    public Fragment getFragmentActive() {
        return mFragmentActive;
    }

    public void setFragmentActive(Fragment fragmentActive) {
        mFragmentActive = fragmentActive;
    }

    public Fragment getFragmentPromptActive() { return mFragmentPromptActive; }

    public void setFragmentPromptActive(Fragment fragmentPromptActive) {
        mFragmentPromptActive = fragmentPromptActive;
    }

    public SideMenuItem getSideMenuActive() {
        return mSideMenuActive;
    }

    public void setSideMenuActive(SideMenuItem sideMenuActive) {
        mSideMenuActive = sideMenuActive;
    }

    public Draft getDraftInEdit() {
        return mDraftInEdit;
    }

    public void setDraftInEdit(Draft draft) {
        this.mDraftInEdit = draft;
    }
}
