package au.com.houspect.atlas.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.adapters.InspectionSectionItemRecyclerViewAdapter;
import au.com.houspect.atlas.databinding.FragmentInspectionSectionBinding;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.utilities.MediaUtil;
import au.com.houspect.atlas.viewmodels.InspectionSectionItemViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InspectionSectionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InspectionSectionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

// TODO - possible code refactoring - Use AttachPhotoUtil
public class InspectionSectionFragment extends Fragment implements  InspectionSectionItemRecyclerViewAdapter.Listener {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static Event PHOTO_TAKEN = new Event(201);
        public static Event COMMENT = new Event(202);
        public static Event RECOMMENDATION = new Event(203);
        public static Event NEXT = new Event(204);
        public static Event BACK = new Event(205);
        public static Event PHOTO_GALLERY = new Event(206);
        public static Event PHOTO_ERROR = new Event(207);
        public static Event PHOTO_STORAGE_PERMISSION_REQUEST_TAKE_A_PHOTO = new Event(208);
        public static Event PHOTO_STORAGE_PERMISSION_REQUEST_PICK_A_PHOTO = new Event(209);
    }

    private static class RequestCode {
        public static int TAKE_A_PHOTO = 9999;
        public static int PICK_A_PHOTO = 9000;
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_INSPECTION_SECTION = "inspectionSection";

    private InspectionSection mInspectionSection;
    private InspectionSectionItemRecyclerViewAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ViewGroup mViewGroupCameraPreview;
    private Button mButtonCamera;
    private Button mButtonComment;
    private Button mButtonRecommendation;
    private Button mButtonNext;
    private Button mButtonBack;
    private View mViewCommentQuantityIndicator;
    private TextView mTextViewCommentQuantityIndicator;
    private View mViewRecommendationQuantityIndicator;
    private TextView mTextViewRecommendationQuantityIndicator;

    private ViewGroup mViewGroupPhotoGallery;
    private Button mButtonPhotoGallery;
    private View mViewPhotoQuantityIndicator;
    private TextView mTextViewPhotoQuantityIndicator;


    private OnFragmentInteractionListener mListener;

    private Uri mUriPhoto;
    private String mPathPhoto;

    public InspectionSectionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param inspectionSection InspectionSection to be displayed.
     * @param parentInspection Parent Inspection that owns the InspectionSection.
     * @return A new instance of fragment InspectionSectionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InspectionSectionFragment newInstance(InspectionSection inspectionSection, Inspection parentInspection) {
        InspectionSectionFragment fragment = new InspectionSectionFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_INSPECTION_SECTION, inspectionSection);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mInspectionSection = getArguments().getParcelable(ARG_INSPECTION_SECTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //View view = inflater.inflate(R.layout.fragment_inspection_section, container, false);
        FragmentInspectionSectionBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_inspection_section, container, false);
        View view = binding.getRoot();
        binding.setInspectionSection(mInspectionSection);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewInspectionSectionItems);
        mAdapter = new InspectionSectionItemRecyclerViewAdapter(mInspectionSection.getItems(),
                R.layout.fragment_inspection_section_enum_item,
                R.layout.fragment_inspection_section_text_item,
                R.layout.fragment_inspection_section_two_numbers_item,
                this);

        mRecyclerView.setAdapter(mAdapter);

        //mTextViewTitle.setText(mInspectionSection.getName());

        //mViewGroupCameraPreview = (ViewGroup) view.findViewById(R.id.viewGroupCameraPreview);

        final HashMap<String,Object> dataObject = new HashMap<>();
        dataObject.put("inspectionSection", mInspectionSection);

        final WeakReference<InspectionSectionFragment> weakSelf = new WeakReference<InspectionSectionFragment>(this);
        mButtonCamera = (Button) view.findViewById(R.id.buttonCamera);
        mButtonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPhotoActionOptions();
            }
        });

        mButtonComment = (Button) view.findViewById(R.id.buttonComment);
        mButtonComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(weakSelf.get(), Event.COMMENT, dataObject);
            }
        });

        mButtonRecommendation = (Button) view.findViewById(R.id.buttonRecommendation);
        mButtonRecommendation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(weakSelf.get(), Event.RECOMMENDATION, dataObject);
            }
        });

        mButtonNext = (Button) view.findViewById(R.id.buttonNext);
        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(weakSelf.get(), Event.NEXT, dataObject);
            }
        });
        mButtonBack = (Button) view.findViewById(R.id.buttonBack);
        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String,Object> dataObject = new HashMap<>();
                dataObject.put("inspectionSection", mInspectionSection);
                mListener.onFragmentInteraction(weakSelf.get(), Event.BACK, dataObject);
            }
        });

        mViewPhotoQuantityIndicator = view.findViewById(R.id.viewPhotoQuantityIndicator);
        mTextViewPhotoQuantityIndicator = (TextView) view.findViewById(R.id.textViewPhotoQuantityIndicator);

        mViewCommentQuantityIndicator = view.findViewById(R.id.viewCommentQuantityIndicator);
        mTextViewCommentQuantityIndicator = (TextView) view.findViewById(R.id.textViewCommentQuantityIndicator);

        mViewRecommendationQuantityIndicator = view.findViewById(R.id.viewRecommendationQuantityIndicator);
        mTextViewRecommendationQuantityIndicator = (TextView) view.findViewById(R.id.textViewRecommendationQuantityIndicator);

        mViewGroupPhotoGallery = (ViewGroup) view.findViewById(R.id.viewGroupPhotoGallery);
        mButtonPhotoGallery = (Button) view.findViewById(R.id.buttonPhotoGallery);
        mButtonPhotoGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(weakSelf.get(), Event.PHOTO_GALLERY, dataObject);
            }
        });

        refreshDisplay();

/*
        final GestureDetector gesture = new GestureDetector(getActivity(),
                new GestureDetector.SimpleOnGestureListener() {

                    @Override
                    public boolean onDown(MotionEvent e) {
                        return true;
                    }

                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                           float velocityY) {
                        //Log.i(Constants.APP_TAG, "onFling has been called!");
                        final int SWIPE_MIN_DISTANCE = 120;
                        final int SWIPE_MAX_OFF_PATH = 250;
                        final int SWIPE_THRESHOLD_VELOCITY = 200;
                        try {
                            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                                return false;
                            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                                //Log.i(Constants.APP_TAG, "Right to Left");
                                mListener.onFragmentInteraction(this, Event.BACK, null, mInspectionSection);
                            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                                //Log.i(Constants.APP_TAG, "Left to Right");
                                mListener.onFragmentInteraction(this, Event.NEXT, null, mInspectionSection);
                            }
                        } catch (Exception e) {
                            // nothing
                        }
                        return super.onFling(e1, e2, velocityX, velocityY);
                    }
                });

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gesture.onTouchEvent(event);
            }
        });*/


        return view;
    }

    private int getNumberOfPhotos() {
        int numberOfPhotos = 0;
        if (mInspectionSection.getImages() != null) numberOfPhotos = mInspectionSection.getImages().size();
/*        String pathPrefix = mInspectionSection.getParentInspectionId() + "_" + mInspectionSection.getId();
        File photosFolderPath = null; //MediaUtil.getOutputMediaFolder(getContext(), pathPrefix);

        int numberOfPhotos = 0;
        if (photosFolderPath.exists()) {
            String[] photoFilenames = photosFolderPath.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    if (name.endsWith(".jpg")) return true;
                    return false;
                }
            });
            numberOfPhotos = photoFilenames.length;
        }
        return numberOfPhotos;*/

        return numberOfPhotos;
    }

    public void refreshDisplay() {

        int numberOfPhotos = getNumberOfPhotos();
        if (numberOfPhotos == 0) mViewPhotoQuantityIndicator.setVisibility(View.GONE);
        else mViewPhotoQuantityIndicator.setVisibility(View.VISIBLE);

        if (numberOfPhotos == 0) mViewGroupPhotoGallery.setVisibility(View.GONE);
        else mViewGroupPhotoGallery.setVisibility(View.VISIBLE);

        mTextViewPhotoQuantityIndicator.setText(numberOfPhotos+"");

        boolean hasComments = mInspectionSection.getComments() != null && mInspectionSection.getComments().length() > 0;
        if (!hasComments) mViewCommentQuantityIndicator.setVisibility(View.GONE);
        else mViewCommentQuantityIndicator.setVisibility(View.VISIBLE);

        boolean hasRecommendations = mInspectionSection.getRecommendations() != null && mInspectionSection.getRecommendations().length() > 0;
        if (!hasRecommendations) mViewRecommendationQuantityIndicator.setVisibility(View.GONE);
        else mViewRecommendationQuantityIndicator.setVisibility(View.VISIBLE);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    //public interface OnListFragmentInteractionListener {
    //    // TODO: Update argument type and name
    //    void onListFragmentInteraction(InspectionSectionItem item);
    //}

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(InspectionSectionFragment inspectionSectionFragment,
                                   Event event,
                                   HashMap<String,Object> dataObject);
    }

    long captureTime = 0;
    public void invokeCamera(){

        String prefix = mInspectionSection.getParentInspectionCode();
        if (prefix == null) prefix = "";
        String uuid = prefix + "-" + UUID.randomUUID().toString();

        File photoFile = MediaUtil.getOutputMediaFile(getActivity(), MediaUtil.MEDIA_TYPE_IMAGE, uuid); //MediaUtil.getOutputMediaFile(getContext(), MEDIA_TYPE_IMAGE, uuid);

        if (photoFile == null) {
            HashMap<String,Object> dataObject = new HashMap<>();
            dataObject.put("inspectionSection", mInspectionSection);
            mListener.onFragmentInteraction(this, Event.PHOTO_ERROR, dataObject);
            return;
        }
        mPathPhoto = photoFile.getAbsolutePath();

        mUriPhoto = MediaUtil.getImageUri(getActivity(), photoFile);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); //"android.media.action.IMAGE_CAPTURE");
        //cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,mUriPhoto);
        cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        captureTime = (new Date()).getTime();

        startActivityForResult(cameraIntent, RequestCode.TAKE_A_PHOTO);
    }

    public void invokePhotoLibrary() {
        Intent photoLibraryIntent = new Intent(Intent.ACTION_PICK);
        photoLibraryIntent.setType("image/*");
        startActivityForResult(photoLibraryIntent, RequestCode.PICK_A_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == getActivity().RESULT_OK){

            File file = null;
            if (requestCode == RequestCode.TAKE_A_PHOTO) {

                file = new File(mPathPhoto);

                // Rotate the saved image file based on rotation of the camera - matches what the end user see to watch was captured
                /*if (rotate != 0) {
                    Bitmap bitmap = BitmapFactory.decodeFile(mPathPhoto);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotate);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
                    MediaUtil.saveBitmap(bitmap, file);
                }*/

                // TODO:: Test only - remove me
                //MediaUtil.Base64ImageInfo base64ImageInfo = MediaUtil.base64EncodeImageFile(mPathPhoto, false);
                //int rotation = MediaUtil.getImageRotation(getContext(), MediaUtil.getImageUri(getContext(), file));

                addPhotoToGallery(mPathPhoto);

            }
            else if (requestCode == RequestCode.PICK_A_PHOTO) {
                if (data == null) {
                    HashMap<String,Object> dataObject = new HashMap<>();
                    dataObject.put("inspectionSection", mInspectionSection);
                    mListener.onFragmentInteraction(this, Event.PHOTO_ERROR, dataObject);
                    return;
                }

                String uuid = UUID.randomUUID().toString();

                file = MediaUtil.getOutputMediaFile(getActivity(), MediaUtil.MEDIA_TYPE_IMAGE, uuid); //MediaUtil.getOutputMediaFile(getContext(), MEDIA_TYPE_IMAGE, uuid);


                Uri pickedImage = data.getData();
                // Let's read picked image path using content resolver
                String[] columns = { MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION };
                Cursor cursor = getActivity().getContentResolver().query(pickedImage, columns, null, null, null);
                cursor.moveToFirst();
                String imagePath = cursor.getString(cursor.getColumnIndex(columns[0]));
                int orientation = cursor.getInt(cursor.getColumnIndex(columns[1]));

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
                Matrix matrix = new Matrix();
                matrix.postRotate(orientation);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
                MediaUtil.saveBitmap(bitmap, file);

                // At the end remember to close the cursor or you will end with the RuntimeException!
                cursor.close();

            }
            else {
                Log.i("Atlas", "InspectionSectionFragment unknown Dialog Request: " + requestCode);
                return;
            }

            InspectionSectionImage inspectionSectionImage = new InspectionSectionImage();
            inspectionSectionImage.setId("");
            inspectionSectionImage.setFileName(file.getName());
            mInspectionSection.getImages().add(inspectionSectionImage);

            // TODO:: remove me - Test only
            //String resizedImageFilePath = MediaUtil.resizeImage(file.getAbsolutePath(), 1024);
            //if (resizedImageFilePath != null) {
            //    File resizedImageFile = new File(resizedImageFilePath);
            //    InspectionSectionImage inspectionSectionImageResized = new InspectionSectionImage();
            //    inspectionSectionImageResized.setId("");
            //    inspectionSectionImageResized.setFileName(resizedImageFile.getName());
            //    mInspectionSection.getImages().add(inspectionSectionImageResized);
            //}


            HashMap<String, Object> dataObject = new HashMap<>();
            dataObject.put("inspectionSection", mInspectionSection);
            dataObject.put("inspectionSectionImage", inspectionSectionImage);

            mListener.onFragmentInteraction(this, Event.PHOTO_TAKEN, dataObject);
            mUriPhoto = null;
            mPathPhoto = null;

            refreshDisplay();
        }
    }

    private static int getCameraRotation() {
        int orientationCameraFacingBack = -1;
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                orientationCameraFacingBack = cameraInfo.orientation;
            }
        }
        return orientationCameraFacingBack;
    }

    private void addPhotoToGalleryBak(String photoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(photoPath);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private void addPhotoToGallery(String photoPath) {

        // ScanFile so it will be appeared on Gallery
        MediaScannerConnection.scanFile(getActivity(),
                new String[]{photoPath}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        captureTime = 0;

                        // TODO:: Test only - remove me
                        //MediaUtil.Base64ImageInfo base64ImageInfo = MediaUtil.base64EncodeImageFile(mPathPhoto, false);
                        //int rotation = MediaUtil.getImageRotation(getContext(), uri);
                        //int i = 0;
                    }
                });
    }

    private void showPhotoActionOptions() {

        final HashMap<String,Object> dataObject = new HashMap<>();
        dataObject.put("inspectionSection", mInspectionSection);
        final WeakReference<InspectionSectionFragment> weakSelf = new WeakReference<InspectionSectionFragment>(this);

        CharSequence colors[] = new CharSequence[] {"Picture Library", "Take a Photo" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Attach Photo");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                boolean shouldTakeAPhoto = which == 1;
                boolean shouldSelectFromLibrary = which == 0;

                if (shouldSelectFromLibrary) {
                    mListener.onFragmentInteraction(weakSelf.get(), Event.PHOTO_STORAGE_PERMISSION_REQUEST_PICK_A_PHOTO, dataObject);
                }
                else if (shouldTakeAPhoto) {
                    mListener.onFragmentInteraction(weakSelf.get(), Event.PHOTO_STORAGE_PERMISSION_REQUEST_TAKE_A_PHOTO, dataObject);
                }
            }
        });
        builder.show();
    }

    public int getPhotoRotation(String filePath) {
        int rotation =-1;
        long fileSize = new File(filePath).length();

        Cursor mediaCursor = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[] {MediaStore.Images.ImageColumns.ORIENTATION, MediaStore.MediaColumns.SIZE }, MediaStore.MediaColumns.DATE_ADDED + ">=?", new String[]{String.valueOf(captureTime/1000 - 1)}, MediaStore.MediaColumns.DATE_ADDED + " desc");

        if (mediaCursor != null && captureTime != 0 && mediaCursor.getCount() !=0 ) {
            while(mediaCursor.moveToNext()){
                long size = mediaCursor.getLong(1);
                //Extra check to make sure that we are getting the orientation from the proper file
                if(size == fileSize){
                    rotation = mediaCursor.getInt(0);
                    break;
                }
            }
        }
        return rotation;
    }

    public static void setCameraDisplayOrientation(Activity activity,
                                                   int cameraId, android.hardware.Camera camera) {

        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    //region Other NA related

    @Override
    public void onRequestForOtherNaOptions(InspectionSectionItemViewModel viewModel) {
        showOtherNaOptions(viewModel);
    }

    private void showOtherNaOptions(final InspectionSectionItemViewModel inspectionSectionEnumItemViewModel) {

        // Safe guard - this is added as there is occassion where server returns empty NAText
        String option1 = inspectionSectionEnumItemViewModel.getModel().getNaText();
        if (TextUtils.isEmpty(option1)) option1 = "NAText";

        CharSequence colors[] = new CharSequence[] {option1, "I want to enter custom text" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Other - N/A");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                boolean shouldUseNaText = which == 0;
                boolean shouldEnterCustomText = which == 1;

                if (shouldUseNaText) inspectionSectionEnumItemViewModel.configureOtherNaWithNaText();
                else if (shouldEnterCustomText) inspectionSectionEnumItemViewModel.configureOtherNaWithCustomText();
            }
        });
        // TODO:: fixme - crash here
        builder.show();
    }
    //endregion
}
