package au.com.houspect.atlas.viewmodels;

import android.databinding.ObservableBoolean;
import android.view.View;
import android.widget.CompoundButton;

/**
 * Created by alfredwong on 3/3/17.
 */

public class SettingsViewModel {

    private Runnable onSave;
    private Runnable onCancel;
    private Runnable onRequestConfirmationForUncheckingShouldUploadPhotoWhenOnline;
    private Runnable onRequestConfirmationForRemoveAllUserData;

    public ObservableBoolean shouldUploadPhotoWhenOnline = new ObservableBoolean(false);
    public ObservableBoolean shouldResizePhotoWhenUpload = new ObservableBoolean(false);
    public final ObservableBoolean isProcessing = new ObservableBoolean(true); // default to true


    public SettingsViewModel(Runnable onSave,
                             Runnable onCancel,
                             Runnable onRequestConfirmationForUncheckingShouldUploadPhotoWhenOnline,
                             Runnable onRequestConfirmationForRemoveAllUserData) {
        this.onSave = onSave;
        this.onCancel = onCancel;
        this.onRequestConfirmationForUncheckingShouldUploadPhotoWhenOnline = onRequestConfirmationForUncheckingShouldUploadPhotoWhenOnline;
        this.onRequestConfirmationForRemoveAllUserData = onRequestConfirmationForRemoveAllUserData;
    }

    public void onSaveClick(View v) {
        onSave.run();
    }

    public void onCancelClick(View v) {
        onCancel.run();
    }

    public void onShouldUploadPhotoWhenOnlineCheckedChanged(CompoundButton checkbox, boolean isChecked) {
        if (!isChecked) onRequestConfirmationForUncheckingShouldUploadPhotoWhenOnline.run();
    }

    public void confirmShouldUploadPhotoWhenOnline(boolean isChecked) {
        shouldUploadPhotoWhenOnline.set(isChecked);
    }

    public void onRemoveAllUserDataClick(View v) {
        onRequestConfirmationForRemoveAllUserData.run();
    }

}
