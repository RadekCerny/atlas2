package au.com.houspect.atlas.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import au.com.houspect.atlas.fragments.InspectionSectionFragment;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;


/**
 * Created by alfredwong on 30/1/17.
 */

public class InspectionSectionFragmentStatePagerAdapter extends FragmentStatePagerAdapter {

    private List<InspectionSection> mInspectionSections;
    private Inspection mInspection;
    private Fragment mFragmentStart;
    private Fragment mFragmentEnd;

    public InspectionSectionFragmentStatePagerAdapter(FragmentManager fragmentManager,
                                                      List<InspectionSection> inspectionSections,
                                                      Inspection parentInspection,
                                                      Fragment start,
                                                      Fragment end) {
        super(fragmentManager);
        mInspectionSections = inspectionSections;
        mInspection = parentInspection;
        mFragmentStart = start;
        mFragmentEnd = end;
    }

    @Override
    public Fragment getItem(int position) {

        boolean isStart = position == 0;
        int endPosition = mInspectionSections.size()-1;
        if (mFragmentStart != null) endPosition++;
        if (mFragmentEnd != null) endPosition++;
        boolean isEnd = position == endPosition;

        boolean hasStart = mFragmentStart != null;
        boolean hasEnd = mFragmentEnd != null;

        if (isStart && hasStart) {
            return mFragmentStart;
        }
        if (isEnd && hasEnd) {
            return mFragmentEnd;
        }

        int arrayPosition = position;
        if (hasStart) arrayPosition--;

        InspectionSection x = mInspectionSections.get(arrayPosition);
        InspectionSectionFragment inspectionSectionFragment = InspectionSectionFragment.newInstance(x, mInspection);

        // TODO - depending on positions - change back or next button
        return inspectionSectionFragment;
    }

    @Override
    public int getCount() {
        int extra = 0;
        if (mFragmentStart != null) extra++;
        if (mFragmentEnd != null) extra++;
        return mInspectionSections == null ? extra : mInspectionSections.size()+extra;
    }
}
