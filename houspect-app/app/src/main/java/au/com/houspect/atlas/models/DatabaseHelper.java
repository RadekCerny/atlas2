package au.com.houspect.atlas.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

import au.com.houspect.atlas.services.DataServiceImpl;

/**
 * Created by alfredwong on 18/02/2017.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    // name of the database file for your application -- change to something appropriate for your app
    private static final String DATABASE_NAME = "houspect.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 18;


    public List<Draft> draftsToBeMigrated = null;

    // the DAO object we use to access the SimpleData table
//    private Dao<Job, String> jobDao = null;
//    private RuntimeExceptionDao

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Job.class);
            TableUtils.createTable(connectionSource, Inspection.class);
            TableUtils.createTable(connectionSource, Draft.class);
            TableUtils.createTable(connectionSource, InspectionSectionImageUploadLog.class);
            TableUtils.createTable(connectionSource, RecentInspection.class);
            TableUtils.createTable(connectionSource, InspectionImageUploadLog.class);
            TableUtils.createTable(connectionSource, CalendarItem.class);

            if (draftsToBeMigrated != null) {
                (new DataServiceImpl(null, null, null)).save(draftsToBeMigrated, Draft.class);
                Log.d("DatabaseHelper", "draftsToBeMigrated saved = " + draftsToBeMigrated.size());
            }
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }

        // here we try inserting data in the on-create as a test
        /*RuntimeExceptionDao<SimpleData, Integer> dao = getSimpleDataDao();
        long millis = System.currentTimeMillis();
        // create some entries in the onCreate
        SimpleData simple = new SimpleData(millis);
        dao.create(simple);
        simple = new SimpleData(millis + 1);
        dao.create(simple);
        Log.i(DatabaseHelper.class.getName(), "created new entries in onCreate: " + millis);*/
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");

            // migration from 16 to 17
            if (oldVersion == 16) {
                db.execSQL("ALTER TABLE 'Draft' ADD COLUMN createdByUsername;");
                db.execSQL("UPDATE 'Draft' SET createdByUsername = '';");
            }


            draftsToBeMigrated = (new DataServiceImpl(null, null, null)).getList(null, Draft.class);
            Log.d("DatabaseHelper", "draftsToBeMigrated size = " + draftsToBeMigrated.size());

            // TODO:: future database schema upgrade when app is in production, need to cater for data migration
            TableUtils.dropTable(connectionSource, Job.class, true);
            TableUtils.dropTable(connectionSource, Inspection.class, true);
            TableUtils.dropTable(connectionSource, Draft.class, true);

            TableUtils.dropTable(connectionSource, InspectionSectionImageUploadLog.class, true);
            TableUtils.dropTable(connectionSource, RecentInspection.class, true);
            TableUtils.dropTable(connectionSource, InspectionImageUploadLog.class, true);
            TableUtils.dropTable(connectionSource, CalendarItem.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the Database Access Object (DAO) for our SimpleData class. It will create it or just give the cached
     * value.
     */
    /*public Dao<Job, Integer> getDao() throws SQLException {
        if (jobDao == null) {
            jobDao = getDao(Job.class);
        }
        return jobDao;
    }*/

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
        //jobDao = null;
    }
}
