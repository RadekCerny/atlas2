package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.adapters.SideMenuAdapter;
import au.com.houspect.atlas.models.SideMenuItem;
import au.com.houspect.atlas.utilities.SessionUtil;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SideMenuFragment extends Fragment {

    public static final SideMenuItem ITEM_JOBS = new SideMenuItem(R.mipmap.ic_job,"Jobs");
    public static final SideMenuItem ITEM_DRAFTS = new SideMenuItem(R.mipmap.ic_inspection,"Drafts");
    public static final SideMenuItem ITEM_RECENT_INSPECTIONS = new SideMenuItem(R.mipmap.ic_history,"Recent Inspections");
    public static final SideMenuItem ITEM_SETTINGS = new SideMenuItem(R.mipmap.ic_settings,"Settings");
    public static final SideMenuItem ITEM_LOGOUT = new SideMenuItem(R.mipmap.ic_logout,"Logout");

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private SideMenuAdapter mAdapter;

    // TODO:: dependency inject
    public SessionUtil mSessionUtil;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SideMenuFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static SideMenuFragment newInstance(int columnCount) {
        SideMenuFragment fragment = new SideMenuFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

        // TODO:: dependency injection
        mSessionUtil = SessionUtil.singleton;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sidemenu_list, container, false);
        boolean isRecyclerView = view instanceof RecyclerView;
        RecyclerView recyclerView = isRecyclerView ? (RecyclerView)view :  (RecyclerView)view.findViewById(R.id.list);

        // Set the adapter
        if (recyclerView != null) {
            Context context = view.getContext();
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            List<SideMenuItem> items = new ArrayList<>();
            items.add(ITEM_JOBS);
            items.add(ITEM_DRAFTS);
            items.add(ITEM_RECENT_INSPECTIONS);
            items.add(ITEM_SETTINGS);
            items.add(ITEM_LOGOUT);

            SideMenuAdapter adapter = new SideMenuAdapter(items, mListener, R.layout.fragment_sidemenu, mSessionUtil);
            mAdapter = adapter;

            recyclerView.setAdapter(mAdapter);
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(SideMenuItem item);
    }

    public void refresh() {
        mAdapter.notifyDataSetChanged();
    }
}
