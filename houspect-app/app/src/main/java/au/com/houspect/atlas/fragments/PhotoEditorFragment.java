package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.utilities.MediaUtil;


/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * {@link PhotoEditorFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PhotoEditorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PhotoEditorFragment extends Fragment {


    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static Event SAVE = new Event(501);
        public static Event CANCEL = new Event(502);
    }

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PHOTO_PATH = "photoPath";
    private static final String ARG_INSPECTION_SECTION = "inspectionSection";

    private Button mButtonCancel;
    private Button mButtonSave;
    private ImageView mImageViewPhoto;
    private String mPhotoPath;

    private OnFragmentInteractionListener mListener;

    private InspectionSection mInspectionSection;

    public PhotoEditorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
    // * @param param1 Parameter 1.
    // * @param param2 Parameter 2.
     * @return A new instance of fragment CameraPreviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PhotoEditorFragment newInstance(String photoPath, InspectionSection inspectionSection) {
        PhotoEditorFragment fragment = new PhotoEditorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PHOTO_PATH, photoPath);
        args.putParcelable(ARG_INSPECTION_SECTION, inspectionSection);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPhotoPath = getArguments().getString(ARG_PHOTO_PATH);
            mInspectionSection = getArguments().getParcelable(ARG_INSPECTION_SECTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photo_editor, container, false);

        mButtonCancel = (Button) view.findViewById(R.id.buttonCancel);
        mButtonSave = (Button) view.findViewById(R.id.buttonSave);
        mImageViewPhoto = (ImageView) view.findViewById(R.id.imageViewPhoto);
        Bitmap bitmap = MediaUtil.getBitmap(mPhotoPath, false);
        mImageViewPhoto.setImageBitmap(bitmap);

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(Event.CANCEL);
            }
        });
        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO:: save image into bytes
                mListener.onFragmentInteraction(Event.SAVE);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Event event);
    }

}
