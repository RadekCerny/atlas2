package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.Callable;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.services.SharedPreferencesService;
import au.com.houspect.atlas.services.SharedPreferencesServiceImpl;
import au.com.houspect.atlas.viewmodels.CheckboxConfirmationViewModel;
import au.com.houspect.atlas.viewmodels.SettingsViewModel;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static au.com.houspect.atlas.fragments.LoginFragment.Event.NOT_ME;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends ViewModelFragment<SettingsViewModel> {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static Event CANCEL = new Event(2301);
        public static Event SAVE = new Event(2302);
        public static Event REMOVE_ALL_USER_DATA_REQUESTED = new Event(2303);
    }

    private OnFragmentInteractionListener mListener;
    private SharedPreferencesService mSharedPreferencesService;
    private LayoutInflater mLayoutInflater;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    protected void onViewModelCreation() {
        // TODO - dependency inject
        mSharedPreferencesService = new SharedPreferencesServiceImpl(getContext());
        mViewModel = new SettingsViewModel
                (new Runnable() {
                    @Override
                    public void run() {


                        mViewModel.isProcessing.set(true);
                        Single.fromCallable(new Callable<Object>() {
                            @Override
                            public Object call() throws Exception {

                                mSharedPreferencesService.saveShouldUploadPhotosWhenOnline(mViewModel.shouldUploadPhotoWhenOnline.get());
                                mSharedPreferencesService.saveShouldResizePhotosWhenUpload(mViewModel.shouldResizePhotoWhenUpload.get());

                                return true;
                            }
                        }).subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new SingleObserver<Object>() {
                                    @Override
                                    public void onSubscribe(Disposable d) {

                                    }

                                    @Override
                                    public void onSuccess(Object value) {
                                        mListener.onFragmentInteraction(Event.SAVE);
                                        mViewModel.isProcessing.set(false);
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        mViewModel.isProcessing.set(false);
                                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT);
                                    }
                                });
                    }
                },
                        new Runnable() {
                            @Override
                            public void run() {
                                mListener.onFragmentInteraction(Event.CANCEL);
                            }
                        },
                        new Runnable() {
                            @Override
                            public void run() {
                                showUncheckUploadPhotosWhenOnlineConfirmationPrompt();
                            }
                        },
                        new Runnable() {
                            @Override
                            public void run() {
                                showRemoveAllUserDataConfirmationPrompt();
                            }
                        });




        mViewModel.isProcessing.set(true);
        Single.fromCallable(new Callable<Object>() {
            @Override
            public Object call() throws Exception {

                mViewModel.shouldUploadPhotoWhenOnline.set(mSharedPreferencesService.getShouldUploadPhotosWhenOnline());
                mViewModel.shouldResizePhotoWhenUpload.set(mSharedPreferencesService.getShouldResizePhotosWhenUpload());

                return true;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Object value) {
                        mViewModel.isProcessing.set(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mViewModel.isProcessing.set(false);
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT);
                    }
                });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_settings;
    }

    @Override
    protected void onConfigureViewDataBinding(ViewDataBinding viewDataBinding) {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CommentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mLayoutInflater = inflater;
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Event event);
    }

    private void showUncheckUploadPhotosWhenOnlineConfirmationPrompt() {

        CharSequence colors[] = new CharSequence[] {"Yes", "No"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String message = getResources().getString(R.string.message_uncheck_upload_photo_when_online_confirmation);

        TextView  textView = (TextView) mLayoutInflater.inflate(R.layout.alertdialog_title, null);
        textView.setText(message);
        //builder.setTitle(message);
        builder.setCustomTitle(textView);
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        // confirming yes to uncheck
                        mViewModel.confirmShouldUploadPhotoWhenOnline(false);
                        break;
                    case 1:
                        // confirming no to uncheck
                        mViewModel.confirmShouldUploadPhotoWhenOnline(true);
                        break;
                }
            }
        });
        builder.show();
    }

    private void showRemoveAllUserDataConfirmationPrompt() {

        CharSequence colors[] = new CharSequence[] {"Yes", "No"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String message = getResources().getString(R.string.message_remove_all_user_data_confirmation);

        TextView  textView = (TextView) mLayoutInflater.inflate(R.layout.alertdialog_title, null);
        textView.setText(message);
        builder.setCustomTitle(textView);
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        // confirming yes to uncheck
                        mListener.onFragmentInteraction(Event.REMOVE_ALL_USER_DATA_REQUESTED);
                        break;
                    case 1:
                        // confirming no to uncheck
                        break;
                }
            }
        });
        builder.show();
    }
}
