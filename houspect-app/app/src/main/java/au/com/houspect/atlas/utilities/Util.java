package au.com.houspect.atlas.utilities;

import android.content.Context;
import android.util.DisplayMetrics;

import com.saasplications.genie.domain.model.Column;
import com.saasplications.genie.domain.model.GridData;
import com.saasplications.genie.domain.model.Row;
import com.saasplications.genie.domain.model.TextGridDataCell;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import au.com.houspect.atlas.models.CalendarItem;
import au.com.houspect.atlas.models.ITemporal;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionClient;
import au.com.houspect.atlas.models.InspectionClientRole;
import au.com.houspect.atlas.models.InspectionImage;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.models.InspectionSectionItem;
import au.com.houspect.atlas.models.InspectionSectionItemTwoNumbers;
import au.com.houspect.atlas.models.InspectionSectionSnippet;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.models.RecentInspection;

/**
 * Created by alfredwong on 4/2/17.
 */

public class Util {
    public static int calculateNoOfColumns(Context context, float columnWidth, float padding) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        dpWidth -= padding;
        int noOfColumns = (int) (dpWidth / columnWidth);
        return noOfColumns;
    }

    public static int dpsToPixels(int dps, Context context) {
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (dps * scale + 0.5f);
        return pixels;
    }


    public static String removeTrailingNumber(String string) {
        string = string.replaceAll("\\d*$", "");
        string = string.trim();
        return string;
    }

    public static String dateTimeToString(long time) {
        SimpleDateFormat dest = new SimpleDateFormat("HH:mm dd/MM/yyyy", Locale.ENGLISH);
        Date date = new Date(time);
        String result = dest.format(date);
        return result;
    }


    public static class GenieColumn {

        public static enum BindingNameType {
            SearchCode("SearchCode"),
            Type("Type"),
            Street("Street"),
            Suburb("Suburb"),
            Client("Client"),
            Date("Date"),
            Time("Time"),
            // RecentInspection related
            Mobile("Mobile"),
            Status("Status");

            String name;
            private BindingNameType(String name) {
                this.name = name;
            }
        }

        public enum FieldType {
            Date("Date"),
            Time("Time"),
            Note("Note"),
            Street("Street"),
            Suburb("Suburb");

            String name;
            FieldType(String name) { this.name = name; }
        }

        private String id;
        private String label;
        private String bindingName;
        private String field;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getBindingName() {
            return bindingName;
        }

        public void setBindingName(String bindingName) {
            this.bindingName = bindingName;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }
    }

    public static List<Job> parseJobsGenieGridData(GridData data) {

        HashMap<String, GenieColumn> idToGenieColumn = new HashMap<>();

        for (Column column : data.getColumns()) {
            GenieColumn genieColumn = new GenieColumn();
            genieColumn.setId(column.getId());
            genieColumn.setBindingName(column.getBindingName());
            genieColumn.setLabel(column.getLabel());
            idToGenieColumn.put(genieColumn.getId(), genieColumn);
        }

        List<Job> jobs = new ArrayList<>();
        for (Row row : data.getRows()) {

            Job job = new Job();
            job.setId(row.getId());

            for (TextGridDataCell cell : row.textCells()) {
                GenieColumn genieColumn = idToGenieColumn.get(cell.getCellId());
                String textContent = cell.getText();
                if (GenieColumn.BindingNameType.SearchCode.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setCode(textContent);
                else if (GenieColumn.BindingNameType.Type.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setReportType(textContent);
                else if (GenieColumn.BindingNameType.Street.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setStreet(textContent);
                else if (GenieColumn.BindingNameType.Suburb.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setSuburb(textContent);
                else if (GenieColumn.BindingNameType.Client.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setClientName(textContent);
                else if (GenieColumn.BindingNameType.Date.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setDate(textContent);
                else if (GenieColumn.BindingNameType.Time.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setTime(textContent);
            }

            jobs.add(job);
        }

        return jobs;
    }

    public static List<RecentInspection> parseRecentInspectionsGenieGridData(GridData data) {

        HashMap<String, GenieColumn> idToGenieColumn = new HashMap<>();

        for (Column column : data.getColumns()) {
            GenieColumn genieColumn = new GenieColumn();
            genieColumn.setId(column.getId());
            genieColumn.setBindingName(column.getBindingName());
            genieColumn.setLabel(column.getLabel());
            idToGenieColumn.put(genieColumn.getId(), genieColumn);
        }

        List<RecentInspection> recentInspections = new ArrayList<>();
        for (Row row : data.getRows()) {

            RecentInspection recentInspection = new RecentInspection();
            recentInspection.setId(row.getId());

            for (TextGridDataCell cell : row.textCells()) {
                GenieColumn genieColumn = idToGenieColumn.get(cell.getCellId());
                String textContent = cell.getText();
                if (GenieColumn.BindingNameType.SearchCode.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    recentInspection.setCode(textContent);
                else if (GenieColumn.BindingNameType.Type.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    recentInspection.setReportType(textContent);
                else if (GenieColumn.BindingNameType.Street.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    recentInspection.setAddress(textContent);
                else if (GenieColumn.BindingNameType.Suburb.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    recentInspection.setSuburb(textContent);
                else if (GenieColumn.BindingNameType.Client.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    recentInspection.setClientName(textContent);
                else if (GenieColumn.BindingNameType.Date.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    recentInspection.setDate(textContent);
                else if (GenieColumn.BindingNameType.Mobile.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    recentInspection.setMobile(textContent);
                else if (GenieColumn.BindingNameType.Status.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    recentInspection.setStatus(textContent);
            }

            recentInspections.add(recentInspection);
        }

        return recentInspections;
    }



    private static HashMap<String, GenieColumn> parseJobsColumns(Element columnsElement) {
        Element elementL1 = columnsElement;

        HashMap<String, GenieColumn> idToGenieColumn = new HashMap<>();

        NodeList elementsL2 = elementL1.getElementsByTagName("Column");
        for (int i = 0; i < elementsL2.getLength(); i++) {
            Element elementL2 = (Element)elementsL2.item(i);
            GenieColumn genieColumn = new GenieColumn();
            genieColumn.setId(elementL2.getAttribute("id"));
            genieColumn.setBindingName(elementL2.getAttribute("bindingName"));
            genieColumn.setLabel(elementL2.getAttribute("label"));
            idToGenieColumn.put(genieColumn.getId(), genieColumn);
        }
        return idToGenieColumn;
    }

    public static List<Job> parseJobsXml(String xml) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Reader reader = new StringReader(xml);
        InputSource inputSource = new InputSource(reader);
        Document document = builder.parse(inputSource);

        Element eL1 = document.getDocumentElement();
        Element eL2 = (Element) eL1.getElementsByTagName("Columns").item(0);

        HashMap<String, GenieColumn> idToGenieColumn = parseJobsColumns(eL2);

        eL2 = (Element) eL1.getElementsByTagName("Rows").item(0);
        NodeList eL3List = eL2.getElementsByTagName("Row");
        List<Job> jobs = new ArrayList<>();
        for (int i = 0; i < eL3List.getLength(); i++) {
            Element eL3 = (Element)eL3List.item(i);
            NodeList eL4List = eL3.getElementsByTagName("Cell");

            Job job = new Job();
            job.setId(eL3.getAttribute("id"));

            for (int j = 0; j < eL4List.getLength(); j++) {
                Element eL4 = (Element)eL4List.item(j);
                String eL4Id = eL4.getAttribute("id");
                GenieColumn genieColumn = idToGenieColumn.get(eL4Id);
                String textContent = eL4.getTextContent();
                if (GenieColumn.BindingNameType.SearchCode.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setCode(textContent);
                else if (GenieColumn.BindingNameType.Type.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setReportType(textContent);
                else if (GenieColumn.BindingNameType.Street.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setStreet(textContent);
                else if (GenieColumn.BindingNameType.Suburb.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setSuburb(textContent);
                else if (GenieColumn.BindingNameType.Client.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setClientName(textContent);
                else if (GenieColumn.BindingNameType.Date.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setDate(textContent);
                else if (GenieColumn.BindingNameType.Time.name.equalsIgnoreCase(genieColumn.getBindingName()))
                    job.setTime(textContent);
            }

            jobs.add(job);
        }
        return jobs;
    }

    public static String xmlNodeToString(Node node) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            // below code to remove XML declaration
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(node), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Document toXmlDocument(Inspection inspection) {
        Document document = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument();
            Element element = toXmlElement(inspection, document);
            document.appendChild(element);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return document;
    }

    public static Element toXmlElement(Inspection inspection, Document document) {
        Element element = document.createElement("PropertyInspection");
        element.setAttribute("id", inspection.getId());
        element.setAttribute("code", inspection.getCode());
        element.setAttribute("submit", inspection.getSubmit());

        Element recommendationsElement = document.createElement("Recommendations");
        recommendationsElement.setTextContent(inspection.getRecommendations());

        Element propertyAddressElement = document.createElement("PropertyAddress");

        propertyAddressElement.setTextContent(inspection.getAddress());
        propertyAddressElement.setAttribute("lat", inspection.getLatitude()+"");
        propertyAddressElement.setAttribute("long", inspection.getLongitude()+"");

        Element reportTypeElement = document.createElement("ReportType");
        reportTypeElement.setTextContent(inspection.getInspectionReportType());

        Element dateElement = document.createElement("InspectionDate");
        dateElement.setTextContent(inspection.getDate() != null ? inspection.getDate() : "");

        Element timeElement = document.createElement("InspectionTime");
        timeElement.setTextContent(inspection.getTime() != null ? inspection.getTime() : "");

        Element durationElement = document.createElement("Duration");
        durationElement.setTextContent(inspection.getDuration() != null ? inspection.getDuration() : "");

        Element jobNotesElement = document.createElement("JobNotes");
        jobNotesElement.setTextContent(inspection.getJobNotes() != null ? inspection.getJobNotes() : "");

        Element theirJobNoElement = document.createElement("TheirJobNo");
        theirJobNoElement.setTextContent(inspection.getTheirJobNo() != null ? inspection.getTheirJobNo() : "");

        Element occupancyElement = document.createElement("Occupancy");
        occupancyElement.setTextContent(inspection.getOccupancy());

        Element bedroomsElement = document.createElement("Bedrooms");
        bedroomsElement.setTextContent(inspection.getNoOfBedrooms()+"");

        Element bathroomsElement = document.createElement("Bathrooms");
        bathroomsElement.setTextContent(inspection.getNoOfBathrooms()+"");

        Element propertyLevelsElememt = document.createElement("PropertyLevels");
        propertyLevelsElememt.setTextContent(inspection.getPropertyLevels());

        Element propertyFeaturesElement = document.createElement("PropertyFeatures");
        propertyFeaturesElement.setTextContent(inspection.getPropertyFeatures());

        Element numberOfUnitsElement = document.createElement("NumberOfUnits");
        numberOfUnitsElement.setTextContent(inspection.getNumberOfUnits());

        element.appendChild(recommendationsElement);
        element.appendChild(propertyAddressElement);
        element.appendChild(reportTypeElement);
        element.appendChild(dateElement);
        element.appendChild(timeElement);
        element.appendChild(durationElement);
        element.appendChild(jobNotesElement);
        element.appendChild(theirJobNoElement);
        element.appendChild(occupancyElement);
        element.appendChild(bedroomsElement);
        element.appendChild(bathroomsElement);
        element.appendChild(propertyLevelsElememt);
        element.appendChild(propertyFeaturesElement);
        element.appendChild(numberOfUnitsElement);

        Element clientElement = toXmlElement(inspection.getClient(), "Client", document, false);
        if (clientElement != null) element.appendChild(clientElement);

        Element contactsElement = document.createElement("Contacts");
        if (inspection.getContacts() != null) {
            for (InspectionClient contact : inspection.getContacts()) {
                Element contactElement = toXmlElement(contact, "Contact", document, true);
                contactsElement.appendChild(contactElement);
            }
        }
        element.appendChild(contactsElement);


        if (inspection.getImage() != null) {
            Element imageElement = toXmlElement(inspection.getImage(), document);
            element.appendChild(imageElement);
        }


        for (InspectionSection section : inspection.getSections()) {
            Element sectionElement = toXmlElement(section, document);
            element.appendChild(sectionElement);
        }

        return element;
    }

    private static Element toXmlElement(InspectionClient inspectionClient, String rootElementXmlTag, Document document, boolean shouldConsiderRole) {
        if (inspectionClient == null) return null;
        Element element = document.createElement(rootElementXmlTag);
        Element elementL2 = document.createElement("Name");
        elementL2.setTextContent(inspectionClient.getName() != null ? inspectionClient.getName() : "");
        element.appendChild(elementL2);
        elementL2 = document.createElement("Address");
        elementL2.setTextContent(inspectionClient.getAddress() != null ? inspectionClient.getAddress() : "");
        element.appendChild(elementL2);
        elementL2 = document.createElement("EmailAddress");
        elementL2.setTextContent(inspectionClient.getEmailAddress() != null ? inspectionClient.getEmailAddress() : "");
        element.appendChild(elementL2);
        elementL2 = document.createElement("MobilePhone");
        elementL2.setTextContent(inspectionClient.getMobilePhone() != null ? inspectionClient.getMobilePhone() : "");
        element.appendChild(elementL2);
        elementL2 = document.createElement("Phone");
        elementL2.setTextContent(inspectionClient.getPhone() != null ? inspectionClient.getPhone() : "");
        element.appendChild(elementL2);

        if (shouldConsiderRole) {
            Element roleElement = document.createElement("Role");
            Element idElement = document.createElement("Id");
            idElement.setTextContent(inspectionClient.getRole().getId());
            Element groupElement = document.createElement("Group");
            groupElement.setTextContent(inspectionClient.getRole().getGroup());
            Element descriptionElement = document.createElement("Description");
            descriptionElement.setTextContent(inspectionClient.getRole().getDescription());
            roleElement.appendChild(idElement);
            roleElement.appendChild(groupElement);
            roleElement.appendChild(descriptionElement);
            element.appendChild(roleElement);
        }
        return element;
    }

    private static Element toXmlElement(InspectionImage inspectionImage, Document document) {
        Element element = document.createElement("Image");
        element.setAttribute("filename", inspectionImage.getFileName());
        element.setAttribute("id", inspectionImage.getId());
        return element;
    }

    private static Element toXmlElement(InspectionSection inspectionSection, Document document) {
        Element element = document.createElement("Section");
        element.setAttribute("id", inspectionSection.getId());
        element.setAttribute("key", inspectionSection.getKey());
        element.setAttribute("name", inspectionSection.getName());
        element.setAttribute("optional", inspectionSection.isOptional() ? "Yes" : "No");
        element.setAttribute("status", inspectionSection.getStatus());
        element.setAttribute("heading", inspectionSection.getHeading());
        if (inspectionSection.getRepeatingKey() != null) element.setAttribute("repeatingKey", inspectionSection.getRepeatingKey());
        if (inspectionSection.getRepeatingSeq() != null) element.setAttribute("repeatingSeq", inspectionSection.getRepeatingSeq());
        element.setAttribute("na", inspectionSection.isNa() ? "1" : "0");

        element.setAttribute("clone", inspectionSection.isAClone() ? "1" : "0");
        if (inspectionSection.getInstanceName() != null && !inspectionSection.getInstanceName().isEmpty()) element.setAttribute("instanceName", inspectionSection.getInstanceName());
        if (inspectionSection.getCloneSourceName() != null & !inspectionSection.getCloneSourceName().isEmpty()) element.setAttribute("cloneSourceName", inspectionSection.getCloneSourceName());

        // TODO:: Header
        // TODO:: Footer

        Element commentsElement = document.createElement("Comments");
        commentsElement.setTextContent(inspectionSection.getComments());

        // TODO:: rectify server spelling mistakes
        Element recommendationsElement = document.createElement("Recommendations");
        recommendationsElement.setTextContent(inspectionSection.getRecommendations());

        element.appendChild(commentsElement);
        element.appendChild(recommendationsElement);

        if (inspectionSection.getImages() != null && inspectionSection.getImages().size() > 0) {
            for (InspectionSectionImage inspectionSectionImage : inspectionSection.getImages()) {
                Element imageElement = toXmlElement(inspectionSectionImage, document);
                element.appendChild(imageElement);
            }
        }


        for (InspectionSectionItem item : inspectionSection.getItems()) {
            Element itemElement = toXmlElement(item, document);
            element.appendChild(itemElement);
        }

        Element snippetsElement = document.createElement("Snippets");
        for (InspectionSectionSnippet snippet : inspectionSection.getSnippets()) {
            Element snippetElement = toXmlElement(snippet, document);
            snippetsElement.appendChild(snippetElement);
        }

        element.appendChild(snippetsElement);

        return element;
    }

    private static Element toXmlElement(InspectionSectionImage inspectionSectionImage, Document document) {
        Element element = document.createElement("Image");
        element.setAttribute("filename", inspectionSectionImage.getFileName());
        element.setAttribute("id", inspectionSectionImage.getId());
        if (inspectionSectionImage.getLabel() != null) {
            element.setAttribute("label", inspectionSectionImage.getLabel());
        }
        return element;
    }

    private static Element toXmlElement(InspectionSectionSnippet inspectionSectionSnippet, Document document) {
        Element element = document.createElement("Snippet");
        element.setAttribute("id", inspectionSectionSnippet.getId());
        element.setAttribute("comment", inspectionSectionSnippet.isComment() ? "True" : "False");
        // TODO:: rectify server mis-spelling
        element.setAttribute("reccomendation", inspectionSectionSnippet.isRecommendation() ? "True" : "False");
        element.setTextContent(inspectionSectionSnippet.getValue());
        return element;
    }

    private static Element toXmlElement(InspectionSectionItem inspectionSectionItem, Document document) {

        Element element = document.createElement("Item");
        element.setAttribute("id", inspectionSectionItem.getId());
        element.setAttribute("na", inspectionSectionItem.getNa());
        element.setAttribute("mandatory", inspectionSectionItem.isMandatory() ? "Yes" : "No");

        if (inspectionSectionItem.getSeedId() != null && !inspectionSectionItem.getSeedId().isEmpty()) element.setAttribute("seedId", inspectionSectionItem.getSeedId());

        Element naTextElement = document.createElement("NAText");
        naTextElement.setTextContent(inspectionSectionItem.getNaText());

        Element resultTypeElement = document.createElement("ResultType");
        resultTypeElement.setTextContent(inspectionSectionItem.getResultType());

        Element textElement = document.createElement("Text");
        textElement.setTextContent(inspectionSectionItem.getText());

        Element resultElement = document.createElement("Result");
        resultElement.setTextContent(inspectionSectionItem.getResult());

        Element resultCommentsElement = document.createElement("ResultComments");
        resultCommentsElement.setTextContent(inspectionSectionItem.getResultComments());

        element.appendChild(naTextElement);
        element.appendChild(resultTypeElement);
        element.appendChild(textElement);
        element.appendChild(resultElement);
        element.appendChild(resultCommentsElement);

        for (InspectionSectionItem.Option option : inspectionSectionItem.getOptions()) {
            Element optionElement = toXmlElement(option, document);
            element.appendChild(optionElement);
        }

        boolean isTwoNumbers = inspectionSectionItem instanceof InspectionSectionItemTwoNumbers;
        if (isTwoNumbers) populateXmlElement(element, (InspectionSectionItemTwoNumbers)inspectionSectionItem);

        return element;
    }

    /**
     * Populates an xml Element with only TwoNumbers specific properties considered i.e. the common ones are ignored
     * @param element
     * @param inspectionSectionItemTwoNumbers
     * @return
     */
    private static Element populateXmlElement(Element element, InspectionSectionItemTwoNumbers inspectionSectionItemTwoNumbers) {
        element.setAttribute("label1", inspectionSectionItemTwoNumbers.getLabel1());
        element.setAttribute("label2", inspectionSectionItemTwoNumbers.getLabel2());
        element.setAttribute("number1", inspectionSectionItemTwoNumbers.getNumber1());
        element.setAttribute("number2", inspectionSectionItemTwoNumbers.getNumber2());
        return element;
    }

    private static Element toXmlElement(InspectionSectionItem.Option inspectionSectionItemOption, Document document) {
        Element element = document.createElement("Option");
        element.setAttribute("id", inspectionSectionItemOption.getId());
        element.setAttribute("text", inspectionSectionItemOption.getText());
        return element;
    }

    public static List<Inspection> parseInspectionsXml(String xml) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Reader reader = new StringReader(xml);
        InputSource inputSource = new InputSource(reader);
        Document document = builder.parse(inputSource);

        List<Inspection> inspections = new ArrayList<>();

        NodeList eL2 = document.getDocumentElement().getElementsByTagName("PropertyInspection");
        for (int i = 0; i < eL2.getLength(); i++) {
            Element x = (Element)eL2.item(i);
            Inspection inspection = new Inspection();
            parseInspectionXml(x, inspection);
            inspections.add(inspection);
            String nodeXml = xmlNodeToString(x);
            inspection.setXml(nodeXml);
        }

        return inspections;
    }

    public static void parseInspectionXml(String xml, Inspection inspection) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Reader reader = new StringReader(xml);
        InputSource inputSource = new InputSource(reader);
        Document document = builder.parse(inputSource);
        parseInspectionXml(document.getDocumentElement(), inspection);
        inspection.setXmlParsed(true);
    }

    public static void parseInspectionXml(Element inspectionElement, Inspection inspection) {
        //Inspection inspection = new Inspection();

        Element elementL1 = inspectionElement; //document.getDocumentElement();
        inspection.setId(elementL1.getAttribute("id"));
        inspection.setCode(elementL1.getAttribute("code"));
        inspection.setSubmit(elementL1.getAttribute("submit"));

        inspection.setRecommendations(parseTextContent(elementL1, "Recommendations"));
        inspection.setAddress(parseTextContent(elementL1, "PropertyAddress"));
        Element elementPropertyAddress = getSingleChildElement(elementL1, "PropertyAddress");
        inspection.setLatitude(parseDoubleAttribute(elementPropertyAddress, "lat"));
        inspection.setLongitude(parseDoubleAttribute(elementPropertyAddress, "long"));
        inspection.setInspectionReportType(parseTextContent(elementL1, "ReportType"));
        inspection.setDate(parseTextContent(elementL1, "InspectionDate"));
        inspection.setTime(parseTextContent(elementL1, "InspectionTime"));
        inspection.setDuration(parseTextContent(elementL1, "Duration"));
        inspection.setJobNotes(parseTextContent(elementL1, "JobNotes"));
        inspection.setTheirJobNo(parseTextContent(elementL1, "TheirJobNo"));
        inspection.setOccupancy(parseTextContent(elementL1, "Occupancy"));
        inspection.setNoOfBedrooms(parseIntContent(elementL1, "Bedrooms"));
        inspection.setNoOfBathrooms(parseIntContent(elementL1, "Bathrooms"));
        inspection.setPropertyLevels(parseTextContent(elementL1, "PropertyLevels"));
        inspection.setPropertyFeatures(trimAndReplaceDelimiter(",", ", ", parseTextContent(elementL1, "PropertyFeatures")));
        inspection.setNumberOfUnits(parseTextContent(elementL1, "NumberOfUnits"));

        NodeList elementsL2 = elementL1.getElementsByTagName("Client");
        if (elementsL2 != null && elementsL2.getLength() > 0) {
            inspection.setClient(parseInspectionClient((Element) elementsL2.item(0)));
        }

        elementsL2 = elementL1.getElementsByTagName("Contacts");
        if (elementsL2 != null && elementsL2.getLength() > 0) {
            NodeList elementsL3 = ((Element)elementsL2.item(0)).getElementsByTagName("Contact");
            List<InspectionClient> contacts = new ArrayList<>();
            for (int i = 0; i < elementsL3.getLength(); i++) {
                InspectionClient contact = parseInspectionClient((Element) elementsL3.item(i));
                contacts.add(contact);
            }
            inspection.setContacts(contacts);
        }

        NodeList elementsImages = inspectionElement.getElementsByTagName("Image");
        if (elementsImages != null && elementsImages.getLength() > 0) {
            for (int i = 0; i < elementsImages.getLength(); i++) {
                Node descendantNode = elementsImages.item(i);
                boolean isInspectionImage = descendantNode.getParentNode() == inspectionElement;
                if (!isInspectionImage) continue;
                InspectionImage inspectionImage = parseInspectionImage((Element)elementsImages.item(i));
                inspection.setImage(inspectionImage);
            }
        }

        List<InspectionSection> list = new ArrayList<InspectionSection>();
        elementsL2 = elementL1.getElementsByTagName("Section");
        for (int i = 0; i < elementsL2.getLength(); i++) {
            InspectionSection inspectionSection = parseInspectionSection((Element)elementsL2.item(i));
            inspectionSection.setParentInspectionId(inspection.getId());
            inspectionSection.setParentInspectionCode(inspection.getCode());
            list.add(inspectionSection);
            // meta statistic information - used by clone - naming number
            inspection.getMeta().addSection(inspectionSection);
        }
        inspection.setSections(list);

        //return inspection;
    }

    public static InspectionClient parseInspectionClient(Element clientElement) {
        InspectionClient inspectionClient = new InspectionClient();
        if (clientElement == null) return inspectionClient;
        inspectionClient.setName(parseTextContent(clientElement, "Name"));
        inspectionClient.setAddress(parseTextContent(clientElement, "Address"));
        inspectionClient.setEmailAddress(parseTextContent(clientElement, "EmailAddress"));
        inspectionClient.setMobilePhone(parseTextContent(clientElement, "MobilePhone"));
        inspectionClient.setPhone(parseTextContent(clientElement, "Phone"));

        NodeList roleElements = clientElement.getElementsByTagName("Role");
        if (roleElements != null && roleElements.getLength() > 0) {
            InspectionClientRole inspectionClientRole = parseInspectionClientRole((Element)roleElements.item(0));
            inspectionClient.setRole(inspectionClientRole);
        }
        return inspectionClient;
    }

    public static InspectionClientRole parseInspectionClientRole(Element clientRoleElement) {
        InspectionClientRole inspectionClientRole = new InspectionClientRole();
        if (clientRoleElement == null) return inspectionClientRole;
        inspectionClientRole.setId(parseTextContent(clientRoleElement, "Id"));
        inspectionClientRole.setGroup(parseTextContent(clientRoleElement, "Group"));
        inspectionClientRole.setDescription(parseTextContent(clientRoleElement, "Description"));
        return inspectionClientRole;
    }

    public static InspectionImage parseInspectionImage(Element inspectionImageElement) {
        InspectionImage inspectionImage = new InspectionImage();
        inspectionImage.setFileName(inspectionImageElement.getAttribute("filename"));
        inspectionImage.setId(inspectionImageElement.getAttribute("id"));
        return inspectionImage;
    }

    public static InspectionSection parseInspectionSection(Element sectionElement) {
        InspectionSection inspectionSection = new InspectionSection();
        inspectionSection.setId(sectionElement.getAttribute("id"));
        inspectionSection.setKey(sectionElement.getAttribute("key"));
        inspectionSection.setName(sectionElement.getAttribute("name"));
        inspectionSection.setOptional(parseBooleanAttribute(sectionElement, "optional", false));
        inspectionSection.setStatus(sectionElement.getAttribute("status"));
        inspectionSection.setHeading(sectionElement.getAttribute("heading"));
        String repeatingKey = sectionElement.getAttribute("repeatingKey");
        if (repeatingKey != null && repeatingKey.trim().isEmpty()) repeatingKey = null;
        String repeatingSeq = sectionElement.getAttribute("repeatingSeq");
        if (repeatingSeq != null && repeatingSeq.trim().isEmpty()) repeatingSeq = null;
        inspectionSection.setRepeatingKey(repeatingKey);
        inspectionSection.setRepeatingSeq(repeatingSeq);
        inspectionSection.setNa(parseBooleanAttribute(sectionElement, "na", false));

        inspectionSection.setInstanceName(sectionElement.getAttribute("instanceName"));
        inspectionSection.setAClone(parseBooleanAttribute(sectionElement, "clone", false));
        inspectionSection.setCloneSourceName(sectionElement.getAttribute("cloneSourceName"));

        // header
        // footer

        String comments = parseTextContent(sectionElement, "Comments");
        if (comments != null) comments = comments.trim();
        inspectionSection.setComments(comments);

        // TODO:: rectify spelling mistakes - Recommendations
        String recommendations = parseTextContent(sectionElement, "Recommendations");
        if (recommendations != null) recommendations = recommendations.trim();
        inspectionSection.setRecommendations(recommendations);

        List<InspectionSectionImage> images = new ArrayList<>();
        NodeList elementsImages = sectionElement.getElementsByTagName("Image");
        if (elementsImages != null && elementsImages.getLength() > 0) {
            for (int i = 0; i < elementsImages.getLength(); i++) {
                InspectionSectionImage inspectionSectionImage = parseInspectionSectionImage((Element)elementsImages.item(i));
                images.add(inspectionSectionImage);
            }
        }
        inspectionSection.setImages(images);

        List<InspectionSectionItem> list = new ArrayList<>();
        NodeList elementsL3 = sectionElement.getElementsByTagName("Item");
        for (int i = 0; i < elementsL3.getLength(); i++) {
            InspectionSectionItem inspectionSectionItem = parseInspectionSectionItem((Element)elementsL3.item(i));
            list.add(inspectionSectionItem);
        }
        inspectionSection.setItems(list);

        List<InspectionSectionSnippet> snippets = new ArrayList<>();
        NodeList x = sectionElement.getElementsByTagName("Snippets");
        if (x != null && x.getLength() > 0) {
            Element elementL3 = (Element)x.item(0);
            NodeList elementsL4 = elementL3.getElementsByTagName("Snippet");
            for (int i = 0; i < elementsL4.getLength(); i++) {
                InspectionSectionSnippet inspectionSectionSnippet = parseInspectionSectionSnippet((Element)elementsL4.item(i));
                snippets.add(inspectionSectionSnippet);
            }
        }
        inspectionSection.setSnippets(snippets);

        return inspectionSection;
    }

    public static InspectionSectionImage parseInspectionSectionImage(Element sectionImageElement) {
        InspectionSectionImage inspectionSectionImage = new InspectionSectionImage();
        inspectionSectionImage.setFileName(sectionImageElement.getAttribute("filename"));
        inspectionSectionImage.setId(sectionImageElement.getAttribute("id"));
        inspectionSectionImage.setLabel(sectionImageElement.getAttribute("label"));
        return inspectionSectionImage;
    }

    public static InspectionSectionSnippet parseInspectionSectionSnippet(Element sectionSnippetElement) {
        InspectionSectionSnippet inspectionSectionSnippet = new InspectionSectionSnippet();

        inspectionSectionSnippet.setId(sectionSnippetElement.getAttribute("id"));
        inspectionSectionSnippet.setComment(parseBooleanAttribute(sectionSnippetElement,"comment", false));
        // TODO: rectify the mis-spelling recommendatio with server
        inspectionSectionSnippet.setRecommendation(parseBooleanAttribute(sectionSnippetElement,"reccomendation", false));
        inspectionSectionSnippet.setValue(sectionSnippetElement.getTextContent());

        return inspectionSectionSnippet;
    }

    public static InspectionSectionItem parseInspectionSectionItem(Element sectionItemElement) {

        InspectionSectionItem inspectionSectionItem = new InspectionSectionItem();
        inspectionSectionItem.setResultType(parseTextContent(sectionItemElement, "ResultType"));
        boolean isTwoNumbers = inspectionSectionItem.getType() == InspectionSectionItem.Type.TwoNumbers;
        if (isTwoNumbers) inspectionSectionItem = parseInspectionSectionItemTwoNumbers(sectionItemElement);

        inspectionSectionItem.setId(sectionItemElement.getAttribute("id"));
        inspectionSectionItem.setNa(sectionItemElement.getAttribute("na"));
        inspectionSectionItem.setMandatory(parseBooleanAttribute(sectionItemElement, "mandatory", true));

        inspectionSectionItem.setSeedId(sectionItemElement.getAttribute("seedId"));

        inspectionSectionItem.setNaText(parseTextContent(sectionItemElement, "NAText"));
        if (!inspectionSectionItem.isMandatory() && (inspectionSectionItem.getNaText() == null || "".equalsIgnoreCase(inspectionSectionItem.getNaText()))) inspectionSectionItem.setNaText("NAText");

        inspectionSectionItem.setResultType(parseTextContent(sectionItemElement, "ResultType"));
        inspectionSectionItem.setText(parseTextContent(sectionItemElement, "Text"));
        inspectionSectionItem.setResult(parseTextContent(sectionItemElement, "Result"));
        inspectionSectionItem.setResultComments(parseTextContent(sectionItemElement, "ResultComments").trim());

        List<InspectionSectionItem.Option> list = new ArrayList<>();
        NodeList elementsL4 = sectionItemElement.getElementsByTagName("Option");
        for (int i = 0; i < elementsL4.getLength(); i++) {
            InspectionSectionItem.Option inspectionSectionItemOption = parseInspectionSectionItemOption((Element)elementsL4.item(i));
            list.add(inspectionSectionItemOption);
        }
        inspectionSectionItem.setOptions(list);

        return inspectionSectionItem;
    }

    /**
     * Parse/traverse sectionItemElement (XML) and returns an instance of InspectionSectionItemTwoNumbers (with only two numbers specific properties populated i.e. not the common ones).
     * @param sectionItemElement
     * @return
     */
    public static InspectionSectionItemTwoNumbers parseInspectionSectionItemTwoNumbers(Element sectionItemElement) {
        InspectionSectionItemTwoNumbers inspectionSectionItem = new InspectionSectionItemTwoNumbers();
        inspectionSectionItem.setLabel1(sectionItemElement.getAttribute("label1"));
        inspectionSectionItem.setLabel2(sectionItemElement.getAttribute("label2"));
        inspectionSectionItem.setNumber1(sectionItemElement.getAttribute("number1"));
        inspectionSectionItem.setNumber2(sectionItemElement.getAttribute("number2"));
        return inspectionSectionItem;
    }

    public static InspectionSectionItem.Option parseInspectionSectionItemOption(Element sectionItemOptionElement) {
        InspectionSectionItem.Option option = new InspectionSectionItem.Option();

        option.setId(sectionItemOptionElement.getAttribute("id"));
        option.setText(sectionItemOptionElement.getAttribute("text"));

        return option;
    }

    public static int parseIntContent(Element element, String childTagName) {
        int value = 0;
        String stringValue = parseTextContent(element, childTagName);
        if (stringValue == null) return value;
        value = Integer.parseInt(stringValue);
        return value;
    }

    private static Element getSingleChildElement(Element element, String childTagName) {
        NodeList list = element.getElementsByTagName(childTagName);
        if (list == null || list.getLength() == 0) return null;
        Element childElement = (Element)list.item(0);
        return childElement;
    }

    public static String parseTextContent(Element element, String childTagName) {
        String value = null;
        Element childElement = getSingleChildElement(element, childTagName);
        if (childElement == null) return value;
        value = childElement.getTextContent();
        if (value != null) value.trim();
        return value;
    }

    public static boolean parseBooleanAttribute(Element element, String attributeName, boolean defaultValue) {
        boolean hasAttribute = element.hasAttribute(attributeName);
        if (!hasAttribute) return defaultValue;
        String booleanString = element.getAttribute(attributeName);
        boolean value = booleanString != null && ("yes".equalsIgnoreCase(booleanString) || "true".equalsIgnoreCase(booleanString) || "t".equalsIgnoreCase(booleanString) || "1".equalsIgnoreCase(booleanString));
        return value;
    }

    public static double parseDoubleAttribute(Element element, String attributeName) {
        String doubleString = element.getAttribute(attributeName);
        double value = 0;
        if (doubleString == null || doubleString.isEmpty()) return value;
        value = Double.parseDouble(doubleString);
        return value;
    }

    public static String getLocalPhotoFilePath(Context context, InspectionSectionImage inspectionSectionImage) {
        return getLocalPhotoFilePath(context, inspectionSectionImage.getFileName());
    }

    public static String getLocalPhotoFilePath(Context context, String filename) {
        File photoFolder = MediaUtil.getOutputMedaiaFolder(context);
        File photoFile = new File(photoFolder.getPath() + File.separator + filename);
        if (!photoFile.exists()) return null;
        return photoFile.getAbsolutePath();
    }

    public static void sortTemporals(List<ITemporal> unsortedTemporals, Boolean ascending) {

        Collections.sort(unsortedTemporals, (t1, t2) -> {
            long t1Time = timeIn24HourStringFormatToDouble(t1.getTime());
            long t2Time = timeIn24HourStringFormatToDouble(t2.getTime());
            if (t1Time == t2Time) return 0;
            boolean isT1Smaller = t1Time < t2Time;
            if (isT1Smaller && ascending) return -1;
            else if (isT1Smaller && !ascending) return 1;
            else if (!isT1Smaller && ascending) return 1;
            else if (!isT1Smaller && !ascending) return -1;
            return 0;
        });
    }

    /**
     * Server might return time string without semi-colon. This function detects such case and injects the semi-colon.
     * @param timeIn24Hour
     */
    public static String normalise24HourTimeString(String timeIn24Hour) {

        if (timeIn24Hour == null) return null;
        boolean containsSemiColon = timeIn24Hour.contains(":");
        if (containsSemiColon) return timeIn24Hour;
        timeIn24Hour = timeIn24Hour.trim();
        String normalisedValue = timeIn24Hour.substring(0, timeIn24Hour.length()-2) + ":" + timeIn24Hour.substring(timeIn24Hour.length()-2);
        return normalisedValue;

    }

    /**
     * Parse a 24 hour formatted time string as Date (use the default Date) and returns the resultant Date.getTime().
     * If the string is not parseable, this functions return 0.
     * @param timeIn24HourStringFormat
     * @return
     */
    public static long timeIn24HourStringFormatToDouble(String timeIn24HourStringFormat) {
        String dateTimeString = "01-01-2018 " + (timeIn24HourStringFormat + "").trim();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date date;
        try {
            date = dateFormat.parse(dateTimeString);
            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    //region CalendarItem xml parsing
    public static List<CalendarItem> parseCalendarItemsGenieGridData(GridData data) {

        HashMap<String, GenieColumn> idToGenieColumn = new HashMap<>();

        for (Column column : data.getColumns()) {
            GenieColumn genieColumn = new GenieColumn();
            genieColumn.setId(column.getId());
            genieColumn.setBindingName(column.getBindingName());
            genieColumn.setLabel(column.getLabel());
            genieColumn.setField(column.getFieldId());
            idToGenieColumn.put(genieColumn.getId(), genieColumn);
        }

        List<CalendarItem> calendarItems = new ArrayList<>();
        for (Row row : data.getRows()) {

            CalendarItem calendarItem = new CalendarItem();
            calendarItem.setId(row.getId());

            for (TextGridDataCell cell : row.textCells()) {
                GenieColumn genieColumn = idToGenieColumn.get(cell.getCellId());
                String textContent = cell.getText();
                if (GenieColumn.FieldType.Street.name.equalsIgnoreCase(genieColumn.getField()))
                    calendarItem.setStreet(textContent);
                else if (GenieColumn.FieldType.Suburb.name.equalsIgnoreCase(genieColumn.getField()))
                    calendarItem.setSuburb(textContent);
                else if (GenieColumn.FieldType.Note.name.equalsIgnoreCase(genieColumn.getField()))
                    calendarItem.setNote(textContent);
                else if (GenieColumn.FieldType.Date.name.equalsIgnoreCase(genieColumn.getField()))
                    calendarItem.setDate(textContent);
                else if (GenieColumn.FieldType.Time.name.equalsIgnoreCase(genieColumn.getField()))
                    calendarItem.setTime(normalise24HourTimeString(textContent));
            }

            calendarItems.add(calendarItem);
        }

        return calendarItems;
    }
    //endregion

    /**
     * Given a string, returns a new string with oldDelimiter replaced by newDelimiter.
     * In the split process, the tokens are trimmed of starting and trailing whitespaces.
     * @param oldDelimiter
     * @param newDelimiter
     * @param subject
     * @return
     */
    public static String trimAndReplaceDelimiter(String oldDelimiter, String newDelimiter, String subject) {

        if (subject == null) return null;
        if (oldDelimiter == null) return null;
        if (newDelimiter == null) return null;

        String[] tokens = subject.split(oldDelimiter);
        // note migrate to use String,join & stream() api while min api can be upgraded to 26
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i].trim();
            boolean isLastItem = i == tokens.length-1;
            stringBuffer.append(token);
            if (!isLastItem) stringBuffer.append(newDelimiter);
        }

        return stringBuffer.toString();
    }

    /**
     * Compute time in milliseconds since January 1, 1970 base on getDate() and getTime()
     * @return
     */
    public static long computeTimeSince1stJan1970(String dateInString, String timeInString) {

        String simpleDateFormatForTime = "hh:mm a";
        String simpleDateFormatForDate = "dd/MM/yy";

        // Sanitise time string e.g. from " 9:00AM " to "9:00 AM"
        String sanitisedTime = (timeInString != null && timeInString.trim().length() > 0 ? timeInString.trim() : "00:00 AM").trim();
        boolean isAm = sanitisedTime.endsWith("AM");
        boolean isPm = sanitisedTime.endsWith("PM");
        boolean is24HourFormat = !isAm && !isPm;
        if (!is24HourFormat) {
            if (isAm) sanitisedTime = sanitisedTime.replace("AM", "").trim() + " AM";
            else if (isPm) sanitisedTime = sanitisedTime.replace("PM", "").trim() + " PM";
        }
        else {
            simpleDateFormatForTime = "HH:mm";
        }

        // Sanitise date string
        String sanitisedDate = (dateInString != null) ? dateInString.trim() : "";
        boolean hasDate = sanitisedDate.length() > 0;
        if (!hasDate) simpleDateFormatForDate = "";

        String simpleDateFormat = simpleDateFormatForDate + " " + simpleDateFormatForTime;
        SimpleDateFormat format = new SimpleDateFormat(simpleDateFormat, Locale.ENGLISH);
        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        try {
            Date date = format.parse(sanitisedDate + " " + sanitisedTime);
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }

        long timeSince1stJan1970 = calendar != null ? calendar.getTime().getTime() : 0;
        return timeSince1stJan1970;
    }
}
