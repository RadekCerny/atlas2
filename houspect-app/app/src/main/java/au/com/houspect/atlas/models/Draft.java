package au.com.houspect.atlas.models;

import android.os.Parcel;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by alfredwong on 30/1/17.
 */

public class Draft extends Inspection {
    @DatabaseField
    private long modifiedTime;
    @DatabaseField
    private long declaredTime;
    // reportType for list display purposes vs inspectionReportType (to be extracted from Xml)
    @DatabaseField
    private String reportType;
    @DatabaseField
    private int noOfCompletedSections;
    @DatabaseField
    private int noOfSections;

    // Duplicated for performance purpose - draft list display without the need for parsing the underlying xml
    @DatabaseField
    private String codeForListDisplay;

    // Duplicated for performance purpose - draft list display without the need for parsing the underlying xml
    @DatabaseField
    private String clientNameForListDisplay;

    // possible value: 0 = Atlas Draft, 1 = Zeus Draft, 2 = Zeus Final
    @DatabaseField
    private int mode;

    public static final int MODE_ATLAS_DRAFT = 0;
    public static final int MODE_ZEUS_DRAFT = 1;
    public static final int MODE_ZEUS_FINAL_REPORT = 2;

    public static final String SUBMIT_VALUE_ATLAS_DRAFT = "0";
    public static final String SUBMIT_VALUE_ZEUS_DRAFT = "draft";
    public static final String SUBMIT_VALUE_ZEUS_FINAL_REPORT = "final";


    @DatabaseField
    private long uploadedTime;

    @DatabaseField
    private String createdByUsername = ""; // default to empty

    public long getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(long modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public long getDeclaredTime() {
        return declaredTime;
    }

    public void setDeclaredTime(long declaredTime) {
        this.declaredTime = declaredTime;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public int getNoOfCompletedSections() {
        return noOfCompletedSections;
    }

    public void setNoOfCompletedSections(int noOfCompletedSections) {
        this.noOfCompletedSections = noOfCompletedSections;
    }

    public int getNoOfSections() {
        return noOfSections;
    }

    public void setNoOfSections(int noOfSections) {
        this.noOfSections = noOfSections;
    }

    public String getCodeForListDisplay() {
        return codeForListDisplay;
    }

    public void setCodeForListDisplay(String codeForListDisplay) {
        this.codeForListDisplay = codeForListDisplay;
    }

    public String getClientNameForListDisplay() {
        return clientNameForListDisplay;
    }

    public void setClientNameForListDisplay(String clientNameForListDisplay) {
        this.clientNameForListDisplay = clientNameForListDisplay;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public long getUploadedTime() {
        return uploadedTime;
    }

    public void setUploadedTime(long uploadedTime) {
        this.uploadedTime = uploadedTime;
    }

    public String getCreatedByUsername() {
        return createdByUsername;
    }

    public void setCreatedByUsername(String createdByUsername) {
        this.createdByUsername = createdByUsername;
    }

    public Draft() {}

    //region Parcelable
    private Draft(Parcel in) {
        super(in);
        modifiedTime = in.readLong();
        declaredTime = in.readLong();
        reportType = in.readString();
        noOfCompletedSections = in.readInt();
        noOfSections = in.readInt();
        codeForListDisplay = in.readString();
        clientNameForListDisplay = in.readString();
        mode = in.readInt();
        uploadedTime = in.readLong();
        createdByUsername = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(modifiedTime);
        dest.writeLong(declaredTime);
        dest.writeString(reportType);
        dest.writeInt(noOfCompletedSections);
        dest.writeInt(noOfSections);
        dest.writeString(codeForListDisplay);
        dest.writeString(clientNameForListDisplay);
        dest.writeInt(mode);
        dest.writeLong(uploadedTime);
        dest.writeString(createdByUsername);
    }

    public static final Creator CREATOR = new Creator() {

        @Override
        public Draft createFromParcel(Parcel source) {
            return new Draft(source);
        }

        @Override
        public Draft[] newArray(int size) {
            return new Draft[size];
        }
    };
    //endregion

    public String getPropertyInspectionElementOnly(boolean isActive) {
        return "<PropertyInspection id=\"" + getId() + "\" code=\"" + getCode() + "\" active=\"" + (isActive ? "1" : "0") + "\" />";
    }

    //region Atlas Draft, Zeus Draft & Zeus Final Report related functions
    public void setAsAtlasDraft() {
        if (isZeusDraft() || isZeusFinalReport()) throw new RuntimeException("Backward conversion to Atlas Draft from Zeus Draft or Final Report is not permitted");
        mode = MODE_ATLAS_DRAFT;
        setSubmit(SUBMIT_VALUE_ATLAS_DRAFT);
    }

    public void setAsZeusDraft() {
        if (isZeusFinalReport()) throw new RuntimeException("Backward conversion to Zeus Draft from Zeus Final Report is not permitted");
        if (isZeusDraft()) return;
        mode = MODE_ZEUS_DRAFT;
        setUploadedTime(0); // reset uploadedTime to indicate that Zeus Draft hasn't been uploaded
        setSubmit(SUBMIT_VALUE_ZEUS_DRAFT);
        // TODO:: ask Radek on which flag to use in order to differentiate Atlas draft from Zeus draft
    }

    public void setAsZeusFinal() {
        if (isZeusDraft()) throw new RuntimeException("Conversion to Zeus Final Report from Zeus Draft is not permitted");
        if (isZeusFinalReport()) return;
        mode = MODE_ZEUS_FINAL_REPORT;
        setUploadedTime(0); // reset uploadedTime to indicate that Zeus Final Report hasn't been submitted
        setSubmit(SUBMIT_VALUE_ZEUS_FINAL_REPORT);
    }

    public boolean isAtlasDraft() {
        return mode == MODE_ATLAS_DRAFT;
    }

    public boolean isZeusDraft() {
        return mode == MODE_ZEUS_DRAFT;
    }

    public boolean isZeusFinalReport() {
        return mode == MODE_ZEUS_FINAL_REPORT;
    }

    public boolean isPendingForUploadToServer() {
        return uploadedTime == 0;
    }

    public boolean isValidZeusDraft() {

        if ((getSections() == null || getSections().size() == 0) && isXmlParsed()) return false;

        for (InspectionSection inspectionSection : getSections()) {
            boolean isValidZeusSection = inspectionSection.isOptional() || (inspectionSection.isComplete());
            if (!isValidZeusSection) return false;
        }
        return true;
    }

    public boolean isValidZeusFinal() {
        return isComplete();
    }

    private boolean isComplete() {
        boolean isValid = true;
        for (InspectionSection section : getSections()) {
            if (section.isComplete()) continue;
            isValid = false;
            break;
        }
        return isValid;
    }
    //endregion
}
