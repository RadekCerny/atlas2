package au.com.houspect.atlas.viewmodels;

import au.com.houspect.atlas.models.InspectionClient;
import au.com.houspect.atlas.models.RecentInspection;

/**
 * Created by alfredwong on 06/06/17.
 */

public class RecentInspectionViewModel {

    private RecentInspection model;

    public RecentInspection getModel() {
        return model;
    }

    public void setModel(RecentInspection model) {
        this.model = model;
    }

    public RecentInspectionViewModel(RecentInspection model) {
        this.model = model;
    }

}
