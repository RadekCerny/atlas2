package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.adapters.InspectionEndSectionItemRecyclerViewAdapter;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.services.DataService;
import au.com.houspect.atlas.services.DataServiceImpl;
import au.com.houspect.atlas.services.GenieService;
import au.com.houspect.atlas.services.GenieServiceImpl;
import au.com.houspect.atlas.services.SharedPreferencesService;
import au.com.houspect.atlas.services.SharedPreferencesServiceImpl;
import au.com.houspect.atlas.utilities.Util;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InspectionEndFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InspectionEndFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InspectionEndFragment extends Fragment implements InspectionEndSectionItemRecyclerViewAdapter.OnInteractionListener {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static InspectionEndFragment.Event SAVE_AS_ATLAS_DRAFT = new InspectionEndFragment.Event(9010);
        public static InspectionEndFragment.Event SAVE_AS_ZEUS_DRAFT = new InspectionEndFragment.Event(9011);
        public static InspectionEndFragment.Event SUBMIT_FINAL_REPORT = new InspectionEndFragment.Event(9012);
        public static InspectionEndFragment.Event BACK = new InspectionEndFragment.Event(902);
        public static InspectionEndFragment.Event SELECT_SECTION = new InspectionEndFragment.Event(903);
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_INSPECTION_ID = "inspectionId";

    private Inspection mInspection;

    private FragmentActivity mContext;

    private OnFragmentInteractionListener mListener;

    private Button mButtonSave;
    private Button mButtonBack;

    private RecyclerView mRecyclerViewInspectionSections;


    public InspectionEndFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param inspectionId
     * @return A new instance of fragment InspectionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InspectionEndFragment newInstance(String inspectionId) {
        InspectionEndFragment fragment = new InspectionEndFragment();
        Bundle args = new Bundle();
        //args.putParcelable(ARG_INSPECTION, inspection);
        args.putString(ARG_INSPECTION_ID, inspectionId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // Note:: certain device cannot handle storage of Inspection in bundle, hence store inspectionId instead. Revise this design in the future.

            String inspectionId = getArguments().getString(ARG_INSPECTION_ID);

            // TODO:: dependency inject
            GenieService genieService = new GenieServiceImpl();
            SharedPreferencesService sharedPreferencesService = new SharedPreferencesServiceImpl(getContext());
            DataService dataService = new DataServiceImpl(getContext(), genieService, sharedPreferencesService);
            mInspection = dataService.get(inspectionId, Draft.class);
            try {
                Util.parseInspectionXml(mInspection.getXml(), mInspection);
            } catch (Exception e) {
                Log.e("SideMenu", e.getLocalizedMessage());
            }

            // End Note
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inspection_end, container, false);
        mButtonSave = (Button) view.findViewById(R.id.buttonSave);
        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSaveActionOptions();
            }
        });
        mButtonBack = (Button) view.findViewById(R.id.buttonBack);
        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(InspectionEndFragment.Event.BACK, mInspection);
            }
        });
        mRecyclerViewInspectionSections = (RecyclerView) view.findViewById(R.id.recyclerViewInspectionSections);
        InspectionEndSectionItemRecyclerViewAdapter adapter = new InspectionEndSectionItemRecyclerViewAdapter(mInspection.getSections(), R.layout.fragment_inspection_end_section_item, this);
        mRecyclerViewInspectionSections.setAdapter(adapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            mContext = (FragmentActivity) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Event event, Object context);
    }

    //region InspectionEndSectionItemRecyclerViewAdapter.OnInteractionListener
    @Override
    public void onSelection(InspectionSection inspectionSection, int sectionIndex) {
        mListener.onFragmentInteraction(Event.SELECT_SECTION, sectionIndex);
    }
    //endregion


    private void showSaveActionOptions() {

        CharSequence colors[] = new CharSequence[] {"Save as Atlas Draft", "Save as Zeus Draft", "Submit Final Report" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Save Options");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        mListener.onFragmentInteraction(Event.SAVE_AS_ATLAS_DRAFT, mInspection);
                        break;
                    case 1:
                        mListener.onFragmentInteraction(Event.SAVE_AS_ZEUS_DRAFT, mInspection);
                        break;
                    case 2:
                        mListener.onFragmentInteraction(Event.SUBMIT_FINAL_REPORT, mInspection);
                        break;
                }
            }
        });
        builder.show();
    }
}
