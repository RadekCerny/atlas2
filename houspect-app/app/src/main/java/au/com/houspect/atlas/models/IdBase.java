package au.com.houspect.atlas.models;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by alfredwong on 3/2/17.
 */

public class IdBase {

    @DatabaseField(id = true)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
