package au.com.houspect.atlas.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.fragments.JobListFragment;
import au.com.houspect.atlas.fragments.dummy.DummyContent.DummyItem;
import au.com.houspect.atlas.models.InspectionSectionSnippet;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link JobListFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class SnippetRecyclerViewAdapter extends RecyclerView.Adapter<SnippetRecyclerViewAdapter.ViewHolder> {


    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SECTION_HEADER = 1;

    private final List<InspectionSectionSnippet> mValues;
    private final OnSnippetInteractionListener mListener;
    private int mItemResourceId;

    public interface OnSnippetInteractionListener {
        public void onSnippetSelected(InspectionSectionSnippet selectedSnippt);
    }

    public SnippetRecyclerViewAdapter(List<InspectionSectionSnippet> items, OnSnippetInteractionListener listener,
                                      int itemResourceId) {

        mValues = items;
        mListener = listener;
        mItemResourceId = itemResourceId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int resourceId = mItemResourceId;
        View view = LayoutInflater.from(parent.getContext())
                .inflate(resourceId, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final InspectionSectionSnippet item = mValues.get(position);
        final ItemViewHolder itemViewHolder = (ItemViewHolder)holder;
        itemViewHolder.mItem = item;
        itemViewHolder.mTextView.setText(item.getValue());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onSnippetSelected(item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
        }
    }


    class ItemViewHolder extends ViewHolder {

        public final TextView mTextView;
        public InspectionSectionSnippet mItem;

        public ItemViewHolder(View view) {
            super(view);
            mTextView = (TextView) view.findViewById(R.id.textView);
        }
    }
}
