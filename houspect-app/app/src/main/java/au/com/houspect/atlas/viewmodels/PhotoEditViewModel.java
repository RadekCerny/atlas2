package au.com.houspect.atlas.viewmodels;

import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.widget.ToggleButton;

import au.com.houspect.atlas.models.InspectionSectionImage;
import io.reactivex.Completable;

public class PhotoEditViewModel {
    public ObservableBoolean isLoading = new ObservableBoolean(true);
    public ObservableField<String> label = new ObservableField<>("");
    private ObservableBoolean didClickToCloseObservable = new ObservableBoolean(false);
    public ObservableField<String> toggledBrushObservable = new ObservableField("");
    private InspectionSectionImage model;

    public Completable closeEvent = Completable.create(subscriber -> {
        final Observable.OnPropertyChangedCallback callback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (sender != didClickToCloseObservable) return;
                if (subscriber.isDisposed()) return;
                if (didClickToCloseObservable.get()) subscriber.onComplete();
            }
        };
        didClickToCloseObservable.addOnPropertyChangedCallback(callback);
    });

    public io.reactivex.Observable<String> toggleBrushEvent = io.reactivex.Observable.create(subscriber -> {
        final Observable.OnPropertyChangedCallback callback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (sender != toggledBrushObservable) return;
                if (subscriber.isDisposed()) return;
                subscriber.onNext(toggledBrushObservable.get());
            }
        };
        toggledBrushObservable.addOnPropertyChangedCallback(callback);
    });

    public PhotoEditViewModel(InspectionSectionImage model) {
        this.model = model;
        label.set(model.getLabel());
        label.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                model.setLabel(label.get());
            }
        });
    }

    public void onClickToClose() {
        didClickToCloseObservable.set(true);
    }

    public void onToggleBrush(ToggleButton toggleButton, String brushColor) {
        toggledBrushObservable.set(brushColor);
        if (!toggleButton.isChecked()) toggledBrushObservable.set("");
    }

}
