package au.com.houspect.atlas.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.fragments.SideMenuFragment;
import au.com.houspect.atlas.fragments.SideMenuInspectionTOCFragment;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.SideMenuItem;
import au.com.houspect.atlas.utilities.SessionUtil;

/**
 * {@link RecyclerView.Adapter} that can display a {@link SideMenuItem} and makes a call to the
 * specified {@link SideMenuFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class SideMenuInspectionTOCAdapter extends RecyclerView.Adapter<SideMenuInspectionTOCAdapter.ViewHolder> {

    public interface Listener {
        // @param int selectedTOCIndex - 0-based index inclusive of start, sections, end
        void onCloneRequested(InspectionSection selectedSection, int selectedTOCIndex);
        void onRemoveCloneRequested(InspectionSection selectedSection, int selectedTOCIndex);
        void onItemSelected(InspectionSection selectedSection, int selectedTOCIndex);
    }

    private final List<InspectionSection> mValues;
    private final Listener mListener;
    private int mItemResourceId;
    // TODO:: dependency inject
    private SessionUtil mSessionUtil;

    public SideMenuInspectionTOCAdapter(List<InspectionSection> items,
                                        Listener listener,
                                        int itemResourceId,
                                        SessionUtil sessionUtil) {
        mValues = items;
        mListener = listener;
        mItemResourceId = itemResourceId;
        mSessionUtil = sessionUtil;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(mItemResourceId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        boolean isStart = position == 0;
        int endPosition = mValues.size()+1+1;
        boolean isEnd = position == endPosition;
        boolean isSecondLastPage = position == endPosition-1;

        String number = "";
        String title = "";

        final int selectedTOCIndex = position;

        if (isStart) {
            title = "Start";
            holder.mItem = null;
        }
        else if (isEnd) {
            title = "End";
            holder.mItem = null;
        }
        else if (isSecondLastPage) {
            title = "Recommendation Summary";
            holder.mItem = null;
        }
        else {
            int arrayPosition = position;
            arrayPosition--; // minus start


            holder.mItem = mValues.get(arrayPosition);
            number = (arrayPosition+1)+"";
            title = holder.mItem.getName() + " " + holder.mItem.getInstanceName();


            if (holder.mItem.canClone()) {
                holder.mViewContainerClone.setVisibility(View.VISIBLE);
                holder.mButtonClone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.mItem.setSectionIndex(selectedTOCIndex-1);
                        mListener.onCloneRequested(holder.mItem, selectedTOCIndex);
                    }
                });
            }
            else if (holder.mItem.isAClone()) {
                holder.mViewContainerRemoveClone.setVisibility(View.VISIBLE);
                holder.mButtonRemoveClone.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        holder.mItem.setSectionIndex(selectedTOCIndex-1);
                        mListener.onRemoveCloneRequested(holder.mItem, selectedTOCIndex);
                    }
                });
            }

        }

        holder.mTextViewNumber.setText(number);
        holder.mTextViewTitle.setText(title);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onItemSelected(holder.mItem, selectedTOCIndex);
                }
            }
        });

        // check for highlight information
        //if (mSessionUtil != null) {
        //    boolean shouldHighlight = mSessionUtil.getSideMenuActive() != null && mSessionUtil.getSideMenuActive().getTitle().equals(holder.mItem.getTitle());
        //    if (shouldHighlight) holder.mView.setBackgroundResource(R.color.colorSideMenuHighlight);
        //    else holder.mView.setBackgroundResource(android.R.color.transparent);
        //}
    }

    @Override
    public int getItemCount() {
        int extra = 2; // start & end
        extra += 1; // recommendation summary
        return mValues.size() + extra;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTextViewNumber;
        public final TextView mTextViewTitle;
        public final View mViewContainerClone;
        public final Button mButtonClone;
        public final View mViewContainerRemoveClone;
        public final Button mButtonRemoveClone;
        public InspectionSection mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTextViewNumber = (TextView) view.findViewById(R.id.textViewNumber);
            mTextViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            mViewContainerClone = view.findViewById(R.id.viewContainerClone);
            mButtonClone = (Button) view.findViewById(R.id.buttonClone);
            mViewContainerRemoveClone = view.findViewById(R.id.viewContainerRemoveClone);
            mButtonRemoveClone = (Button) view.findViewById(R.id.buttonRemoveClone);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTextViewTitle.getText() + "'";
        }
    }

}
