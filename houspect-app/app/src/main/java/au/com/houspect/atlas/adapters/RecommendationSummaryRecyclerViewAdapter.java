package au.com.houspect.atlas.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeSet;

import au.com.houspect.atlas.BR;
import au.com.houspect.atlas.R;
import au.com.houspect.atlas.fragments.InspectionRecommendationSummaryFragment;
import au.com.houspect.atlas.fragments.JobListFragment;
import au.com.houspect.atlas.fragments.dummy.DummyContent.DummyItem;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.viewmodels.InspectionSectionItemViewModel;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link JobListFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class RecommendationSummaryRecyclerViewAdapter extends RecyclerView.Adapter<RecommendationSummaryRecyclerViewAdapter.ViewHolder> {

    public interface Listener {
        void onRecommendationSummaryHeaderSelected(InspectionSection selectedInspectionSection, int sectionIndex);
    }


    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SECTION_HEADER = 1;

    private final List<InspectionSection> mValues;
    private final Map<Integer,String> mSectionHeaders = new HashMap<>(); // map position
    private final Listener mListener;
    private int mItemResourceId;
    private int mSectionResourceId;

    public RecommendationSummaryRecyclerViewAdapter(List<InspectionSection> items,
                                                    Listener listener,
                                                    int itemResourceId) {

        mValues = items;
        mListener = listener;
        mItemResourceId = itemResourceId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int resourceId = mItemResourceId;
        View view = LayoutInflater.from(parent.getContext())
                .inflate(resourceId, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

            InspectionSection item = mValues.get(position);
            final ItemViewHolder itemViewHolder = (ItemViewHolder)holder;
            itemViewHolder.viewDataBinding.setVariable(BR.model, item);
            itemViewHolder.mItem = item;

            final int sectionIndex = position;
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mListener) {
                        // Notify the active callbacks interface (the activity, if the
                        // fragment is attached to one) that an item has been selected.
                        mListener.onRecommendationSummaryHeaderSelected(itemViewHolder.mItem, sectionIndex);
                    }
                }
            });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        ViewDataBinding viewDataBinding;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            viewDataBinding = DataBindingUtil.bind(view);
        }
    }

    class ItemViewHolder extends ViewHolder {

        public InspectionSection mItem;

        public ItemViewHolder(View view) {
            super(view);
        }
    }

}
