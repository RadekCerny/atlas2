package au.com.houspect.atlas.utilities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import au.com.houspect.atlas.fragments.InspectionSectionFragment;
import au.com.houspect.atlas.models.InspectionSectionImage;

/**
 * Created by alfredwong on 5/8/17.
 */

public class AttachPhotoUtil {

    private static class RequestCode {
        public static int TAKE_A_PHOTO = 9999;
        public static int PICK_A_PHOTO = 9000;
    }

    public interface Listener {
        void onPhotoReadyToBeAttached(File file);
    }

    private Fragment mContainerFragment;
    private Runnable mOnPhotoError;
    private Runnable mOnShouldSelectFromLibrary;
    private Runnable mOnShouldTakeAPhoto;
    private Listener mListener;

    //region session variables
    private String mPathPhoto;
    private Uri mUriPhoto;
    private long captureTime;
    //endregion

    public AttachPhotoUtil(Fragment containerFragment,
                           Runnable onPhotoError,
                           Runnable onShouldSelectFromLibrary,
                           Runnable onShouldTakeAPhoto,
                           Listener listener) {
        mContainerFragment = containerFragment;
        mOnPhotoError = onPhotoError;
        mOnShouldSelectFromLibrary = onShouldSelectFromLibrary;
        mOnShouldTakeAPhoto = onShouldTakeAPhoto;
        mListener = listener;
    }

    public void invokeCamera() {

        String uuid = UUID.randomUUID().toString();
        File photoFile = MediaUtil.getOutputMediaFile(mContainerFragment.getActivity(), MediaUtil.MEDIA_TYPE_IMAGE, uuid);

        if (photoFile == null) {
            mOnPhotoError.run();
            //HashMap<String,Object> dataObject = new HashMap<>();
            //dataObject.put("inspectionSection", mInspectionSection);
            //mListener.onFragmentInteraction(this, InspectionSectionFragment.Event.PHOTO_ERROR, dataObject);
            return;
        }
        mPathPhoto = photoFile.getAbsolutePath();

        mUriPhoto = MediaUtil.getImageUri(mContainerFragment.getActivity(), photoFile);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mUriPhoto);
        cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        captureTime = (new Date()).getTime();

        mContainerFragment.startActivityForResult(cameraIntent, AttachPhotoUtil.RequestCode.TAKE_A_PHOTO);
    }

    public void invokePhotoLibrary() {
        Intent photoLibraryIntent = new Intent(Intent.ACTION_PICK);
        photoLibraryIntent.setType("image/*");
        mContainerFragment.startActivityForResult(photoLibraryIntent, AttachPhotoUtil.RequestCode.PICK_A_PHOTO);
    }

    private void addPhotoToGallery(String photoPath) {

        // ScanFile so it will be appeared on Gallery
        MediaScannerConnection.scanFile(mContainerFragment.getActivity(),
                new String[]{photoPath}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        captureTime = 0;

                        // TODO:: Test only - remove me
                        //MediaUtil.Base64ImageInfo base64ImageInfo = MediaUtil.base64EncodeImageFile(mPathPhoto, false);
                        //int rotation = MediaUtil.getImageRotation(getContext(), uri);
                        //int i = 0;
                    }
                });
    }

    public void showPhotoActionOptions() {

        //final HashMap<String,Object> dataObject = new HashMap<>();
        //dataObject.put("inspectionSection", mInspectionSection);
        //final WeakReference<InspectionSectionFragment> weakSelf = new WeakReference<InspectionSectionFragment>(this);

        CharSequence colors[] = new CharSequence[]{"Picture Library", "Take a Photo"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContainerFragment.getActivity());

        builder.setTitle("Attach Photo");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                boolean shouldTakeAPhoto = which == 1;
                boolean shouldSelectFromLibrary = which == 0;

                if (shouldSelectFromLibrary) {
                    mOnShouldSelectFromLibrary.run();
                    //mListener.onFragmentInteraction(weakSelf.get(), InspectionSectionFragment.Event.PHOTO_STORAGE_PERMISSION_REQUEST_PICK_A_PHOTO, dataObject);
                } else if (shouldTakeAPhoto) {
                    mOnShouldTakeAPhoto.run();
                    //mListener.onFragmentInteraction(weakSelf.get(), InspectionSectionFragment.Event.PHOTO_STORAGE_PERMISSION_REQUEST_TAKE_A_PHOTO, dataObject);
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == mContainerFragment.getActivity().RESULT_OK) {

            File file = null;
            if (requestCode == AttachPhotoUtil.RequestCode.TAKE_A_PHOTO) {

                file = new File(mPathPhoto);
                addPhotoToGallery(mPathPhoto);

            } else if (requestCode == AttachPhotoUtil.RequestCode.PICK_A_PHOTO) {
                if (data == null) {
                    mOnPhotoError.run();
                    return;
                }

                String uuid = UUID.randomUUID().toString();

                file = MediaUtil.getOutputMediaFile(mContainerFragment.getActivity(), MediaUtil.MEDIA_TYPE_IMAGE, uuid);


                Uri pickedImage = data.getData();
                // Let's read picked image path using content resolver
                String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION};
                Cursor cursor = mContainerFragment.getActivity().getContentResolver().query(pickedImage, columns, null, null, null);
                cursor.moveToFirst();
                String imagePath = cursor.getString(cursor.getColumnIndex(columns[0]));
                int orientation = cursor.getInt(cursor.getColumnIndex(columns[1]));

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
                Matrix matrix = new Matrix();
                matrix.postRotate(orientation);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
                MediaUtil.saveBitmap(bitmap, file);

                // At the end remember to close the cursor or you will end with the RuntimeException!
                cursor.close();

            } else {
                Log.i("Atlas", "AttachPhotoUtil unknown Dialog Request: " + requestCode);
                return;
            }

            mListener.onPhotoReadyToBeAttached(file);
            mUriPhoto = null;
            mPathPhoto = null;

        }
    }
}
