package au.com.houspect.atlas.viewmodels;

import android.view.View;

import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionClient;

/**
 * Created by alfredwong on 17/2/17.
 */

public class InspectionStartViewModel {

    private Inspection model;
    private String reportType;
    private String clientContactDetails;
    private String siteContactName;
    private String siteContactDetails;
    private String siteContactRole;
    private String siteContactName2;
    private String siteContactDetails2;
    private String siteContactRole2;
    private String theirJobNo;
    private Runnable onBack;
    private Runnable onNext;

    public Inspection getModel() {
        return model;
    }

    public void setModel(Draft model) {
        this.model = model;
    }

    public String getReportType() {
        return reportType;
    }

    public String getClientContactDetails() {
        return clientContactDetails;
    }

    public String getSiteContactName() {
        return siteContactName;
    }

    public String getSiteContactDetails() {
        return siteContactDetails;
    }

    public String getSiteContactName2() {
        return siteContactName2;
    }

    public String getSiteContactDetails2() {
        return siteContactDetails2;
    }

    public String getSiteContactRole() {
        return siteContactRole;
    }

    public String getSiteContactRole2() {
        return siteContactRole2;
    }

    public String getTheirJobNo() {
        return theirJobNo;
    }

    public void setTheirJobNo(String theirJobNo) {
        this.theirJobNo = theirJobNo;
    }

    public InspectionStartViewModel(Inspection model, Runnable onBack, Runnable onNext) {
        this.model = model;
        this.onBack = onBack;
        this.onNext = onNext;

        clientContactDetails = "";
        if (model.getClient() != null && model.getClient().getMobilePhone() != null && !model.getClient().getMobilePhone().isEmpty()) clientContactDetails += (clientContactDetails.isEmpty() ? "" : "   ") + "M:" + model.getClient().getMobilePhone();
        if (model.getClient() != null && model.getClient().getPhone() != null && !model.getClient().getPhone().isEmpty()) clientContactDetails += (clientContactDetails.isEmpty() ? "" : "   ") + "P:" + model.getClient().getPhone();
        if (model.getClient() != null && model.getClient().getEmailAddress() != null && !model.getClient().getEmailAddress().isEmpty()) clientContactDetails += (clientContactDetails.isEmpty() ? "" : "   ") + "E:" + model.getClient().getEmailAddress();

        siteContactName = "";
        if (model.getContacts() != null && model.getContacts().size() > 0) {
            InspectionClient firstContactAsSiteContact = model.getContacts().get(0);
            siteContactName = firstContactAsSiteContact.getName();
            siteContactDetails = consolidateSiteContactDetails(firstContactAsSiteContact);
            siteContactRole = firstContactAsSiteContact.getRole().getDescription();
        }

        if (model.getContacts() != null && model.getContacts().size() > 1) {
            InspectionClient secondContactAsSiteContact = model.getContacts().get(1);
            siteContactName2 = secondContactAsSiteContact.getName();
            siteContactDetails2 = consolidateSiteContactDetails(secondContactAsSiteContact);
            siteContactRole2 = secondContactAsSiteContact.getRole().getDescription();
        }

        theirJobNo = "";
        if (model.getTheirJobNo() != null) theirJobNo = model.getTheirJobNo();

    }

    private String consolidateSiteContactDetails(InspectionClient siteContactAsInspectionClient) {
        String result = "";
        if (siteContactAsInspectionClient.getMobilePhone() != null && !siteContactAsInspectionClient.getMobilePhone().isEmpty()) result += (result.isEmpty() ? "" : "   ") + "M:" + siteContactAsInspectionClient.getMobilePhone();
        if (siteContactAsInspectionClient.getPhone() != null && !siteContactAsInspectionClient.getPhone().isEmpty()) result += (result.isEmpty() ? "" : "   ") + "P:" + siteContactAsInspectionClient.getPhone();
        if (siteContactAsInspectionClient.getEmailAddress() != null && !siteContactAsInspectionClient.getEmailAddress().isEmpty()) result += (result.isEmpty() ? "" : "   ") + "E:" + siteContactAsInspectionClient.getEmailAddress();
        return result;
    }

    public void onBackClick(View v) {
        onBack.run();
    }

    public void onNextClick(View v) {
        onNext.run();
    }

}
