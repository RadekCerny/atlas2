package au.com.houspect.atlas.models;

import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alfredwong on 10/07/18.
 */

public class InspectionSectionItemTwoNumbers extends InspectionSectionItem implements Parcelable, Cloneable {

    // Questions related
    private String number1; // if blank = empty
    private String number2; // if blank = empty
    private String label1;
    private String label2;


    public InspectionSectionItemTwoNumbers() {
        super();
    }

    public String getNumber1() {
        return number1;
    }

    public void setNumber1(String number1) {
        this.number1 = number1;
    }

    public String getNumber2() {
        return number2;
    }

    public void setNumber2(String number2) {
        this.number2 = number2;
    }

    public String getLabel1() {
        return label1;
    }

    public void setLabel1(String label1) {
        this.label1 = label1;
    }

    public String getLabel2() {
        return label2;
    }

    public void setLabel2(String label2) {
        this.label2 = label2;
    }

    public boolean isNumber1Empty() {
        return number1 == null || number1.trim().length() == 0 || "na".equalsIgnoreCase(number1) || !isNumeric(getNumber1());
    }

    public boolean isNumeric(String string) {
        try {
            Double.parseDouble(string);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    public boolean isNumber2Empty() {
        return number2 == null || number2.trim().length() == 0 || "na".equalsIgnoreCase(number2) || !isNumeric(getNumber2());
    }

    public double calculateTotal() {
        double n1 = 0, n2 = 0;
        try {
            n1 = Double.parseDouble(getNumber1());
        }
        catch (Exception e) { }
        try {
            n2 = Double.parseDouble(getNumber2());
        }
        catch (Exception e) { }
        if (isNumber1Empty() && !isNumber2Empty()) return n2;
        else if (!isNumber1Empty() && isNumber2Empty()) return n1;

        return n1 * n2;
    }

    //region Parcelable
    private InspectionSectionItemTwoNumbers(Parcel in) {
        super(in);
        number1 = in.readString();
        number2 = in.readString();
        label1 = in.readString();
        label2 = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(getNumber1());
        dest.writeString(getNumber2());
        dest.writeString(getLabel1());
        dest.writeString(getLabel2());
    }

    public static final Creator CREATOR = new Creator() {

        @Override
        public InspectionSectionItemTwoNumbers createFromParcel(Parcel source) {
            return new InspectionSectionItemTwoNumbers(source);
        }

        @Override
        public InspectionSectionItemTwoNumbers[] newArray(int size) {
            return new InspectionSectionItemTwoNumbers[size];
        }
    };
    //endregion Parcelable

    //region Cloneable
    @Override
    protected Object clone() throws CloneNotSupportedException {

        InspectionSectionItem rawClone = (InspectionSectionItem) super.clone();

        // honor the super clone logic - get the cloned properties as is
        InspectionSectionItemTwoNumbers clone = new InspectionSectionItemTwoNumbers();
        clone.setId(rawClone.getId());
        clone.setSeedId(rawClone.getSeedId()); // rawClone is a result of super.clone(), it already has the correct seedId set to the origin's id
        clone.setNa(rawClone.getNa()); // do not clone result / answer
        clone.setMandatory(rawClone.isMandatory());
        clone.setText(rawClone.getText());
        clone.setResultType(rawClone.getResultType());
        clone.setNaText(rawClone.getNaText());
        clone.setOptions(rawClone.getOptions());
        clone.setResult(rawClone.getResult());
        clone.setResultComments(rawClone.getResultComments());

        // clone the TwoNumbers specific properties
        clone.setNumber1("");
        clone.setNumber2("");

        // if number1 = na, copy across
        if ("na".equalsIgnoreCase(getNumber1())) clone.setNumber1("na");
        // if number2 = na, copy across
        if ("na".equalsIgnoreCase(getNumber2())) clone.setNumber2("na");

        clone.setLabel1(getLabel1());
        clone.setLabel2(getLabel2());

        return clone;
    }
    //endregion

}
