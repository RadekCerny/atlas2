package au.com.houspect.atlas.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.ViewDataBinding;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.lang.ref.WeakReference;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.services.DisposableManager;
import au.com.houspect.atlas.utilities.MediaUtil;
import au.com.houspect.atlas.viewmodels.CheckboxConfirmationViewModel;
import au.com.houspect.atlas.viewmodels.PhotoEditViewModel;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import ja.burhanrashid52.photoeditor.OnPhotoEditorListener;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import ja.burhanrashid52.photoeditor.ViewType;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PhotoEditFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PhotoEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PhotoEditFragment extends ViewModelFragment<PhotoEditViewModel> {

    public static class Event {
        private int mType;

        public Event(int type) {
            mType = type;
        }
    }

    public static final String ARG_INSPECTION_ID = "inspectionId";
    public static final String ARG_INSPECTION_SECTION_ID = "inspectionSectionId";
    public static final String ARG_PHOTO_PATH = "photoPath"; // existing photo path
    public static final String ARG_INSPECTION_SECTION_IMAGE = "inspectionSectionImage";
    public static final String ARG_INSPECTION_SECTION_IMAGE_IS_MARKUPED = "isMarkUped";

    private OnFragmentInteractionListener mListener;

    private String inspectionId;
    private String inspectionSectionId;
    private String photoPath;
    private InspectionSectionImage inspectionSectionImage;
    private ImageView mImageViewSelected;
    private PhotoEditorView mPhotoEditorView;
    private PhotoEditor mPhotoEditor;
    private boolean mIsPhotoMarkUped;

    public PhotoEditFragment() {
        // Required empty public constructor
    }

    @Override
    protected void onViewModelCreation() {

        mViewModel = new PhotoEditViewModel(inspectionSectionImage);
        DisposableManager.add(mViewModel.closeEvent.subscribe(() -> {

            MediaUtil.checkAndRequestPhotoStoragePermission(getActivity(),
                    MediaUtil.PermissionRequestCode.REQUEST_WRITE_STORAGE_EDIT_PHOTO);
            mPhotoEditor.saveAsFile(photoPath, new PhotoEditor.OnSaveListener() {
                @Override
                public void onSuccess(@NonNull String imagePath) {
                    Log.d("PhotoEditFragment", "Photo edits saved successfully");
                    Toast.makeText(getActivity(), "Photo edits saved successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }

                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.d("PhotoEditFragment", "Fail to save photo edits (" + exception.getLocalizedMessage() + ")");
                    Toast.makeText(getActivity(), "Fail to save photo edits (" + exception.getLocalizedMessage() + ")", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });



        }));
    }

    private void finish() {
        Intent data = new Intent();
        data.putExtra(ARG_INSPECTION_ID, inspectionId);
        data.putExtra(ARG_INSPECTION_SECTION_ID, inspectionSectionId);
        data.putExtra(ARG_PHOTO_PATH, photoPath);
        data.putExtra(ARG_INSPECTION_SECTION_IMAGE, inspectionSectionImage);
        data.putExtra(ARG_INSPECTION_SECTION_IMAGE_IS_MARKUPED, mIsPhotoMarkUped);
        getActivity().setResult(RESULT_OK, data);
        getActivity().finish();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_photo_edit;
    }

    @Override
    protected void onConfigureViewDataBinding(ViewDataBinding viewDataBinding) {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PhotoEditFragment.
     */
    public static PhotoEditFragment newInstance(String inspectionId, String inspectionSectionId, String photoPath, InspectionSectionImage inspectionSectionImage) {
        PhotoEditFragment fragment = new PhotoEditFragment();
        Bundle args = new Bundle();
        args.putString(ARG_INSPECTION_ID, inspectionId);
        args.putString(ARG_INSPECTION_SECTION_ID, inspectionSectionId);
        args.putString(ARG_PHOTO_PATH, photoPath);
        args.putParcelable(ARG_INSPECTION_SECTION_IMAGE, inspectionSectionImage);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            inspectionId = getArguments().getString(ARG_INSPECTION_ID);
            inspectionSectionId = getArguments().getString(ARG_INSPECTION_SECTION_ID);
            photoPath = getArguments().getString(ARG_PHOTO_PATH);
            inspectionSectionImage = getArguments().getParcelable(ARG_INSPECTION_SECTION_IMAGE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mImageViewSelected = (ImageView) view.findViewById(R.id.imageViewSelected);
        mPhotoEditorView = (PhotoEditorView) view.findViewById(R.id.photoEditorView);
        mPhotoEditor = new PhotoEditor.Builder(getContext(), mPhotoEditorView)
                .setPinchTextScalable(true)
                //.setDefaultTextTypeface(mTextRobotoTf)
                //.setDefaultEmojiTypeface(mEmojiTypeFace)
                .build();
        WeakReference<PhotoEditFragment> weakSelf = new WeakReference<>(this);
        mPhotoEditor.setOnPhotoEditorListener(new OnPhotoEditorListener() {
            @Override
            public void onEditTextChangeListener(View rootView, String text, int colorCode) {
                //Log.d("PhotoEditor", "onEditTextChangeListener");
            }

            @Override
            public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {
                //Log.d("PhotoEditor", "onAddViewListener");
            }

            @Override
            public void onRemoveViewListener(int numberOfAddedViews) {
                //Log.d("PhotoEditor", "onRemoveViewListener");
            }

            @Override
            public void onRemoveViewListener(ViewType viewType, int numberOfAddedViews) {
                //Log.d("PhotoEditor", "onRemoveViewListener");
            }

            @Override
            public void onStartViewChangeListener(ViewType viewType) {
                //Log.d("PhotoEditor", "onStartViewChangeListener");
                weakSelf.get().mIsPhotoMarkUped = true;
            }

            @Override
            public void onStopViewChangeListener(ViewType viewType) {
                //Log.d("PhotoEditor", "onStopViewChangeListener");
            }
        });
        refresh();
        DisposableManager.add(
                mViewModel.closeEvent.subscribe(() -> {
                    Log.d("PhotoEditFragment", "closeEvent");

        }));
        DisposableManager.add(
        mViewModel.toggleBrushEvent.subscribe((brushColor) -> {
            Log.d("PhotoEditFragment", "toggleBrushEvent - " + brushColor);
            if ("red".equalsIgnoreCase(brushColor)) mPhotoEditor.setBrushColor(getResources().getColor(android.R.color.holo_red_dark));
            else if ("white".equalsIgnoreCase(brushColor)) mPhotoEditor.setBrushColor(getResources().getColor(android.R.color.white));
            else if ("black".equalsIgnoreCase(brushColor)) mPhotoEditor.setBrushColor(getResources().getColor(android.R.color.black));
            mPhotoEditor.setBrushDrawingMode(true);
            if ("".equalsIgnoreCase(brushColor)) mPhotoEditor.brushEraser();

        }));
        return view;
    }

    private void refresh() {

        mViewModel.isLoading.set(true);

        Log.d("PhotoEditFragment", "refresh");

        DisposableManager.add(
                Single.create(subscriber -> {
                    Uri uri = Uri.fromFile(new File(photoPath));
                    int rotation = MediaUtil.getImageRotation(getContext(), uri);
                    Bitmap bitmap = BitmapFactory.decodeFile(photoPath);
                    bitmap = MediaUtil.rotateBitmap(bitmap, rotation);
                    subscriber.onSuccess(bitmap);
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(bitmap -> {
                            mImageViewSelected.setImageBitmap((Bitmap)bitmap);
                            mPhotoEditorView.getSource().setImageBitmap((Bitmap)bitmap);
                            mViewModel.isLoading.set(false);
                        })
        );


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Event event, Parcelable tag);
    }


    @Override
    public void onDestroy() {
        DisposableManager.dispose();
        super.onDestroy();
    }
}
