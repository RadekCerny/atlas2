package au.com.houspect.atlas.viewmodels;

import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by alfredwong on 17/2/17.
 */

public class ForgotPasswordViewModel {

    private String title;
    private String username;
    private String email;
    private Runnable onCancel;
    private Runnable onSubmit;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ForgotPasswordViewModel(String title, String username, Runnable onCancel, Runnable onSubmit) {
        this.title = title;
        this.username = username;
        this.onCancel = onCancel;
        this.onSubmit = onSubmit;
    }

    public void onCancelClick(View button) {
        onCancel.run();
    }

    public void onSubmitClick(View button) {
        onSubmit.run();
    }

    public boolean validateData() {
        if (TextUtils.isEmpty(username)) return false;
        if (TextUtils.isEmpty(email)) return false;
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) return false;
        return true;
    }
}
