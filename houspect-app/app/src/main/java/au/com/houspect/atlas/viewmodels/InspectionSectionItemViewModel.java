package au.com.houspect.atlas.viewmodels;

import android.databinding.ObservableBoolean;
import android.widget.CompoundButton;

import au.com.houspect.atlas.adapters.InspectionSectionItemRecyclerViewAdapter;
import au.com.houspect.atlas.models.InspectionSectionItem;
import io.reactivex.Observable;

/**
 * Created by alfredwong on 29/6/17.
 */

public class InspectionSectionItemViewModel {

    private InspectionSectionItem model;
    public ObservableBoolean shouldLockOtherComments;
    public ObservableBoolean isOtherChecked = new ObservableBoolean(false);
    public ObservableBoolean isOtherNaChecked = new ObservableBoolean(false);
    private InspectionSectionItemRecyclerViewAdapter.Listener listener;

    // Event to notify observer/subscriver that other has been checked
    public Observable<Boolean> otherCheckedEvent = Observable.create(subscriber -> {

        final android.databinding.Observable.OnPropertyChangedCallback callback = new android.databinding.Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(android.databinding.Observable sender, int propertyId) {
                if (sender != isOtherChecked && sender != isOtherNaChecked) return;
                if (subscriber.isDisposed()) return;
                if (((ObservableBoolean)sender).get()) subscriber.onNext(true);
            }
        };

        if (subscriber.isDisposed()) {
            isOtherNaChecked.removeOnPropertyChangedCallback(callback);
            isOtherNaChecked.removeOnPropertyChangedCallback(callback);
            return;
        }

        isOtherChecked.addOnPropertyChangedCallback(callback);
        isOtherNaChecked.addOnPropertyChangedCallback(callback);

    });

    public InspectionSectionItemViewModel(InspectionSectionItem model, InspectionSectionItemRecyclerViewAdapter.Listener listener) {
        this.model = model;
        this.listener = listener;
        this.isOtherChecked.set(model.isNa());
        this.isOtherNaChecked.set(model.isNa());
        this.shouldLockOtherComments = new ObservableBoolean(model.isOtherNaWithNaText());
    }

    public InspectionSectionItem getModel() {
        return model;
    }

    public void onOtherCheckedChanged(CompoundButton checkbox, boolean isChecked) {
        if (!model.isMandatory()) {
            onOtherNaCheckedChanged(checkbox, isChecked);
            return;
        }

        isOtherChecked.set(isChecked);

        if (isChecked) {
            model.setAsOther();
            // Populate question as comments if NA is checked + there is no existing comments
            boolean isEmptyComment = "".equalsIgnoreCase(model.observableResultComments.get());
            if (isEmptyComment) {
                model.observableResultComments.set(model.getText());
            }
        }
        else model.resetOther();
        shouldLockOtherComments.set(false);
    }

    public void onOtherNaCheckedChanged(CompoundButton checkbox, boolean isChecked) {
        isOtherNaChecked.set(isChecked);
        if (isChecked) {
            listener.onRequestForOtherNaOptions(this);
        }
        else {
            model.resetOtherNa();
        }
        shouldLockOtherComments.set(model.isOtherNaWithNaText());
    }

    public void configureOtherNaWithNaText() {
        model.setAsOtherNaWithNaText();
        shouldLockOtherComments.set(model.isOtherNaWithNaText());
    }

    public void configureOtherNaWithCustomText() {
        model.setAsOtherNa();
        shouldLockOtherComments.set(model.isOtherNaWithNaText());
    }

}
