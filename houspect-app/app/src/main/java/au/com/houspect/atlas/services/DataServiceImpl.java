package au.com.houspect.atlas.services;

import android.content.Context;
import android.net.Uri;
import android.support.v4.util.Pair;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.saasplications.genie.domain.model.ActivityInstance;

import org.w3c.dom.Document;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import au.com.houspect.atlas.fragments.DraftListFragment;
import au.com.houspect.atlas.models.CalendarItem;
import au.com.houspect.atlas.models.DatabaseHelper;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.IdBase;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionImage;
import au.com.houspect.atlas.models.InspectionImageUploadLog;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.models.InspectionSectionImageUploadLog;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.models.RecentInspection;
import au.com.houspect.atlas.utilities.MediaUtil;
import au.com.houspect.atlas.utilities.SessionUtil;
import au.com.houspect.atlas.utilities.Util;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by alfredwong on 18/02/2017.
 */

public class DataServiceImpl implements DataService {

    private Context context;
    private DatabaseHelper databaseHelper = null;
    private ActivityInstance activityInstance = null;
    private AtomicReference<ActivityInstance> activityInstanceForGetRecentInspectionsForDraftDeletion = new AtomicReference<>(null);
    private GenieService genieService;
    private SharedPreferencesService sharedPreferencesService;

    private DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        return databaseHelper;
    }

    public DataServiceImpl(Context context, GenieService genieService, SharedPreferencesService sharedPreferencesService) {
        this.context = context;
        this.genieService = genieService;
        this.sharedPreferencesService = sharedPreferencesService;
    }

    //region Refresh jobs & inspections
    @Override
    public void refreshJobsAndInspections(final Context context, final RefreshCallback callback) {

        activityInstance = null; // Note: old activityInstance would be invalid on new login/session, hence old activitInstance should not be used
        Single single = Single.fromCallable(new Callable<GenieServiceResponse<android.support.v4.util.Pair<List<Job>,List<Inspection>>>>() {
            @Override
            public GenieServiceResponse<android.support.v4.util.Pair<List<Job>, List<Inspection>>> call() throws Exception {

                //region download jobs from server
                final CountDownLatch signal = new CountDownLatch(1);
                final HashMap<String, Object> resultData = new HashMap<String, Object>();
                getJobs(context, new DataService.GetDataCallback<Job>() {
                    @Override
                    public void onCompletion(List<Job> data) {
                        resultData.put("jobs", data);
                        signal.countDown();
                    }

                    @Override
                    public void onError(String message) {
                        resultData.put("errorMessage", message);
                        signal.countDown();
                    }
                });
                signal.await(30, TimeUnit.SECONDS);


                boolean hasError = resultData.containsKey("errorMessage");
                if (hasError) throw new Exception((String)resultData.get("errorMessage"));
                //endregion

                //region download inspections from server
                final CountDownLatch signal2 = new CountDownLatch(1);
                getInspections(context, new DataService.GetDataCallback<Inspection>() {
                    @Override
                    public void onCompletion(List<Inspection> data) {
                        resultData.put("inspections", data);
                        signal2.countDown();
                    }

                    @Override
                    public void onError(String message) {
                        resultData.put("errorMessage", message);
                        signal2.countDown();
                    }
                });
                signal2.await(90, TimeUnit.SECONDS);

                hasError = resultData.containsKey("errorMessage");
                if (hasError) throw new Exception((String)resultData.get("errorMessage"));
                //endregion

                //region download recent inspections from server
                final CountDownLatch signal3 = new CountDownLatch(1);
                getRecentInspections(context, new DataService.GetDataCallback<RecentInspection>() {
                    @Override
                    public void onCompletion(List<RecentInspection> data) {
                        resultData.put("recentInspections", data);
                        signal3.countDown();
                    }

                    @Override
                    public void onError(String message) {
                        resultData.put("errorMessage", message);
                        signal3.countDown();
                    }
                });
                signal3.await(30, TimeUnit.SECONDS);

                hasError = resultData.containsKey("errorMessage");
                if (hasError) throw new Exception((String)resultData.get("errorMessage"));
                //endregion

                //region download calendar items from server
                final CountDownLatch signal4 = new CountDownLatch(1);
                Disposable disposable = getUpcomingCalendarItems()
                        // subscribe on current thread - thread blocking
                        .subscribe(calendarItems -> {
                            resultData.put("calendarItems", calendarItems);
                            signal4.countDown();
                        }, error -> {
                            resultData.put("errorMessage", error.getMessage());
                            signal4.countDown();
                        });
                // note - this wasn't needed as getUpcomingCalendarItems is already thread blocking - however, it is needed for Timeout in case of non-connectivity
                signal4.await(30, TimeUnit.SECONDS);
                disposable.dispose();

                hasError = resultData.containsKey("errorMessage");
                if (hasError) throw new Exception((String)resultData.get("errorMessage"));
                //endregion

                //region process jobs
                final HashMap<String, Boolean> idToDoesDraftExist = new HashMap<>();
                List<Draft> drafts = getList(null, Draft.class);
                for (int i = 0; i < drafts.size(); i++ ) {
                    idToDoesDraftExist.put(drafts.get(i).getId(), true);
                }

                removeAll(Job.class);

                List<Job> downloadedJobs = (List<Job>)resultData.get("jobs");
                List<Job> filteredJobs = new ArrayList<Job>();
                for (Job job : downloadedJobs) {
                    boolean shouldIgnoreJob = idToDoesDraftExist.containsKey(job.getId());
                    if (shouldIgnoreJob) continue;
                    filteredJobs.add(job);
                }
                save(filteredJobs, Job.class);
                //endregion

                //region process inspections
                removeAll(Inspection.class);

                List<Inspection> downloadedInspections = (List<Inspection>) resultData.get("inspections");
                List<Inspection> filteredInspections = new ArrayList<Inspection>();
                for (Inspection inspection : downloadedInspections) {
                    boolean shouldIgnoreInspection = idToDoesDraftExist.containsKey(inspection.getId()); //draft != null;
                    if (shouldIgnoreInspection) continue;
                    filteredInspections.add(inspection);
                }

                save(filteredInspections, Inspection.class);
                //endregion

                //region process recent inspections

                // remove all recent inspections except those that are flagged as isPendingUploadToServer = true
                List<RecentInspection> oldRecentInspections = getList(null, RecentInspection.class);
                for (RecentInspection x : oldRecentInspections) {
                    if (x.getPendingUploadToServer() == true) continue;
                    remove(x.getId(), RecentInspection.class);
                }
                // save new recent inspections
                List<RecentInspection> recentInspections = (List<RecentInspection>) resultData.get("recentInspections");
                save(recentInspections, RecentInspection.class);
                //endregion

                //region process calendar items
                List<CalendarItem> calendarItemsToDelete = getList(new DataService.GetCallback<CalendarItem>() {
                    @Override
                    public QueryBuilder<CalendarItem, ?> preQuery(QueryBuilder<CalendarItem, ?> queryBuilder) {
                        try {
                            String username = sharedPreferencesService.getUsername();
                            Where where = queryBuilder.where();
                            where.eq("createdByUsername", username);
                        } catch (Exception e) {
                            Log.e("Atlas CalenderItem list", e.getLocalizedMessage());
                        }
                        return queryBuilder;
                    }
                }, CalendarItem.class);
                remove(calendarItemsToDelete, CalendarItem.class);

                List<CalendarItem> downloadedCalendarItems = (List<CalendarItem>)resultData.get("calendarItems");
                for (CalendarItem x : downloadedCalendarItems) {
                    x.setCreatedByUsername(sharedPreferencesService.getUsername());
                }

                save(downloadedCalendarItems, CalendarItem.class);
                //endregion

                GenieServiceResponse response = new GenieServiceResponse(true, null, null);
                return response;
            }
        });
        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver() {
            @Override
            public void onSubscribe(Disposable d) {
                // TODO - add to activity list of disposable - to be disposed on destroy
            }

            @Override
            public void onSuccess(Object value) {
                callback.onSuccess();
            }

            @Override
            public void onError(Throwable e) {
                callback.onError(e);
            }
        });
    }
    //endregion

    //region Upload data to server


    @Override
    public void saveAtlasDraft(Draft draft, UploadCallback callback, boolean shouldSkipUpload) {
        draft.setAsAtlasDraft();
        saveDraft(draft);
        if (!shouldSkipUpload) uploadDraft(draft, callback);
    }

    @Override
    public void saveZeusDraft(Draft draft, UploadCallback callback, boolean shouldSkipUpload) {
        draft.setAsZeusDraft();
        saveDraft(draft);
        if (!shouldSkipUpload) uploadDraft(draft, callback);
    }

    @Override
    public void submitFinalReport(final Draft draft, final UploadCallback callback, boolean shouldSkipUpload) {
        RecentInspection recentInspection = null;
        try {
            recentInspection = draftToRecentInspection(draft);
            save(recentInspection, RecentInspection.class);
        } catch (Exception e) {
            Log.e("Atlas", "Failed to submit final report (" + e.getLocalizedMessage() + ")");
            callback.onEror(e);
            return;
        }
        draft.setAsZeusFinal();
        saveDraft(draft);

        if (shouldSkipUpload) return;

        final RecentInspection recentInspectionFinal = recentInspection;
        uploadDraft(draft, new UploadCallback() {
            @Override
            public void onSuccess() {
                remove(recentInspectionFinal.getId(), RecentInspection.class);
                remove(draft.getId(), Draft.class);
                callback.onSuccess();
            }

            @Override
            public void onEror(Throwable e) {
                callback.onEror(e);
            }
        });

    }

    @Override
    public void retrySubmitFinalReport(final RecentInspection recentInspection, final UploadCallback callback) {

        final Draft draft = get(recentInspection.getId(), Draft.class);
        if (draft == null) {
            callback.onEror(new Exception("Original draft record data cannot be found"));
            return;
        }
        uploadDraft(draft, new UploadCallback() {
            @Override
            public void onSuccess() {
                remove(recentInspection.getId(), RecentInspection.class);
                remove(draft.getId(), Draft.class);
                callback.onSuccess();
            }

            @Override
            public void onEror(Throwable e) {
                callback.onEror(e);
            }
        });
    }

    @Override
    public void uploadDraft(final Draft draft, final UploadCallback callback) {

        Single single = Single.fromCallable(new Callable() {
            @Override
            public Object call() throws Exception {

                GetSessionResult result = getSessionToken();
                if (result.getSessionToken() == null || result.getSessionToken().isEmpty()) {
                    throw new Exception("Cannot obtain valid session with server (" + result.getErrorMessage() + ").");
                }
                final String sessionToken = result.getSessionToken();

                final HashMap<String, Object> results = new HashMap<String, Object>();

                if (activityInstance == null) {

                    final CountDownLatch signal = new CountDownLatch(1);
                    genieService.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                        @Override
                        public void completed(GenieServiceResponse<ActivityInstance> response) {
                            if (response.isSuccess()) activityInstance = response.getData();
                            else results.put("error", new Exception(response.getErrorMessage()));
                            signal.countDown();
                        }
                    });

                    signal.await(30, TimeUnit.SECONDS);
                }

                boolean hasError = results.containsKey("error");
                if (hasError) throw (Exception)results.get("error");
                if (activityInstance == null) throw new Exception("Cannot connect to server");

                // This is needed in order to obtain isSubmit value - this would determine if the Draft record should be removed from ORM on successful upload
                if (!draft.isXmlParsed()) {
                    Util.parseInspectionXml(draft.getXml(), draft);
                }

                final CountDownLatch uploadSignal = new CountDownLatch(1);

                results.put("receivedSuccessResponse", false);
                genieService.uploadDraft(draft, sessionToken, activityInstance, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                    @Override
                    public void completed(GenieServiceResponse<ActivityInstance> response) {
                        if (!response.isSuccess()) {
                            results.put("error", new Exception(response.getErrorMessage()));
                            activityInstance = null; // Note - discard activityInstance - prepare for next retry
                        }
                        else {
                            draft.setUploadedTime((new Date()).getTime());
                            save(draft, Draft.class);
                            results.put("receivedSuccessResponse", true);
                        }
                        uploadSignal.countDown();
                    }
                });

                uploadSignal.await(30, TimeUnit.SECONDS);

                hasError = results.containsKey("error");
                if (hasError) throw (Exception)results.get("error");

                boolean isTimeout = !((boolean)results.get("receivedSuccessResponse"));
                if (isTimeout) throw new Exception("Timeout receiving response from server");

                return new GenieServiceResponse(true, null, null);
            }
        });

        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        // TODO - add to activity list of disposable - to be disposed on destroy
                    }

                    @Override
                    public void onSuccess(Object value) {
                        callback.onSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onEror(e);
                    }
                });

    }

    @Override
    public void notifyDraftIsActive(final Draft draft, final UploadCallback callback) {

        Single single = Single.fromCallable(new Callable() {
            @Override
            public Object call() throws Exception {

                GetSessionResult result = getSessionToken();
                if (result.getSessionToken() == null || result.getSessionToken().isEmpty()) {
                    throw new Exception("Cannot obtain valid session with server (" + result.getErrorMessage() + ").");
                }
                final String sessionToken = result.getSessionToken();

                final HashMap<String, Object> results = new HashMap<String, Object>();

                if (activityInstance == null) {

                    final CountDownLatch signal = new CountDownLatch(1);
                    genieService.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                        @Override
                        public void completed(GenieServiceResponse<ActivityInstance> response) {
                            if (response.isSuccess()) activityInstance = response.getData();
                            else results.put("error", new Exception(response.getErrorMessage()));
                            signal.countDown();
                        }
                    });

                    signal.await(30, TimeUnit.SECONDS);
                }

                boolean hasError = results.containsKey("error");
                if (hasError) throw (Exception)results.get("error");
                if (activityInstance == null) throw new Exception("Cannot connect to server");

                // This is needed in order to obtain isSubmit value - this would determine if the Draft record should be removed from ORM on successful upload
                if (!draft.isXmlParsed()) {
                    Util.parseInspectionXml(draft.getXml(), draft);
                }

                final CountDownLatch uploadSignal = new CountDownLatch(1);

                results.put("receivedSuccessResponse", false);
                genieService.notifyDraftIsActive(draft, sessionToken, activityInstance, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                    @Override
                    public void completed(GenieServiceResponse<ActivityInstance> response) {
                        if (!response.isSuccess()) {
                            results.put("error", new Exception(response.getErrorMessage()));
                            activityInstance = null; // Note - discard activityInstance - prepare for next retry
                        }
                        else {
                            results.put("receivedSuccessResponse", true);
                        }
                        uploadSignal.countDown();
                    }
                });

                uploadSignal.await(30, TimeUnit.SECONDS);

                hasError = results.containsKey("error");
                if (hasError) throw (Exception)results.get("error");

                boolean isTimeout = !((boolean)results.get("receivedSuccessResponse"));
                if (isTimeout) throw new Exception("Timeout receiving response from server");

                return new GenieServiceResponse(true, null, null);
            }
        });

        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        // TODO - add to activity list of disposable - to be disposed on destroy
                    }

                    @Override
                    public void onSuccess(Object value) {
                        callback.onSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onEror(e);
                    }
                });
    }

    @Override
    public void uploadImage(final InspectionSectionImageUploadLog inspectionSectionImageUploadLog,
                            final UploadCallback callback) {
        Single single = Single.fromCallable(new Callable() {
            @Override
            public Object call() throws Exception {

                GetSessionResult result = getSessionToken();
                if (result.getSessionToken() == null || result.getSessionToken().isEmpty()) {
                    throw new Exception("Cannot obtain valid session with server (" + result.getErrorMessage() + ").");
                }
                final String sessionToken = result.getSessionToken();

                final HashMap<String, Object> results = new HashMap<String, Object>();

                if (activityInstance == null) {

                    final CountDownLatch signal = new CountDownLatch(1);
                    genieService.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                        @Override
                        public void completed(GenieServiceResponse<ActivityInstance> response) {
                            if (response.isSuccess()) activityInstance = response.getData();
                            else results.put("error", new Exception(response.getErrorMessage()));
                            signal.countDown();
                        }
                    });

                    signal.await(30, TimeUnit.SECONDS);
                }

                boolean hasError = results.containsKey("error");
                if (hasError) throw (Exception)results.get("error");
                if (activityInstance == null) throw new Exception("Cannot connect to server");

                String localImageFilePath = Util.getLocalPhotoFilePath(context, inspectionSectionImageUploadLog.getFileName());

                boolean isImageStillExist = localImageFilePath != null;
                boolean isForDelete = inspectionSectionImageUploadLog.isForDeletion();

                if (isImageStillExist || isForDelete) {
                    final CountDownLatch uploadSignal = new CountDownLatch(1);

                    results.put("receivedSuccessResponse", false);


                    boolean shouldResizePhotos = sharedPreferencesService.getShouldResizePhotosWhenUpload();
                    boolean shouldGzip = false;

                    int width = 0;
                    int height = 0;
                    String base64 = "";

                    if (!isForDelete) {

                        Uri uri = Uri.fromFile(new File(localImageFilePath)); //MediaUtil.getImageUri(context, new File(localImageFilePath));
                        int rotation = MediaUtil.getImageRotation(context, uri);


                        if (shouldResizePhotos) {
                            int maxWidthOrHeight = sharedPreferencesService.getResizePhotosToMaxWidthOrHeight();
                            MediaUtil.ImageInfo resizedImageInfo = MediaUtil.resizeImage(localImageFilePath, maxWidthOrHeight);
                            localImageFilePath = resizedImageInfo.getAbsolutePath();
                        }

                        MediaUtil.Base64ImageInfo base64ImageInfo = MediaUtil.base64EncodeImageFile(localImageFilePath, rotation, shouldGzip);

                        Log.i("Atlas", "uploadImage base64 length (" + base64ImageInfo.getBase64String().length() + ")");

                        width = base64ImageInfo.getWidth();
                        height = base64ImageInfo.getHeight();
                        base64 = base64ImageInfo.getBase64String();
                    }

                    genieService.uploadImage(
                            inspectionSectionImageUploadLog,
                            width,
                            height,
                            base64,
                            shouldGzip,
                            sessionToken,
                            activityInstance, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                                @Override
                                public void completed(GenieServiceResponse<ActivityInstance> response) {
                                    if (!response.isSuccess()) {
                                        results.put("error", new Exception(response.getErrorMessage()));
                                        activityInstance = null; // Note - discard activityInstance - prepare for next retry
                                    } else {
                                        results.put("receivedSuccessResponse", true);
                                    }
                                    uploadSignal.countDown();
                                }
                            });

                    uploadSignal.await(60*3, TimeUnit.SECONDS);

                    hasError = results.containsKey("error");
                    if (hasError) throw (Exception) results.get("error");

                    boolean isTimeout = !((boolean) results.get("receivedSuccessResponse"));
                    if (isTimeout) throw new Exception("Timeout receiving response from server");
                }
                else {
                    remove(inspectionSectionImageUploadLog.getId(), InspectionSectionImageUploadLog.class);
                    Log.i("Atlas", "uploadImage file not found (" + inspectionSectionImageUploadLog.getFileName() + " - deleting record ");
                    throw new Exception("Photo file not found. It might have been removed manually from the device. Deleting photo upload instruction.");
                }

                return new GenieServiceResponse(true, null, null);
            }
        });

        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        // TODO - add to activity list of disposable - to be disposed on destroy
                    }

                    @Override
                    public void onSuccess(Object value) {
                        long now = (new Date()).getTime();
                        inspectionSectionImageUploadLog.setLastSuccessfulUploadTime(now);
                        save(inspectionSectionImageUploadLog, InspectionSectionImageUploadLog.class);
                        // Note: review logic - check if there is a need to retain log record
                        remove(inspectionSectionImageUploadLog.getId(), InspectionSectionImageUploadLog.class);
                        callback.onSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onEror(e);
                    }
                });
    }

    @Override
    public void uploadImage(final InspectionImageUploadLog inspectionImageUploadLog,
                            final UploadCallback callback) {
        Single single = Single.fromCallable(new Callable() {
            @Override
            public Object call() throws Exception {

                GetSessionResult result = getSessionToken();
                if (result.getSessionToken() == null || result.getSessionToken().isEmpty()) {
                    throw new Exception("Cannot obtain valid session with server (" + result.getErrorMessage() + ").");
                }
                final String sessionToken = result.getSessionToken();

                final HashMap<String, Object> results = new HashMap<String, Object>();

                if (activityInstance == null) {

                    final CountDownLatch signal = new CountDownLatch(1);
                    genieService.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                        @Override
                        public void completed(GenieServiceResponse<ActivityInstance> response) {
                            if (response.isSuccess()) activityInstance = response.getData();
                            else results.put("error", new Exception(response.getErrorMessage()));
                            signal.countDown();
                        }
                    });

                    signal.await(30, TimeUnit.SECONDS);
                }

                boolean hasError = results.containsKey("error");
                if (hasError) throw (Exception)results.get("error");
                if (activityInstance == null) throw new Exception("Cannot connect to server");

                String localImageFilePath = Util.getLocalPhotoFilePath(context, inspectionImageUploadLog.getFileName());

                boolean isImageStillExist = localImageFilePath != null;
                boolean isForDelete = inspectionImageUploadLog.isForDeletion();

                if (isImageStillExist || isForDelete) {
                    final CountDownLatch uploadSignal = new CountDownLatch(1);

                    results.put("receivedSuccessResponse", false);


                    boolean shouldResizePhotos = sharedPreferencesService.getShouldResizePhotosWhenUpload();
                    boolean shouldGzip = false;

                    int width = 0;
                    int height = 0;
                    String base64 = "";

                    if (!isForDelete) {

                        Uri uri = Uri.fromFile(new File(localImageFilePath)); //MediaUtil.getImageUri(context, new File(localImageFilePath));
                        int rotation = MediaUtil.getImageRotation(context, uri);


                        if (shouldResizePhotos) {
                            int maxWidthOrHeight = sharedPreferencesService.getResizePhotosToMaxWidthOrHeight();
                            MediaUtil.ImageInfo resizedImageInfo = MediaUtil.resizeImage(localImageFilePath, maxWidthOrHeight);
                            localImageFilePath = resizedImageInfo.getAbsolutePath();
                        }

                        MediaUtil.Base64ImageInfo base64ImageInfo = MediaUtil.base64EncodeImageFile(localImageFilePath, rotation, shouldGzip);

                        Log.i("Atlas", "uploadImage base64 length (" + base64ImageInfo.getBase64String().length() + ")");

                        width = base64ImageInfo.getWidth();
                        height = base64ImageInfo.getHeight();
                        base64 = base64ImageInfo.getBase64String();
                    }

                    genieService.uploadImage(
                            inspectionImageUploadLog,
                            width,
                            height,
                            base64,
                            shouldGzip,
                            sessionToken,
                            activityInstance, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                                @Override
                                public void completed(GenieServiceResponse<ActivityInstance> response) {
                                    if (!response.isSuccess()) {
                                        results.put("error", new Exception(response.getErrorMessage()));
                                        activityInstance = null; // Note - discard activityInstance - prepare for next retry
                                    } else {
                                        results.put("receivedSuccessResponse", true);
                                    }
                                    uploadSignal.countDown();
                                }
                            });

                    uploadSignal.await(60*3, TimeUnit.SECONDS);

                    hasError = results.containsKey("error");
                    if (hasError) throw (Exception) results.get("error");

                    boolean isTimeout = !((boolean) results.get("receivedSuccessResponse"));
                    if (isTimeout) throw new Exception("Timeout receiving response from server");
                }
                else {
                    remove(inspectionImageUploadLog.getId(), InspectionImageUploadLog.class);
                    Log.i("Atlas", "uploadImage file not found (" + inspectionImageUploadLog.getFileName() + " - deleting record ");
                    throw new Exception("Photo file not found. It might have been removed manually from the device. Deleting photo upload instruction.");
                }

                return new GenieServiceResponse(true, null, null);
            }
        });

        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        // TODO - add to activity list of disposable - to be disposed on destroy
                    }

                    @Override
                    public void onSuccess(Object value) {
                        long now = (new Date()).getTime();
                        inspectionImageUploadLog.setLastSuccessfulUploadTime(now);
                        save(inspectionImageUploadLog, InspectionImageUploadLog.class);
                        // Note: review logic - check if there is a need to retain log record
                        remove(inspectionImageUploadLog.getId(), InspectionImageUploadLog.class);
                        callback.onSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onEror(e);
                    }
                });
    }

    //endregion

    //region Scan and clean obsolete drafts

    @Override
    public Single<List<Draft>> scanAndCleanObsoleteDrafts(String username) {
        return getRecentInspectionsForDraftDeletion()
                .flatMap(recentInspectionsForDraftDeletion -> getRemainingDraftsAfterProcessingObsoleteDrafts(recentInspectionsForDraftDeletion, username))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //endregion

    @Override
    public void saveDraft(Draft draft) {
        Document document = Util.toXmlDocument(draft);
        String xml = Util.xmlNodeToString(document);
        draft.setXml(xml);
        java.util.Date date = new Date();
        draft.setModifiedTime(date.getTime());
        draft.setNoOfSections(draft.getSections().size());
        int noOfCompletedSections = 0;
        for (int i = 0; i < draft.getSections().size(); i++) {
            if (!draft.getSections().get(i).isComplete()) continue;
            noOfCompletedSections++;
        }
        draft.setNoOfCompletedSections(noOfCompletedSections);
        save(draft, Draft.class);
    }

    //region Download data from server
    @Override
    public void getJobs(Context context, final DataService.GetDataCallback<Job> callback) throws Exception {

        String sessionToken;
        ActivityInstance localActivityInstance;
        try {
            sessionToken = getSessionTokenAsString();
            localActivityInstance = getActivityInstance(sessionToken);

        } catch (Exception e) {
            callback.onError(e.getMessage());
            return;
        }

        genieService.getJobs(sessionToken, localActivityInstance, new GenieServiceCallback<GenieServiceResponse<List<Job>>>() {
            @Override
            public void completed(GenieServiceResponse<List<Job>> response) {
                if (!response.isSuccess()) {
                    callback.onError(response.getErrorMessage());
                    activityInstance = null; // Note - discard activityInstance - prepare for next retry
                }
                else {
                    callback.onCompletion(response.getData());
                }
            }
        });
    }


    @Override
    public void getInspections(Context context, final DataService.GetDataCallback<Inspection> callback) throws Exception {

        String sessionToken;
        ActivityInstance localActivityInstance;
        try {
            sessionToken = getSessionTokenAsString();
            localActivityInstance = getActivityInstance(sessionToken);

        } catch (Exception e) {
            callback.onError(e.getMessage());
            return;
        }

        genieService.getInspections(sessionToken, localActivityInstance, new GenieServiceCallback<GenieServiceResponse<List<Inspection>>>() {
            @Override
            public void completed(GenieServiceResponse<List<Inspection>> response) {
                if (!response.isSuccess()) {
                    callback.onError(response.getErrorMessage());
                    activityInstance = null; // Note - discard activityInstance - prepare for next retry
                }
                else {
                    callback.onCompletion(response.getData());
                }
            }
        });
    }

    @Override
    public void getRecentInspections(Context context, final DataService.GetDataCallback<RecentInspection> callback) throws Exception {

        String sessionToken;
        ActivityInstance localActivityInstance;
        try {
            sessionToken = getSessionTokenAsString();
            localActivityInstance = getActivityInstance(sessionToken);

        } catch (Exception e) {
            callback.onError(e.getMessage());
            return;
        }

        genieService.getRecentInspections(sessionToken, localActivityInstance, new GenieServiceCallback<GenieServiceResponse<List<RecentInspection>>>() {
            @Override
            public void completed(GenieServiceResponse<List<RecentInspection>> response) {
                if (!response.isSuccess()) {
                    callback.onError(response.getErrorMessage());
                    activityInstance = null; // Note - discard activityInstance - prepare for next retry
                }
                else {
                    callback.onCompletion(response.getData());
                }
            }
        });
    }

    @Override
    public void getRecentInspectionsForDraftDeletion(Context context, final GetDataCallback<RecentInspection> callback) throws Exception {
        GetSessionResult result = getSessionToken();
        if (result.getSessionToken() == null || result.getSessionToken().isEmpty()) {
            callback.onError("Cannot obtain valid session with server (" + result.getErrorMessage() + ").");
            return;
        }
        String sessionToken = result.getSessionToken();

        final HashMap<String, Object> results = new HashMap<>();
        results.put("hasReceivedResponse", false);

        if (activityInstanceForGetRecentInspectionsForDraftDeletion.get() == null) {

            final CountDownLatch signal = new CountDownLatch(1);
            genieService.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                @Override
                public void completed(GenieServiceResponse<ActivityInstance> response) {
                    results.put("hasReceivedResponse", true);
                    if (response.isSuccess()) results.put("activityInstance", response.getData()); // = response.getData();
                    else results.put("error", response.getErrorMessage()); //callback.onError(response.getErrorMessage());
                    signal.countDown();
                }
            }, genieService.recentInspectionForDraftDeletionDataPublication());

            signal.await(30, TimeUnit.SECONDS);

            boolean hasReceivedResponse = (boolean)results.get("hasReceivedResponse");
            if (!hasReceivedResponse) {
                callback.onError("Timeout receiving response from server");
                return;
            }
            boolean hasError = results.containsKey("error");
            if (hasError) {
                callback.onError((String)results.get("error"));
                return;
            }
            activityInstanceForGetRecentInspectionsForDraftDeletion.set((ActivityInstance)results.get("activityInstance"));

            if (activityInstanceForGetRecentInspectionsForDraftDeletion.get() == null) throw new Exception("Cannot create activity instance");
        }

        genieService.getRecentInspectionsForDraftDeletion(sessionToken, activityInstanceForGetRecentInspectionsForDraftDeletion.get(), new GenieServiceCallback<GenieServiceResponse<List<RecentInspection>>>() {
            @Override
            public void completed(GenieServiceResponse<List<RecentInspection>> response) {

                if (!response.isSuccess()) {
                    callback.onError(response.getErrorMessage());
                    activityInstanceForGetRecentInspectionsForDraftDeletion.set(null); // Note - discard activityInstance - prepare for next retry
                }
                else {
                    callback.onCompletion(response.getData());
                }
            }
        });
    }

    @Override
    public Single<List<RecentInspection>> getRecentInspectionsForDraftDeletion() {
        return Single.create(subscriber -> {

            GetSessionResult result = getSessionToken();
            if (result.getSessionToken() == null || result.getSessionToken().isEmpty()) {
                subscriber.onError(new Exception("Cannot obtain valid session with server (" + result.getErrorMessage() + ")."));
                return;
            }
            String sessionToken = result.getSessionToken();

            final HashMap<String, Object> results = new HashMap<>();
            results.put("hasReceivedResponse", false);

            if (activityInstanceForGetRecentInspectionsForDraftDeletion.get() == null) {

                final CountDownLatch signal = new CountDownLatch(1);
                genieService.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                    @Override
                    public void completed(GenieServiceResponse<ActivityInstance> response) {
                        results.put("hasReceivedResponse", true);
                        if (response.isSuccess()) results.put("activityInstance", response.getData()); // = response.getData();
                        else results.put("error", response.getErrorMessage()); //callback.onError(response.getErrorMessage());
                        signal.countDown();
                    }
                }, genieService.recentInspectionForDraftDeletionDataPublication());

                signal.await(30, TimeUnit.SECONDS);

                boolean hasReceivedResponse = (boolean)results.get("hasReceivedResponse");
                if (!hasReceivedResponse) {
                    subscriber.onError(new Exception("Timeout receiving response from server"));
                    return;
                }
                boolean hasError = results.containsKey("error");
                if (hasError) {
                    subscriber.onError(new Exception((String)results.get("error")));
                    return;
                }
                activityInstanceForGetRecentInspectionsForDraftDeletion.set((ActivityInstance)results.get("activityInstance"));

                if (activityInstanceForGetRecentInspectionsForDraftDeletion.get() == null) subscriber.onError(new Exception("Cannot create activity instance"));
            }

            genieService.getRecentInspectionsForDraftDeletion(sessionToken, activityInstanceForGetRecentInspectionsForDraftDeletion.get(), new GenieServiceCallback<GenieServiceResponse<List<RecentInspection>>>() {
                @Override
                public void completed(GenieServiceResponse<List<RecentInspection>> response) {

                    if (!response.isSuccess()) {
                        subscriber.onError(new Exception(response.getErrorMessage()));
                        activityInstanceForGetRecentInspectionsForDraftDeletion.set(null); // Note - discard activityInstance - prepare for next retry
                    }
                    else {
                        subscriber.onSuccess(response.getData());
                    }
                }
            });
        });
    }

    @Override
    public Single<List<CalendarItem>> getUpcomingCalendarItems() {
        return Single.create(subscriber -> {
            String sessionToken;
            ActivityInstance localActivityInstance;
            try {
                sessionToken = getSessionTokenAsString();
                localActivityInstance = getActivityInstance(sessionToken);

            } catch (Exception e) {
                subscriber.onError(e);
                return;
            }

            genieService.getUpcomingCalendarItems(sessionToken, localActivityInstance)
                   .subscribe(response -> {
                      if (!response.isSuccess()) {
                          subscriber.onError(new Exception(response.getErrorMessage()));
                          activityInstance = null;  // Note - discard activityInstance - prepare for next retry
                      }
                      else {
                          subscriber.onSuccess(response.getData());
                      }
                   }, error -> {
                        subscriber.onError(error);
                   });
        });
    }

    //endregion

    //region Get Data from app offline data storage ORMLite
    @Override
    public <T extends IdBase> List<T> getList(GetCallback<T> callback, Class<T> classType) {
        List<T> data = null;

        try {
            Dao<T, String> dao = getDatabaseHelper().getDao(classType);
            QueryBuilder<T,?> queryBuilder = dao.queryBuilder();
            if (callback != null) queryBuilder = callback.preQuery(queryBuilder);
            data = queryBuilder.query();
            if (data == null) data = new ArrayList<>();
        } catch (SQLException e) {
            Log.e(DataServiceImpl.class.getName(), e.getLocalizedMessage());
        }

        return data;
    }

    @Override
    public <T extends IdBase> T get(String id, Class<T> classType) {
        T data = null;

        try {
            Dao<T, String> dao = getDatabaseHelper().getDao(classType);
            data = dao.queryForId(id);
        } catch (SQLException e) {
            Log.e(DataServiceImpl.class.getName(), e.getLocalizedMessage());
        }

        return data;
    }

    @Override
    public <T extends IdBase> void removeAll(Class<T> classType) {
        try {
            Dao<T, String> dao = getDatabaseHelper().getDao(classType);
            dao.deleteBuilder().delete();
        } catch (SQLException e) {
            Log.e(DataServiceImpl.class.getName(), e.getLocalizedMessage());
        }
    }

    @Override
    public <T extends IdBase> void remove(String id, Class<T> classType) {
        try {
            Dao<T, String> dao = getDatabaseHelper().getDao(classType);
            dao.deleteById(id);
        } catch (SQLException e) {
            Log.e(DataServiceImpl.class.getName(), e.getLocalizedMessage());
        }
    }

    @Override
    public <T extends IdBase> void remove(List<T> data, Class<T> classType) {
        try {
            Dao<T, String> dao = getDatabaseHelper().getDao(classType);
            dao.delete(data);
        } catch (SQLException e) {
            Log.e(DataServiceImpl.class.getName(), e.getLocalizedMessage());
        }
    }

    @Override
    public <T extends IdBase> void save(List<T> data, Class<T> classType) {
        try {
            Dao<T, String> dao = getDatabaseHelper().getDao(classType);
            dao.create(data);
        } catch (SQLException e) {
            Log.e(DataServiceImpl.class.getName(), e.getLocalizedMessage());
        }

    }

    @Override
    public <T extends IdBase> void save(T data, Class<T> classType) {
        try {
            Dao<T, String> dao = getDatabaseHelper().getDao(classType);
            dao.createOrUpdate(data);
        } catch (SQLException e) {
            Log.e(DataServiceImpl.class.getName(), e.getLocalizedMessage());
        }
    }

    @Override
    public Draft inspectionToDraft(Inspection inspection) throws Exception {
        Draft draft = new Draft();
        draft.setXml(inspection.getXml());
        Util.parseInspectionXml(draft.getXml(), draft);
        draft.setNoOfSections(draft.getSections().size());
        int noOfCompletedSections = 0;
        for (int i = 0; i < draft.getSections().size(); i++) {
            if (!draft.getSections().get(i).isComplete()) continue;
            noOfCompletedSections++;
        }
        draft.setNoOfCompletedSections(noOfCompletedSections);
        draft.setCodeForListDisplay(draft.getCode());
        String clientName = "";
        if (draft.getClient() != null) clientName = draft.getClient().getName();
        draft.setClientNameForListDisplay(clientName);

        // default to Atlas Draft on Draft creation
        draft.setAsAtlasDraft();

        draft.setCreatedByUsername(sharedPreferencesService.getUsername());
        return draft;
    }

    public RecentInspection draftToRecentInspection(Draft draft) throws Exception {
        if (!draft.isXmlParsed()) {
            Util.parseInspectionXml(draft.getXml(), draft);
        }
        RecentInspection recentInspection = new RecentInspection();
        recentInspection.setId(draft.getId());
        recentInspection.setCode(draft.getCodeForListDisplay());
        if (draft.getClient() != null) {
            recentInspection.setMobile(draft.getClient().getMobilePhone());
            recentInspection.setClientName(draft.getClient().getName());
        }
        recentInspection.setDate(draft.getDate());
        recentInspection.setReportType(draft.getReportType());
        recentInspection.setAddress(draft.getAddress());
        recentInspection.setSuburb("");
        recentInspection.setPendingUploadToServer(true);
        return recentInspection;
    }

    @Override
    public InspectionSectionImageUploadLog getInspectionSectionImageUploadLog(InspectionSectionImage inspectionSectionImage, InspectionSection inspectionSection) {
        return getInspectionSectionImageUploadLog(inspectionSectionImage, inspectionSection, false);
    }

    @Override
    public InspectionSectionImageUploadLog getInspectionSectionImageUploadLog(InspectionSectionImage inspectionSectionImage, InspectionSection inspectionSection, boolean isForDeletion) {
        InspectionSectionImageUploadLog inspectionSectionImageUploadLog = get(inspectionSectionImage.getFileName(), InspectionSectionImageUploadLog.class);
        if (inspectionSectionImageUploadLog == null) {
            inspectionSectionImageUploadLog = new InspectionSectionImageUploadLog();
            inspectionSectionImageUploadLog.setFileName(inspectionSectionImage.getFileName());
            inspectionSectionImageUploadLog.setSectiondId(inspectionSection.getId());
            inspectionSectionImageUploadLog.setSectionKey(inspectionSection.getKey());
            inspectionSectionImageUploadLog.setInspectionId(inspectionSection.getParentInspectionId());
            inspectionSectionImageUploadLog.setLastSuccessfulUploadTime(0);
            inspectionSectionImageUploadLog.setForDeletion(isForDeletion);
            inspectionSectionImageUploadLog.setRepeatingKey(inspectionSection.getRepeatingKey());
            inspectionSectionImageUploadLog.setRepeatingSeq(inspectionSection.getRepeatingSeq());
            save(inspectionSectionImageUploadLog, InspectionSectionImageUploadLog.class);
        }
        return inspectionSectionImageUploadLog;
    }

    @Override
    public InspectionImageUploadLog getInspectionImageUploadLog(InspectionImage inspectionImage, Inspection inspection) {
        return getInspectionImageUploadLog(inspectionImage, inspection, false);
    }

    @Override
    public InspectionImageUploadLog getInspectionImageUploadLog(InspectionImage inspectionImage, Inspection inspection, boolean isForDeletion) {
        InspectionImageUploadLog inspectionImageUploadLog = get(inspectionImage.getFileName(), InspectionImageUploadLog.class);
        if (inspectionImageUploadLog == null) {
            inspectionImageUploadLog = new InspectionImageUploadLog();
            inspectionImageUploadLog.setId(inspectionImage.getFileName());
            inspectionImageUploadLog.setFileName(inspectionImage.getFileName());
            inspectionImageUploadLog.setInspectionId(inspection.getId());
            inspectionImageUploadLog.setLastSuccessfulUploadTime(0);
            inspectionImageUploadLog.setForDeletion(isForDeletion);
            save(inspectionImageUploadLog, InspectionImageUploadLog.class);
        }
        return inspectionImageUploadLog;
    }

    //endregion

    //region Process data offline

    public Single<List<Draft>> getDraftsCreatedBy(final String username) {

        return Single.create(subscriber -> {
            List<Draft> drafts = getList(new DataService.GetCallback<Draft>() {
                @Override
                public QueryBuilder<Draft, ?> preQuery(QueryBuilder<Draft, ?> queryBuilder) {
                    try {
                        Where where = queryBuilder.where();
                        where.and(where.ne("mode", Draft.MODE_ZEUS_FINAL_REPORT),
                                where.eq("createdByUsername", username).or().eq("createdByUsername", ""));
                    } catch (Exception e) {
                        Log.e("Atlas - Draft list", e.getLocalizedMessage());
                    }
                    return queryBuilder.orderBy("modifiedTime", false);
                }
            }, Draft.class);
            subscriber.onSuccess(drafts);
        });
    }
    public Single<List<Draft>> getRemainingDraftsAfterProcessingObsoleteDrafts(final List<RecentInspection> recentInspectionsForDraftDeletion, String username) {
        return Single.create(subscriber -> {

            for (RecentInspection recentInspection : recentInspectionsForDraftDeletion) {

                Draft draftToDelete = get(recentInspection.getId(), Draft.class);
                if (draftToDelete == null) continue;
                remove(draftToDelete.getId(), Draft.class);
            }

            subscriber.onSuccess(true);

        }).flatMap(success -> getDraftsCreatedBy(username));
    }
    //endregion


    public class GetSessionResultImpl implements GetSessionResult {

        private String sessionToken;
        private String errorMessage;

        public void setSessionToken(String sessionToken) {
            this.sessionToken = sessionToken;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        @Override
        public String getSessionToken() {
            return this.sessionToken;
        }

        @Override
        public String getErrorMessage() {
            return this.errorMessage;
        }
    }

    // Helper function. Attempts to get sessionToken from sharedPreferencesService. If such sessionToken is null, a login attempts is performed.
    // This is a thread blocking method.
    public GetSessionResult getSessionToken() throws Exception {
        String sessionToken = sharedPreferencesService.getSessionToken();

        if (sessionToken != null) {
            final CountDownLatch pingSignal = new CountDownLatch(1);
            genieService.ping(sessionToken,
                    new GenieServiceCallback<GenieServiceResponse<String>>() {
                        @Override
                        public void completed(GenieServiceResponse<String> response) {

                            if (!response.isSuccess()) {
                                sharedPreferencesService.saveSessionToken(null);
                            }

                            pingSignal.countDown();
                        }
                    });
            pingSignal.await(30, TimeUnit.SECONDS);

            sessionToken = sharedPreferencesService.getSessionToken();
        }

        final GetSessionResultImpl result = new GetSessionResultImpl();
        String defaultErrorMessage = "Timeout";
        result.setErrorMessage(defaultErrorMessage);
        if (sessionToken == null || sessionToken.isEmpty()) {
            final CountDownLatch loginSignal = new CountDownLatch(1);
            login(context,
                    new LoginCallback() {
                        @Override
                        public void onSuccess() {
                            result.setErrorMessage(null); // login successful, hence reset error message
                            loginSignal.countDown();
                        }

                        @Override
                        public void onError(Throwable error) {
                            result.setErrorMessage(error.getMessage());
                            loginSignal.countDown();
                        }
                    });

            loginSignal.await(30, TimeUnit.SECONDS);

            sessionToken = sharedPreferencesService.getSessionToken();
        }

        result.setSessionToken(sessionToken);
        return result;
    }

    private static interface LoginCallback {
        void onSuccess();
        void onError(Throwable error);
    }

    private void login(final Context context, final LoginCallback callback) {

        Single single = Single.fromCallable(new Callable() {
            @Override
            public Object call() throws Exception {
                String username = sharedPreferencesService.getUsername();
                String password = sharedPreferencesService.getPassword();

                if (username != null) username = username.trim();
                if (password != null) password = password.trim();

                return new Pair(username, password);
            }
        });

        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Pair<String,String>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        // TODO:: add d to list to be disposed
                    }

                    @Override
                    public void onSuccess(Pair<String, String> value) {

                        final String username = value.first;
                        final String password = value.second;
                        GenieService genieService = new GenieServiceImpl();

                        genieService.login(username, password, new GenieServiceCallback<GenieServiceResponse<String>>() {

                            @Override
                            public void completed(final GenieServiceResponse<String> response) {
                                if (response.isSuccess()) {

                                    final String sessionToken = response.getData();

                                    Single single1 = Single.fromCallable(new Callable() {
                                        @Override
                                        public Object call() throws Exception {

                                            sharedPreferencesService.saveSessionToken(sessionToken);
                                            return true;
                                        }
                                    });

                                    single1.subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new SingleObserver() {
                                                @Override
                                                public void onSubscribe(Disposable d) {
                                                    // TODO : add d to list for disposal
                                                }

                                                @Override
                                                public void onSuccess(Object value) {
                                                    callback.onSuccess();
                                                }

                                                @Override
                                                public void onError(Throwable e) {
                                                    callback.onError(e);
                                                }
                                            });

                                }
                                else {
                                    Single single1 = Single.fromCallable(new Callable() {
                                        @Override
                                        public Object call() throws Exception {

                                            sharedPreferencesService.saveSessionToken(null);
                                            return true;
                                        }
                                    });

                                    single1.subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new SingleObserver() {
                                                @Override
                                                public void onSubscribe(Disposable d) {
                                                    // TODO : add d to list for disposal
                                                }

                                                @Override
                                                public void onSuccess(Object value) {
                                                    callback.onError(new Exception(response.getErrorMessage()));
                                                }

                                                @Override
                                                public void onError(Throwable e) {
                                                    callback.onError(e);
                                                }
                                            });
                                }
                            }
                        });

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e);
                    }
                });

    }


    /**
     * Get or create a sessionToken as string.
     * Note: this method is thread blocking.
     * @return
     * @throws Exception
     */
    private String getSessionTokenAsString() throws Exception {
        GetSessionResult result = getSessionToken();
        if (result.getSessionToken() == null || result.getSessionToken().isEmpty()) {
            throw new Exception("Cannot obtain valid session with server (" + result.getErrorMessage() + ").");
        }
        String sessionToken = result.getSessionToken();
        return sessionToken;
    }

    /**
     * Get activityInstance. If it is null, this function attempts to create a new instance with the server.
     * Note: this method is thread blocking.
     * @param sessionToken sessionToken needed to create an activity instance with the server
     * @return
     * @throws Exception
     */
    private ActivityInstance getActivityInstance(String sessionToken) throws Exception {

        final HashMap<String, Object> results = new HashMap<>();
        results.put("hasReceivedResponse", false);
        if (activityInstance == null) {

            final CountDownLatch signal = new CountDownLatch(1);
            genieService.createActivity(sessionToken, new GenieServiceCallback<GenieServiceResponse<ActivityInstance>>() {
                @Override
                public void completed(GenieServiceResponse<ActivityInstance> response) {
                    results.put("hasReceivedResponse", true);
                    if (response.isSuccess()) results.put("activityInstance", response.getData()); // = response.getData();
                    else results.put("error", response.getErrorMessage()); //callback.onError(response.getErrorMessage());
                    signal.countDown();
                }
            });

            signal.await(30, TimeUnit.SECONDS);

            boolean hasReceivedResponse = (boolean)results.get("hasReceivedResponse");
            if (!hasReceivedResponse) {
                throw new Exception("Timeout receiving response from server");
            }
            boolean hasError = results.containsKey("error");
            if (hasError) {
                throw new Exception((String)results.get("error"));
            }
            activityInstance = (ActivityInstance)results.get("activityInstance");

            if (activityInstance == null) throw new Exception("Cannot create activity instance");
        }

        return activityInstance;
    }
}
