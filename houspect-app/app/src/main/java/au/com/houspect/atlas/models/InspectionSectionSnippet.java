package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alfredwong on 30/1/17.
 */

public class InspectionSectionSnippet extends IdBase implements Parcelable, Cloneable {
    private String value;
    private boolean isComment;
    private boolean isRecommendation;

    public InspectionSectionSnippet() {}

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isComment() {
        return isComment;
    }

    public void setComment(boolean comment) {
        isComment = comment;
    }

    public boolean isRecommendation() {
        return isRecommendation;
    }

    public void setRecommendation(boolean recommendation) {
        isRecommendation = recommendation;
    }

    //region Parcelable
    private InspectionSectionSnippet(Parcel in) {
        setId(in.readString());
        value = in.readString();
        isComment = in.readByte() != 0;
        isRecommendation = in.readByte() != 0;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(value);
        dest.writeByte((byte)(isComment ? 1 : 0));
        dest.writeByte((byte)(isRecommendation ? 1 : 0));
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public InspectionSectionSnippet createFromParcel(Parcel source) {
            return new InspectionSectionSnippet(source);
        }

        @Override
        public InspectionSectionSnippet[] newArray(int size) {
            return new InspectionSectionSnippet[size];
        }
    };
    //endregion

    //region Cloneable
    protected Object clone() throws CloneNotSupportedException {
        InspectionSectionSnippet snippet = new InspectionSectionSnippet();
        snippet.setId(getId());
        snippet.setValue(getValue());
        snippet.setComment(isComment());
        snippet.setRecommendation(isRecommendation());
        return  snippet;
    }
    //endregion
}
