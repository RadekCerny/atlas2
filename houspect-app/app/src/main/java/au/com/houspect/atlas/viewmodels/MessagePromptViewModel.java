package au.com.houspect.atlas.viewmodels;

import android.view.View;

/**
 * Created by alfredwong on 05/3/17.
 */

public class MessagePromptViewModel {

    private String title;
    private String content;
    private Runnable onOk;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MessagePromptViewModel(String title, String content, Runnable onOk) {
        this.title = title;
        this.content = content;
        this.onOk = onOk;
    }

    public void onOkClick(View button) {
        onOk.run();
    }

}
