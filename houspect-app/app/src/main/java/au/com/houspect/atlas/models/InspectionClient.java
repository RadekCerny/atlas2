package au.com.houspect.atlas.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alfredwong on 27/3/17.
 */

public class InspectionClient implements Parcelable {

    private String name;
    private String address;
    private String emailAddress;
    private String mobilePhone;
    private String phone;
    private InspectionClientRole role;

    public InspectionClient() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public InspectionClientRole getRole() {
        return role;
    }

    public void setRole(InspectionClientRole role) {
        this.role = role;
    }

    //region Parcelable
    protected InspectionClient(Parcel in) {
        name = in.readString();
        address = in.readString();
        emailAddress = in.readString();
        mobilePhone = in.readString();
        phone = in.readString();
        role = in.readParcelable(InspectionClientRole.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(emailAddress);
        dest.writeString(mobilePhone);
        dest.writeString(phone);
        dest.writeParcelable(role, flags);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public InspectionClient createFromParcel(Parcel source) {
            return new InspectionClient(source);
        }

        @Override
        public InspectionClient[] newArray(int size) {
            return new InspectionClient[size];
        }
    };
    //endregion

}
