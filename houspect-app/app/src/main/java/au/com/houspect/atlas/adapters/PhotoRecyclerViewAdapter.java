package au.com.houspect.atlas.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.fragments.dummy.DummyContent.DummyItem;
import au.com.houspect.atlas.utilities.MediaUtil;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link PhotoRecyclerViewAdapter.OnPhotoInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class PhotoRecyclerViewAdapter extends RecyclerView.Adapter<PhotoRecyclerViewAdapter.ViewHolder> {

    public interface OnPhotoInteractionListener {
        void onPhotoDelete(Value value, int position);
        void onPhotoSelected(Value value, int position);
    }

    public static class Value<T> {
        public T dataObject;
        public String photoPath;
    }

    private final List<Value> mValues;
    private final OnPhotoInteractionListener mListener;
    private int mItemResourceId;
    private Context mContext;

    public PhotoRecyclerViewAdapter(List<Value> items,
                                    OnPhotoInteractionListener listener,
                                    int itemResourceId,
                                    Context context) {
        mValues = items;
        mListener = listener;
        mItemResourceId = itemResourceId;
        mContext = context;
    }

    public List<Value> getValues() {
        return mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int resourceId = mItemResourceId;
        View view = LayoutInflater.from(parent.getContext())
                .inflate(resourceId, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Value value = mValues.get(position);
        final ItemViewHolder itemViewHolder = (ItemViewHolder)holder;
        itemViewHolder.mItem = value.photoPath;

        Picasso.with(mContext).load(new File(value.photoPath)).resize(120,120).centerInside().into(itemViewHolder.mImageView);

        // TODO:: Test only - remove me
        //MediaUtil.Base64ImageInfo base64ImageInfo = MediaUtil.base64EncodeImageFile(value.photoPath, false);
        //Uri uri = MediaUtil.getImageUri(mContext, new File(value.photoPath));
        //int rotation = MediaUtil.getImageRotation(mContext,uri);

        final WeakReference<ViewHolder> weakViewHolder = new WeakReference<ViewHolder>(itemViewHolder);
        itemViewHolder.mImageButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPhotoDelete(value, weakViewHolder.get().getAdapterPosition());
            }
        });


        itemViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onPhotoSelected(value, weakViewHolder.get().getAdapterPosition());
                }
            }
        });
        /*
    // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        // Set inSampleSize
        options.inSampleSize = 4;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

        itemViewHolder.mImageView.setImageBitmap(bitmap);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onPhotoInteraction(itemViewHolder.mItem);
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
        }
    }


    class ItemViewHolder extends ViewHolder {

        public final ImageView mImageView;
        public ImageButton mImageButtonDelete;
        public String mItem;

        public ItemViewHolder(View view) {
            super(view);

            mImageView = (ImageView) view.findViewById(R.id.imageViewThumbnail);
            mImageButtonDelete = (ImageButton) view.findViewById(R.id.imageButtonDelete);
        }
    }

}
