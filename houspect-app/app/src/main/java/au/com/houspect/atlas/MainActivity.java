package au.com.houspect.atlas;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evernote.android.job.JobManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import au.com.houspect.atlas.constants.YesNoConfirmationFragmentRequestCode;
import au.com.houspect.atlas.fragments.CameraPreviewFragment;
import au.com.houspect.atlas.fragments.CheckboxConfirmationFragment;
import au.com.houspect.atlas.fragments.CommentFragment;
import au.com.houspect.atlas.fragments.DraftListFragment;
import au.com.houspect.atlas.fragments.ForgotPasswordFragment;
import au.com.houspect.atlas.fragments.InspectionEndFragment;
import au.com.houspect.atlas.fragments.InspectionFragment;
import au.com.houspect.atlas.fragments.InspectionRecommendationSummaryFragment;
import au.com.houspect.atlas.fragments.InspectionSectionFragment;
import au.com.houspect.atlas.fragments.InspectionStartFragment;
import au.com.houspect.atlas.fragments.JobListFragment;
import au.com.houspect.atlas.fragments.LoginFragment;
import au.com.houspect.atlas.fragments.MessagePromptFragment;
import au.com.houspect.atlas.fragments.PhotoEditorFragment;
import au.com.houspect.atlas.fragments.PhotoGalleryFragment;
import au.com.houspect.atlas.fragments.RecentInspectionListFragment;
import au.com.houspect.atlas.fragments.SettingsFragment;
import au.com.houspect.atlas.fragments.SideMenuFragment;
import au.com.houspect.atlas.fragments.SideMenuInspectionTOCFragment;
import au.com.houspect.atlas.fragments.YesNoConfirmationFragment;
import au.com.houspect.atlas.models.CalendarItem;
import au.com.houspect.atlas.models.DatabaseHelper;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionClient;
import au.com.houspect.atlas.models.InspectionImage;
import au.com.houspect.atlas.models.InspectionImageUploadLog;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.models.InspectionSectionImageUploadLog;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.models.RecentInspection;
import au.com.houspect.atlas.models.SideMenuItem;
import au.com.houspect.atlas.services.DataIntentServiceReceiver;
import au.com.houspect.atlas.services.DataService;
import au.com.houspect.atlas.services.DataServiceImpl;
import au.com.houspect.atlas.services.DataSyncJob;
import au.com.houspect.atlas.services.DataSyncJobCreator;
import au.com.houspect.atlas.services.DisposableManager;
import au.com.houspect.atlas.services.GenieService;
import au.com.houspect.atlas.services.GenieServiceCallback;
import au.com.houspect.atlas.services.GenieServiceImpl;
import au.com.houspect.atlas.services.GenieServiceResponse;
import au.com.houspect.atlas.services.SharedPreferencesService;
import au.com.houspect.atlas.services.SharedPreferencesServiceImpl;
import au.com.houspect.atlas.services.SynchronisedService;
import au.com.houspect.atlas.utilities.MediaUtil;
import au.com.houspect.atlas.utilities.SessionUtil;
import au.com.houspect.atlas.utilities.Util;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
                   SideMenuFragment.OnListFragmentInteractionListener,
                    SideMenuInspectionTOCFragment.OnListFragmentInteractionListener,
                    LoginFragment.OnFragmentInteractionListener,
                    JobListFragment.OnListFragmentInteractionListener,
                    RecentInspectionListFragment.OnListFragmentInteractionListener,
                    InspectionFragment.OnFragmentInteractionListener,
                    InspectionStartFragment.OnFragmentInteractionListener,
                    InspectionRecommendationSummaryFragment.OnListFragmentInteractionListener,
                    InspectionEndFragment.OnFragmentInteractionListener,
                    InspectionSectionFragment.OnFragmentInteractionListener,
                    CameraPreviewFragment.OnFragmentInteractionListener,
                    PhotoEditorFragment.OnFragmentInteractionListener,
                    PhotoGalleryFragment.OnFragmentInteractionListener,
                    CommentFragment.OnFragmentInteractionListener,
                    DraftListFragment.OnListFragmentInteractionListener,
                    CheckboxConfirmationFragment.OnFragmentInteractionListener,
                    YesNoConfirmationFragment.OnFragmentInteractionListener,
                    MessagePromptFragment.OnFragmentInteractionListener,
                    ForgotPasswordFragment.OnFragmentInteractionListener,
                    DataIntentServiceReceiver.Listener,
                    SettingsFragment.OnFragmentInteractionListener {

    private DrawerLayout mDrawerLayout;
    private ImageView mImageViewLogo;
    private View mViewFragmentBackgroundOverlay;
    private SideMenuFragment mFragmentSideMenu;
    private NavigationView mNavigationView;
    private SessionUtil mSessionUtil;
    private Menu mMenu;
    private ProgressDialog mProgressDialog;
    private ViewGroup mViewGroupActivityOverlay;
    private TextView mTextViewActivityOverlayMessage;
    private DataIntentServiceReceiver mDataIntentServiceReceiver;

    //region ScreenOption internal types for screen navigation
    private static class ScreenOption {
        private int value;
        public ScreenOption(int value) {
            this.value = value;
        }
        public static ScreenOption Jobs = new ScreenOption(0);
        public static ScreenOption Drafts = new ScreenOption(1);
        public static ScreenOption RecentInspections = new ScreenOption(2);
    }
    //endregion

    // This flag is to used to track the activity onSavedInstance status - determines if fragment transaction commit can be called
    private boolean mAllowCommit;

    //region Genie
    private DataService mDataService;
    private GenieService mGenieService;
    private SharedPreferencesService mSharedPreferencesService;
    //endregion

    //region OrmLite
    private  DatabaseHelper mDatabaseHelper = null;

    private DatabaseHelper getDatabaseHelper() {
        if (mDatabaseHelper == null) mDatabaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        return mDatabaseHelper;
    }
    //endregion


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAllowCommit = true;
        setContentView(R.layout.activity_main);

        // TODO:: dependency inject
        mGenieService = new GenieServiceImpl();
        mSharedPreferencesService = new SharedPreferencesServiceImpl(getApplicationContext());
        mDataService = new DataServiceImpl(this, mGenieService, mSharedPreferencesService);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //fab.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View view) {
        //        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
        //                .setAction("Action", null).show();
        //    }
        //});

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout = drawer;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nagivationView);
        navigationView.setNavigationItemSelectedListener(this);


        mNavigationView = navigationView;
        mSessionUtil = SessionUtil.singleton;

        mImageViewLogo = (ImageView) findViewById(R.id.imageViewLogo);
        mViewFragmentBackgroundOverlay = findViewById(R.id.frameLayoutContentBackgroundOverlay);

        LoginFragment loginFragment = new LoginFragment();
        showFragment(loginFragment, "", false);

        mFragmentSideMenu = SideMenuFragment.newInstance(1);
        mFragmentSideMenu.mSessionUtil = mSessionUtil;

        refreshSideMenu(null);

        checkForUpdates();

        mViewGroupActivityOverlay = (ViewGroup)findViewById(R.id.viewGroupActivityOverlay);
        mTextViewActivityOverlayMessage = (TextView)findViewById(R.id.textViewActivityOverlayMessage);

        mDataIntentServiceReceiver = new DataIntentServiceReceiver(this);

        IntentFilter statusIntentFilter = new IntentFilter(
                DataSyncJob.BROADCAST_ACTION);


        // Registers the DownloadStateReceiver and its intent filters
        LocalBroadcastManager.getInstance(this).registerReceiver(mDataIntentServiceReceiver, statusIntentFilter);

        JobManager.create(this).addJobCreator(new DataSyncJobCreator());

        //JobManager.instance().getConfig().setAllowSmallerIntervalsForMarshmallow(true); //.setAllowSmallerIntervals(true);

    }

    //region HockeyApp
    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }
    //endregion

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
    }

    @Override
    protected void onResumeFragments() {
        mAllowCommit = true;
        super.onResumeFragments();
        Log.i("Houspect", "onResumeFragments");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mAllowCommit = false;
        super.onSaveInstanceState(outState);
        Log.i("Houspect", "onSaveInstanceState");
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    protected void onDestroy() {
        DisposableManager.dispose();
        super.onDestroy();
        unregisterManagers();
        if (mDatabaseHelper != null) {
            OpenHelperManager.releaseHelper();
            mDatabaseHelper = null;
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDataIntentServiceReceiver);
        mDataIntentServiceReceiver = null;

        DataSyncJob.cancelAll();
    }

    private void refreshSideMenu(Inspection inspection) {
        mNavigationView.removeAllViews();
        boolean isInspectionTOCSideMenu = inspection != null;
        if (isInspectionTOCSideMenu) {
            hideFragment(mFragmentSideMenu);
            SideMenuInspectionTOCFragment fragment = SideMenuInspectionTOCFragment.newInstance(1,inspection);
            ViewGroup.LayoutParams layoutParams = mNavigationView.getLayoutParams();
            layoutParams.width = Util.dpsToPixels(320, this);
            mNavigationView.setLayoutParams(layoutParams);
            showFragment(fragment, R.id.nagivationView, false);
        }
        else {
            ViewGroup.LayoutParams layoutParams = mNavigationView.getLayoutParams();
            layoutParams.width = Util.dpsToPixels(95, this);
            mNavigationView.setLayoutParams(layoutParams);
            showFragment(mFragmentSideMenu, R.id.nagivationView, false);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (mSessionUtil.getFragmentPromptActive() != null) {
                // suppress back if setting prompt is active
                if (mSessionUtil.getFragmentPromptActive() instanceof SettingsFragment) {
                    return;
                }
                else {
                    hideFragmentPrompt();
                }
            }
            else if (mSessionUtil.getFragmentActive() instanceof InspectionFragment) {
                hideFragment(mSessionUtil.getFragmentActive());
                List<Job> jobs = mDataService.getList(new DataService.GetCallback<Job>() {
                    @Override
                    public QueryBuilder<Job, ?> preQuery(QueryBuilder<Job, ?> queryBuilder) {
                        return queryBuilder.orderBy("timeSince1stJan1970", true);
                    }
                }, Job.class);
                List<CalendarItem> calendarItems = mDataService.getList(null, CalendarItem.class);
                JobListFragment fragment = JobListFragment.newInstance(1, new ArrayList<Job>(jobs), new ArrayList<>(calendarItems));
                showFragment(fragment, "Jobs", true);
                refreshSideMenu(null);
                refreshActionBarMenu();
            }
            else {
                super.onBackPressed();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        mMenu = menu;
        refreshActionBarMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.bak.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
        //    return true;
        //}

        if (id == R.id.actionRefreshJobs) {
            mSessionUtil.setSideMenuActive(SideMenuFragment.ITEM_JOBS);
            refreshJobs(new Runnable() {
                @Override
                public void run() {
                }
            }, new Runnable() {
                @Override
                public void run() {
                }
            });
        }
        else if (id == R.id.actionUploadDrafts) {

            if (!SynchronisedService.isServerActivityInProgress.get()) {

                final WeakReference<MainActivity> weakSelf = new WeakReference<MainActivity>(this);
                Single single = Single.fromCallable(new Callable<Boolean>() {

                    @Override
                    public Boolean call() throws Exception {

                        DataSyncJob x = new DataSyncJob();
                        x.synchroniseData(false, weakSelf.get());
                        return true;
                    }
                });


                showProgress("Please wait", "Uploading Drafts", true);
                single.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                DisposableManager.add(d);
                            }

                            @Override
                            public void onSuccess(Object value) {
                                Toast.makeText(weakSelf.get(), "Drafts uploaded successfully", Toast.LENGTH_SHORT);
                                hideProgress();
                                SynchronisedService.isServerActivityInProgress.set(false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                hideProgress();
                                SynchronisedService.isServerActivityInProgress.set(false);
                            }
                        });

            }
            else {
                Toast.makeText(this, "Another data synchronisation activity is in progress, please try again later", Toast.LENGTH_SHORT).show();
            }

        }
        else if (id == R.id.actionUploadPhotos) {
            if (!SynchronisedService.isServerActivityInProgress.compareAndSet(false,true)) {
                Toast.makeText(this, "Another data synchronisation activity is in progress, please try again later", Toast.LENGTH_SHORT).show();
            }
            else {

                final WeakReference<MainActivity> weakSelf = new WeakReference<MainActivity>(this);
                Single single = Single.fromCallable(new Callable<Boolean>() {

                    @Override
                    public Boolean call() throws Exception {

                        DataSyncJob x = new DataSyncJob();
                        x.synchronisePhotos(mDataService, weakSelf.get());
                        return true;
                    }
                });


                showProgress("Please wait", "Uploading photos", true);
                single.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                DisposableManager.add(d);
                            }

                            @Override
                            public void onSuccess(Object value) {
                                hideProgress();
                                SynchronisedService.isServerActivityInProgress.set(false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                hideProgress();
                                SynchronisedService.isServerActivityInProgress.set(false);
                            }
                        });

            }
        }
        else if (id == R.id.actionRemoveObsoleteDrafts) {
            scanForObsoleteDrafts();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        /*if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void showFragment(Fragment fragment, String title, boolean shouldShowBackgroundOverlay) {

        mSessionUtil.setFragmentActive(fragment);

        boolean shouldShowLogo = (fragment instanceof LoginFragment);
        mImageViewLogo.setVisibility(shouldShowLogo ? View.VISIBLE : View.GONE);
        boolean shouldShowTitle = !shouldShowLogo;
        getSupportActionBar().setDisplayShowTitleEnabled(shouldShowTitle);
        getSupportActionBar().setTitle(title);
        showFragment(fragment, R.id.frameLayoutContent, true);
        //mActiveFragment = fragment;

        mViewFragmentBackgroundOverlay.setVisibility(shouldShowBackgroundOverlay ? View.VISIBLE : View.GONE);

        if (mFragmentSideMenu != null) mFragmentSideMenu.refresh();
    }

    void showFragmentOverlay(Fragment fragment) {
        //showFragment(fragment, R.id.frameLayoutOverlay);
        //mActiveFragmentOverlay = fragment;
        //mFrameLayoutOverlay.setVisibility(View.VISIBLE);
    }

    void showFragmentPrompt(Fragment fragment) {
        mSessionUtil.setFragmentPromptActive(fragment);
        showFragment(fragment, R.id.viewGroupPrompt, true);
        //mFrameLayoutPrompt.setVisibility(View.VISIBLE);
        //mActiveFragmentPrompt = fragment;
    }

    void showFragment(Fragment fragment, int containerId, boolean shouldAnimate) {
        if (!mAllowCommit) return;
        // Note: do not perform mSessionUtil.setFragmentActive as this is also used by showFragmentPrompt
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (shouldAnimate) transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(containerId, fragment);
        transaction.commit();

    }

    void hideFragmentPrompt() {
        hideFragment(mSessionUtil.getFragmentPromptActive());
        mSessionUtil.setFragmentPromptActive(null);
        //mActiveFragmentPrompt = null;
        //mFrameLayoutPrompt.setVisibility(View.GONE);
    }

    void hideFragmentOverlay() {
        //hideFragment(mActiveFragmentOverlay);
        //mActiveFragmentOverlay = null;
        //mFrameLayoutOverlay.setVisibility(View.GONE);
    }

    void hideFragment(Fragment fragment) {
        if (!mAllowCommit) return;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.remove(fragment);
        transaction.commit();
    }

    private void refreshJobs(final Runnable onSuccess, final Runnable onError) {

        final WeakReference<MainActivity> weakSelf = new WeakReference(this);
        Single single = Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                (new DataSyncJob()).synchroniseData(false, weakSelf.get());
                return true;
            }
        });

        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        DisposableManager.add(d);
                    }

                    @Override
                    public void onSuccess(Object value) {
                        onSuccess.run();
                    }

                    @Override
                    public void onError(Throwable e) {
                        onError.run();
                    }
                });

        /*
        if (!SynchronisedService.isServerActivityInProgress.compareAndSet(false,true)) {
            Toast.makeText(this, "Another data synchronisation activity is in progress, please try again later", Toast.LENGTH_SHORT).show();
            return;
        }

        showProgress("Refreshing Jobs", "Please wait", false);
        mDataService.refreshJobsAndInspections(this, new DataService.RefreshCallback() {
            @Override
            public void onSuccess() {
                onSuccess.run();
                SynchronisedService.isServerActivityInProgress.set(false);
            }

            @Override
            public void onError(Throwable e) {
                Fragment fragment = MessagePromptFragment.newInstance("Cannot refresh jobs", e.getMessage());
                showFragmentPrompt(fragment);
                hideProgress();
                onError.run();
                SynchronisedService.isServerActivityInProgress.set(false);
            }
        });*/

    }

    private void refreshDraftList() {
        Single single = Single.fromCallable(new Callable<List<Draft>>() {

            @Override
            public List<Draft> call() throws Exception {
                List<Draft> drafts = mDataService.getList(new DataService.GetCallback<Draft>() {
                    @Override
                    public QueryBuilder<Draft, ?> preQuery(QueryBuilder<Draft, ?> queryBuilder) {
                        try {
                            String username = mSharedPreferencesService.getUsername();
                            Where where = queryBuilder.where();
                            where.and(where.ne("mode", Draft.MODE_ZEUS_FINAL_REPORT),
                                    where.eq("createdByUsername", username).or().eq("createdByUsername", ""));
                        } catch (Exception e) {
                            Log.e("Atlas - Draft list", e.getLocalizedMessage());
                        }
                        return queryBuilder.orderBy("modifiedTime", false);
                    }
                }, Draft.class);

                return drafts;
            }
        });
        showProgress("Please wait", "Retrieving Drafts", true);
        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        DisposableManager.add(d);
                    }

                    @Override
                    public void onSuccess(Object value) {
                        hideModalProgress();
                        List<Draft> drafts = (List<Draft>) value;
                        DraftListFragment fragment = DraftListFragment.newInstance(1, drafts);
                        showFragment(fragment, "Drafts", true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideModalProgress();
                    }
                });
    }

    private void scanForObsoleteDrafts() {

        if (!SynchronisedService.isServerActivityInProgress.compareAndSet(false,true)) {
            Toast.makeText(this, "Another data synchronisation activity is in progress. Please try again later.", Toast.LENGTH_SHORT).show();
            return;
        }

        showProgress("Please wait", "Scanning for drafts to delete", true);


        final WeakReference<MainActivity> weakReference = new WeakReference<>(this);
        String username = mSharedPreferencesService.getUsername();
        DisposableManager.add(mDataService.scanAndCleanObsoleteDrafts(username)
                .subscribe(drafts -> {
                    hideModalProgress();
                    SynchronisedService.isServerActivityInProgress.set(false);
                    DraftListFragment fragment = DraftListFragment.newInstance(1, drafts);
                    showFragment(fragment, "Drafts", true);
                }, error -> {
                    hideModalProgress();
                    Toast.makeText(weakReference.get(), "Failed to scan for obsolete drafts (" + error.getMessage() + ")", Toast.LENGTH_SHORT).show();
                    SynchronisedService.isServerActivityInProgress.set(false);
                }));

    }

    @Override
    public void onListFragmentInteraction(SideMenuItem item) {

        boolean isLoggedIn = !(mSessionUtil.getFragmentActive() instanceof LoginFragment);
        if (item == SideMenuFragment.ITEM_DRAFTS) {

            if (!isLoggedIn) {
                Toast.makeText(this, "Please login to see drafts", Toast.LENGTH_SHORT).show();
                return;
            }
            refreshDraftList();
        }
        else if (item == SideMenuFragment.ITEM_JOBS) {

            if (!isLoggedIn) {
                Toast.makeText(this, "Please login to see jobs", Toast.LENGTH_SHORT).show();
                return;
            }
            List<Job> jobs = mDataService.getList(new DataService.GetCallback<Job>() {
                @Override
                public QueryBuilder<Job, ?> preQuery(QueryBuilder<Job, ?> queryBuilder) {
                    return queryBuilder.orderBy("timeSince1stJan1970", true);
                }
            }, Job.class);
            List<CalendarItem> calendarItems = mDataService.getList(null, CalendarItem.class);
            JobListFragment fragment = JobListFragment.newInstance(1, new ArrayList<Job>(jobs), calendarItems);
            showFragment(fragment, "Jobs", true);
        }
        else if (item == SideMenuFragment.ITEM_RECENT_INSPECTIONS) {
            if (!isLoggedIn) {
                Toast.makeText(this, "Please login to see recent inspections", Toast.LENGTH_SHORT).show();
                return;
            }
            List<RecentInspection> recentInspections = mDataService.getList(new DataService.GetCallback<RecentInspection>() {
                @Override
                public QueryBuilder<RecentInspection, ?> preQuery(QueryBuilder<RecentInspection, ?> queryBuilder) {
                    return queryBuilder.orderBy("date", true);
                }
            }, RecentInspection.class);
            RecentInspectionListFragment fragment = RecentInspectionListFragment.newInstance(new ArrayList<RecentInspection>(recentInspections));
            showFragment(fragment, "Recent Inspections", true);
        }
        else if (item == SideMenuFragment.ITEM_LOGOUT) {

            if (!isLoggedIn) {
                Toast.makeText(this, "You are already logged out", Toast.LENGTH_SHORT).show();
                return;
            }

            logout();
        }
        else if (item == SideMenuFragment.ITEM_SETTINGS) {

            SettingsFragment fragment = SettingsFragment.newInstance();
            showFragmentPrompt(fragment);

        }
        mDrawerLayout.closeDrawer(Gravity.LEFT);
        refreshActionBarMenu();
    }

    private void logout() {
        DataSyncJob.cancelAll();

        LoginFragment fragment = LoginFragment.newInstance(null, null);
        showFragment(fragment, "", false);

        DataSyncJob.cancelAll();

        setActionAndStatusBarColor(0x00ffffff);

        SynchronisedService.isServerActivityInProgress.set(false);
    }

    // LoginFragment.onFragmentInteraction
    @Override
    public void onFragmentInteraction(LoginFragment.Event event, String message) {

        if (LoginFragment.Event.LOGIN_IN_PROGRESS == event) {
            showProgress("Login in progress", "Please wait", true);
        }

        else if (LoginFragment.Event.LOGIN_SUCCESS == event) {

            final Runnable onSuccess = new Runnable() {
                @Override
                public void run() {
                    showProgress("Please wait", "Processing Inspections", false);
                    List<Job> jobs = mDataService.getList(new DataService.GetCallback<Job>() {
                        @Override
                        public QueryBuilder<Job, ?> preQuery(QueryBuilder<Job, ?> queryBuilder) {
                            return queryBuilder.orderBy("timeSince1stJan1970", true);
                        }
                    }, Job.class);
                    List<CalendarItem> calendarItems = mDataService.getList(null, CalendarItem.class);
                    JobListFragment fragment = JobListFragment.newInstance(1, jobs, calendarItems);
                    showFragment(fragment, "Jobs", true);
                    refreshActionBarMenu();

                    hideProgress();

                    setActionAndStatusBarColor(0xff3498db);
                }
            };
            refreshJobs(new Runnable() {
                @Override
                public void run() {

                    DataSyncJob.schedulePeriodicJob();
                }
            }, new Runnable() {
                @Override
                public void run() {

                    DataSyncJob.schedulePeriodicJob();
                }
            });

            onSuccess.run();
        }
        else if (LoginFragment.Event.FORGOT_PASSWORD == event) {
            ForgotPasswordFragment fragment = ForgotPasswordFragment.newInstance(message);
            showFragmentPrompt(fragment);
        }
        else if (LoginFragment.Event.LOGIN_FAILURE == event) {
            hideProgress();
            Fragment fragment = MessagePromptFragment.newInstance("Cannot Login", message);
            showFragmentPrompt(fragment);
        }
        else if (LoginFragment.Event.NOT_ME == event) {
            DataSyncJob.cancelAll();
        }

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    @Override
    public void onListFragmentInteraction(Job item) {

        /*
        Inspection inspection = mDataService.get(item.getId(), Inspection.class);
        if (inspection == null) {
            MessagePromptFragment messagePromptFragment = MessagePromptFragment.newInstance("Cannot load Job details", "Inspection data is missing. Please refresh jobs data.");
            showFragmentPrompt(messagePromptFragment);
            return;
        }
        boolean isEditingDraft = false;
        Fragment fragment = InspectionStartFragment.newInstance(inspection, isEditingDraft);
        showFragment(fragment, "Inspection", true);
        */

        Inspection inspection = mDataService.get(item.getId(), Inspection.class);
        try {
            Util.parseInspectionXml(inspection.getXml(), inspection);
        } catch (Exception e) {
            MessagePromptFragment messagePromptFragment = MessagePromptFragment.newInstance("Cannot open job details", "Invalid data. Please refresh jobs data. Error Reason: " + e.getLocalizedMessage());
            return;
        }
        InspectionFragment fragment = InspectionFragment.newInstance(inspection, 0, false);
        showFragment(fragment, inspection.getCode() + " - " + inspection.getClient().getName() + ", " + inspection.getAddress(), true);

        refreshSideMenu(inspection);
    }

    //region RecentInspectionListFragment

    @Override
    public void onListFragmentInteraction(RecentInspectionListFragment.Event event, RecentInspection recentInspection) {

        if (!recentInspection.getPendingUploadToServer()) return;

        final Draft draft = mDataService.get(recentInspection.getId(), Draft.class);

        if (draft == null) {
            // Note: this shouldn't happen
            MessagePromptFragment messagePromptFragment = MessagePromptFragment.newInstance("Cannot retry submitting Zeus Final Report", "Draft data is missing.");
            showFragmentPrompt(messagePromptFragment);
            return;
        }

        if (!SynchronisedService.isServerActivityInProgress.compareAndSet(false,true)) {
            Toast.makeText(this, "Another data synchronisation activity is in progress. Cannot retry uploading Zeus Draft (" + draft.getCodeForListDisplay() + "). Please try again later.", Toast.LENGTH_SHORT).show();
        }
        else {
            final WeakReference<MainActivity> weakReference = new WeakReference<MainActivity>(this);
            DataSyncJob.broadcast("Retry submitting Zeus Final Report (" + draft.getCodeForListDisplay() + ")", this);
            mDataService.retrySubmitFinalReport(recentInspection, new DataService.UploadCallback() {
                @Override
                public void onSuccess() {
                    Toast.makeText(weakReference.get(), "Zeus Final Report (" + draft.getCodeForListDisplay() + ") submitted successfully.", Toast.LENGTH_SHORT).show();
                    DataSyncJob.broadcast("", weakReference.get());
                    SynchronisedService.isServerActivityInProgress.set(false);
                }

                @Override
                public void onEror(Throwable e) {
                    Toast.makeText(weakReference.get(), "Cannot submit Zeus Final Report (" + draft.getCodeForListDisplay() + "). " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    DataSyncJob.broadcast("", weakReference.get());
                    SynchronisedService.isServerActivityInProgress.set(false);
                }
            });
        }
    }

    //endregion

    @Override
    public void onFragmentInteraction(InspectionFragment.Event event) {

        if (event == InspectionFragment.Event.CANCEL) {

            List<Job> jobs = mDataService.getList(new DataService.GetCallback<Job>() {
                @Override
                public QueryBuilder<Job, ?> preQuery(QueryBuilder<Job, ?> queryBuilder) {
                    return queryBuilder.orderBy("timeSince1stJan1970", true);
                }
            }, Job.class);
            List<CalendarItem> calendarItems = mDataService.getList(null, CalendarItem.class);

            JobListFragment fragment = JobListFragment.newInstance(1, new ArrayList<Job>(jobs), calendarItems);
            hideFragment(mSessionUtil.getFragmentActive());
            showFragment(fragment, "Jobs", true);

            refreshSideMenu(null);
            refreshActionBarMenu();
        }
    }

    @Override
    public void onFragmentInteraction(InspectionSectionFragment inspectionSectionFragment,
                                      InspectionSectionFragment.Event event,
                                      HashMap<String,Object> dataObject) {

        InspectionSection inspectionSection = null;
        InspectionSectionImage inspectionSectionImage = null;
        if (dataObject.containsKey("inspectionSection")) inspectionSection = (InspectionSection) dataObject.get("inspectionSection");
        if (dataObject.containsKey("inspectionSectionImage")) inspectionSectionImage = (InspectionSectionImage) dataObject.get("inspectionSectionImage");

        if (event == InspectionSectionFragment.Event.PHOTO_TAKEN) {

            final WeakReference<MainActivity> weakSelf = new WeakReference<MainActivity>(this);
            Draft draftInEdit = mSessionUtil.getDraftInEdit();
            mDataService.saveDraft(draftInEdit);

            final InspectionSectionImageUploadLog inspectionSectionImageUploadLog = mDataService.getInspectionSectionImageUploadLog(inspectionSectionImage, inspectionSection);

            boolean shouldSkipUploadPhoto = !mSharedPreferencesService.getShouldUploadPhotosWhenOnline();

            if (shouldSkipUploadPhoto) {
                Toast.makeText(this, "Skipping image upload as per confguration in settings.", Toast.LENGTH_SHORT).show();
                return;
            }

            Toast.makeText(this, "Uploading image to inspection (" + draftInEdit.getCode() + ") section (" + inspectionSection.getName() + ").", Toast.LENGTH_SHORT).show();

            mDataService.uploadImage(inspectionSectionImageUploadLog, new DataService.UploadCallback() {
                @Override
                public void onSuccess() {
                    String messageSuffix = !inspectionSectionImageUploadLog.isForDeletion() ?  "uploaded." : "deleted";
                    Toast.makeText(weakSelf.get(), "Image successfully " + messageSuffix, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onEror(Throwable e) {
                    String message = !inspectionSectionImageUploadLog.isForDeletion()
                                        ? "Failed to upload image (" + e.getMessage() + ")."
                                        : "Failed to request image deletion (" + e.getMessage() + ").";
                    Toast.makeText(weakSelf.get(), message, Toast.LENGTH_SHORT).show();
                }
            });
        }
        else if (event == InspectionSectionFragment.Event.COMMENT) {
            CommentFragment fragment = CommentFragment.newInstance(inspectionSection, CommentFragment.Type.COMMENT);
            showFragmentPrompt(fragment);
        }
        else if (event == InspectionSectionFragment.Event.RECOMMENDATION) {
            CommentFragment fragment = CommentFragment.newInstance(inspectionSection, CommentFragment.Type.RECOMMENDATION);
            showFragmentPrompt(fragment);
        }
        else if (event == InspectionSectionFragment.Event.NEXT) {
            Draft draftInEdit = mSessionUtil.getDraftInEdit();
            if (draftInEdit != null) mDataService.saveDraft(draftInEdit);
            InspectionFragment fragment = (InspectionFragment) mSessionUtil.getFragmentActive();
            fragment.next();
            refreshSideMenu(draftInEdit);
            refreshActionBarMenu();
        }
        else if (event == InspectionSectionFragment.Event.BACK) {
            Draft draftInEdit = mSessionUtil.getDraftInEdit();
            if (draftInEdit != null) mDataService.saveDraft(draftInEdit);
            InspectionFragment fragment = (InspectionFragment) mSessionUtil.getFragmentActive();
            fragment.back();
            refreshSideMenu(draftInEdit);
            refreshActionBarMenu();
        }
        else if (event == InspectionSectionFragment.Event.PHOTO_GALLERY) {
            PhotoGalleryFragment fragment = PhotoGalleryFragment.newInstance(inspectionSection);
            showFragmentPrompt(fragment);
        }
        else if (event == InspectionSectionFragment.Event.PHOTO_ERROR) {
            MessagePromptFragment messagePromptFragment = MessagePromptFragment.newInstance("Cannot take photo", "Error creating storage folder. Please ensure there is storage media available on the device.");
            showFragmentPrompt(messagePromptFragment);
        }
        else if (event == InspectionSectionFragment.Event.PHOTO_STORAGE_PERMISSION_REQUEST_TAKE_A_PHOTO) {
            boolean needsToWaitForUserPermissionAction = MediaUtil.checkAndRequestPhotoStoragePermission(this, MediaUtil.PermissionRequestCode.REQUEST_WRITE_STORAGE_TAKE_A_PHOTO);
            if (!needsToWaitForUserPermissionAction) {
                inspectionSectionFragment.invokeCamera();
            }
        }
        else if (event == InspectionSectionFragment.Event.PHOTO_STORAGE_PERMISSION_REQUEST_PICK_A_PHOTO) {
            boolean needsToWaitForUserPermissionAction = MediaUtil.checkAndRequestPhotoStoragePermission(this, MediaUtil.PermissionRequestCode.REQUEST_WRITE_STORAGE_PICK_A_PHOTO);
            if (!needsToWaitForUserPermissionAction) {
                inspectionSectionFragment.invokePhotoLibrary();
            }
        }
    }

    @Override
    public void onFragmentInteraction(CommentFragment.Event event, String comment) {
        if (event == CommentFragment.Event.CANCEL) {
        }
        else if (event == CommentFragment.Event.DONE) {
            // TODO - save comment
            InspectionFragment fragment = (InspectionFragment) mSessionUtil.getFragmentActive();
            fragment.refreshDisplay();
        }
        hideFragmentPrompt();
    }

    @Override
    public void onFragmentInteraction(CameraPreviewFragment.Event event, Object data) {

        /*hideFragmentPrompt();

        if (event == CameraPreviewFragment.Event.PHOTO_TAKEN) {
            String photoPath = (String) data;
            PhotoEditorFragment fragment = PhotoEditorFragment.newInstance(photoPath);
            showFragmentPrompt(fragment);
        }
        else if (event == CameraPreviewFragment.Event.PHOTO_GALLERY) {
            PhotoGalleryFragment fragment = PhotoGalleryFragment.newInstance(null, null);
            showFragmentPrompt(fragment);
        }*/
    }

    @Override
    public void onFragmentInteraction(PhotoEditorFragment.Event event) {
        hideFragmentPrompt();
        //if (event == PhotoEditorFragment.Event.CANCEL) {
        //    CameraPreviewFragment fragment = CameraPreviewFragment.newInstance(null, null);
        //    showFragmentPrompt(fragment);
        //}
    }

    @Override
    public void onFragmentInteraction(PhotoGalleryFragment.Event event, HashMap<String,Object> dataObject) {


        if (event == PhotoGalleryFragment.Event.PHOTO_SELECTED) {
            //PhotoEditorFragment fragment = PhotoEditorFragment.newInstance(photoPath);
            //showFragmentPrompt(fragment);
            hideFragmentPrompt();
        }
        else if (event == PhotoGalleryFragment.Event.CANCEL) {
            //CameraPreviewFragment fragment = CameraPreviewFragment.newInstance(null, null);
            //showFragmentPrompt(fragment);
            hideFragmentPrompt();
        }
        else if (event == PhotoGalleryFragment.Event.PHOTO_UPDATED) {
            InspectionSection inspectionSection = null;
            InspectionSectionImage inspectionSectionImage = null;
            boolean isPhotoMarkUped = false;
            if (dataObject.containsKey("inspectionSection")) inspectionSection = (InspectionSection) dataObject.get("inspectionSection");
            if (dataObject.containsKey("inspectionSectionImage")) inspectionSectionImage = (InspectionSectionImage) dataObject.get("inspectionSectionImage");
            if (dataObject.containsKey("isPhotoMarkUped")) isPhotoMarkUped = (boolean) dataObject.get("isPhotoMarkUped");

            Draft draftInEdit = mSessionUtil.getDraftInEdit();
            mDataService.saveDraft(draftInEdit);

            if (isPhotoMarkUped) {
                DisposableManager.add(
                        tryImageUpload(inspectionSectionImage, inspectionSection, draftInEdit.getCode(), this, mDataService, mSharedPreferencesService)
                                .subscribeOn(AndroidSchedulers.mainThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(()-> {})
                );
            }


        }
        else if (event == PhotoGalleryFragment.Event.PHOTO_DELETED) {

            InspectionSection inspectionSection = null;
            InspectionSectionImage inspectionSectionImage = null;
            if (dataObject.containsKey("inspectionSection")) inspectionSection = (InspectionSection) dataObject.get("inspectionSection");
            if (dataObject.containsKey("inspectionSectionImage")) inspectionSectionImage = (InspectionSectionImage) dataObject.get("inspectionSectionImage");

            Draft draftInEdit = mSessionUtil.getDraftInEdit();
            mDataService.saveDraft(draftInEdit);

            InspectionFragment fragment = (InspectionFragment) mSessionUtil.getFragmentActive();
            fragment.refreshDisplay();

            final InspectionSectionImageUploadLog inspectionSectionImageUploadLog = mDataService.getInspectionSectionImageUploadLog(inspectionSectionImage, inspectionSection, true);

            boolean shouldSkipUploadPhoto = !mSharedPreferencesService.getShouldUploadPhotosWhenOnline();

            if (shouldSkipUploadPhoto) {
                Toast.makeText(this, "Skipping image upload as per confguration in settings.", Toast.LENGTH_SHORT).show();
                return;
            }

            Toast.makeText(this, "Requesting image deletion from inspection (" + draftInEdit.getCode() + ") section (" + inspectionSection.getName() + ").", Toast.LENGTH_SHORT).show();

            final WeakReference<MainActivity> weakSelf = new WeakReference<MainActivity>(this);

            mDataService.uploadImage(inspectionSectionImageUploadLog, new DataService.UploadCallback() {
                @Override
                public void onSuccess() {
                    String messageSuffix = !inspectionSectionImageUploadLog.isForDeletion() ?  "uploaded." : "deleted";
                    Toast.makeText(weakSelf.get(), "Image successfully " + messageSuffix, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onEror(Throwable e) {
                    String message = !inspectionSectionImageUploadLog.isForDeletion()
                            ? "Failed to upload image (" + e.getMessage() + ")."
                            : "Failed to request image deletion (" + e.getMessage() + ").";
                    Toast.makeText(weakSelf.get(), message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onFragmentInteraction(InspectionEndFragment.Event event, Object context) {

        if (event == InspectionEndFragment.Event.SAVE_AS_ATLAS_DRAFT) {
            final Draft draft = (Draft)context;
            showScreen(ScreenOption.Drafts);
            handleSaveDraftRequest(draft, YesNoConfirmationFragmentRequestCode.SaveAsAtlasDraft);
            refreshSideMenu(null);
        }
        else if (event == InspectionEndFragment.Event.SAVE_AS_ZEUS_DRAFT || event == InspectionEndFragment.Event.SUBMIT_FINAL_REPORT) {

            // TODO:: validate completeness - revise waiting for confirmation from Houspect
            final Draft draft = (Draft)context;
            if (event == InspectionEndFragment.Event.SAVE_AS_ZEUS_DRAFT && !draft.isValidZeusDraft()) {
                MessagePromptFragment messagePromptFragment = MessagePromptFragment.newInstance("Cannot save as Zeus draft", "All mandatory sections must be complete (answer questions or select N/A).");
                showFragmentPrompt(messagePromptFragment);
                return;
            }
            else if (event == InspectionEndFragment.Event.SUBMIT_FINAL_REPORT && !draft.isValidZeusFinal()) {
                MessagePromptFragment messagePromptFragment = MessagePromptFragment.newInstance("Cannot submit final report", "All sections must be complete (answer questions or select N/A).");
                showFragmentPrompt(messagePromptFragment);
                return;
            }

            if (event == InspectionEndFragment.Event.SAVE_AS_ZEUS_DRAFT) {
                YesNoConfirmationFragment promptFragment =
                        YesNoConfirmationFragment.newInstance(
                                getResources().getString(R.string.message_save_as_zeus_draft_title),
                                getResources().getString(R.string.message_save_as_zeus_draft_content),
                                YesNoConfirmationFragmentRequestCode.SaveAsZeusDraft.getValue(),
                                draft);
                showFragmentPrompt(promptFragment);
            }
            else if (event == InspectionEndFragment.Event.SUBMIT_FINAL_REPORT) {
                YesNoConfirmationFragment promptFragment =
                        YesNoConfirmationFragment.newInstance(
                                getResources().getString(R.string.message_submit_final_report_title),
                                getResources().getString(R.string.message_submit_final_report_content),
                                YesNoConfirmationFragmentRequestCode.SubmitFinalReport.getValue(),
                                draft);
                showFragmentPrompt(promptFragment);
            }
        }
        else if (event == InspectionEndFragment.Event.BACK) {
            InspectionFragment fragment = (InspectionFragment) mSessionUtil.getFragmentActive();
            fragment.back();
        }
        else if (event == InspectionEndFragment.Event.SELECT_SECTION) {
            int sectionIndex = (int)context;
            int selectedTOCIndex = sectionIndex + 1; // +1 for Start
            InspectionFragment fragment = (InspectionFragment) mSessionUtil.getFragmentActive();
            fragment.goTo(selectedTOCIndex);
        }
        refreshActionBarMenu();
    }

    @Override
    public void onFragmentInteraction(InspectionStartFragment inspectionStartFragment, InspectionStartFragment.Event event, final Inspection inspection) {

        InspectionFragment fragment = (InspectionFragment) mSessionUtil.getFragmentActive();
        if (event == InspectionStartFragment.Event.NEXT) {
            fragment.next();
        }
        else if (event == InspectionStartFragment.Event.BACK) {
            fragment.back();
        }
        else if (event == InspectionStartFragment.Event.START_INSPECTION) {
            YesNoConfirmationFragment promptFragment =
                    YesNoConfirmationFragment.newInstance(getResources().getString(R.string.message_inspection_to_draft_title),
                            getResources().getString(R.string.message_inspection_to_draft_content),
                            YesNoConfirmationFragmentRequestCode.StartInspection.getValue(),
                            inspection);
            showFragmentPrompt(promptFragment);
        }
        else if (event == InspectionStartFragment.Event.PHOTO_TAKEN) {

            final WeakReference<MainActivity> weakSelf = new WeakReference<MainActivity>(this);
            Draft draftInEdit = mSessionUtil.getDraftInEdit();
            inspectionStartFragment.copyChangesTo(draftInEdit);
            mDataService.saveDraft(draftInEdit);

            // delete existing InspectionImageUploadLog with matching Inspection Id i.e. only the latest cover photo is used
            List<InspectionImageUploadLog> inspectionImageUploadLogs = mDataService.getList(new DataService.GetCallback<InspectionImageUploadLog>() {
                @Override
                public QueryBuilder<InspectionImageUploadLog, ?> preQuery(QueryBuilder<InspectionImageUploadLog, ?> queryBuilder) {

                    try {
                        queryBuilder.where().eq("inspectionId", inspection.getId());
                    } catch (Exception e) {
                        Log.e("Atlas", "Error filtering InspectionImageUploadLogs by inspectionId - " + e.getLocalizedMessage());
                        return null;
                    }
                    return queryBuilder;
                }
            }, InspectionImageUploadLog.class);

            for (InspectionImageUploadLog inspectionImageUploadLog : inspectionImageUploadLogs) {
                mDataService.remove(inspectionImageUploadLog.getId(), InspectionImageUploadLog.class);
            }

            InspectionImage inspectionImage = inspection.getImage();

            final InspectionImageUploadLog inspectionImageUploadLog = mDataService.getInspectionImageUploadLog(inspectionImage, inspection);

            boolean shouldSkipUploadPhoto = !mSharedPreferencesService.getShouldUploadPhotosWhenOnline();

            if (shouldSkipUploadPhoto) {
                Toast.makeText(this, "Skipping image upload as per confguration in settings.", Toast.LENGTH_SHORT).show();
                return;
            }

            Toast.makeText(this, "Uploading image to inspection (" + draftInEdit.getCode() + ")", Toast.LENGTH_SHORT).show();

            mDataService.uploadImage(inspectionImageUploadLog, new DataService.UploadCallback() {
                @Override
                public void onSuccess() {
                    String messageSuffix = !inspectionImageUploadLog.isForDeletion() ?  "uploaded." : "deleted";
                    Toast.makeText(weakSelf.get(), "Image successfully " + messageSuffix, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onEror(Throwable e) {
                    String message = !inspectionImageUploadLog.isForDeletion()
                            ? "Failed to upload image (" + e.getMessage() + ")."
                            : "Failed to request image deletion (" + e.getMessage() + ").";
                    Toast.makeText(weakSelf.get(), message, Toast.LENGTH_SHORT).show();
                }
            });
        }
        else if (event == InspectionStartFragment.Event.PHOTO_ERROR) {
            MessagePromptFragment messagePromptFragment = MessagePromptFragment.newInstance("Cannot take photo", "Error creating storage folder. Please ensure there is storage media available on the device.");
            showFragmentPrompt(messagePromptFragment);
        }
        else if (event == InspectionStartFragment.Event.PHOTO_STORAGE_PERMISSION_REQUEST_TAKE_A_PHOTO) {
            boolean needsToWaitForUserPermissionAction = MediaUtil.checkAndRequestPhotoStoragePermission(this, MediaUtil.PermissionRequestCode.REQUEST_WRITE_STORAGE_TAKE_A_PHOTO);
            if (!needsToWaitForUserPermissionAction) {
                inspectionStartFragment.invokeCamera();
            }
        }
        else if (event == InspectionStartFragment.Event.PHOTO_STORAGE_PERMISSION_REQUEST_PICK_A_PHOTO) {
            boolean needsToWaitForUserPermissionAction = MediaUtil.checkAndRequestPhotoStoragePermission(this, MediaUtil.PermissionRequestCode.REQUEST_WRITE_STORAGE_PICK_A_PHOTO);
            if (!needsToWaitForUserPermissionAction) {
                inspectionStartFragment.invokePhotoLibrary();
            }
        }
        refreshActionBarMenu();
    }

    @Override
    public void onListFragmentInteraction(InspectionRecommendationSummaryFragment source, InspectionRecommendationSummaryFragment.Event event, Inspection inspection, InspectionSection inspectionSection, int sectionIndex) {

        Draft draftInEdit = mSessionUtil.getDraftInEdit();
        source.copyChangesTo(draftInEdit);
        mDataService.saveDraft(draftInEdit);

        InspectionFragment fragment = (InspectionFragment) mSessionUtil.getFragmentActive();
        if (event == InspectionRecommendationSummaryFragment.Event.NEXT) {
            fragment.next();
        }
        else if (event == InspectionRecommendationSummaryFragment.Event.BACK) {
            fragment.back();
        }
        else if (event == InspectionRecommendationSummaryFragment.Event.SELECT) {
            fragment.goToSection(sectionIndex);
        }
    }

    @Override
    public void onListFragmentInteraction(SideMenuInspectionTOCFragment.Event event, InspectionSection selectedSection, int selectedTOCIndex) {
        Fragment fragement = mSessionUtil.getFragmentActive();
        if (fragement instanceof InspectionFragment) {
            InspectionFragment inspectionFragment = (InspectionFragment)fragement;
            if (inspectionFragment.isDraftEditing()) {

                if (event == SideMenuInspectionTOCFragment.Event.ITEM_SELECTED) {

                    Draft dratInEdit = mSessionUtil.getDraftInEdit();
                    inspectionFragment.copyChangesTo(dratInEdit);
                    mDataService.saveDraft(dratInEdit);
                    inspectionFragment.goTo(selectedTOCIndex);
                }
                else if (event == SideMenuInspectionTOCFragment.Event.CLONE_SECTION_REQUESTED) {
                    YesNoConfirmationFragment promptFragment =
                            YesNoConfirmationFragment.newInstance(getResources().getString(R.string.message_clone_confirmation_title),
                                    getResources().getString(R.string.message_clone_confirmation_content),
                                    YesNoConfirmationFragmentRequestCode.CloneInspectionSection.getValue(),
                                    selectedSection);
                    showFragmentPrompt(promptFragment);
                }
                else if (event == SideMenuInspectionTOCFragment.Event.REMOVE_CLONED_SECTION_REQUESTED) {
                    YesNoConfirmationFragment promptFragment =
                            YesNoConfirmationFragment.newInstance(getResources().getString(R.string.message_remove_clone_confirmation_title),
                                    getResources().getString(R.string.message_remove_clone_confirmation_content),
                                    YesNoConfirmationFragmentRequestCode.RemoveCloneInspectionSection.getValue(),
                                    selectedSection);
                    showFragmentPrompt(promptFragment);
                }

            }
            else {
                YesNoConfirmationFragment promptFragment =
                        YesNoConfirmationFragment.newInstance(getResources().getString(R.string.message_inspection_to_draft_title),
                                getResources().getString(R.string.message_inspection_to_draft_content),
                                YesNoConfirmationFragmentRequestCode.StartInspection.getValue(),
                                inspectionFragment.getInspection());
                showFragmentPrompt(promptFragment);
            }
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        refreshActionBarMenu();
    }

    private void refreshActionBarMenu() {
        toggleActionBarMenuItem(R.id.actionRefreshJobs, false);
        toggleActionBarMenuItem(R.id.actionCloneSection, false);
        toggleActionBarMenuItem(R.id.actionUploadDrafts, false);
        toggleActionBarMenuItem(R.id.actionUploadPhotos, false);
        toggleActionBarMenuItem(R.id.actionRemoveObsoleteDrafts, false);
        Fragment fragment = mSessionUtil.getFragmentActive();
        if (fragment instanceof  JobListFragment) {
            toggleActionBarMenuItem(R.id.actionRefreshJobs, true);
        }
        else if (fragment instanceof  DraftListFragment) {
            toggleActionBarMenuItem(R.id.actionUploadDrafts, true);
            toggleActionBarMenuItem(R.id.actionUploadPhotos, true);
            toggleActionBarMenuItem(R.id.actionRemoveObsoleteDrafts, true);
        }
        invalidateOptionsMenu();
    }

    private void toggleActionBarMenuItem(int id, boolean isVisible) {
        MenuItem menuItem = mMenu.findItem(id);
        menuItem.setVisible(isVisible);

    }

    @Override
    public void onListFragmentInteraction(DraftListFragment.Event event, final Draft draft) {

        if (draft.isZeusFinalReport()) {
            Toast.makeText(this, "Not implemented. Final Report is not meant to be visible in this list. Please contact technical support.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (draft.isZeusDraft()) {
            if (!SynchronisedService.isServerActivityInProgress.compareAndSet(false,true)) {
                Toast.makeText(this, "Another data synchronisation activity is in progress. Cannot retry uploading Zeus Draft (" + draft.getCodeForListDisplay() + "). Please try again later.", Toast.LENGTH_SHORT).show();
            }
            else {
                final WeakReference<MainActivity> weakReference = new WeakReference<MainActivity>(this);
                DataSyncJob.broadcast("Retry uploading Zeus Draft (" + draft.getCodeForListDisplay() + ")", this);
                mDataService.uploadDraft(draft, new DataService.UploadCallback() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(weakReference.get(), "Zeus Draft (" + draft.getCodeForListDisplay() + ") uploaded successfully.", Toast.LENGTH_SHORT).show();
                        DataSyncJob.broadcast("", weakReference.get());
                        SynchronisedService.isServerActivityInProgress.set(false);
                    }

                    @Override
                    public void onEror(Throwable e) {
                        Toast.makeText(weakReference.get(), "Cannot upload Zeus Draft (" + draft.getCodeForListDisplay() + "). " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        DataSyncJob.broadcast("", weakReference.get());
                        SynchronisedService.isServerActivityInProgress.set(false);
                    }
                });
            }
            return;
        }

        try {
            Util.parseInspectionXml(draft.getXml(), draft);
        } catch (Exception e) {
            e.printStackTrace();
            // TODO - show error prompt
        }
        InspectionFragment fragment = InspectionFragment.newInstance(draft, 0, true);
        showFragment(fragment, draft.getCode() + " - " + draft.getClient().getName() + ", " + draft.getAddress(), true);

        refreshSideMenu(draft);

        boolean shouldShowDeclaration = draft.getDeclaredTime() <= 0;
        if (shouldShowDeclaration) {

            CheckboxConfirmationFragment promptFragment =
                    CheckboxConfirmationFragment.newInstance(R.string.message_inspection_declaration_title, R.string.message_inspection_declaration_content, R.string.message_inspection_declaration_accept, draft);
            showFragmentPrompt(promptFragment);
        }
    }

    @Override
    public void onFragmentInteraction(CheckboxConfirmationFragment.Event event, Parcelable tag) {
        boolean isConfirmationForInspection = mSessionUtil.getFragmentActive() instanceof InspectionFragment;
        mSessionUtil.setDraftInEdit((Draft)tag);
        if (isConfirmationForInspection && event == CheckboxConfirmationFragment.Event.CANCEL) {
            ((InspectionFragment)mSessionUtil.getFragmentActive()).back();
            hideFragmentPrompt();
        }
        else if (isConfirmationForInspection && event == CheckboxConfirmationFragment.Event.CONFIRMED) {

            Draft draft = (Draft)tag;
            draft.setDeclaredTime((new Date()).getTime());
            mDataService.saveDraft(draft);
            hideFragmentPrompt();
            refreshActionBarMenu();
        }
    }

    @Override
    public void onFragmentInteraction(YesNoConfirmationFragment.Event event, int requestCode, Parcelable context) {

        YesNoConfirmationFragmentRequestCode requestCodeObject = YesNoConfirmationFragmentRequestCode.get(requestCode);

        if (requestCodeObject == YesNoConfirmationFragmentRequestCode.StartInspection) {

            boolean isConfirmationForJob = mSessionUtil.getFragmentActive() instanceof InspectionFragment;
            if (isConfirmationForJob && event == YesNoConfirmationFragment.Event.YES) {

                Inspection inspection = (Inspection) context;

                // TODO:: retrieve Inspection data from offline storage
                Job job = mDataService.get(inspection.getId(), Job.class);

                if (job == null) {
                    MessagePromptFragment messagePromptFragment = MessagePromptFragment.newInstance("Cannot start inspection", "Job data is missing. Please refresh Jobs data.");
                    showFragmentPrompt(messagePromptFragment);
                    return;
                }

                //Inspection inspection = mDataService.get(job.getId(),Inspection.class); // (new DemoUtil()).generateInspection(this, "test.xml");
                Draft draft = null;

                try {
                    draft = mDataService.inspectionToDraft(inspection);
                    draft.setReportType(job.getReportType());
                    mDataService.saveDraft(draft);
                    mDataService.remove(draft.getId(), Job.class);
                    mDataService.remove(draft.getId(), Inspection.class);

                    if (SynchronisedService.isServerActivityInProgress.compareAndSet(false,true)) {

                        mDataService.notifyDraftIsActive(draft, new DataService.UploadCallback() {
                            @Override
                            public void onSuccess() {
                                SynchronisedService.isServerActivityInProgress.set(false);
                            }

                            @Override
                            public void onEror(Throwable e) {
                                SynchronisedService.isServerActivityInProgress.set(false);
                            }
                        });

                    }

                } catch (Exception e) {
                    MessagePromptFragment messagePromptFragment = MessagePromptFragment.newInstance("Cannot convert Job to Draft", "Please try to refresh job data. Error reason: " + e.getLocalizedMessage());
                    return;
                }
                InspectionFragment fragment = InspectionFragment.newInstance(draft, 0, true);
                showFragment(fragment, draft.getCode() + " - " + draft.getClient().getName() + ", " + draft.getAddress(), true);

                refreshSideMenu(draft);

                CheckboxConfirmationFragment promptFragment =
                        CheckboxConfirmationFragment.newInstance(R.string.message_inspection_declaration_title, R.string.message_inspection_declaration_content, R.string.message_inspection_declaration_accept, draft);
                showFragmentPrompt(promptFragment);
            } else if (isConfirmationForJob && event == YesNoConfirmationFragment.Event.NO) {
                hideFragmentPrompt();
            }
        }

        else if (requestCodeObject == YesNoConfirmationFragmentRequestCode.SaveAsZeusDraft ||
                requestCodeObject == YesNoConfirmationFragmentRequestCode.SubmitFinalReport) {

            hideFragmentPrompt();
            if (event == YesNoConfirmationFragment.Event.NO) {
                return;
            }

            final Draft draft = (Draft) context;
            handleSaveDraftRequest(draft, requestCodeObject);
            showScreen(ScreenOption.Jobs);
            refreshSideMenu(null);

        }
        else if (requestCodeObject == YesNoConfirmationFragmentRequestCode.CloneInspectionSection) {
            hideFragmentPrompt();
            if (event == YesNoConfirmationFragment.Event.NO) {
                return;
            }
            InspectionFragment inspectionFragment = (InspectionFragment) mSessionUtil.getFragmentActive();
            InspectionSection inspectionSection = (InspectionSection) context;
            inspectionFragment.cloneInspectionSection(inspectionSection);
            Draft draft = mSessionUtil.getDraftInEdit();
            mDataService.saveDraft(draft);
            refreshSideMenu(draft);
        }
        else if (requestCodeObject == YesNoConfirmationFragmentRequestCode.RemoveCloneInspectionSection) {
            hideFragmentPrompt();
            if (event == YesNoConfirmationFragment.Event.NO) {
                return;
            }
            InspectionFragment inspectionFragment = (InspectionFragment) mSessionUtil.getFragmentActive();
            InspectionSection inspectionSection = (InspectionSection) context;
            inspectionFragment.removeInspectionSection(inspectionSection);
            Draft draft = mSessionUtil.getDraftInEdit();
            mDataService.saveDraft(draft);
            refreshSideMenu(draft);
        }

    }

    private void handleSaveDraftRequest(final Draft draft, YesNoConfirmationFragmentRequestCode requestCode) {

        boolean shouldSkipUpload = false;
        // initiate manual modal upload attempt
        if (!SynchronisedService.isServerActivityInProgress.compareAndSet(false,true)) {
            Toast.makeText(this,"Another data synchronisation activity is in progress. Draft (" + draft.getCode() + ") saved offline.", Toast.LENGTH_SHORT).show();
            shouldSkipUpload = true;
        }

        if (!shouldSkipUpload) DataSyncJob.broadcast("Uploading draft (" + draft.getCode() + ").", this);

        final WeakReference<MainActivity> weakSelf = new WeakReference<MainActivity>(this);
        DataService.UploadCallback uploadCallback = new DataService.UploadCallback() {
            @Override
            public void onSuccess() {
                Toast.makeText(weakSelf.get(), "Draft (" + draft.getCode() + ") uploaded successfully.", Toast.LENGTH_SHORT).show();
                SynchronisedService.isServerActivityInProgress.set(false);
                DataSyncJob.broadcast("", weakSelf.get());
            }

            @Override
            public void onEror(Throwable e) {
                String reason = " (" + e.getMessage() + ")";
                Toast.makeText(weakSelf.get(), "Failed to upload draft (" + draft.getCode() + "). Reason: " + reason + ". Upload will be reattempted at next data sync interval.", Toast.LENGTH_SHORT).show();
                SynchronisedService.isServerActivityInProgress.set(false);
                DataSyncJob.broadcast("", weakSelf.get());
            }
        };
        if (requestCode == YesNoConfirmationFragmentRequestCode.SaveAsZeusDraft) mDataService.saveZeusDraft(draft, uploadCallback, shouldSkipUpload);
        else if (requestCode == YesNoConfirmationFragmentRequestCode.SubmitFinalReport) mDataService.submitFinalReport(draft, uploadCallback, shouldSkipUpload);
        else if (requestCode == YesNoConfirmationFragmentRequestCode.SaveAsAtlasDraft) mDataService.saveAtlasDraft(draft, uploadCallback, shouldSkipUpload);
    }

    @Override
    public void onFragmentInteraction(MessagePromptFragment.Event event) {
        if (event == MessagePromptFragment.Event.OK) hideFragmentPrompt();
    }

    public void onFragmentInteraction(ForgotPasswordFragment.Event event, Object data) {
        hideFragmentPrompt();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        if (event == ForgotPasswordFragment.Event.SUBMIT) {

            showProgress("Please wait", "Submitting forgot password request", true);
            HashMap<String,String> localData = (HashMap<String, String>)data;
            String username = localData.get("username");
            String email = localData.get("email");
            final WeakReference<MainActivity> weakSelf = new WeakReference<MainActivity>(this);
            mGenieService.forgotPassword(username, email, new GenieServiceCallback<GenieServiceResponse<String>>() {
                @Override
                public void completed(GenieServiceResponse<String> response) {
                    if (response.isSuccess()) {
                        Toast.makeText(weakSelf.get(), "Forgot password requested", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(weakSelf.get(), response.getErrorMessage(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                }
            });

        }
    }


    @Override
    public void onFragmentInteraction(SettingsFragment.Event event) {
        hideFragmentPrompt();

        if (event == SettingsFragment.Event.REMOVE_ALL_USER_DATA_REQUESTED) {

            final WeakReference<MainActivity> weakSelf = new WeakReference<MainActivity>(this);
            Single single = Single.fromCallable(new Callable<Boolean>() {

                @Override
                public Boolean call() throws Exception {

                    mDataService.removeAll(Job.class);
                    List<Draft> draftsToBeRemoved = mDataService.getList(new DataService.GetCallback<Draft>() {
                        @Override
                        public QueryBuilder<Draft, ?> preQuery(QueryBuilder<Draft, ?> queryBuilder) {
                            try {
                                String username = mSharedPreferencesService.getUsername();
                                Where where = queryBuilder.where();
                                where.eq("createdByUsername", username).or().eq("createdByUsername", "");
                            } catch (Exception e) {
                                Log.e("Atlas - Draft list", e.getLocalizedMessage());
                            }
                            return queryBuilder.orderBy("modifiedTime", false);
                        }
                    }, Draft.class);
                    mDataService.remove(draftsToBeRemoved, Draft.class);

                    mDataService.removeAll(Inspection.class);
                    mDataService.removeAll(RecentInspection.class);

                    for (Draft draft : draftsToBeRemoved) {
                        mDataService.remove(draft.getId(), InspectionSectionImageUploadLog.class);
                        mDataService.remove(draft.getId(), InspectionImageUploadLog.class);
                    }

                    mSharedPreferencesService.saveUsername(null);
                    mSharedPreferencesService.savePassword(null);

                    return true;
                }
            });


            showProgress("Please wait", "Remove All User Data", true);
            single.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            DisposableManager.add(d);
                        }

                        @Override
                        public void onSuccess(Object value) {
                            hideModalProgress();
                            logout();
                        }

                        @Override
                        public void onError(Throwable e) {
                            hideModalProgress();
                            logout();
                        }
                    });
        }
    }

    //region Progress
    private void showProgress(String title, String message, boolean isModal) {
        if (isModal) showModalProgress(title, message);
        else DataSyncJob.broadcast(message, this);
    }

    private void showModalProgress(String title, String message) {
        ProgressDialog progressDialog = mProgressDialog != null ? mProgressDialog : new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setTitle(title);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        mProgressDialog = progressDialog;
    }

    private void hideProgress() {
        showProgress("", "", false);
        hideModalProgress();
    }

    private void hideModalProgress() {
        if (mProgressDialog == null) return;
        mProgressDialog.dismiss();
        mProgressDialog = null;
    }
    //endregion

    //region DataIntentServiceReceiver
    @Override
    public void onReceive(DataIntentServiceReceiver.Event event, String message) {
        boolean isActivityInProgress = event == DataIntentServiceReceiver.Event.ACTIVITY_START;
        mViewGroupActivityOverlay.setVisibility(isActivityInProgress ? View.VISIBLE : View.GONE);
        mTextViewActivityOverlayMessage.setText(message);

        if (event == DataIntentServiceReceiver.Event.ACTIVITY_END) {
            refreshUI();
        }
    }
    //endregon

    private void refreshUI() {
        if (mSessionUtil.getFragmentActive() instanceof JobListFragment) {
            showScreen(ScreenOption.Jobs);
        }
        else if (mSessionUtil.getFragmentActive() instanceof DraftListFragment) {
            showScreen(ScreenOption.Drafts);
        }
        else if (mSessionUtil.getFragmentActive() instanceof RecentInspectionListFragment) {
            showScreen(ScreenOption.RecentInspections);
        }
    }

    private void showScreen(ScreenOption screenOption) {
        if (screenOption.value == ScreenOption.Jobs.value) {
            hideFragment(mSessionUtil.getFragmentActive());
            List<Job> jobs = mDataService.getList(new DataService.GetCallback<Job>() {
                @Override
                public QueryBuilder<Job, ?> preQuery(QueryBuilder<Job, ?> queryBuilder) {
                    return queryBuilder.orderBy("timeSince1stJan1970", true);
                }
            }, Job.class);
            List<CalendarItem> calendarItems = mDataService.getList(null, CalendarItem.class);
            JobListFragment fragment = JobListFragment.newInstance(1, new ArrayList<Job>(jobs), calendarItems);
            showFragment(fragment, "Jobs", true);
        }
        else if (screenOption.value == ScreenOption.Drafts.value) {
            hideFragment(mSessionUtil.getFragmentActive());
            refreshDraftList();
        }
        else if (screenOption.value == ScreenOption.RecentInspections.value) {
            hideFragment(mSessionUtil.getFragmentActive());
            List<RecentInspection> recentInspections = mDataService.getList(new DataService.GetCallback<RecentInspection>() {
                @Override
                public QueryBuilder<RecentInspection, ?> preQuery(QueryBuilder<RecentInspection, ?> queryBuilder) {
                    return queryBuilder.orderBy("date", true);
                }
            }, RecentInspection.class);
            RecentInspectionListFragment fragment = RecentInspectionListFragment.newInstance(new ArrayList<RecentInspection>(recentInspections));
            showFragment(fragment, "Recent Inspections", true);
        }
        refreshActionBarMenu();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if (grantResults.length == 0
                || grantResults[0] !=
                PackageManager.PERMISSION_GRANTED) {

            Log.i("Atlas", "Photo storage permission has been denied by user");
            return;

        } else {
            Log.i("Atlas", "Photo storage permission has been granted by user");
        }

        Fragment fragment = mSessionUtil.getFragmentActive();
        if (!(fragment instanceof InspectionFragment)) return;
        InspectionFragment inspectionFragment = (InspectionFragment) mSessionUtil.getFragmentActive();


        boolean isTakeAPhoto = requestCode == MediaUtil.PermissionRequestCode.REQUEST_WRITE_STORAGE_TAKE_A_PHOTO.getCode();
        boolean isPickAPhoto = requestCode == MediaUtil.PermissionRequestCode.REQUEST_WRITE_STORAGE_PICK_A_PHOTO.getCode();
        if (isTakeAPhoto) {
            inspectionFragment.invokeCamera();
        }
        else if (isPickAPhoto) {
            inspectionFragment.invokePhotoLibrary();
        }
    }


    private void setActionAndStatusBarColor(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor((new ColorDrawable(color).getColor()));
        }
    }


    //region Private Helper functions

    /**
     * Returns an observable that saves an InspectionSectionImageUploadLog and attempts to upload the photo depending on the sharedPreferencesService getShouldUploadPhotosWhenOnline setting.
     * @param inspectionSectionImage
     * @param inspectionSection
     * @param draftCode
     * @param activityContext
     * @param dataService
     * @param sharedPreferencesService
     * @return
     */
    // TODO: Code refactor - apply this function in MainActivity where sectionImage uploadImage is used
    private Completable tryImageUpload(InspectionSectionImage inspectionSectionImage,
                                   InspectionSection inspectionSection,
                                   String draftCode,
                                   Context activityContext,
                                   DataService dataService,
                                   SharedPreferencesService sharedPreferencesService) {

        WeakReference<Context> weakContext = new WeakReference<>(activityContext);
        WeakReference<DataService> weakDataService = new WeakReference<>(dataService);
        WeakReference<SharedPreferencesService> weakSharedPreference = new WeakReference<>(sharedPreferencesService);

        return Completable.create(subscriber -> {

            final InspectionSectionImageUploadLog inspectionSectionImageUploadLog =
                    weakDataService
                            .get()
                            .getInspectionSectionImageUploadLog(inspectionSectionImage, inspectionSection);

            boolean shouldSkipUploadPhoto = !weakSharedPreference
                                                    .get()
                                                    .getShouldUploadPhotosWhenOnline();

            if (shouldSkipUploadPhoto) {
                Toast.makeText(weakContext.get(), "Skipping image upload as per confguration in settings.", Toast.LENGTH_SHORT).show();
                subscriber.onComplete();
                return;
            }

            Toast.makeText(weakContext.get(), "Uploading image to inspection (" + draftCode + ") section (" + inspectionSection.getName() + ").", Toast.LENGTH_SHORT).show();

            weakDataService
                    .get()
                    .uploadImage(inspectionSectionImageUploadLog, new DataService.UploadCallback() {
                @Override
                public void onSuccess() {
                    String messageSuffix = !inspectionSectionImageUploadLog.isForDeletion() ?  "uploaded." : "deleted";
                    Toast.makeText(weakContext.get(), "Image successfully " + messageSuffix, Toast.LENGTH_SHORT).show();
                    subscriber.onComplete();
                }

                @Override
                public void onEror(Throwable e) {
                    String message = !inspectionSectionImageUploadLog.isForDeletion()
                            ? "Failed to upload image (" + e.getMessage() + ")."
                            : "Failed to request image deletion (" + e.getMessage() + ").";
                    Toast.makeText(weakContext.get(), message, Toast.LENGTH_SHORT).show();
                    subscriber.onComplete();
                }
            });

        });

    }

    //endregion

}
