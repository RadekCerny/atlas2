package au.com.houspect.atlas.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;

import java.lang.ref.WeakReference;

import au.com.houspect.atlas.R;
import au.com.houspect.atlas.adapters.InspectionEndSectionItemRecyclerViewAdapter;
import au.com.houspect.atlas.adapters.RecommendationSummaryRecyclerViewAdapter;
import au.com.houspect.atlas.databinding.FragmentCommentBinding;
import au.com.houspect.atlas.databinding.FragmentInspectionRecommendationSummaryBinding;
import au.com.houspect.atlas.databinding.FragmentRecommendationBinding;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.services.DataService;
import au.com.houspect.atlas.services.DataServiceImpl;
import au.com.houspect.atlas.services.GenieService;
import au.com.houspect.atlas.services.GenieServiceImpl;
import au.com.houspect.atlas.services.SharedPreferencesService;
import au.com.houspect.atlas.services.SharedPreferencesServiceImpl;
import au.com.houspect.atlas.utilities.Util;

//import com.badoualy.stepperindicator.StepperIndicator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InspectionRecommendationSummaryFragment.OnListFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InspectionRecommendationSummaryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InspectionRecommendationSummaryFragment
        extends Fragment
        implements RecommendationSummaryRecyclerViewAdapter.Listener {

    public static class Event {
        private int mType;
        public Event(int type) {
            mType = type;
        }

        public static InspectionRecommendationSummaryFragment.Event NEXT = new InspectionRecommendationSummaryFragment.Event(2000);
        public static InspectionRecommendationSummaryFragment.Event BACK = new InspectionRecommendationSummaryFragment.Event(2001);
        public static InspectionRecommendationSummaryFragment.Event SELECT = new InspectionRecommendationSummaryFragment.Event(2002);
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_INSPECTION_ID = "inspectionId";

    private Inspection mInspection;

    private OnListFragmentInteractionListener mListener;

    private Button mButtonNext;
    private Button mButtonBack;

    private RecyclerView mRecyclerViewInspectionSectionRecommendationSummary;


    public InspectionRecommendationSummaryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param inspectionId
     * @return A new instance of fragment InspectionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InspectionRecommendationSummaryFragment newInstance(String inspectionId) {
        InspectionRecommendationSummaryFragment fragment = new InspectionRecommendationSummaryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_INSPECTION_ID, inspectionId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // Note:: certain device cannot handle storage of Inspection in bundle, hence store inspectionId instead. Revise this design in the future.

            String inspectionId = getArguments().getString(ARG_INSPECTION_ID);

            // TODO:: dependency inject
            GenieService genieService = new GenieServiceImpl();
            SharedPreferencesService sharedPreferencesService = new SharedPreferencesServiceImpl(getContext());
            DataService dataService = new DataServiceImpl(getContext(), genieService, sharedPreferencesService);
            mInspection = dataService.get(inspectionId, Draft.class);
            try {
                Util.parseInspectionXml(mInspection.getXml(), mInspection);
            } catch (Exception e) {
                Log.e("SideMenu", e.getLocalizedMessage());
            }

            // End Note
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewDataBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_inspection_recommendation_summary, container, false);
        View view = binding.getRoot();
        ((FragmentInspectionRecommendationSummaryBinding)binding).setInspection(mInspection);

        final WeakReference weakSelf = new WeakReference(this);

        // Inflate the layout for this fragment
        mButtonNext = (Button) view.findViewById(R.id.buttonNext);
        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                mListener.onListFragmentInteraction((InspectionRecommendationSummaryFragment) weakSelf.get(), InspectionRecommendationSummaryFragment.Event.NEXT, mInspection, null, -1);
            }
        });
        mButtonBack = (Button) view.findViewById(R.id.buttonBack);
        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                mListener.onListFragmentInteraction((InspectionRecommendationSummaryFragment) weakSelf.get(), InspectionRecommendationSummaryFragment.Event.BACK, mInspection, null, -1);
            }
        });
        mRecyclerViewInspectionSectionRecommendationSummary = (RecyclerView) view.findViewById(R.id.recyclerViewInspectionRecommendationSummary);
        RecommendationSummaryRecyclerViewAdapter adapter = new RecommendationSummaryRecyclerViewAdapter(mInspection.getSections(), this, R.layout.fragment_inspection_section_recommendation_item);
        mRecyclerViewInspectionSectionRecommendationSummary.setAdapter(adapter);

        return view;
    }

    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(InspectionRecommendationSummaryFragment source, Event event, Inspection inspection, InspectionSection inspectionSection, int sectionIndex);
    }

    /**
     * This fragment does not hold the same Draft object instance that holds the InspectionSection referenced in (input-centric) InspectionSectionFragment.
     * However, there is possible input from end user - global recommendations.
     * Copy the input changes to the target draft i.e. usually the Draft in the InspectionFragment - the original object instance that leads to this fragment.
     * @param draft
     */
    public void copyChangesTo(Draft draft) {
        draft.setRecommendations(mInspection.getRecommendations());
    }


    //region RecommendationSummaryRecyclerViewAdapter.Listener
    @Override
    public void onRecommendationSummaryHeaderSelected(InspectionSection selectedInspectionSection, int sectionIndex) {
        mListener.onListFragmentInteraction(this, InspectionRecommendationSummaryFragment.Event.SELECT, null, selectedInspectionSection, sectionIndex);
    }
    //endregion
}
