package au.com.houspect.atlas.services;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by alfredwong on 12/3/17.
 */

public class DataSyncJobCreator implements JobCreator {
    @Override
    public Job create(String tag) {
        switch (tag) {
            case DataSyncJob.TAG:
                return new DataSyncJob();
            default:
                return null;
        }
    }
}
