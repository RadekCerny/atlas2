package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.houspect.atlas.models.InspectionSectionItem;
import au.com.houspect.atlas.models.InspectionSectionItemTwoNumbers;
import au.com.houspect.atlas.viewmodels.InspectionSectionItemTwoNumbersViewModel;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class InspectionSectionItemTwoNumbersViewModelUnitTest {

    private InspectionSectionItemTwoNumbers mandatoryItem;
    //private InspectionSectionItem optionalItem;
    //private InspectionSectionItemRecyclerViewAdapter.Listener listener;

    @Before
    public void setup() {

        mandatoryItem = new InspectionSectionItemTwoNumbers();
        mandatoryItem.resetOtherNa();
        mandatoryItem.setNaText("test na text");
        mandatoryItem.setMandatory(true);

        //optionalItem = new InspectionSectionItem();
        //optionalItem.resetOtherNa();;
        //optionalItem.setNaText("test na text");
        //optionalItem.setMandatory(false);

        //listener = new InspectionSectionItemRecyclerViewAdapter.Listener() {
        //    @Override
        //    public void onRequestForOtherNaOptions(InspectionSectionItemViewModel viewModel) {

        //    }
        //};
    }

    /**
     * Ensure isCalculateTotalChecked = true if model.number1 and model.number1 are empty.
     */
    @Test
    public void testConstructor_isCalculateTotalChecked() {
        boolean expected = true;
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        Assert.assertEquals(expected, sut.isCalculateTotalChecked.get());
    }

    /**
     * Ensure isCalculateTotalChecked = false if model.number1 == "na".
     */
    @Test
    public void testConstructor_isCalculateTotalChecked_false() {
        mandatoryItem.setNumber1("na");
        boolean expected = false;
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        Assert.assertEquals(expected, sut.isCalculateTotalChecked.get());
    }

    /**
     * Ensure isCalculateTotalChecked = false if model.number1 == "na" && model.number2 not empty.
     */
    @Test
    public void testConstructor_isCalculateTotalChecked_false_naNumber1_and_notEmptyNumber2() {
        mandatoryItem.setNumber1("na");
        mandatoryItem.setNumber2("230");
        boolean expected = false;
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        Assert.assertEquals(expected, sut.isCalculateTotalChecked.get());
    }

    /**
     * Ensure isCalculateTotalChecked = true if model.number1 is empty && model.number2 is NOT empty.
     */
    @Test
    public void testConstructor_isCalculateTotalChecked_true_emptyNumber1_and_notEmptyNumber2() {
        mandatoryItem.setNumber1("");
        mandatoryItem.setNumber2("123");
        boolean expected = true;
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        Assert.assertEquals(expected, sut.isCalculateTotalChecked.get());
    }

    /**
     * Ensure isCalculateTotalChecked = true if model.number1 is not empty (& not "na") && model.number2 is empty.
     */
    @Test
    public void testConstructor_isCalculateTotalChecked_true_notEmptyNumber1_and_notNaNumber1_and_emptyNumber2() {
        mandatoryItem.setNumber1("1235");
        mandatoryItem.setNumber2(null);
        boolean expected = true;
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        Assert.assertEquals(expected, sut.isCalculateTotalChecked.get());
    }

    /**
     * Ensure observableNumber1 and observableNumber2 are set to "" if model number1 and number2 are not numeric.
     */
    @Test
    public void testConstructor_observableNumbers_empty() {
        mandatoryItem.setNumber1("abc");
        mandatoryItem.setNumber2("123abs");
        String expected = "";
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        Assert.assertEquals(expected, sut.observableNumber1.get());
        Assert.assertEquals(expected, sut.observableNumber2.get());
    }

    /**
     * Ensure observableNumber1 and observableNumber2 are set to "" if model number1 and number2 are not numeric.
     */
    @Test
    public void testConstructor_observableNumber_numeric() {
        mandatoryItem.setNumber1("123");
        mandatoryItem.setNumber2("567");
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        Assert.assertEquals("123", sut.observableNumber1.get());
        Assert.assertEquals("567", sut.observableNumber2.get());
    }

    /**
     * Ensure observableTotal is set correctly to model.result
     */
    @Test
    public void testConstructor_observableTotal() {
        mandatoryItem.setResult("256");
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        Assert.assertEquals("256", sut.observableTotal.get());
    }

    /**
     * Ensure observableTotal is set correctly to model.result
     */
    @Test
    public void testConstructor_observableTotal_null() {
        mandatoryItem.setResult(null);
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        Assert.assertEquals(null, sut.observableTotal.get());
    }

    /**
     * Ensure model values and observableTotal are updated correctly for case when isCalculateTotalChecked = true.
     */
    @Test
    public void testUpdateModelValues_modelValuesUpdated_isCalculateTotalChecked() {
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        sut.isCalculateTotalChecked.set(true);
        sut.observableNumber1.set("123");
        sut.updateModelValues();

        Assert.assertEquals("123", sut.observableTotal.get());
        Assert.assertEquals("123", sut.getModel().getResult());

        sut.observableNumber2.set("2.5");
        sut.updateModelValues();

        Assert.assertEquals("307.5", sut.observableTotal.get());
        Assert.assertEquals("307.5", sut.getModel().getResult());

    }

    /**
     * Ensure model values and observableTotal are updated correctly for case when isCalculateTotalChecked = false.
     */
    @Test
    public void testUpdateModelValues_modelValuesUpdated_isCalculateTotalChecked_false() {
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);
        sut.isCalculateTotalChecked.set(false);
        sut.updateModelValues();

        Assert.assertEquals(null, sut.observableTotal.get());
        Assert.assertEquals(null, sut.getModel().getResult());

        sut.observableTotal.set("20.5");

        Assert.assertEquals("na", sut.getModel().getNumber1());
        Assert.assertEquals("20.5", sut.getModel().getNumber2());
        Assert.assertEquals("20.5", sut.observableTotal.get());
        Assert.assertEquals("20.5", sut.getModel().getResult());

        sut.observableNumber1.set("123");
        sut.observableNumber2.set("234");

        // ignore the observableNumber values as isCalculateTotalChecked = false

        Assert.assertEquals("na", sut.getModel().getNumber1());
        Assert.assertEquals("20.5", sut.getModel().getNumber2());
        Assert.assertEquals("20.5", sut.observableTotal.get());
        Assert.assertEquals("20.5", sut.getModel().getResult());

    }

    /**
     * Ensure correctness of onCalculateTotalCheckedChanged function.
     */
    @Test
    public void testOnCalculateTotalCheckedChanged_not_checked() {
        boolean checked = false;
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);

        String expectedNumber1 = "";
        String expectedNumber2 = "";
        String expectedTotal = "0";
        sut.observableNumber1.set("123");
        sut.observableNumber2.set("10");
        sut.observableTotal.set("1230");

        sut.onCalculateTotalCheckedChanged(null, checked);

        Assert.assertEquals(expectedNumber1, sut.observableNumber1.get());
        Assert.assertEquals(expectedNumber2, sut.observableNumber2.get());


        // Even after observableTotal.set("") - onObservablePropertyChangedCallback recalculates the total to be "0"
        Assert.assertEquals(expectedTotal, sut.observableTotal.get());

    }

    /**
     * Ensure correctness of onCalculateTotalCheckedChanged function.
     */
    @Test
    public void testOnCalculateTotalCheckedChanged_checked() {
        boolean checked = true;
        InspectionSectionItemTwoNumbersViewModel sut = new InspectionSectionItemTwoNumbersViewModel(mandatoryItem);

        String expectedNumber1 = "123";
        String expectedNumber2 = "10";
        String expectedTotal = "1230";
        sut.observableNumber1.set("123");
        sut.observableNumber2.set("10");
        sut.observableTotal.set("1230");

        sut.onCalculateTotalCheckedChanged(null, checked);

        Assert.assertEquals(expectedNumber1, sut.observableNumber1.get());
        Assert.assertEquals(expectedNumber2, sut.observableNumber2.get());

        // Even after observableTotal.set("") - onObservablePropertyChangedCallback recalculates the total to be "1230"
        Assert.assertEquals(expectedTotal, sut.observableTotal.get());

    }


}