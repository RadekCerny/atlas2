package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import au.com.houspect.atlas.models.CalendarItem;
import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.ITemporal;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionItem;
import au.com.houspect.atlas.models.InspectionSectionItemTwoNumbers;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.utilities.Util;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class UtilUnitTest {

    @Test
    public void parseInspections() throws Exception {
        String xml = UnitTestUtil.getXmlStringFromTestFile(this, "inspections.xml");
        Inspection x = new Inspection();
        List<Inspection> inspections = Util.parseInspectionsXml(xml);
    }

    @Test
    public void testRx() throws Exception {

        Single single = Single.create(subscriber -> {
           System.out.println("Subscriber Thread (" + Thread.currentThread().getName() + ") Date time " + (new Date()).getTime() + " (" + new Date() + ")");
           Thread.sleep(5000);
            System.out.println("Subscriber Thread (after sleep) (" + Thread.currentThread().getName() + ") Date time " + (new Date()).getTime() + " (" + new Date() + ")");

            subscriber.onSuccess(Thread.currentThread());
        });

        System.out.println("Calling Thread (" + Thread.currentThread().getName() + ") Date time " + (new Date()).getTime() + " (" + new Date() + ")");

        CountDownLatch signal = new CountDownLatch(1);
        single.subscribeOn(Schedulers.io()).subscribe(result -> {
            System.out.println("Observer Thread (" + Thread.currentThread().getName() + ") Date time " + (new Date()).getTime() + " (" + new Date() + ")");
            signal.countDown();
        });

        System.out.println("End Calling Thread (1) (" + Thread.currentThread().getName() + ") Date time " + (new Date()).getTime() + " (" + new Date() + ")");
        signal.await(30, TimeUnit.SECONDS);
        System.out.println("End Calling Thread (2) (" + Thread.currentThread().getName() + ") Date time " + (new Date()).getTime() + " (" + new Date() + ")");
    }

    @Test
    public void parseInspectionXml() throws Exception {

        // Test parseInspectionXml
        String xml = UnitTestUtil.getXmlStringFromTestFile(this, "test.xml");
        Inspection x = new Inspection();
        Util.parseInspectionXml(xml, x);

        //region - Assert Inspection level properties
        Assert.assertEquals("1", x.getId());
        Assert.assertNotNull(x.getImage());
        Assert.assertEquals("Cover66298-3959-4081-a979-b161b547b7b4.jpg", x.getImage().getFileName());
        Assert.assertEquals("1", x.getImage().getId());
        Assert.assertEquals("IN00001N", x.getCode());
        Assert.assertEquals("", x.getSubmit());
        Assert.assertEquals("Test recommendations", x.getRecommendations());
        Assert.assertEquals("203 alfred st Dee Why NSW 2099", x.getAddress());
        Assert.assertEquals(-33.852, x.getLatitude());
        Assert.assertEquals(151.211, x.getLongitude());
        Assert.assertEquals("** Short Test (Workflow) **", x.getInspectionReportType());
        Assert.assertEquals("10/05/2017", x.getDate());
        Assert.assertEquals("8:30", x.getTime());
        Assert.assertEquals("7", x.getDuration());
        Assert.assertEquals("Test Job Notes 123. Client ABC.", x.getJobNotes());
        Assert.assertEquals("Owner", x.getOccupancy());
        Assert.assertEquals(4, x.getNoOfBedrooms());
        Assert.assertEquals(2, x.getNoOfBathrooms());
        Assert.assertEquals("2", x.getPropertyLevels());
        Assert.assertEquals("Freestanding Garage, Granny flat", x.getPropertyFeatures());
        Assert.assertEquals("12", x.getNumberOfUnits());
        Assert.assertNotNull(x.getClient());
        Assert.assertEquals("Olivia Pope", x.getClient().getName());
        Assert.assertEquals("2 Davidson Avenue Warrawee NSW 2074", x.getClient().getAddress());
        Assert.assertEquals("o@pope.com", x.getClient().getEmailAddress());
        Assert.assertEquals("121321362186", x.getClient().getMobilePhone());
        Assert.assertEquals("972323", x.getClient().getPhone());
        Assert.assertNotNull(x.getContacts());
        Assert.assertEquals(1, x.getContacts().size());
        Assert.assertEquals("Celeste Wright", x.getContacts().get(0).getName());
        Assert.assertEquals("30 Chilton Parade", x.getContacts().get(0).getAddress());
        Assert.assertEquals("celeste@wright.com", x.getContacts().get(0).getEmailAddress());
        Assert.assertEquals("0635776353", x.getContacts().get(0).getMobilePhone());
        Assert.assertEquals("0273192372", x.getContacts().get(0).getPhone());
        Assert.assertEquals("Vendor", x.getContacts().get(0).getRole().getId());
        Assert.assertEquals("OrderContactRole", x.getContacts().get(0).getRole().getGroup());
        Assert.assertEquals("Vendor", x.getContacts().get(0).getRole().getDescription());
        Assert.assertEquals("123-ext", x.getTheirJobNo());
        //endregion

        //region - Test InspectionSection Item properties
        Assert.assertEquals(x.getSections().size(), 53);
        Assert.assertEquals("6", x.getSections().get(0).getId());
        Assert.assertEquals(false, x.getSections().get(0).isNa());
        Assert.assertEquals("General Description", x.getSections().get(0).getHeading());
        Assert.assertNotNull(x.getSections().get(0).getImages());
        Assert.assertEquals(3, x.getSections().get(0).getImages().size());
        Assert.assertEquals("ebd66298-3959-4081-a979-b161b547b7b4.jpg", x.getSections().get(0).getImages().get(0).getFileName());
        Assert.assertEquals("fbda7474-5565-4342-80d0-939f42eebdff.jpg", x.getSections().get(0).getImages().get(1).getFileName());
        Assert.assertEquals("label2", x.getSections().get(0).getImages().get(1).getLabel());
        Assert.assertEquals("82667027-8afc-4ced-b66e-22ef821db709.jpg", x.getSections().get(0).getImages().get(2).getFileName());
        Assert.assertEquals("1", x.getSections().get(0).getImages().get(0).getId());
        Assert.assertEquals("2", x.getSections().get(0).getImages().get(1).getId());
        Assert.assertEquals("3", x.getSections().get(0).getImages().get(2).getId());
        Assert.assertEquals("1", x.getSections().get(0).getKey());
        Assert.assertNull(x.getSections().get(0).getRepeatingKey());
        Assert.assertNull(x.getSections().get(0).getRepeatingSeq());
        Assert.assertEquals(x.getId(), x.getSections().get(0).getParentInspectionId());
        Assert.assertEquals(x.getId(), x.getSections().get(1).getParentInspectionId());
        Assert.assertEquals(x.getId(), x.getSections().get(2).getParentInspectionId());
        Assert.assertEquals(x.getCode(), x.getSections().get(0).getParentInspectionCode());
        Assert.assertEquals(x.getCode(), x.getSections().get(1).getParentInspectionCode());
        Assert.assertEquals(x.getCode(), x.getSections().get(2).getParentInspectionCode());

        // ensure correct parsing of attribute mandatory="Yes"
        InspectionSectionItem x_item1_1 = x.getSections().get(0).getItems().get(0);
        Assert.assertEquals(true, x_item1_1.isMandatory());
        Assert.assertEquals("Enum",x_item1_1.getResultType());
        Assert.assertEquals("The building is a [VAR].", x_item1_1.getText());
        Assert.assertEquals("There is no building", x_item1_1.getNaText());
        Assert.assertEquals("1650", x_item1_1.getResult());
        Assert.assertEquals("", x_item1_1.getResultComments());
        Assert.assertEquals(6, x_item1_1.getOptions().size());
        Assert.assertEquals("1649", x_item1_1.getOptions().get(0).getId());
        Assert.assertEquals("single storey house", x_item1_1.getOptions().get(0).getText());
        Assert.assertEquals("1650", x_item1_1.getOptions().get(1).getId());
        Assert.assertEquals("two storey house", x_item1_1.getOptions().get(1).getText());
        Assert.assertEquals("1651", x_item1_1.getOptions().get(2).getId());
        Assert.assertEquals("three storey house", x_item1_1.getOptions().get(2).getText());
        Assert.assertEquals("1652", x_item1_1.getOptions().get(3).getId());
        Assert.assertEquals("unit", x_item1_1.getOptions().get(3).getText());
        Assert.assertEquals("1653", x_item1_1.getOptions().get(4).getId());
        Assert.assertEquals("semi-detached house", x_item1_1.getOptions().get(4).getText());
        Assert.assertEquals("1654", x_item1_1.getOptions().get(5).getId());
        Assert.assertEquals("duplex", x_item1_1.getOptions().get(5).getText());
        // default mandatory to true if mandatory attribute is missing
        Assert.assertEquals(true, x.getSections().get(0).getItems().get(1).isMandatory());
        // ensure correct parsing of attribute mandatory="No"
        Assert.assertEquals(false, x.getSections().get(0).getItems().get(2).isMandatory());

        // ensure correct parsing of NAText
        Assert.assertEquals("This is not a block of land.", x.getSections().get(0).getItems().get(2).getNaText());
        // ensure default NAText value for optional item if <NAText> is missing or empty from server
        Assert.assertEquals("NAText", x.getSections().get(0).getItems().get(3).getNaText());

        //region - Test TwoNumbers item
        List<InspectionSectionItem> x_section1_items = x.getSections().get(0).getItems();
        Assert.assertTrue("SectionItem of type TwoNumbers should be parsed as InspectionSectionItemTwoNumbers", x_section1_items.get(x_section1_items.size()-1) instanceof InspectionSectionItemTwoNumbers);
        InspectionSectionItemTwoNumbers x_lastItem = (InspectionSectionItemTwoNumbers)x_section1_items.get(x_section1_items.size()-1);

        Assert.assertEquals("34927", x_lastItem.getId());
        Assert.assertEquals(true, x_lastItem.isMandatory());
        Assert.assertEquals("square metres", x_lastItem.getLabel1());
        Assert.assertEquals("$/metre2", x_lastItem.getLabel2());
        Assert.assertEquals("4", x_lastItem.getNumber1());
        Assert.assertEquals("33.00", x_lastItem.getNumber2());
        Assert.assertEquals(false, x_lastItem.isNa());
        Assert.assertEquals("TwoNumbers", x_lastItem.getResultType());
        Assert.assertEquals("Preliminary Rectification Estimate", x_lastItem.getText());
        Assert.assertEquals("", x_lastItem.getNaText());
        Assert.assertEquals("33", x_lastItem.getResult());
        Assert.assertEquals("", x_lastItem.getResultComments());
        //endregion

        //endregion

        //region - Test InspectionSection repeating related attributes
        for (InspectionSection inspectionSection : x.getSections()) {
            if (inspectionSection.getName().equals("Bedroom2")) {
                Assert.assertEquals("1", inspectionSection.getRepeatingKey());
                Assert.assertEquals("2", inspectionSection.getRepeatingSeq());
            }
            else if (inspectionSection.getName().equals("Bedroom3")) {
                Assert.assertEquals("1", inspectionSection.getRepeatingKey());
                Assert.assertEquals("3", inspectionSection.getRepeatingSeq());
            }
        }
        //endregion

        // Test submit attribute persists over conversion
        x.setSubmit(Draft.SUBMIT_VALUE_ZEUS_DRAFT);

        //region - Test clone related attributes
        Assert.assertEquals(false, x.getSections().get(0).isAClone());
        Assert.assertEquals("", x.getSections().get(0).getInstanceName());
        Assert.assertEquals("", x.getSections().get(0).getCloneSourceName());
        Assert.assertEquals("", x.getSections().get(0).getItems().get(0).getSeedId());
        Assert.assertEquals(true, x.getSections().get(2).isAClone());
        Assert.assertEquals("abc", x.getSections().get(2).getInstanceName());
        Assert.assertEquals("Eaves", x.getSections().get(2).getCloneSourceName());
        Assert.assertEquals("457seedId", x.getSections().get(2).getItems().get(0).getSeedId());
        //endregion

        // Test toXmlDocument & xmlDocumentToString
        Document document = Util.toXmlDocument(x);
        String xml2 = Util.xmlNodeToString(document);

        Inspection y = new Inspection();
        Util.parseInspectionXml(xml2, y);

        //region - Assert Inspection level properties
        Assert.assertEquals("1", y.getId());
        Assert.assertEquals("Cover66298-3959-4081-a979-b161b547b7b4.jpg", y.getImage().getFileName());
        Assert.assertEquals("1", y.getImage().getId());
        Assert.assertEquals("IN00001N", y.getCode());
        Assert.assertEquals(Draft.SUBMIT_VALUE_ZEUS_DRAFT, y.getSubmit());
        Assert.assertEquals("Test recommendations", y.getRecommendations());
        Assert.assertEquals("203 alfred st Dee Why NSW 2099", y.getAddress());
        Assert.assertEquals(-33.852, y.getLatitude());
        Assert.assertEquals(151.211, y.getLongitude());
        Assert.assertEquals("** Short Test (Workflow) **", y.getInspectionReportType());
        Assert.assertEquals("10/05/2017", y.getDate());
        Assert.assertEquals("8:30", y.getTime());
        Assert.assertEquals("7", y.getDuration());
        Assert.assertEquals("Test Job Notes 123. Client ABC.", y.getJobNotes());
        Assert.assertEquals("Owner", y.getOccupancy());
        Assert.assertEquals(4, y.getNoOfBedrooms());
        Assert.assertEquals(2, y.getNoOfBathrooms());
        Assert.assertEquals("2", y.getPropertyLevels());
        Assert.assertEquals("Freestanding Garage, Granny flat", y.getPropertyFeatures());
        Assert.assertEquals("12", y.getNumberOfUnits());
        Assert.assertNotNull(y.getClient());
        Assert.assertEquals("Olivia Pope", y.getClient().getName());
        Assert.assertNotNull(y.getContacts());
        Assert.assertEquals(1, y.getContacts().size());
        Assert.assertEquals("Celeste Wright", y.getContacts().get(0).getName());
        Assert.assertEquals("30 Chilton Parade", y.getContacts().get(0).getAddress());
        Assert.assertEquals("celeste@wright.com", y.getContacts().get(0).getEmailAddress());
        Assert.assertEquals("0635776353", y.getContacts().get(0).getMobilePhone());
        Assert.assertEquals("0273192372", y.getContacts().get(0).getPhone());
        Assert.assertEquals("Vendor", y.getContacts().get(0).getRole().getId());
        Assert.assertEquals("OrderContactRole", y.getContacts().get(0).getRole().getGroup());
        Assert.assertEquals("Vendor", y.getContacts().get(0).getRole().getDescription());
        Assert.assertEquals("2 Davidson Avenue Warrawee NSW 2074", y.getClient().getAddress());
        Assert.assertEquals("o@pope.com", y.getClient().getEmailAddress());
        Assert.assertEquals("121321362186", y.getClient().getMobilePhone());
        Assert.assertEquals("972323", y.getClient().getPhone());
        Assert.assertEquals("123-ext", y.getTheirJobNo());
        //endregion

        //region - Assert InspectionSectionItem properties
        Assert.assertEquals(y.getSections().size(), 53);
        Assert.assertEquals("6", y.getSections().get(0).getId());
        Assert.assertEquals(false, y.getSections().get(0).isNa());
        Assert.assertEquals("General Description", y.getSections().get(0).getHeading());
        Assert.assertNotNull(y.getSections().get(0).getImages());
        Assert.assertEquals(3, y.getSections().get(0).getImages().size());
        Assert.assertEquals("ebd66298-3959-4081-a979-b161b547b7b4.jpg", y.getSections().get(0).getImages().get(0).getFileName());
        Assert.assertEquals("fbda7474-5565-4342-80d0-939f42eebdff.jpg", y.getSections().get(0).getImages().get(1).getFileName());
        Assert.assertEquals("label2", y.getSections().get(0).getImages().get(1).getLabel());
        Assert.assertEquals("82667027-8afc-4ced-b66e-22ef821db709.jpg", y.getSections().get(0).getImages().get(2).getFileName());
        Assert.assertEquals("1", y.getSections().get(0).getImages().get(0).getId());
        Assert.assertEquals("2", y.getSections().get(0).getImages().get(1).getId());
        Assert.assertEquals("3", y.getSections().get(0).getImages().get(2).getId());
        Assert.assertEquals("1", y.getSections().get(0).getKey());
        Assert.assertNull(y.getSections().get(0).getRepeatingKey());
        Assert.assertNull(y.getSections().get(0).getRepeatingSeq());
        Assert.assertEquals(y.getId(), y.getSections().get(0).getParentInspectionId());
        Assert.assertEquals(y.getId(), y.getSections().get(1).getParentInspectionId());
        Assert.assertEquals(y.getId(), y.getSections().get(2).getParentInspectionId());
        Assert.assertEquals(y.getCode(), y.getSections().get(0).getParentInspectionCode());
        Assert.assertEquals(y.getCode(), y.getSections().get(1).getParentInspectionCode());
        Assert.assertEquals(y.getCode(), y.getSections().get(2).getParentInspectionCode());

        // ensure correct serialisation of mandatory=true to attribute mandatory="Yes"
        InspectionSectionItem y_item1_1 = y.getSections().get(0).getItems().get(0);
        Assert.assertEquals(true, y_item1_1.isMandatory());
        Assert.assertEquals("Enum",y_item1_1.getResultType());
        Assert.assertEquals("The building is a [VAR].", y_item1_1.getText());
        Assert.assertEquals("There is no building", y_item1_1.getNaText());
        Assert.assertEquals("1650", y_item1_1.getResult());
        Assert.assertEquals("", y_item1_1.getResultComments());
        Assert.assertEquals(6, y_item1_1.getOptions().size());
        Assert.assertEquals("1649", y_item1_1.getOptions().get(0).getId());
        Assert.assertEquals("single storey house", y_item1_1.getOptions().get(0).getText());
        Assert.assertEquals("1650", y_item1_1.getOptions().get(1).getId());
        Assert.assertEquals("two storey house", y_item1_1.getOptions().get(1).getText());
        Assert.assertEquals("1651", y_item1_1.getOptions().get(2).getId());
        Assert.assertEquals("three storey house", y_item1_1.getOptions().get(2).getText());
        Assert.assertEquals("1652", y_item1_1.getOptions().get(3).getId());
        Assert.assertEquals("unit", y_item1_1.getOptions().get(3).getText());
        Assert.assertEquals("1653", y_item1_1.getOptions().get(4).getId());
        Assert.assertEquals("semi-detached house", y_item1_1.getOptions().get(4).getText());
        Assert.assertEquals("1654", y_item1_1.getOptions().get(5).getId());
        Assert.assertEquals("duplex", y_item1_1.getOptions().get(5).getText());

        // ensure correct serialisation of mandatory=true to attribute mandatory="Yes"
        Assert.assertEquals(true, y.getSections().get(0).getItems().get(1).isMandatory());
        // ensure correct serialisation of mandatory=false to attribute mandatory="No"
        Assert.assertEquals(false, y.getSections().get(0).getItems().get(2).isMandatory());

        // ensure correct parsing of NAText
        Assert.assertEquals("This is not a block of land.", y.getSections().get(0).getItems().get(2).getNaText());
        // ensure default NAText value for optional item if <NAText> is missing or empty from server
        Assert.assertEquals("NAText", y.getSections().get(0).getItems().get(3).getNaText());

        //region - Test TwoNumbers item
        List<InspectionSectionItem> y_section1_items = y.getSections().get(0).getItems();
        Assert.assertTrue(y_section1_items.get(y_section1_items.size()-1) instanceof InspectionSectionItemTwoNumbers);
        InspectionSectionItemTwoNumbers y_lastItem = (InspectionSectionItemTwoNumbers)y_section1_items.get(y_section1_items.size()-1);

        Assert.assertEquals("34927", y_lastItem.getId());
        Assert.assertEquals(true, y_lastItem.isMandatory());
        Assert.assertEquals("square metres", y_lastItem.getLabel1());
        Assert.assertEquals("$/metre2", y_lastItem.getLabel2());
        Assert.assertEquals("4", y_lastItem.getNumber1());
        Assert.assertEquals("33.00", y_lastItem.getNumber2());
        Assert.assertEquals(false, y_lastItem.isNa());
        Assert.assertEquals("TwoNumbers", y_lastItem.getResultType());
        Assert.assertEquals("Preliminary Rectification Estimate", y_lastItem.getText());
        Assert.assertEquals("", y_lastItem.getNaText());
        Assert.assertEquals("33", y_lastItem.getResult());
        Assert.assertEquals("", y_lastItem.getResultComments());
        //endregion

        //endregion

        //region - Test InspectionSection repeating related attributes
        for (InspectionSection inspectionSection : y.getSections()) {
            if (inspectionSection.getName().equals("Bedroom2")) {
                Assert.assertEquals("1", inspectionSection.getRepeatingKey());
                Assert.assertEquals("2", inspectionSection.getRepeatingSeq());
            }
            else if (inspectionSection.getName().equals("Bedroom3")) {
                Assert.assertEquals("1", inspectionSection.getRepeatingKey());
                Assert.assertEquals("3", inspectionSection.getRepeatingSeq());
            }
        }
        //endregion

        //region - Test clone related attributes
        Assert.assertEquals(false, y.getSections().get(0).isAClone());
        Assert.assertEquals("", y.getSections().get(0).getInstanceName());
        Assert.assertEquals("", y.getSections().get(0).getCloneSourceName());
        Assert.assertEquals("", y.getSections().get(0).getItems().get(0).getSeedId());
        Assert.assertEquals(true, y.getSections().get(2).isAClone());
        Assert.assertEquals("abc", y.getSections().get(2).getInstanceName());
        Assert.assertEquals("Eaves", y.getSections().get(2).getCloneSourceName());
        Assert.assertEquals("457seedId", y.getSections().get(2).getItems().get(0).getSeedId());
        //endregion
    }

    @Test
    public void xmlDocumentToString() throws Exception {

        File file = UnitTestUtil.getFileFromPath(this, "test.xml");
        FileInputStream fileInputStream = new FileInputStream(file);
        Reader reader = new InputStreamReader(fileInputStream, "UTF8");
        BufferedReader bufferedReader = new BufferedReader(reader);

        StringBuilder stringBuilder = new StringBuilder();
        String line = bufferedReader.readLine();
        while (line != null) {
            stringBuilder.append(line);
            line = bufferedReader.readLine();
        }

        String xml = stringBuilder.toString();
        bufferedReader.close();
        Inspection x = new Inspection();
        Util.parseInspectionXml(xml, x);


    }

    @Test
    public void parseJobsXml() throws Exception {
        File file = UnitTestUtil.getFileFromPath(this, "jobs2.xml");
        FileInputStream fileInputStream = new FileInputStream(file);
        Reader reader = new InputStreamReader(fileInputStream, "UTF8");
        BufferedReader bufferedReader = new BufferedReader(reader);

        StringBuilder stringBuilder = new StringBuilder();
        String line = bufferedReader.readLine();
        while (line != null) {
            stringBuilder.append(line);
            line = bufferedReader.readLine();
        }

        String xml = stringBuilder.toString();
        bufferedReader.close();
        List<Job> jobs = Util.parseJobsXml(xml);

        Assert.assertNotNull(jobs);
        Assert.assertEquals(jobs.size(),3);
        Assert.assertEquals(jobs.get(0).getCode(), "IN00014");
        Assert.assertEquals(jobs.get(0).getReportType(), "Short Test (Workflow)");
        Assert.assertEquals(jobs.get(0).getStreet(), "111 The Bulwalk");
        Assert.assertEquals(jobs.get(0).getSuburb(), "Castlecrag NSW 2068");
        Assert.assertEquals(jobs.get(0).getClientName(), "Jake Birtwhistle");
        Assert.assertEquals(jobs.get(0).getDate(), "13/04/2017");
        Assert.assertEquals(jobs.get(0).getTime(), "9:00");

        Assert.assertEquals(jobs.get(1).getCode(), "IN00016");
        Assert.assertEquals(jobs.get(1).getReportType(), "Pre-Purchase Building Report");
        Assert.assertEquals(jobs.get(1).getStreet(), "20 Eastern Road");
        Assert.assertEquals(jobs.get(1).getSuburb(), "Turramurra NSW 2074");
        Assert.assertEquals(jobs.get(1).getClientName(), "Richard Murray");
        Assert.assertEquals(jobs.get(1).getDate(), "14/04/2017");
        Assert.assertEquals(jobs.get(1).getTime(), "14:00");

        Assert.assertEquals(jobs.get(2).getCode(), "IN00015");
        Assert.assertEquals(jobs.get(2).getReportType(), "Short Test (Workflow)");
        Assert.assertEquals(jobs.get(2).getStreet(), "28 Holt Avenue");
        Assert.assertEquals(jobs.get(2).getSuburb(), "Mosman NSW 2088");
        Assert.assertEquals(jobs.get(2).getClientName(), "Richard Varga");
        Assert.assertEquals(jobs.get(2).getDate(), "21/04/2017");
        Assert.assertEquals(jobs.get(2).getTime(), "8:00");
    }

    @Test
    public void testSortTemporals_ascending() throws Exception {

        ITemporal t1 = new Job();
        ITemporal t2 = new CalendarItem();
        ITemporal t3 = new Job();
        ITemporal t4 = new Job();

        ((Job)t1).setTime("10:15");
        ((CalendarItem)t2).setTime("10:20");
        ((Job)t3).setTime("12:30");
        ((Job)t4).setTime("17:01");

        List<ITemporal> unsorted = Arrays.asList(t4,t1,t3,t2);

        Util.sortTemporals(unsorted, true);

        Assert.assertEquals(unsorted.get(0), t1);
        Assert.assertEquals(unsorted.get(1), t2);
        Assert.assertEquals(unsorted.get(2), t3);
        Assert.assertEquals(unsorted.get(3), t4);

    }

    @Test
    public void testSortTemporals_descending() throws Exception {

        ITemporal t1 = new Job();
        ITemporal t2 = new CalendarItem();
        ITemporal t3 = new Job();
        ITemporal t4 = new Job();

        ((Job)t1).setTime("10:15");
        ((CalendarItem)t2).setTime("10:20");
        ((Job)t3).setTime("12:30");
        ((Job)t4).setTime("17:01");

        List<ITemporal> unsorted = Arrays.asList(t4,t1,t3,t2);

        Util.sortTemporals(unsorted, false);

        Assert.assertEquals(unsorted.get(0), t4);
        Assert.assertEquals(unsorted.get(1), t3);
        Assert.assertEquals(unsorted.get(2), t2);
        Assert.assertEquals(unsorted.get(3), t1);

    }

    @Test
    public void testNormalise24HourTimeString() throws Exception {
        String timeString = "2315";
        String expected = "23:15";
        String actual = Util.normalise24HourTimeString(timeString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testNormalise24HourTimeString_case2() throws Exception {
        String timeString = "0917";
        String expected = "09:17";
        String actual = Util.normalise24HourTimeString(timeString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testNormalise24HourTimeString_case3() throws Exception {
        String timeString = "712";
        String expected = "7:12";
        String actual = Util.normalise24HourTimeString(timeString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testNormalise24HourTimeString_null() throws Exception {
        String timeString = null;
        String expected = null;
        String actual = Util.normalise24HourTimeString(timeString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testTimeIn24HourStringFormat() throws Exception {

        String timeString = "23:15";
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date expected = dateFormat.parse("01-01-2018 " + timeString);

        long actual = Util.timeIn24HourStringFormatToDouble(timeString);

        Assert.assertEquals(expected.getTime(), actual);
    }

    @Test
    public void testTrimAndReplaceDelimiter() {
        String subject = "ABC  ,123,    Three";
        String expected = "ABC, 123, Three";
        String actual = Util.trimAndReplaceDelimiter(",", ", ", subject);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testTrimAndReplaceDelimiter2() {
        String subject = "ABC";
        String expected = "ABC";
        String actual = Util.trimAndReplaceDelimiter(",", ", ", subject);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testTrimAndReplaceDelimiter_empty() {
        String subject = "";
        String expected = "";
        String actual = Util.trimAndReplaceDelimiter(",", ", ", subject);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testTrimAndReplaceDelimiter_null() {
        String subject = null;
        String expected = null;
        String actual = Util.trimAndReplaceDelimiter(",", ", ", subject);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testTrimAndReplaceDelimiter_null_oldDelimiter() {
        String subject = "abc  ";
        String expected = null;
        String actual = Util.trimAndReplaceDelimiter(null, ", ", subject);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testTrimAndReplaceDelimiter_null_newDelimiter() {
        String subject = "abc, 123  ";
        String expected = null;
        String actual = Util.trimAndReplaceDelimiter(",", null, subject);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testComputeTimeSince1stJan1970() {
        String date1 = "9/08/2018";
        String date2 = "09/08/2018";

        long actual1 = Util.computeTimeSince1stJan1970(date1, "00:00");
        long actual2 = Util.computeTimeSince1stJan1970(date2, "00:00");

        Assert.assertEquals(actual1, actual2);
    }
}