package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Test;

import java.util.Arrays;

import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSectionItem;
import au.com.houspect.atlas.models.InspectionSectionItemTwoNumbers;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class InspectionSectionItemTwoNumbersUnitTest {

    /**
     * Ensure correctness of isNumber1Empty function. It should return true if number1 is empty string after trimming.
     */
    @Test
    public void testIsNumber1Empty_empty() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber1("");

        boolean expectedResult = true;
        boolean actualResult = sut.isNumber1Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber1Empty function. It should return true if number1 is empty string after trimming.
     */
    @Test
    public void testIsNumber1Empty_whitespace() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber1(" ");

        boolean expectedResult = true;
        boolean actualResult = sut.isNumber1Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber1Empty function. It should return true if number1 is null.
     */
    @Test
    public void testIsNumber1Empty_null() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber1(null);

        boolean expectedResult = true;
        boolean actualResult = sut.isNumber1Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber1Empty function. It should return true if number1 is na.
     */
    @Test
    public void testIsNumber1Empty_na() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber1("na");

        boolean expectedResult = true;
        boolean actualResult = sut.isNumber1Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber1Empty function. It should return true if number1 has non-numeric value.
     */
    @Test
    public void testIsNumber1Empty_not_numeric() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber1("na123ab");

        boolean expectedResult = true;
        boolean actualResult = sut.isNumber1Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber1Empty function. It should return false if number1 has numeric.
     */
    @Test
    public void testIsNumber1Empty_false() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber1("125");

        boolean expectedResult = false;
        boolean actualResult = sut.isNumber1Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber2Empty function. It should return true if number2 is empty string after trimming.
     */
    @Test
    public void testIsNumber2Empty_empty() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber2("");

        boolean expectedResult = true;
        boolean actualResult = sut.isNumber2Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber2Empty function. It should return true if number2 is empty string after trimming.
     */
    @Test
    public void testIsNumber2Empty_whitespace() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber2(" ");

        boolean expectedResult = true;
        boolean actualResult = sut.isNumber2Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber2Empty function. It should return true if number2 is null.
     */
    @Test
    public void testIsNumber2Empty_null() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber2(null);

        boolean expectedResult = true;
        boolean actualResult = sut.isNumber2Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber2Empty function. It should return true if number2 is na.
     */
    @Test
    public void testIsNumber2Empty_na() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber2("na");

        boolean expectedResult = true;
        boolean actualResult = sut.isNumber2Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber2Empty function. It should return true if number2 has non-numeric value.
     */
    @Test
    public void testIsNumber2Empty_not_numeric() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber2("na123ab");

        boolean expectedResult = true;
        boolean actualResult = sut.isNumber2Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of isNumber2Empty function. It should return true if number2 has numeric value.
     */
    @Test
    public void testIsNumber2Empty_false() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber2("125");

        boolean expectedResult = false;
        boolean actualResult = sut.isNumber2Empty();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of calculate function. It should multiply number1 and number2.
     */
    @Test
    public void testCalculateTotal_correct_multiplication() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber1("12");
        sut.setNumber2("10");
        double expectedResult = 120;
        double actualResult = sut.calculateTotal();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of calculate function. It should return number2 as is if number1 is empty.
     */
    @Test
    public void testCalculateTotal_returnNumber2_ifNumber1IsEmpty() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber1("");
        sut.setNumber2("10");
        double expectedResult = 10;
        double actualResult = sut.calculateTotal();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of calculate function. It should return number1 as is if number2 is empty.
     */
    @Test
    public void testCalculateTotal_returnNumber1_ifNumber2IsEmpty() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber1("126");
        sut.setNumber2(" ");
        double expectedResult = 126;
        double actualResult = sut.calculateTotal();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of calculate function. It should return number2 as is if number1 is empty.
     */
    @Test
    public void testCalculateTotal_return0_ifAnyNumber1OrNumber2IsNonNumeric() {
        InspectionSectionItemTwoNumbers sut = new InspectionSectionItemTwoNumbers();
        sut.setNumber1("abc12ah");
        sut.setNumber2("10");
        double expectedResult = 10;
        double actualResult = sut.calculateTotal();
        Assert.assertEquals(expectedResult, actualResult);
    }

    // clone is a protected method, this is a wrapper class for unit test to invoke the clone function
    class InspectionSectionItemTwoNumbersTest extends InspectionSectionItemTwoNumbers {

        public InspectionSectionItemTwoNumbers cloneWrapper() {
            try {
                return (InspectionSectionItemTwoNumbers) super.clone();
            }
            catch (Exception e) {
                return null;
            }
        }
    }

    /**
     * Ensure correct values are copied during clone operation.
     */
    @Test
    public void testClone() {

        InspectionSectionItemTwoNumbersTest sut = new InspectionSectionItemTwoNumbersTest();
        sut.setId("sut.id");
        sut.setSeedId("sut.seedId");
        sut.setNa("sut.na");
        sut.setMandatory(true);
        sut.setText("sut.text");
        sut.setResultType("sut.resultType");
        sut.setNaText("sut.naText");

        InspectionSectionItem.Option o1 = new InspectionSectionItem.Option();
        sut.setOptions(Arrays.asList(o1));
        sut.setResult("sut.result");
        sut.setResultComments("sut.resultComments");

        sut.setNumber1("sut.number1");
        sut.setNumber2("sut.number2");
        sut.setLabel1("sut.label1");
        sut.setLabel2("sut.label2");

        InspectionSectionItemTwoNumbers actual = sut.cloneWrapper();

        Assert.assertNotNull(actual);

        // id is not cloned
        Assert.assertEquals("0", actual.getId());
        Assert.assertEquals(sut.getId(), actual.getSeedId());
        // na is not cloned
        Assert.assertEquals("0", actual.getNa());
        Assert.assertEquals(sut.isMandatory(), actual.isMandatory());
        Assert.assertEquals(sut.getText(), actual.getText());
        Assert.assertEquals(sut.getResultType(), actual.getResultType());
        Assert.assertEquals(sut.getNaText(), actual.getNaText());

        Assert.assertEquals(sut.getOptions().size(), actual.getOptions().size());
        //Assert.assertEquals(sut.getOptions().get(0), actual.getOptions().get(0));

        // result is not cloned
        Assert.assertEquals("", actual.getResult());
        // resultComments is not cloned
        Assert.assertEquals("", actual.getResultComments());

        // expect number1 not to be cloned
        Assert.assertEquals("", actual.getNumber1());
        // expect number2 not to be cloned
        Assert.assertEquals("", actual.getNumber2());
        Assert.assertEquals(sut.getLabel1(), actual.getLabel1());
        Assert.assertEquals(sut.getLabel2(), actual.getLabel2());

    }

    /**
     * Ensure correct values are copied during clone operation.
     */
    @Test
    public void testCloneWithNumberNaCopiedAcross() {

        InspectionSectionItemTwoNumbersTest sut = new InspectionSectionItemTwoNumbersTest();
        sut.setId("sut.id");
        sut.setSeedId("sut.seedId");
        sut.setNa("sut.na");
        sut.setMandatory(true);
        sut.setText("sut.text");
        sut.setResultType("sut.resultType");
        sut.setNaText("sut.naText");

        InspectionSectionItem.Option o1 = new InspectionSectionItem.Option();
        sut.setOptions(Arrays.asList(o1));
        sut.setResult("sut.result");
        sut.setResultComments("sut.resultComments");

        sut.setNumber1("na");
        sut.setNumber2("na");
        sut.setLabel1("sut.label1");
        sut.setLabel2("sut.label2");

        InspectionSectionItemTwoNumbers actual = sut.cloneWrapper();

        // expect number1 not to be cloned
        Assert.assertEquals("na", actual.getNumber1());
        // expect number2 not to be cloned
        Assert.assertEquals("na", actual.getNumber2());

    }

}