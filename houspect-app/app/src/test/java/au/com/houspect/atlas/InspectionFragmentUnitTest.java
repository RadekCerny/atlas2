package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import au.com.houspect.atlas.fragments.InspectionFragment;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionItemTwoNumbers;
import au.com.houspect.atlas.viewmodels.InspectionSectionItemTwoNumbersViewModel;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class InspectionFragmentUnitTest {

    // Trick - Needs to use InspectionFragmentTest as sut as we want to test protected method
    private class InspectionFragmentTest extends InspectionFragment {

        public int testInsertClonedInspectionSectionAtTheEndOfTheSequence(InspectionSection clone, InspectionSection inspectionSectionCloned, Inspection inspection) {
            return insertClonedInspectionSectionAtTheEndOfTheSequence(clone, inspectionSectionCloned, inspection);
        }
    }

    /**
     * Ensure insertClonedInspectionSectionAtTheEndOfTheSequence inserts the clone at the end of a repeating seq.
     */
    @Test
    public void testInsertClonedInspectionSectionAtTheEndOfTheSequence() {


        Inspection inspection = new Inspection();
        InspectionSection s1 = new InspectionSection();
        s1.setId("s1");
        InspectionSection s2 = new InspectionSection();
        s2.setId("s2");
        s2.setRepeatingKey("key1");
        s2.setRepeatingSeq("1");
        InspectionSection s2_2 = new InspectionSection();
        s2_2.setId("s2_2");
        s2_2.setRepeatingKey("key1");
        s2_2.setRepeatingSeq("2");
        InspectionSection s2_3 = new InspectionSection();
        s2_3.setId("s2_3");
        s2_3.setRepeatingKey("key1");
        s2_3.setRepeatingSeq("3");

        InspectionSection s3 = new InspectionSection();
        s3.setId("s3");
        s3.setRepeatingKey("key2");
        s3.setRepeatingSeq("1");

        inspection.setSections(new ArrayList<>(Arrays.asList(s1, s2, s2_2, s2_3, s3)));

        InspectionSection clone = new InspectionSection();
        clone.setRepeatingKey("key1");
        clone.setRepeatingKey("4");

        InspectionFragmentTest sut = new InspectionFragmentTest();

        int expected = 4;
        int actual = sut.testInsertClonedInspectionSectionAtTheEndOfTheSequence(clone, s2, inspection);

        Assert.assertEquals(expected, actual);

        List<InspectionSection> expectedSections = Arrays.asList(s1, s2, s2_2, s2_3, clone, s3);
        List<InspectionSection> actualSections = inspection.getSections();

        int expectedSize = expectedSections.size();
        int actualSize = inspection.getSections().size();

        Assert.assertEquals(expectedSize, actualSize);

        boolean match = false;

        for (int i = 0; i < actualSections.size(); i++) {
            match = actualSections.get(i) == expectedSections.get(i);
            Assert.assertTrue(match);
        }

    }

    /**
     * Ensure insertClonedInspectionSectionAtTheEndOfTheSequence inserts the clone at the end of a repeating seq.
     */
    @Test
    public void testInsertClonedInspectionSectionAtTheEndOfTheSequence2_beforeSectionWithNullRepeatingKey() {


        Inspection inspection = new Inspection();
        InspectionSection s1 = new InspectionSection();
        s1.setId("s1");
        InspectionSection s2 = new InspectionSection();
        s2.setId("s2");
        s2.setRepeatingKey("key1");
        s2.setRepeatingSeq("1");
        InspectionSection s2_2 = new InspectionSection();
        s2_2.setId("s2_2");
        s2_2.setRepeatingKey("key1");
        s2_2.setRepeatingSeq("2");
        InspectionSection s2_3 = new InspectionSection();
        s2_3.setId("s2_3");
        s2_3.setRepeatingKey("key1");
        s2_3.setRepeatingSeq("3");

        InspectionSection s3 = new InspectionSection();
        s3.setId("s3");
        s3.setRepeatingKey(null);
        s3.setRepeatingSeq(null);

        inspection.setSections(new ArrayList<>(Arrays.asList(s1, s2, s2_2, s2_3, s3)));

        InspectionSection clone = new InspectionSection();
        clone.setRepeatingKey("key1");
        clone.setRepeatingKey("4");

        InspectionFragmentTest sut = new InspectionFragmentTest();

        int expected = 4;
        int actual = sut.testInsertClonedInspectionSectionAtTheEndOfTheSequence(clone, s2, inspection);

        Assert.assertEquals(expected, actual);

        List<InspectionSection> expectedSections = Arrays.asList(s1, s2, s2_2, s2_3, clone, s3);
        List<InspectionSection> actualSections = inspection.getSections();

        int expectedSize = expectedSections.size();
        int actualSize = inspection.getSections().size();

        Assert.assertEquals(expectedSize, actualSize);

        boolean match = false;

        for (int i = 0; i < actualSections.size(); i++) {
            match = actualSections.get(i) == expectedSections.get(i);
            Assert.assertTrue(match);
        }

    }

}