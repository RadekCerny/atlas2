package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Test;
import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.List;

import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.Inspection;
import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.Job;
import au.com.houspect.atlas.utilities.Util;

public class UnitTestUtil {

    public static File getFileFromPath(Object obj, String fileName) {
        ClassLoader classLoader = obj.getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        return new File(resource.getPath());
    }

    public static String getXmlStringFromTestFile(Object testClass, String filename) throws Exception {
        File file = getFileFromPath(testClass, filename);
        FileInputStream fileInputStream = new FileInputStream(file);
        Reader reader = new InputStreamReader(fileInputStream, "UTF8");
        BufferedReader bufferedReader = new BufferedReader(reader);

        StringBuilder stringBuilder = new StringBuilder();
        String line = bufferedReader.readLine();
        while (line != null) {
            stringBuilder.append(line);
            line = bufferedReader.readLine();
        }

        // Test parseInspectionXml
        String xml = stringBuilder.toString();

        bufferedReader.close();
        return xml;
    }
}