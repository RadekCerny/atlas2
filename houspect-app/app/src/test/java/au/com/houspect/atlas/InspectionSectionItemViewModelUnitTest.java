package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.houspect.atlas.adapters.InspectionSectionItemRecyclerViewAdapter;
import au.com.houspect.atlas.models.InspectionSectionItem;
import au.com.houspect.atlas.models.InspectionSectionItemTwoNumbers;
import au.com.houspect.atlas.viewmodels.InspectionSectionItemViewModel;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class InspectionSectionItemViewModelUnitTest {

    private InspectionSectionItem mandatoryItem;
    private InspectionSectionItem optionalItem;
    private InspectionSectionItemRecyclerViewAdapter.Listener listener;

    @Before
    public void setup() {

        mandatoryItem = new InspectionSectionItemTwoNumbers();
        //mandatoryItem.resetOtherNa();
        //mandatoryItem.setNaText("test na text");
        //mandatoryItem.setMandatory(true);

        optionalItem = new InspectionSectionItem();
        optionalItem.resetOtherNa();;
        optionalItem.setNaText("test na text");
        optionalItem.setMandatory(false);

        listener = new InspectionSectionItemRecyclerViewAdapter.Listener() {
            @Override
            public void onRequestForOtherNaOptions(InspectionSectionItemViewModel viewModel) {

            }
        };
    }

    /**
     * Ensure correctness of configureOtherNaWithNaText function.
     */
    @Test
    public void testConfigureOtherNaWithNaText() {
        InspectionSectionItemViewModel sut = new InspectionSectionItemViewModel(optionalItem, listener);
        sut.configureOtherNaWithNaText();
        boolean expectedResult = true;
        boolean actualResult = sut.getModel().isNa();
        String expectedResultComments = "test na text";
        String actualResultsComments = sut.getModel().getResultComments();
        boolean expectedShouldLockOtherComments = true;
        boolean actualShouldLockOtherComments = sut.shouldLockOtherComments.get();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultsComments);
        Assert.assertEquals(expectedShouldLockOtherComments, actualShouldLockOtherComments);
    }

    /**
     * Ensure correctness of configureOtherNaWithCustomText function.
     */
    @Test
    public void testConfigureOtherNaWithCustomText() {
        InspectionSectionItemViewModel sut = new InspectionSectionItemViewModel(optionalItem, listener);
        sut.configureOtherNaWithCustomText();
        boolean expectedResult = true;
        boolean actualResult = sut.getModel().isNa();
        String expectedResultComments = "";
        String actualResultsComments = sut.getModel().getResultComments();
        boolean expectedShouldLockOtherComments = false;
        boolean actualShouldLockOtherComments = sut.shouldLockOtherComments.get();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultsComments);
        Assert.assertEquals(expectedShouldLockOtherComments, actualShouldLockOtherComments);
    }

    /**
     * Ensure correctness of onOtherCheckedChanged function.
     */
    @Test
    public void testOnOtherCheckedChanged() {
        boolean isChecked = true;
        InspectionSectionItemViewModel sut = new InspectionSectionItemViewModel(optionalItem, listener);
        sut.onOtherCheckedChanged(null, isChecked);

        boolean expectedResult = false;
        boolean actualResult = sut.getModel().isNa();
        String expectedResultComments = "";
        String actualResultsComments = sut.getModel().getResultComments();
        boolean expectedShouldLockOtherComments = false;
        boolean actualShouldLockOtherComments = sut.shouldLockOtherComments.get();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultsComments);
        Assert.assertEquals(expectedShouldLockOtherComments, actualShouldLockOtherComments);

        Assert.assertEquals(false, sut.isOtherChecked.get());
        Assert.assertEquals(isChecked, sut.isOtherNaChecked.get());
    }

    /**
     * Ensure correctness of onOtherCheckedChanged function.
     */
    @Test
    public void testOnOtherCheckedChanged2() {
        boolean isChecked = false;
        InspectionSectionItemViewModel sut = new InspectionSectionItemViewModel(optionalItem, listener);
        sut.getModel().setResultComments("Random");
        sut.onOtherCheckedChanged(null, isChecked);

        boolean expectedResult = false;
        boolean actualResult = sut.getModel().isNa();
        String expectedResultComments = "";
        String actualResultsComments = sut.getModel().getResultComments();
        boolean expectedShouldLockOtherComments = false;
        boolean actualShouldLockOtherComments = sut.shouldLockOtherComments.get();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultsComments);
        Assert.assertEquals(expectedShouldLockOtherComments, actualShouldLockOtherComments);

        Assert.assertEquals(false, sut.isOtherChecked.get());
        Assert.assertEquals(isChecked, sut.isOtherNaChecked.get());
    }

    /**
     * Ensure correctness of onOtherCheckedChanged function.
     */
    @Test
    public void testOnOtherCheckedChanged_mandatoryItem() {
        boolean isChecked = true;
        InspectionSectionItemViewModel sut = new InspectionSectionItemViewModel(mandatoryItem, listener);
        sut.getModel().setResultComments("Random");
        sut.onOtherCheckedChanged(null, isChecked);

        boolean expectedResult = true;
        boolean actualResult = sut.getModel().isNa();
        String expectedResultComments = "Random";
        String actualResultsComments = sut.getModel().getResultComments();
        boolean expectedShouldLockOtherComments = false;
        boolean actualShouldLockOtherComments = sut.shouldLockOtherComments.get();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultsComments);
        Assert.assertEquals(expectedShouldLockOtherComments, actualShouldLockOtherComments);

        Assert.assertEquals(isChecked, sut.isOtherChecked.get());
        Assert.assertEquals(false, sut.isOtherNaChecked.get());
    }

    /**
     * Ensure correctness of onOtherCheckedChanged function.
     */
    @Test
    public void testOnOtherCheckedChanged_mandatoryItem2() {
        boolean isChecked = false;
        InspectionSectionItemViewModel sut = new InspectionSectionItemViewModel(mandatoryItem, listener);
        sut.getModel().setResultComments("Random");
        sut.onOtherCheckedChanged(null, isChecked);

        boolean expectedResult = false;
        boolean actualResult = sut.getModel().isNa();
        String expectedResultComments = ""; // updated since 2018 Scope 3 of work - Item 7
        String actualResultsComments = sut.getModel().getResultComments();
        boolean expectedShouldLockOtherComments = false;
        boolean actualShouldLockOtherComments = sut.shouldLockOtherComments.get();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultsComments);
        Assert.assertEquals(expectedShouldLockOtherComments, actualShouldLockOtherComments);


        Assert.assertEquals(isChecked, sut.isOtherChecked.get());
        Assert.assertEquals(false, sut.isOtherNaChecked.get());
    }

    /**
     * Ensure correctness of onOtherNaCheckedChanged function.
     */
    @Test
    public void testOnOtherNaCheckedChanged() {
        boolean isChecked = true;
        InspectionSectionItemViewModel sut = new InspectionSectionItemViewModel(optionalItem, listener);
        sut.onOtherNaCheckedChanged(null, isChecked);

        boolean expectedResult = false;
        boolean actualResult = sut.getModel().isNa();
        String expectedResultComments = "";
        String actualResultsComments = sut.getModel().getResultComments();
        boolean expectedShouldLockOtherComments = false;
        boolean actualShouldLockOtherComments = sut.shouldLockOtherComments.get();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultsComments);
        Assert.assertEquals(expectedShouldLockOtherComments, actualShouldLockOtherComments);


        Assert.assertEquals(false, sut.isOtherChecked.get());
        Assert.assertEquals(isChecked, sut.isOtherNaChecked.get());
    }

    /**
     * Ensure correctness of onOtherNaCheckedChanged function.
     */
    @Test
    public void testOnOtherNaCheckedChanged2() {
        boolean isChecked = false;
        InspectionSectionItemViewModel sut = new InspectionSectionItemViewModel(optionalItem, listener);
        sut.getModel().setResultComments("Random");
        sut.onOtherNaCheckedChanged(null, isChecked);

        boolean expectedResult = false;
        boolean actualResult = sut.getModel().isNa();
        String expectedResultComments = "";
        String actualResultsComments = sut.getModel().getResultComments();
        boolean expectedShouldLockOtherComments = false;
        boolean actualShouldLockOtherComments = sut.shouldLockOtherComments.get();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultsComments);
        Assert.assertEquals(expectedShouldLockOtherComments, actualShouldLockOtherComments);


        Assert.assertEquals(false, sut.isOtherChecked.get());
        Assert.assertEquals(isChecked, sut.isOtherNaChecked.get());
    }

}