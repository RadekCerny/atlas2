package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import au.com.houspect.atlas.models.Job;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class JobUnitTest {
    @Test
    public void computeTimeSince1stJan1970() throws Exception {

        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH, 3-1);
        calendar.set(Calendar.DATE, 7);
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.HOUR, 9);
        calendar.set(Calendar.MINUTE, 32);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.AM_PM, Calendar.AM);

        long expectedResult = calendar.getTime().getTime();

        String dateString = "07/03/17";
        String timeString = "9:32 AM";

        Job job = new Job();

        assertEquals(0, job.getTimeSince1stJan1970());

        job.setDate(dateString);
        job.setTime(timeString);

        assertEquals(expectedResult, job.getTimeSince1stJan1970());


        dateString = "7/3/17";
        job.setDate(dateString);

        assertEquals(expectedResult, job.getTimeSince1stJan1970());

        timeString = "9:32AM";
        job.setTime(timeString);

        assertEquals(expectedResult, job.getTimeSince1stJan1970());

        dateString = "07/3/2017";
        job.setDate(dateString);

        assertEquals(expectedResult, job.getTimeSince1stJan1970());
    }

    @Test
    public void timeSince1stJan1970Order() {


        Job job1 = new Job();
        job1.setDate("08/03/17");
        job1.setTime("8:00");
        long t1 = job1.computeTimeSince1stJan1970();

        Job job2 = new Job();
        job2.setDate("13/03/17");
        job2.setTime("");
        long t2 = job2.computeTimeSince1stJan1970();

        Job job3 = new Job();
        job3.setDate("14/03/17");
        job3.setTime("");
        long t3 = job3.computeTimeSince1stJan1970();

        Assert.assertTrue(job1.getTimeSince1stJan1970() < job2.getTimeSince1stJan1970());
        Assert.assertTrue(job2.getTimeSince1stJan1970() < job3.getTimeSince1stJan1970());

    }

}