package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import au.com.houspect.atlas.models.InspectionSection;
import au.com.houspect.atlas.models.InspectionSectionImage;
import au.com.houspect.atlas.models.InspectionSectionItem;
import au.com.houspect.atlas.models.InspectionSectionSnippet;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class InspectionSectionUnitTest {
    /**
     * Ensure correct values are copied during clone operation.
     */
    @Test
    public void testClone() {

        InspectionSection sut = new InspectionSection();
        sut.setAClone(false);
        sut.setId("sut.id");
        sut.setKey("");
        sut.setParentInspectionId("sut.parentInspectionId");
        sut.setName("sut.name");
        sut.setCloneSourceName("sut.cloneSourceName");
        sut.setOptional(true);
        sut.setStatus("sut.status");
        sut.setInstanceName("");

        String repeatingSeqToBeSetOnClone = "sut.repeatingSeqToBeSetOnClone";

        //InspectionSectionItem i = new InspectionSectionItem(); // TODO - mock InspectionSectionItem content
        sut.setItems(Arrays.asList());

        InspectionSectionSnippet s = new InspectionSectionSnippet(); // TODO - mock InspectionSectionSnippet content
        sut.setSnippets(Arrays.asList(s));

        sut.setRecommendations("sut.recommendations");
        sut.setNa(true);
        sut.setRepeatingKey("sut.repeatingKey");
        sut.setRepeatingSeq("sut.repeatingSeq");
        sut.setHeading("sut.heading");

        InspectionSectionImage image0 = new InspectionSectionImage(); // TODO - mock InspectionSectionImage content
        sut.setImages(Arrays.asList(image0));

        InspectionSection actual = null;

        try {
            actual = (InspectionSection) sut.clone(repeatingSeqToBeSetOnClone);
        } catch (Exception e) {
            Assert.fail();
        }

        Assert.assertNotNull(actual);

        Assert.assertEquals(true, actual.isAClone());
        Assert.assertEquals(sut.getId(), actual.getId());
        Assert.assertEquals("0", actual.getKey());
        Assert.assertEquals(sut.getParentInspectionId(), actual.getParentInspectionId());
        Assert.assertEquals(sut.getName(), actual.getName());
        // clone's cloneSourceName = origin's name
        Assert.assertEquals(sut.getName(), actual.getCloneSourceName());
        Assert.assertEquals(sut.isOptional(), actual.isOptional());
        Assert.assertEquals(sut.getStatus(), actual.getStatus());
        Assert.assertEquals(repeatingSeqToBeSetOnClone, actual.getInstanceName());

        Assert.assertEquals(sut.getItems().size(), actual.getItems().size());
        // TODO - assert InspectionSectionItem values? or add unit test for InspectionSectionItem clone function
        Assert.assertEquals(sut.getSnippets().size(), actual.getSnippets().size());
        // TODO - assert InspectionSectionSnippet values? or add unit test for InspectionSectionSnippet clone function
        Assert.assertEquals("", actual.getRecommendations());
        Assert.assertEquals(sut.isNa(), actual.isNa());
        Assert.assertEquals(sut.getRepeatingKey(), actual.getRepeatingKey());
        Assert.assertEquals(repeatingSeqToBeSetOnClone, actual.getRepeatingSeq());
        Assert.assertEquals(sut.getHeading(), actual.getHeading());

        // do not clone images
        Assert.assertEquals(0, actual.getImages().size());



    }

}