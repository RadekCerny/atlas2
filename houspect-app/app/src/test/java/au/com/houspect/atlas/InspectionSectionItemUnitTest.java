package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Test;

import au.com.houspect.atlas.models.InspectionSectionItem;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class InspectionSectionItemUnitTest {

    /**
     * Ensure correctness of isNa function. It needs to detect correct na string value.
     */
    @Test
    public void testIsNa() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setNa("1");
        boolean expectedResult = true;
        boolean actualResult = sut.isNa();
        Assert.assertEquals(expectedResult, actualResult);

        sut = new InspectionSectionItem();
        sut.setNa("other");
        boolean expectedResult2 = true;
        boolean actualResult2 = sut.isNa();
        Assert.assertEquals(expectedResult2, actualResult2);
    }

    /**
     * Ensure correctness of isNa function (positive negatives). It needs to detect correct na string value.
     */
    @Test
    public void testIsNa_false() {
        InspectionSectionItem sut = new InspectionSectionItem();
        boolean expectedResult = false;
        boolean actualResult = sut.isNa();
        Assert.assertEquals(expectedResult, actualResult);

        sut = new InspectionSectionItem();
        sut.setNa("0");
        boolean expectedResult2 = false;
        boolean actualResult2 = sut.isNa();
        Assert.assertEquals(expectedResult2, actualResult2);
    }

    /**
     * Ensure correctness of resetOther function.
     */
    @Test
    public void testResetOther() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setNa("abc");
        sut.setResultComments("some comments");
        sut.resetOther();
        String expectedResult = "0";
        String actualResult = sut.getNa();
        String expectedResultComments = ""; // updated since 2018 Scope 3 of work - Item 7
        String actualResultComments = sut.getResultComments();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultComments);

        sut = new InspectionSectionItem();
        sut.setResultComments("some comments2");
        sut.resetOther();
        String expectedResult2 = "0";
        String actualResult2 = sut.getNa();
        String expectedResultComments2 = "";  // updated since 2018 Scope 3 of work - Item 7
        String actualResultComments2 = sut.getResultComments();
        Assert.assertEquals(expectedResult2, actualResult2);
        Assert.assertEquals(expectedResultComments2, actualResultComments2);
    }

    /**
     * Ensure correctness of setAsOther function.
     */
    @Test
    public void testSetAsOther() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setNa("abc");
        sut.setAsOther();
        String expectedResult = "1";
        String actualResult = sut.getNa();
        Assert.assertEquals(expectedResult, actualResult);

        sut = new InspectionSectionItem();
        sut.setAsOther();
        String expectedResult2 = "1";
        String actualResult2 = sut.getNa();
        Assert.assertEquals(expectedResult2, actualResult2);
    }

    /**
     * Ensure correctness of resetOtherNa function.
     */
    @Test
    public void testResetOtherNa() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setResultComments("some comments");
        sut.setNa("abc");
        sut.resetOtherNa();
        String expectedResult = "0";
        String actualResult = sut.getNa();
        String expectedResultComments = "";
        String actualResultComments = sut.getResultComments();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultComments);

        sut = new InspectionSectionItem();
        sut.setResultComments("some comments2");
        sut.resetOtherNa();
        String expectedResult2 = "0";
        String actualResult2 = sut.getNa();
        String expectedResultComments2 = "";
        String actualResultComments2 = sut.getResultComments();
        Assert.assertEquals(expectedResult2, actualResult2);
        Assert.assertEquals(expectedResultComments2, actualResultComments2);
    }

    /**
     * Ensure correctness of setAsOtherNaWithNaText function.
     */
    @Test
    public void testSetAsOtherNaWithNaText() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setNaText("some natext");
        sut.setResultComments("some comments");
        sut.setNa("abc");
        sut.setAsOtherNaWithNaText();
        String expectedResult = "1";
        String actualResult = sut.getNa();
        String expectedResultComments = "some natext";
        String actualResultComments = sut.getResultComments();
        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertEquals(expectedResultComments, actualResultComments);
    }

    /**
     * Ensure correctness of setAsOtherNa function.
     */
    @Test
    public void testSetAsOtherNa() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setNa("abc");
        sut.setAsOtherNa();
        String expectedResult = "other";
        String actualResult = sut.getNa();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of resetOtherNa function.
     */
    @Test
    public void testIsOtherNaWithNaText() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setNaText("some natext");
        sut.setResultComments("some natext");
        sut.setNa("1");
        boolean expectedResult = true;
        boolean actualResult = sut.isOtherNaWithNaText();
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure correctness of resetOtherNa function (positive negatives).
     */
    @Test
    public void testIsOtherNaWithNaText_false() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setNaText("some natext");
        sut.setResultComments("some natext11");
        sut.setNa("1");
        boolean expectedResult = false;
        boolean actualResult = sut.isOtherNaWithNaText();
        Assert.assertEquals(expectedResult, actualResult);

        sut = new InspectionSectionItem();
        sut.setNaText("some natext");
        sut.setResultComments("some natext");
        sut.setNa("other");
        boolean expectedResult2 = false;
        boolean actualResult2 = sut.isOtherNaWithNaText();
        Assert.assertEquals(expectedResult2, actualResult2);

        sut = new InspectionSectionItem();
        sut.setNaText("some natext");
        sut.setResultComments("some natext");
        sut.setNa("0");
        boolean expectedResult3 = false;
        boolean actualResult3 = sut.isOtherNaWithNaText();
        Assert.assertEquals(expectedResult3, actualResult3);

        sut = new InspectionSectionItem();
        sut.setNaText("some natext");
        sut.setResultComments(null);
        sut.setNa("1");
        boolean expectedResult4 = false;
        boolean actualResult4 = sut.isOtherNaWithNaText();
        Assert.assertEquals(expectedResult4, actualResult4);
    }

    /**
     * Ensure getType detects Enum type
     */
    @Test
    public void testGetType_Enum() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setResultType("Enum");

        Assert.assertEquals(InspectionSectionItem.Type.Enum, sut.getType());
    }
    /**
     * Ensure getType detects Enum type
     */
    @Test
    public void testGetType_Enum_lowercase() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setResultType("enum");

        Assert.assertEquals(InspectionSectionItem.Type.Enum, sut.getType());
    }

    /**
     * Ensure getType detects Enum type
     */
    @Test
    public void testGetType_Enum_Contains() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setResultType("SomeThingElseContainsEnum");

        Assert.assertEquals(InspectionSectionItem.Type.Enum, sut.getType());
    }

    /**
     * Ensure getType detects TwoNumbers type
     */
    @Test
    public void testGetType_TwoNumbers() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setResultType("TwoNumbers");

        Assert.assertEquals(InspectionSectionItem.Type.TwoNumbers, sut.getType());
    }
    /**
     * Ensure getType detects TwoNumbers type
     */
    @Test
    public void testGetType_TwoNumbers_lowercase() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setResultType("twonumbers");

        Assert.assertEquals(InspectionSectionItem.Type.TwoNumbers, sut.getType());
    }

    /**
     * Ensure getType detects TwoNumbers type
     */
    @Test
    public void testGetType_TwoNumbers_Contains() {
        InspectionSectionItem sut = new InspectionSectionItem();
        sut.setResultType("SomeThingElseContainsTwoNumbers");

        Assert.assertEquals(InspectionSectionItem.Type.TwoNumbers, sut.getType());
    }

}