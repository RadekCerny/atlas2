package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.models.InspectionClient;
import au.com.houspect.atlas.models.RecentInspection;
import au.com.houspect.atlas.services.DataServiceImpl;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class DataServiceImplUnitTest {

    @Test
    public void testDraftToRecentInspection() throws Exception {

        String expectedCode = "TestCode";
        String expectedId = "TestId";
        String expectedMobile = "TestMobile";
        String expectedClientName = "TestClientName";
        String expectedAddress = "TestAddress, TestSuburb"; // Draft has address field with street and suburb merged
        String expectedDate = "TestDate";
        String expectedReportType = "TestReportType";
        String expectedSuburb = "";
        boolean expectedIsPendingUploadToServer = true;

        Draft draft = new Draft();
        draft.setCodeForListDisplay(expectedCode);
        draft.setId(expectedId);
        InspectionClient inspectionClient = new InspectionClient();
        inspectionClient.setName(expectedClientName);
        inspectionClient.setMobilePhone(expectedMobile);
        draft.setClient(inspectionClient);
        draft.setAddress(expectedAddress);
        draft.setDate(expectedDate);
        draft.setReportType(expectedReportType);
        draft.setXmlParsed(true);

        DataServiceImpl sut = new DataServiceImpl(null, null, null);

        RecentInspection recentInspection = sut.draftToRecentInspection(draft);

        String actualCode = recentInspection.getCode();
        String actualId = recentInspection.getId();
        String actualMobile = recentInspection.getMobile();
        String actualClientName = recentInspection.getClientName();
        String actualDate = recentInspection.getDate();
        String actualReportType = recentInspection.getReportType();
        String actualAddress = recentInspection.getAddress();
        String actualSuburb = recentInspection.getSuburb();
        boolean actualIsPendingUploadToServer = recentInspection.getPendingUploadToServer();

        Assert.assertEquals(expectedCode, actualCode);
        Assert.assertEquals(expectedId, actualId);
        Assert.assertEquals(expectedMobile, actualMobile);
        Assert.assertEquals(expectedClientName, actualClientName);
        Assert.assertEquals(expectedDate, actualDate);
        Assert.assertEquals(expectedReportType, actualReportType);
        Assert.assertEquals(expectedAddress, actualAddress);
        Assert.assertEquals(expectedSuburb, actualSuburb);
        Assert.assertEquals(expectedIsPendingUploadToServer, actualIsPendingUploadToServer);


    }
}