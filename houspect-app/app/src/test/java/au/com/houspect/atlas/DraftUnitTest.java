package au.com.houspect.atlas;

import junit.framework.Assert;

import org.junit.Test;

import au.com.houspect.atlas.models.Draft;
import au.com.houspect.atlas.utilities.Util;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class DraftUnitTest {

    /**
     * Ensure immutability of constant MODE_ATLAS_DRAFT over app versions as the actual value is stored in ORMLite and depended on by app logic.
     */
    @Test
    public void testMODE_ATLAS_DRAFT() {
        int expectedResult = 0;
        int actualResult = Draft.MODE_ATLAS_DRAFT;
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure immutability of constant MODE_ZEUS_DRAFT over app versions as the actual value is stored in ORMLite and depended on by app logic.
     */
    @Test
    public void testMODE_ZEUS_DRAFT() {
        int expectedResult = 1;
        int actualResult = Draft.MODE_ZEUS_DRAFT;
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure immutability of constant MODE_ZEUS_FINAL_REPORT over app versions as the actual value is stored in ORMLite and depended on by app logic.
     */
    @Test
    public void testMODE_ZEUS_FINAL_REPORT() {
        int expectedResult = 2;
        int actualResult = Draft.MODE_ZEUS_FINAL_REPORT;
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure immutability of constant SUBMIT_VALUE_ATLAS_DRAFT over app versions as the actual value is stored in ORMLite and depended on by app logic.
     */
    @Test
    public void testSUBMIT_VALUE_ATLAS_DRAFT() {
        String expectedResult = "0";
        String actualResult = Draft.SUBMIT_VALUE_ATLAS_DRAFT;
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure immutability of constant SUBMIT_VALUE_ZEUS_DRAFT over app versions as the actual value is stored in ORMLite and depended on by app logic.
     */
    @Test
    public void testSUBMIT_VALUE_ZEUS_DRAFT() {
        String expectedResult = "draft";
        String actualResult = Draft.SUBMIT_VALUE_ZEUS_DRAFT;
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Ensure immutability of constant SUBMIT_VALUE_ZEUS_FINAL_REPORT over app versions as the actual value is stored in ORMLite and depended on by app logic.
     */
    @Test
    public void testSUBMIT_VALUE_ZEUS_FINAL_REPORT() {
        String expectedResult = "final";
        String actualResult = Draft.SUBMIT_VALUE_ZEUS_FINAL_REPORT;
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsAtlasDraft() throws Exception {
        Draft sut = new Draft();
        sut.setMode(Draft.MODE_ATLAS_DRAFT);
        boolean expectedResult = true;
        boolean actualResult = sut.isAtlasDraft();
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsZeusDraft() throws Exception {
        Draft sut = new Draft();
        sut.setMode(Draft.MODE_ZEUS_DRAFT);
        boolean expectedResult = true;
        boolean actualResult = sut.isZeusDraft();
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsZeusFinalReport() throws Exception {
        Draft sut = new Draft();
        sut.setMode(Draft.MODE_ZEUS_FINAL_REPORT);
        boolean expectedResult = true;
        boolean actualResult = sut.isZeusFinalReport();
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsAtlasDraft_false() throws Exception {
        Draft sut = new Draft();
        sut.setMode(Draft.MODE_ZEUS_DRAFT);
        boolean expectedResult = false;
        boolean actualResult = sut.isAtlasDraft();
        Assert.assertEquals(expectedResult, actualResult);

        sut.setMode(Draft.MODE_ZEUS_FINAL_REPORT);
        actualResult = sut.isAtlasDraft();
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsZeusDraft_false() throws Exception {
        Draft sut = new Draft();
        sut.setMode(Draft.MODE_ATLAS_DRAFT);
        boolean expectedResult = false;
        boolean actualResult = sut.isZeusDraft();
        Assert.assertEquals(expectedResult, actualResult);

        sut.setMode(Draft.MODE_ZEUS_FINAL_REPORT);
        actualResult = sut.isZeusDraft();
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsZeusFinalReport_false() throws Exception {
        Draft sut = new Draft();
        sut.setMode(Draft.MODE_ATLAS_DRAFT);
        boolean expectedResult = false;
        boolean actualResult = sut.isZeusFinalReport();
        Assert.assertEquals(expectedResult, actualResult);

        sut.setMode(Draft.MODE_ZEUS_DRAFT);
        actualResult = sut.isZeusFinalReport();
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsPendingForUploadToServer() {
        Draft sut = new Draft();
        sut.setUploadedTime(0);
        boolean expectedResult = true;
        boolean actualResult = sut.isPendingForUploadToServer();
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsPendingForUploadToServer_false() {
        Draft sut = new Draft();
        sut.setUploadedTime(12032);
        boolean expectedResult = false;
        boolean actualResult = sut.isPendingForUploadToServer();
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testSetAsAtlasDraft() {
        Draft sut = new Draft();
        sut.setMode(328163); // set invalid mode
        sut.setSubmit("junkvalue"); // set invalid submit for Atlas Draft

        sut.setAsAtlasDraft();

        int expectedMode = Draft.MODE_ATLAS_DRAFT;
        String expectedSubmit = Draft.SUBMIT_VALUE_ATLAS_DRAFT;
        int actualMode = sut.getMode();
        String actualSubmit = sut.getSubmit();

        Assert.assertEquals(expectedMode, actualMode);
        Assert.assertEquals(expectedSubmit, actualSubmit);

    }

    @Test
    public void testSetAsZeusDraft() {
        Draft sut = new Draft();
        sut.setMode(328163); // set invalid mode
        sut.setSubmit("junkvalue"); // set invalid submit for Zeus Draft
        sut.setUploadedTime(9836423); // set invalid uploadedTime

        sut.setAsZeusDraft();

        int expectedMode = Draft.MODE_ZEUS_DRAFT;
        String expectedSubmit = Draft.SUBMIT_VALUE_ZEUS_DRAFT;
        long expectedUploadedTime = 0;
        int actualMode = sut.getMode();
        String actualSubmit = sut.getSubmit();
        long actualUploadedTime = sut.getUploadedTime();

        Assert.assertEquals(expectedMode, actualMode);
        Assert.assertEquals(expectedSubmit, actualSubmit);
        Assert.assertEquals(expectedUploadedTime, actualUploadedTime); // ensure uploadedTime is reset - app offline logic depends on it

        // Ensure that setAsZeusDraft is only effective on first call esp on the uploadedTime reset - app offline logic / retry upload depends on it
        // simulate Zeus Draft as been uploaded where uploadedTime is set
        sut.setUploadedTime(73686432);

        sut.setAsZeusDraft();

        expectedUploadedTime = 73686432;
        actualUploadedTime = sut.getUploadedTime();

        Assert.assertEquals(expectedUploadedTime, actualUploadedTime);
    }

    @Test
    public void testSetAsZeusFinal() {
        Draft sut = new Draft();
        sut.setMode(328163); // set invalid mode
        sut.setSubmit("junkvalue"); // set invalid submit for Zeus Final Report
        sut.setUploadedTime(9836423); // set invalid uploadedTime

        sut.setAsZeusFinal();

        int expectedMode = Draft.MODE_ZEUS_FINAL_REPORT;
        String expectedSubmit = Draft.SUBMIT_VALUE_ZEUS_FINAL_REPORT;
        long expectedUploadedTime = 0;
        int actualMode = sut.getMode();
        String actualSubmit = sut.getSubmit();
        long actualUploadedTime = sut.getUploadedTime();

        Assert.assertEquals(expectedMode, actualMode);
        Assert.assertEquals(expectedSubmit, actualSubmit);
        Assert.assertEquals(expectedUploadedTime, actualUploadedTime);

        // Ensure that setAsZeusDraft is only effective on first call esp on the uploadedTime reset - app offline logic / retry upload depends on it
        // simulate Zeus Draft as been uploaded where uploadedTime is set
        sut.setUploadedTime(73686432);

        sut.setAsZeusFinal();

        expectedUploadedTime = 73686432;
        actualUploadedTime = sut.getUploadedTime();

        Assert.assertEquals(expectedUploadedTime, actualUploadedTime);

    }

    @Test
    public void testCatchingInvalidConversionFromZeusDraftToAtlasDraft() {

        Draft sut = new Draft();
        sut.setAsZeusDraft();

        boolean expectedResult = true;
        boolean actualResult = false;

        try {
            sut.setAsAtlasDraft();
        } catch (RuntimeException e) {
            if (e.getMessage().equalsIgnoreCase("Backward conversion to Atlas Draft from Zeus Draft or Final Report is not permitted")) {
                actualResult = true;
            }
        }

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testCatchingInvalidConversionFromZeusFinalReportToAtlasDraft() {

        Draft sut = new Draft();
        sut.setAsZeusFinal();

        boolean expectedResult = true;
        boolean actualResult = false;

        try {
            sut.setAsAtlasDraft();
        } catch (RuntimeException e) {
            if (e.getMessage().equalsIgnoreCase("Backward conversion to Atlas Draft from Zeus Draft or Final Report is not permitted")) {
                actualResult = true;
            }
        }

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testCatchingInvalidConversionFromZeusFinalToZeusDraft() {

        Draft sut = new Draft();
        sut.setAsZeusFinal();

        boolean expectedResult = true;
        boolean actualResult = false;

        try {
            sut.setAsZeusDraft();
        } catch (RuntimeException e) {
            if (e.getMessage().equalsIgnoreCase("Backward conversion to Zeus Draft from Zeus Final Report is not permitted")) {
                actualResult = true;
            }
        }

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsValidZeusDraft() throws Exception {

        Draft sut = new Draft();

        String xml = UnitTestUtil.getXmlStringFromTestFile(this, "draft_valid_zeus_draft.xml");
        sut.setXml(xml);

        sut.setAsZeusDraft();
        Util.parseInspectionXml(xml, sut);

        boolean expectedResult = true;
        boolean actualResult = sut.isValidZeusDraft();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsValidZeusDraft_false() throws Exception {

        Draft sut = new Draft();

        String xml = UnitTestUtil.getXmlStringFromTestFile(this, "draft_invalid_zeus_draft.xml");
        sut.setXml(xml);

        sut.setAsZeusDraft();
        Util.parseInspectionXml(xml, sut);

        boolean expectedResult = false;
        boolean actualResult = sut.isValidZeusDraft();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsValidZeusFinalReport() throws Exception {

        Draft sut = new Draft();

        String xml = UnitTestUtil.getXmlStringFromTestFile(this, "draft_valid_zeus_final_report.xml");
        sut.setXml(xml);

        sut.setAsZeusDraft();
        Util.parseInspectionXml(xml, sut);

        boolean expectedResult = true;
        boolean actualResult = sut.isValidZeusFinal();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsValidZeusFinalReport_false() throws Exception {

        Draft sut = new Draft();

        String xml = UnitTestUtil.getXmlStringFromTestFile(this, "draft_invalid_zeus_final_report.xml");
        sut.setXml(xml);

        sut.setAsZeusDraft();
        Util.parseInspectionXml(xml, sut);

        boolean expectedResult = false;
        boolean actualResult = sut.isValidZeusFinal();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIsValidZeusFinalReport_false_incomplete_optional_section() throws Exception {

        Draft sut = new Draft();

        String xml = UnitTestUtil.getXmlStringFromTestFile(this, "draft_invalid_zeus_final_report2.xml");
        sut.setXml(xml);

        sut.setAsZeusDraft();
        Util.parseInspectionXml(xml, sut);

        boolean expectedResult = false;
        boolean actualResult = sut.isValidZeusFinal();

        Assert.assertEquals(expectedResult, actualResult);
    }
}